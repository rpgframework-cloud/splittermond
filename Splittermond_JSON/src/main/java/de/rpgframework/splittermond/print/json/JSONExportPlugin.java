package de.rpgframework.splittermond.print.json;

import java.io.IOException;
import java.lang.System.Logger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.prefs.Preferences;

import org.prelle.splimo.SpliMoCharacter;

import de.rpgframework.ConfigOption;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.export.CharacterExportPlugin;

/**
 * This is the print plugin for the creation of json output for a
 * {@link SpliMoCharacter}. This class registers itself to the
 * {@link CommandBus}.
 */
public class JSONExportPlugin implements CharacterExportPlugin<SpliMoCharacter> {

	private static Logger logger = System.getLogger(JSONExportPlugin.class.getPackageName());

	private static Preferences usr = Preferences.userRoot().node("/org/prelle/splittermond/print");
	private ConfigOption<String> OPTION_PATH;

	//-------------------------------------------------------------------
	public JSONExportPlugin() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.ExportPlugin#getRoleplayingSystem()
	 */
	public RoleplayingSystem getRoleplayingSystem() {
		return RoleplayingSystem.SPLITTERMOND;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.CharacterExportPlugin#getFileType()
	 */
	public String getFileType() {
		return ".json";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.ExportPlugin#getName(java.util.Locale)
	 */
	public String getName(Locale loc) {
		return "Standard";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.CharacterExportPlugin#getIcon()
	 */
	public byte[] getIcon() {
//		InputStream is = StandardPDFConfiguration.class.getResourceAsStream("/Mondtor_Export_PDF.png");
//		if (is!=null) {
//			try {
//				return is.readAllBytes();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.CharacterExportPlugin#getConfiguration()
	 */
	@Override
    public List<ConfigOption<?>> getConfiguration() {
        return Arrays.asList(OPTION_PATH);
    }

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
//	 */
//	@Override
//	public void attachConfigurationTree(ConfigContainer addBelow) {
//		ConfigContainer cfgSpliMo = (ConfigContainer)addBelow.getChild("splittermond");
//		if (cfgSpliMo==null) {
//			logger.error("Expected splittermond node below "+addBelow.getPathID());
//			return;
//		}
//		ConfigContainer cfgJSON = cfgSpliMo.createContainer("json");
//		cfgJSON.changePreferences(usr);
//		cfgJSON.setResourceBundle( (PropertyResourceBundle)ResourceBundle.getBundle(JSONExportPlugin.class.getName()));
//		OPTION_PATH = cfgJSON.createOption("path", ConfigOption.Type.DIRECTORY, System.getProperty("user.home"));
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
//	 */
//	@Override
//	public CommandResult handleCommand(Object src, CommandType type,
//			Object... values) {
//		CommandResult commandResult = null;
//		switch (type) {
//		case PRINT_GET_OPTIONS:
//			Object[] result = new Object[2];
//			result[0] = Arrays.asList(PrintType.JSON);
//			result[1] = getConfiguration();
//			commandResult = new CommandResult(type, result);
//			break;
//		case PRINT:
//			SpliMoCharacter model = (SpliMoCharacter) values[1];
//			// 2. Scene
//			// 3. ScreenManager
//			PrintType format = (PrintType) values[4];
//			logger.info("print called  "+format);
//			if (format == PrintType.JSON) {
//				try {
//					/*
//					 * Create your JSON
//					 */
//					logger.info("Export as resolved JSON format: "+model.getName());
//
//					String json = new JSONExportService().exportCharacter(model);
//					logger.debug(json);
//					// Write
//					Path   printToFile = new File(new File(OPTION_PATH.getStringValue()), model.getName()+".json").toPath();
//					Files.writeString(printToFile, json);
//					System.out.println("printToFile = " + json);
//
//					commandResult = new CommandResult(type, printToFile);
//				} catch (Exception e) {
//					logger.error("Failed",e);
//					commandResult = new CommandResult(type, false, e.toString());
//				}
//			}
//			break;
//			// Continue with default
//		default:
//			commandResult = new CommandResult(type, false, "Not supported");
//		}
//
//		logger.trace(commandResult);
//		return commandResult;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.export.CharacterExportPlugin#createExport(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public byte[] createExport(SpliMoCharacter charac) {
		try {
			String json = new JSONExportService().exportCharacter(charac);
			return json.getBytes(StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}

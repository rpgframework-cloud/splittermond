/**
 * @author Stefan Prelle
 *
 */
module splittermond.json {
	exports de.rpgframework.splittermond.print.json;
	exports de.rpgframework.splittermond.print.json.model;

	opens de.rpgframework.splittermond.print.json.model;

	requires java.prefs;
	requires de.rpgframework.core;
	requires com.google.gson;
	requires transitive splittermond.core;
	requires de.rpgframework.rules;
	requires java.desktop;

}
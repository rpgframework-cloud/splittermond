package de.rpgframework.splittermond.print.json;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.simplepersist.Persister;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author prelle
 *
 */
public class ExportJSONTest {

    private static Persister serializer;

    //-------------------------------------------------------------------
    @BeforeClass
    public static void beforeClass() {
        //System.setProperty("logdir", "C:\\Users\\anja");
        Locale.setDefault(Locale.GERMAN);
        SplittermondDataPlugin plugin = new SplittermondDataPlugin();
        plugin.init();
        serializer = new Persister();
    }

    //-------------------------------------------------------------------
    private static SpliMoCharacter decodeCharacter(byte[] raw) {
        try {
            SpliMoCharacter ret = serializer.read(SpliMoCharacter.class, new ByteArrayInputStream(raw));
            SplitterTools.resolveChar(ret);
            SplitterTools.runProcessors(ret, Locale.GERMANY);
            return ret;
        } catch (IOException e) {
            StringWriter mess = new StringWriter();
            mess.append("Failed loading character\n\n");
            e.printStackTrace(new PrintWriter(mess));
            BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, mess.toString());
        }
        return null;
    }

    //-------------------------------------------------------------------
	@Test
	public void loadDataTest() throws IOException {
        JSONExportPlugin plugin = new JSONExportPlugin();
        String name = "Tiai";
//        name="Amberion";
//        name="Carimea";
        FileInputStream fis = new FileInputStream("src/test/resources/testdata/characters/" + name + ".xml");
        byte[] data = fis.readAllBytes();
        System.out.println("Read " + data.length + " bytes");
        SpliMoCharacter character = decodeCharacter(data);
        System.out.println("Loaded " + character.getName());

        byte[] jsonData = plugin.createExport(character);
        System.out.println("Exported JSON with " + jsonData.length + " bytes");

        File file = new File(name + ".json");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(jsonData);
        fos.flush();
        fos.close();
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
	}

}

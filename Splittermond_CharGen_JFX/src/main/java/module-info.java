open module splittermond.chargen.jfx {
	exports org.prelle.splittermond.chargen.jfx;
	exports org.prelle.splittermond.chargen.jfx.creatures;
	exports org.prelle.splittermond.chargen.jfx.fxml;
	exports org.prelle.splittermond.chargen.jfx.page;
	exports org.prelle.splittermond.chargen.jfx.pane;
	exports org.prelle.splittermond.chargen.jfx.section;
	exports org.prelle.splittermond.chargen.jfx.selector;
	exports org.prelle.splittermond.chargen.jfx.wizard;

	requires de.rpgframework.core;
	requires de.rpgframework.rules;
	requires de.rpgframework.javafx;
	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.extensions;
	requires javafx.fxml;
	requires javafx.graphics;
	requires transitive splittermond.chargen;
	requires transitive splittermond.core;
	requires java.xml;
	requires org.controlsfx.controls;
}
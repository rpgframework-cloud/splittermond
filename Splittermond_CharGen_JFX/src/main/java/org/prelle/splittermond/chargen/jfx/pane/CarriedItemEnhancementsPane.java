package org.prelle.splittermond.chargen.jfx.pane;

import java.util.List;

import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.EnhancementType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;

import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

/**
 *
 */
public class CarriedItemEnhancementsPane extends FlowPane {

	private CarriedItem<ItemTemplate> item;
	private EnhancementController ctrl;
	private List<EnhancementType> allowed;

	//-------------------------------------------------------------------
	public CarriedItemEnhancementsPane(EnhancementType...allowed) {
		initComponents();
		initLayout();
		this.allowed = List.of(allowed);
	}

	//-------------------------------------------------------------------
	public CarriedItemEnhancementsPane(CarriedItem<ItemTemplate> item, EnhancementController ctrl, EnhancementType...allowed) {
		this(allowed);
		setData(item, ctrl);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.setVgap(10);
		this.setHgap(10);
//		super.getChildren().addAll(tcLoad, tcRigid, tcMaterial);
	}

	//-------------------------------------------------------------------
	public void setData(CarriedItem<ItemTemplate> item, EnhancementController ctrl) {
		this.item = item;
		this.ctrl = ctrl;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		getChildren().clear();
		if (item==null) return;
		List<ItemEnhancementValue<Enhancement>> list = item.getEnhancements();
		System.err.println("CarriedItemEnhancementsPane.refresh: "+item.getEnhancements());
		for (ItemEnhancementValue<Enhancement> val : list) {
			if (!allowed.contains(val.getResolved().getType()))
				continue;

			Label label = new Label(val.getNameWithRating());
			getChildren().add(label);
		}
	}

}

package org.prelle.splittermond.chargen.jfx.section;

import org.prelle.javafx.Section;
import org.prelle.splimo.Attribute;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.jfx.rules.AttributeTable;
import javafx.beans.property.ReadOnlyObjectProperty;

/**
 * @author prelle
 *
 */
public class DerivedAttributeSection extends Section {

	private SpliMoCharacterController control;

	private AttributeTable<Attribute> derived;


	//-------------------------------------------------------------------
	public DerivedAttributeSection(String title, SpliMoCharacterController ctrl) {
		super(title, null);
		control = ctrl;
//		if (ctrl instanceof SR6CharacterLeveller)
//			setSettingsButton( new Button(null, new SymbolIcon("setting")) );

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		derived = new AttributeTable<Attribute>(Attribute.secondaryValuesWithoutDRAndDefense());
		derived.setMode(AttributeTable.Mode.SHOW_ONLY);
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		setContent(derived);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		if (getSettingsButton()!=null)
//			getSettingsButton().setOnAction(ev -> onSettings());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<IAttribute> showHelpForProperty() {
		return derived.selectedAttributeProperty();
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		this.control = ctrl;
		derived.setModel(control.getModel());
		derived.setController(control.getAttributeController());
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		derived.refresh();
	}

//	//-------------------------------------------------------------------
//	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
//		return showHelpFor;
//	}
//
//	//-------------------------------------------------------------------
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getAttributeController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

}

package org.prelle.splittermond.chargen.jfx.selector;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.prelle.javafx.CloseType;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMPieceOfGearVariant;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.FilterItemTemplate;
import org.prelle.splittermond.chargen.jfx.pane.ItemTemplatePane;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.Possible.State;
import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelector extends Selector<ItemTemplate, CarriedItem<ItemTemplate>> {

	private final static Logger logger = System.getLogger(ItemTemplateSelector.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(ItemTemplateSelector.class.getPackageName()+".Selectors");

	protected SpliMoCharacterController charGen;
	protected CarryMode carry = CarryMode.CARRIED;

	// Shall character requirements be ignored
	private CheckBox cbIgnoreRequirements;
	private SMPieceOfGearVariant variant;

	//-------------------------------------------------------------------
	public ItemTemplateSelector(SpliMoCharacterController charGen, Predicate<ItemTemplate> templateFilter) {
		super(charGen.getEquipmentController(),
				templateFilter,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()),
				new FilterItemTemplate(CarryMode.CARRIED));
		this.charGen = charGen;
//		if (container!=null) {
//			listPossible.setCellFactory( lv -> new ItemTemplateListCell( () -> charGen.getEquipmentController(), container, hook));
//		} else {
//			if (mode==CarryMode.EMBEDDED)
//				throw new NullPointerException("CarryMode is EMBEDDED, but container is NULL");
//			listPossible.setCellFactory( lv -> new ItemTemplateListCell( () -> charGen.getEquipmentController(), carry));
			listPossible.setCellFactory( lv -> new ComplexDataItemListCell<ItemTemplate>( () -> charGen.getEquipmentController()));
//		}

		// Button control
    	listPossible.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
    		logger.log(Level.DEBUG, "Selected {0}", n);
    		if (n==null)
    			return;
    		Possible poss = null;
//    		if (carry==CarryMode.EMBEDDED) {
//    			poss = charGen.getEquipmentController().canBeEmbedded(container, hook, n, null);
//    		} else if (n!=null) {
       			poss = charGen.getEquipmentController().canBeSelected(n);
//    		}
    		logger.log(Level.DEBUG, "Selection possible = {0}",poss);
    		if (btnCtrl!=null) {
    			btnCtrl.setDisabled(CloseType.OK, !poss.get());
    		}
    	});

		genericDescr= new ItemTemplatePane(r -> SplitterTools.getRequirementString(r, Locale.getDefault()),carry);

		logger.log(Level.WARNING, "Show filter for item types");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#selectionChanged(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	protected void selectionChanged(ItemTemplate n) {
		Pane box = getDescriptionNode(n);
		logger.log(Level.DEBUG, "Selected {0} and show description node {1}", n, box);
		col2.getChildren().setAll(box, lbNotPossible);
		VBox.setVgrow(box, Priority.ALWAYS);
		help.getChildren().setAll(col1, col2);
		if (n==null) return;

		IEquipmentController equipCtrl = (IEquipmentController) control;
		AGearData relevant = n.getMainOrVariant(carry);
		String variantID = null;
		if (relevant instanceof SMPieceOfGearVariant) {
			variant = (SMPieceOfGearVariant) relevant;
			variantID = relevant.getId();
		}
		Possible poss = equipCtrl.canBeSelected(n);
		//logger.log(Level.TRACE, "  poss= {0}  btnCtrl={1}", poss, btnCtrl);
		if (btnCtrl!=null) {
			btnCtrl.setDisabled(CloseType.OK, !poss.get());
		}
		if (poss.getState()==State.REQUIREMENTS_NOT_MET) {
			List<String> problems = poss.getUnfulfilledRequirements().stream().map(r -> resolver.apply(r)).collect(Collectors.toList());
			lbNotPossible.setText(String.join(", ", problems));
		} else if (!poss.get())
			lbNotPossible.setText(poss.getMostSevere().getMessage());
		else
			lbNotPossible.setText(null);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variantID
	 */
	public SMPieceOfGearVariant getVariant() {
		return variant;
	}

}

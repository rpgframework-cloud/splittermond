package org.prelle.splittermond.chargen.jfx.creatures;

import java.util.Locale;

import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.creature.SMLifeform.LifeformType;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author prelle
 *
 */
public class LifeformPane<L extends DataItem & SMLifeform> extends ADescriptionPane<L> {

	private CreatureTypeViewPane  typeView;
	private AttributeViewPane attribView;
	private WeaponViewPane  weaponView;
	private SkillViewPane  skillView;
	private MastershipViewPane  masterView;
	private SpellViewPane  spellView;
	private CreatureFeatureViewPane  featView;
	private ServicesPane serviceView;

	private ImageView iView;
	private TextFlow description;

	private L value;

	//-------------------------------------------------------------------
	public LifeformPane(boolean editable) {
		initComponents(editable);
		initStyle();
		initLayout();

		setStyle("-fx-pref-width: 28em");
	}

	//--------------------------------------------------------------------
	private void initComponents(boolean editable) {
		typeView   = new CreatureTypeViewPane(editable);
		attribView = new AttributeViewPane(editable);
		weaponView = new WeaponViewPane();
		skillView  = new SkillViewPane(editable, !editable);
		masterView = new MastershipViewPane();
		spellView  = new SpellViewPane();
		featView   = new CreatureFeatureViewPane();
		serviceView= new ServicesPane();

		iView      = new ImageView();
		description= new TextFlow();
	}

	//--------------------------------------------------------------------
	private void initStyle() {
		typeView.getStyleClass().add("content");
		attribView.getStyleClass().add("content");
		weaponView.getStyleClass().add("content");
		skillView.getStyleClass().add("content");
		masterView.getStyleClass().add("content");
		spellView.getStyleClass().add("content");
		featView.getStyleClass().add("content");
		serviceView.getStyleClass().add("content");
	}

	@Override
	protected void initExtraLayout() {
		setSpacing(4);
		attribView.setMaxWidth(Double.MAX_VALUE);
		weaponView.setMaxWidth(Double.MAX_VALUE);

		VBox data = new VBox(10);
		data.getChildren().add(typeView);
		data.getChildren().add(attribView);
		data.getChildren().add(weaponView);
		data.getChildren().add(skillView);
		data.getChildren().add(masterView);
		data.getChildren().add(spellView);
		data.getChildren().add(featView);
		data.getChildren().add(serviceView);

		// Set descriptive text
		if (value!=null) {
			RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, value.getDescription(Locale.getDefault()));
			iView.setVisible(true);
			iView.setManaged(true);
		} else {
			iView.setVisible(false);
			iView.setManaged(false);
		}

		VBox perMetaDetails = new VBox(5, descTitle, descSources, data, iView, description);
		perMetaDetails.setMaxHeight(Double.MAX_VALUE);
		ScrollPane scroll = new ScrollPane(perMetaDetails);
		scroll.setFitToWidth(true);
		scroll.setMaxHeight(Double.MAX_VALUE);

		VBox.setVgrow(scroll, Priority.ALWAYS);
		getChildren().addAll(scroll);

	}

	//--------------------------------------------------------------------
	public void refresh() {
		if (this.value==null) return;
		typeView.refresh();
		attribView.refresh();
		weaponView.refresh();
		skillView.refresh();
		masterView.refresh();
		spellView.refresh();
		featView.refresh();
		serviceView.refresh();

		if (value!=null) {
			descTitle.setText(value.getName(Locale.getDefault()));
			descSources.setText(RPGFrameworkJavaFX.createSourceText(value));
		}
	}

	//--------------------------------------------------------------------
	public void setData(L model) {
		this.value = model;

		typeView.setData(model);
		attribView.setData(model);
		weaponView.setData(model);
		skillView.setData(model);
		masterView.setData(model);
		spellView.setData(model);
		featView.setData(model);
		if (model.getLifeformType()==LifeformType.SUMMONABLE) {
			serviceView.setData(model);
			serviceView.setVisible(true);
			serviceView.setManaged(true);
		} else {
			serviceView.setVisible(false);
			serviceView.setManaged(false);
		}
		if (value!=null) {
			descTitle.setText(value.getName(Locale.getDefault()));
			descSources.setText(RPGFrameworkJavaFX.createSourceText(value));
			RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, value.getDescription(Locale.getDefault()));
		}

		refresh();
	}

}

package org.prelle.splittermond.chargen.jfx.page;

import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.section.SkillSection;

import de.rpgframework.ResourceI18N;
import de.rpgframework.jfx.GenericDescriptionVBox;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class SkillPage extends Page {

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkillPage.class.getPackageName()+".Pages");

	private SkillSection secNormal;

	private FlexGridPane flex;
	private OptionalNodePane layout;
	private GenericDescriptionVBox descBox;

	//-------------------------------------------------------------------
	public SkillPage() {
		super(ResourceI18N.get(RES, "page.skills.title"));
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		secNormal = new SkillSection(ResourceI18N.get(RES, "section.skills.normal"), SkillType.NORMAL);
		secNormal.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinHeight(secNormal, 13);
		FlexGridPane.setMinWidth(secNormal, 4);
		FlexGridPane.setMediumWidth(secNormal, 12);
		FlexGridPane.setMaxWidth(secNormal, 12);

		descBox = new GenericDescriptionVBox(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()));

	}

	//-------------------------------------------------------------------
	private void initLayout() {
		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secNormal);

		layout = new OptionalNodePane(flex, new Label("Langer Text"));
		layout.setTitle("Erklärung");
		setContent(layout);
//		setTitle("Basics");
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		secNormal.selectedSkillProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void showDescription(SMSkillValue n) {
		if (n==null) {
			layout.setOptional(new Label("Langer Text"));
		} else {
			descBox.setData(n.getModifyable());
			layout.setOptional(descBox);
		}
	}

	//-------------------------------------------------------------------
	public void setController(SpliMoCharacterController ctrl) {
		if (ctrl==null)
			throw new NullPointerException("controller is null");

		secNormal.updateController(ctrl);
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		secNormal.refresh();
	}

}

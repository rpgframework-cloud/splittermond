package org.prelle.splittermond.chargen.jfx.selector;

import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.jfx.pane.FilterEnhancements;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.Selector;

/**
 *
 */
public class EnhancementSelector extends Selector<Enhancement, ItemEnhancementValue<Enhancement>> implements ResponsiveControl {

	//-------------------------------------------------------------------
	/**
	 * @param ctrl
	 * @param resolver
	 * @param mResolver
	 * @param filter
	 */
	public EnhancementSelector(EnhancementController ctrl) {
		super(ctrl,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()), new FilterEnhancements());
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param ctrl
	 * @param baseFilter
	 * @param resolver
	 * @param mResolver
	 * @param filter
	 */
	public EnhancementSelector(EnhancementController ctrl, Predicate<Enhancement> baseFilter) {
		super(ctrl, baseFilter, SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()), new FilterEnhancements());
		// TODO Auto-generated constructor stub
	}

}

package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.page.SkillPage;

import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.section.ListSection;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class ResourceSection extends ListSection<ResourceValue> {

	private final static Logger logger = System.getLogger(ResourceSection.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkillPage.class.getPackageName()+".Pages");

	private SpliMoCharacterController control;
	private SpliMoCharacter model;

	private Label lbToDo;

	//-------------------------------------------------------------------
	public ResourceSection(String title) {
		super(title);
		initComponents();
		initLayout();

		list.setCellFactory(lv -> new ComplexDataItemValueListCell<Resource, ResourceValue>( () -> control.getResourceController()));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbToDo = new Label("?");
		lbToDo.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbToDo.setStyle("-fx-text-fill: highlight");
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		setHeaderNode(lbToDo);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		// TODO Auto-generated method stub
		logger.log(Level.DEBUG, "onAdd");

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(ResourceValue item) {
		logger.log(Level.DEBUG, "onDelete");
//		if (control.getQualityController().deselect(item)) {
//			list.getItems().remove(item);
//		}
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (SpliMoCharacter) ctrl.getModel();
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		ResourceController ctrl = control.getResourceController();
		if (model!=null) {
			setData(model.getResources());
			lbToDo.setText(GenericRPGTools.getToDoString(ctrl.getToDos(), Locale.getDefault()));
//			lbTotal.setText( String.valueOf( ctrl.getMaxResourcePoints() ));
//			lbUnspent.setText( String.valueOf( ctrl.getUnsedResourcePoints() ));
		} else
			setData(new ArrayList<>());
	}

}

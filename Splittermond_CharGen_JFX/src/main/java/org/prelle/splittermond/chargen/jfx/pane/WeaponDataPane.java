package org.prelle.splittermond.chargen.jfx.pane;

import java.util.ArrayList;
import java.util.List;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.persist.WeaponDamageConverter;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;

import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ValueRequirement;
import de.rpgframework.jfx.NumericalValueField;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

/**
 *
 */
public class WeaponDataPane extends GridPane {

	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvDamage;
	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvSpeed;
	private Label lbAttrib;
	private FlowPane lbFeatures;

	private CarriedItem<ItemTemplate> item;
	private ICarriedItemController ctrl;

	//-------------------------------------------------------------------
	public WeaponDataPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	public WeaponDataPane(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		initComponents();
		initLayout();
		setData(item, ctrl);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		nvDamage = new NumericalValueField<>(()-> item.getAsValue(SMItemAttribute.DAMAGE));
		nvDamage.setConverter( iaVal -> {
			int val = iaVal.getModifiedValue();
			return WeaponDamageConverter.toString(val);
		});
		nvSpeed = new NumericalValueField<>(()-> item.getAsValue(SMItemAttribute.SPEED));
		lbAttrib = new Label();
		lbFeatures = new FlowPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdDamage = new Label("Schaden"); hdDamage.setMaxWidth(Double.MAX_VALUE);
		Label lbSpeed  = new Label("WGS"); lbSpeed.setMaxWidth(Double.MAX_VALUE);
		Label hdRequires = new Label("Mindest-attribute"); hdRequires.setMaxWidth(Double.MAX_VALUE);
		Label hdFeatures = new Label("Merkmale"); hdFeatures.setMaxWidth(Double.MAX_VALUE);
		hdDamage .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		lbSpeed.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdRequires.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdFeatures .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");

		this.add(hdDamage, 0, 0);
		this.add(nvDamage, 0, 1);
		this.add(lbSpeed , 1, 0);
		this.add(nvSpeed , 1, 1);
		this.add(hdRequires, 2, 0);
		this.add(lbAttrib, 2, 1);
		this.add(hdFeatures, 3, 0);
		this.add(lbFeatures, 3, 1);

		GridPane.setMargin(nvDamage, new Insets(5));
		GridPane.setMargin(nvSpeed, new Insets(5));
		GridPane.setMargin(lbAttrib, new Insets(5));
		GridPane.setMargin(lbFeatures, new Insets(5));
	}

	//-------------------------------------------------------------------
	public void setData(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		this.item = item;
		this.ctrl = ctrl;

		if (ctrl!=null) nvDamage.setController(ctrl.getValueController(SMItemAttribute.DAMAGE));
		if (ctrl!=null) nvSpeed.setController(ctrl.getValueController(SMItemAttribute.SPEED));
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		nvDamage.refresh();
		nvSpeed.refresh();

		List<String> toMerge = new ArrayList<>();
		for (Requirement tmp : item.getRequirements()) {
			if (tmp instanceof ValueRequirement) {
				ValueRequirement req = (ValueRequirement)tmp;
				Attribute attr = Attribute.valueOf( req.getKey() );
				toMerge.add(attr.getShortName()+" "+req.getValue());
			}
		}
		lbAttrib.setText( String.join(", ", toMerge));
		requestLayout();
	}

}

package org.prelle.splittermond.chargen.jfx.pane;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.TitledComponent;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.jfx.NumericalValueField;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

/**
 *
 */
public class CarriedItemBasicDataPane extends GridPane {

	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvLoad;
	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvRigid;
	private Label lbSkill;
	private Label lbSpec;

	private CarriedItem<ItemTemplate> item;
	private ICarriedItemController ctrl;

	//-------------------------------------------------------------------
	public CarriedItemBasicDataPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	public CarriedItemBasicDataPane(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		this.item = item;
		initComponents();
		initLayout();
		setData(item, ctrl);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		nvLoad  = new NumericalValueField<>( ()-> item.getAsValue(SMItemAttribute.LOAD));
		nvRigid = new NumericalValueField<>( ()-> item.getAsValue(SMItemAttribute.RIGIDITY));
		lbSkill = new Label();
		lbSpec  = new Label();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdLoad  = new Label("Last"); hdLoad.setMaxWidth(Double.MAX_VALUE);
		Label hdRigid = new Label("Härte"); hdRigid.setMaxWidth(Double.MAX_VALUE);
		Label hdSkill = new Label("Fertigkeit"); hdSkill.setMaxWidth(Double.MAX_VALUE);
		Label hdSpec  = new Label("Schwerpunkt"); hdSpec.setMaxWidth(Double.MAX_VALUE);
		hdLoad .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdRigid.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdSkill.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdSpec .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");

		add(hdLoad , 0,0);
		add(hdRigid, 1,0);
		add(hdSkill, 2,0);
		add(hdSpec , 3,0);

		add(nvLoad , 0,1);
		add(nvRigid, 1,1);
		add(lbSkill, 2,1);
		add(lbSpec , 3,1);

		GridPane.setMargin(nvLoad, new Insets(5));
		GridPane.setMargin(nvRigid, new Insets(5));
		GridPane.setMargin(lbSkill, new Insets(5));
		GridPane.setMargin(lbSpec, new Insets(5));

		setGridLinesVisible(false);
	}

	//-------------------------------------------------------------------
	public void setData(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		this.item = item;
		this.ctrl = ctrl;

		if (ctrl!=null) nvLoad.setController(ctrl.getValueController(SMItemAttribute.LOAD));
		if (ctrl!=null) nvRigid.setController(ctrl.getValueController(SMItemAttribute.RIGIDITY));
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		nvLoad.refresh();
		nvRigid.refresh();
		if (item.hasAttribute(SMItemAttribute.SKILL))
			lbSkill.setText( ((SMSkill)item.getAsObject(SMItemAttribute.SKILL).getModifiedValue()).getName() );
		if (item.hasAttribute(SMItemAttribute.SPECIALIZATION))
			lbSpec.setText( ((SMSkill)item.getAsObject(SMItemAttribute.SPECIALIZATION).getModifiedValue()).getName() );
	}

}

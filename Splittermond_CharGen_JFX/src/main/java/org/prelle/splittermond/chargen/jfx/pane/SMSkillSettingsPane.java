package org.prelle.splittermond.chargen.jfx.pane;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SkillController;

import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.jfx.GenericDescriptionVBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SMSkillSettingsPane extends VBox {

	private final static Logger logger = System.getLogger(SMSkillSettingsPane.class.getPackageName());

	private  SkillController control;
	private UUID nameUUID;
	private SMSkillValue data;
	private GenericDescriptionVBox desc;

	private TextField tfName;

	private VBox bxMasteries, bxSpecial;

	//-------------------------------------------------------------------
	public SMSkillSettingsPane(SMSkillValue data, SkillController ctrl) {
		this.data = data;
		this.control = ctrl;
		if (!data.getModifyable().getChoices().isEmpty()) {
			nameUUID = data.getModifyable().getChoices().get(0).getUUID();
		}

		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		desc   = new GenericDescriptionVBox(SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault())
				);
		initMasteries();
		initSpecial();
	}

	//-------------------------------------------------------------------
	private void initMasteries() {
		bxMasteries = new VBox(5);
		List<Mastership> list = new ArrayList(data.getSkill().getMasterships());
		Collections.sort(list, new Comparator<Mastership>() {
			public int compare(Mastership o1, Mastership o2) {
				int c = Integer.compare(o1.getLevel(), o2.getLevel());
				if (c!=0) return c;

				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		for (Mastership master : list) {
			CheckBox box = new CheckBox(master.getName());
			bxMasteries.getChildren().add(box);
		}
	}

	//-------------------------------------------------------------------
	private void initSpecial() {
		bxSpecial = new VBox(5);
		List<SkillSpecialization<SMSkill>> list = new ArrayList(data.getSkill().getSpecializations());
		Collections.sort(list, new Comparator<SkillSpecialization<SMSkill>>() {
			public int compare(SkillSpecialization<SMSkill> o1, SkillSpecialization<SMSkill> o2) {
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		for (SkillSpecialization<SMSkill> master : list) {
			CheckBox box = new CheckBox(master.getName());
			bxSpecial.getChildren().add(box);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setSpacing(20);

		TabPane tabs = new TabPane();

		ScrollPane scrMasteries = new ScrollPane(bxMasteries);
		scrMasteries.setFitToWidth(true);
		Tab tbMaster = new Tab("Meister", scrMasteries);
		tabs.getTabs().add(tbMaster);

		ScrollPane scrSpecial = new ScrollPane(bxSpecial);
		scrMasteries.setFitToWidth(true);
		Tab tbSpecial = new Tab("Spezial", scrSpecial);
		tabs.getTabs().add(tbSpecial);

		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		OptionalNodePane opt = new OptionalNodePane(tabs,desc);
		getChildren().add(opt);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		if (nameUUID!=null) {
			tfName.textProperty().addListener( (ov,o,n) -> data.updateDecision(nameUUID, n));
		}
	}

	//-------------------------------------------------------------------
	private void changeCheckbox(CheckBox cb, SkillSpecialization<SMSkill> spec, boolean expert, boolean selected) {
		logger.log(Level.ERROR, "Change Checkbox "+spec);
//		if (selected) {
//			OperationResult<SkillSpecializationValue<SMSkill>> res = control.select(data, spec, expert);
//			if (!res.wasSuccessful())
//				logger.log(Level.WARNING, "Selecting specialization failed: {}", res.toString());
//		} else {
//			SkillSpecializationValue<SMSkill> sVal = data.getSpecialization(spec);
//			boolean res = control.deselect(data,sVal);
//			if (!res)
//				logger.log(Level.WARNING, "Deselecting specialization failed");
//		}
//		refresh();
	}


	//-------------------------------------------------------------------
	private void refresh() {
//		for (SkillSpecialization<SMSkill> spec : boxesSpecia.keySet()) {
//			CheckBox cb = boxesSpecia.get(spec);
//			SkillSpecializationValue<SMSkill> val =  data.getSpecialization(spec);
//			boolean selected = val!=null && val.getDistributed()==0;
//			cb.setSelected(selected);
//			if (selected) {
//				cb.setDisable( !control.canDeselectSpecialization(data, val).get());
//			} else {
//				cb.setDisable( !control.canSelectSpecialization(data, spec, false).get());
//			}
//		}
//		for (SkillSpecialization<SMSkill> spec : boxesExpert.keySet()) {
//			CheckBox cb = boxesExpert.get(spec);
//			SkillSpecializationValue<SMSkill> val =  data.getSpecialization(spec);
//			boolean selected = val!=null && val.getDistributed()==1;
//			cb.setSelected(selected);
//			if (selected) {
//				cb.setDisable( !control.canDeselectSpecialization(data, val).get());
//			} else {
//				cb.setDisable( !control.canSelectSpecialization(data, spec, false).get());
//			}
//		}
//		eventBlocked = false;

	}

}

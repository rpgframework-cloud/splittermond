package org.prelle.splittermond.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.section.ArtifactsSection;
import org.prelle.splittermond.chargen.jfx.section.GearSection;
import org.prelle.splittermond.chargen.jfx.section.SkillSection;
import org.prelle.splittermond.chargen.jfx.section.SpellSection;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.jfx.GenericDescriptionVBox;

/**
 * @author prelle
 *
 */
public class MagicPage extends Page {

	private final static Logger logger = System.getLogger(MagicPage	.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkillPage.class.getPackageName()+".Pages");

	private SpliMoCharacterController ctrl;

	private SkillSection secMagic;
	private SpellSection secSpells;
	private ArtifactsSection secArtifacts;

	private FlexGridPane flex;
	private OptionalNodePane layout ;
	private GenericDescriptionVBox descBox;

	//-------------------------------------------------------------------
	public MagicPage() {
		super(ResourceI18N.get(RES, "page.magic.title"));
		// Flow 1
		initMagicSkills();
		initSpells();
		initArtifacts();
		initInteractivity();

		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secMagic, secSpells, secArtifacts);


		descBox = new GenericDescriptionVBox(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()));
		layout = new OptionalNodePane(flex, null);
		layout.setTitle("Erklärung");
		setContent(layout);
//		setTitle("Basics");
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initMagicSkills() {
		secMagic = new SkillSection(ResourceI18N.get(RES, "section.skills.magic"), SkillType.MAGIC);
		secMagic.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinHeight(secMagic, 12);
		FlexGridPane.setMinWidth(secMagic, 4);
		FlexGridPane.setMediumWidth(secMagic, 11);
	}

	//-------------------------------------------------------------------
	private void initSpells() {
		secSpells = new SpellSection(ResourceI18N.get(RES, "section.spells"));
		secSpells.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinHeight(secSpells, 12);
		FlexGridPane.setMinWidth(secSpells, 4);
		FlexGridPane.setMediumWidth(secSpells, 5);
	}

	//-------------------------------------------------------------------
	private void initArtifacts() {
		secArtifacts = new ArtifactsSection();
		secArtifacts.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secArtifacts, 4);
		FlexGridPane.setMinHeight(secArtifacts, 8);
		FlexGridPane.setMediumWidth(secArtifacts, 6);
		FlexGridPane.setMediumHeight(secArtifacts, 8);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		secMagic.refresh();
		secSpells.refresh();
		secArtifacts.refresh();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		secMagic.selectedSkillProperty().addListener( (ov,o,n) -> showDescription(n));
		secSpells.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
		secArtifacts.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void showDescription(DataItemValue n) {
		if (n==null) {
			layout.setOptional(null);
		} else {
			descBox.setData((DataItem) n.getModifyable());
			layout.setOptional(descBox);
		}
	}

	//-------------------------------------------------------------------
	public void setController(SpliMoCharacterController ctrl) {
		logger.log(Level.INFO, "setController");
		if (ctrl==null)
			throw new NullPointerException("controller is null");
		this.ctrl = ctrl;
		secMagic.updateController(ctrl);
		secSpells.updateController(ctrl);
		secArtifacts.updateController(ctrl);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	public SpliMoCharacterController getCharacterController() {
		return ctrl;
	}
}

package org.prelle.splittermond.chargen.jfx;


import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.javafx.Page;

import de.rpgframework.jfx.rules.AttributeTable;
import javafx.fxml.FXML;

public class AttributesPageController {
	
	private final static Logger logger = System.getLogger(AttributesPageController.class.getPackageName());

	private transient Page page;

	@FXML
	private AttributeTable table;

	//-------------------------------------------------------------------
	public AttributesPageController() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
	}

	//-------------------------------------------------------------------
	public void setComponent(Page page) {
		this.page = page;
		logger.log(Level.WARNING, "TODO: set start page");
	}

}

package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.Mode;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMPieceOfGearVariant;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.listcells.CarriedItemListCell;
import org.prelle.splittermond.chargen.jfx.selector.ChoiceSelectorDialog;
import org.prelle.splittermond.chargen.jfx.selector.ItemTemplateSelector;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.jfx.section.ComplexDataItemListSection;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class GearSection extends ComplexDataItemListSection<ItemTemplate, CarriedItem<ItemTemplate>> {

	private final static Logger logger = System.getLogger(GearSection.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(GearSection.class.getPackageName()+".Sections");

	private CarryMode carry = CarryMode.CARRIED;
	private Predicate<CarriedItem<ItemTemplate>> filter;
	private Predicate<ItemTemplate> templateFilter;
	
	private SpliMoCharacterController control;
	private SpliMoCharacter model;
	
	private List<ItemType> allowedTypes;

	private ToggleSwitch cbRuleNegativeNuyen;
	private ToggleSwitch cbRulePayGear;

	//-------------------------------------------------------------------
	/**
	 * @param title
	 */
	public GearSection(String title, ItemType...types) {
		super(title);
		allowedTypes = List.of(types);
		list.setCellFactory(lv -> new CarriedItemListCell( () -> control.getEquipmentController()));
		initSecondaryContent();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initSecondaryContent() {
		cbRuleNegativeNuyen = new ToggleSwitch();
		cbRuleNegativeNuyen.setGraphicTextGap(0);
		cbRulePayGear       = new ToggleSwitch();
		cbRulePayGear.setGraphicTextGap(0);

//		cbRuleNegativeNuyen.selectedProperty().addListener( (ov,o,n) -> {
//			if (model!=null) control.getRuleController().setRuleValue(ShadowrunRules.CHARGEN_NEGATIVE_NUYEN, n);
//		});
//		cbRulePayGear.selectedProperty().addListener( (ov,o,n) -> {
//			if (model!=null) control.getRuleController().setRuleValue(ShadowrunRules.CAREER_PAY_GEAR, n);
//		});

		setMode(Mode.BACKDROP);

		VBox bxRules = new VBox(10);
//		bxRules.getChildren().add(makeLabel(cbRuleNegativeNuyen, SplittermondRules.CHARGEN_NEGATIVE_NUYEN));
//		bxRules.getChildren().add(makeLabel(cbRulePayGear      , SplittermondRules.CAREER_PAY_GEAR));
		setSecondaryContent(bxRules);

	}

	//-------------------------------------------------------------------
	private Label makeLabel(ToggleSwitch ts, Rule rule) {
		Label lb = new Label(rule.getName(Locale.getDefault()), ts);
		lb.setWrapText(true);
		lb.setAlignment(Pos.TOP_LEFT);
		VBox.setMargin(lb, new Insets(0, 0, 0, -20));
		return lb;
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.log(Level.INFO, "ENTER: onAdd(carry={0}, templateFilter={1}",carry,templateFilter);

		ItemTemplateSelector selector = new ItemTemplateSelector(control, templateFilter);
//		if (templateFilter!=null)
//			selector.setBaseFilter(templateFilter);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES, "section.gear.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
		CloseType closed = FlexibleApplication.getInstance().showAndWait(dialog);
		if (closed==CloseType.OK) {
			ItemTemplate selected = selector.getSelected();
			SMPieceOfGearVariant suggestedVariant = selector.getVariant();
			OperationResult<CarriedItem<ItemTemplate>> result = null;
			// Eventually show decision dialog
			boolean needToAsk = !selected.getChoices().isEmpty();
			needToAsk |= !selected.getVariants().isEmpty();
//			if (  control.getRuleController().getRuleValueAsBoolean(SplittermondRules.ALWAYS_ASK_FOR_FLAGS))
//				needToAsk |= !selected.getUserSelectableFlags(SMItemFlag.class).isEmpty();
			if (needToAsk) {
				logger.log(Level.DEBUG, "Select/Embed with choices or variants or flags");
				ChoiceSelectorDialog<ItemTemplate, CarriedItem<ItemTemplate>> dia2 = new ChoiceSelectorDialog<ItemTemplate, CarriedItem<ItemTemplate>>(control.getEquipmentController());
//				dia2.setSuggestedVariant(suggestedVariant);
				Decision[] dec = dia2.apply(selected, selected.getChoices());
				if (dec!=null) {
					// Not cancelled
					String variantID = dia2.getSelectedVariant();
					logger.log(Level.DEBUG, "After dialog: variant   = "+variantID);
					logger.log(Level.DEBUG, "After dialog: decisions = "+Arrays.toString(dec));
					logger.log(Level.WARNING, "Select with decisions");
					result = control.getEquipmentController().select(selector.getSelected(), dec);
				}
			} else {
				logger.log(Level.WARNING, "Select without decisions");
				result = control.getEquipmentController().select(selector.getSelected());
			}
			if (result != null) {
				if (result.wasSuccessful()) {
					logger.log(Level.WARNING, "Successful");
					refresh();
				} else {
					logger.log(Level.WARNING, "Failed: " + result.getError());
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, result.getError());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(CarriedItem<ItemTemplate> item) {
		logger.log(Level.DEBUG, "onDelete {0} ", item);
		if (control.getEquipmentController().deselect(item)) {
			refresh();
		}
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		logger.log(Level.DEBUG, "updateController");
		this.control = ctrl;
		model = ctrl.getModel();
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	public void refresh() {
		logger.log(Level.DEBUG, "refresh");
		
		if (model==null) return;
		List<CarriedItem<ItemTemplate>> data2 = new ArrayList<>( model.getCarriedItems() );
		for (CarriedItem<ItemTemplate> tmp :data2) {
			if (tmp.getResolved()==null) {
				System.err.println("No resolved item '"+tmp.getKey()+"' for item "+tmp.getUuid());
				continue;
			}
			if (tmp.getResolved().getType()==null) {
				System.err.println("No item type for item "+tmp.getKey()+" UUID "+tmp.getUuid());
				System.exit(1);
			}
		}
		
		List<CarriedItem<ItemTemplate>> data = data2
			.stream()
			.filter(item -> allowedTypes.contains(item.getResolved().getType()))
			.collect(Collectors.toList());
		list.getItems().setAll(data);
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.SplittermondRules;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.RuleValue;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author stefa
 *
 */
public class WizardPageResources extends WizardPage implements ResponsiveControl, ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageResources.class.getPackageName()+".power");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageResources.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	private Label lbPPCurrent, lbPPMax;

	protected ComplexDataItemControllerNode<Resource, ResourceValue> selection;
	protected GenericDescriptionVBox<Resource> bxDescription;
	protected ToggleSwitch cbAllowExtremeResources;
	protected OptionalNodePane layout;
	protected NumberUnitBackHeader backHeaderKarma;

	//-------------------------------------------------------------------
	public WizardPageResources(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.resources.title"));
		initComponents();
		initLayout();
		initBackHeader();
		initInteractivity();

		charGen.addListener(this);
	}
	//-------------------------------------------------------------------
	protected void initComponents() {
		lbPPCurrent = new Label("?");
		lbPPMax     = new Label("?");

		selection = new ComplexDataItemControllerNode<>(charGen.getResourceController());
		selection.setMaxHeight(Double.MAX_VALUE);

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.resources.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.resources.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<Resource>( () -> charGen.getResourceController()));
		selection.setSelectedCellFactory(lv -> new ComplexDataItemValueListCell<Resource,ResourceValue>( () -> charGen.getResourceController()));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		selection.setAvailableStyle("-fx-max-width: 22em");
		selection.setSelectedStyle("-fx-max-width: 22em");
		bxDescription = new GenericDescriptionVBox<Resource>(null,null);

		cbAllowExtremeResources = new ToggleSwitch(SplittermondRules.CHARGEN_EXTREME_RESOURCES.getName(Locale.getDefault()));
		cbAllowExtremeResources.setGraphicTextGap(20);
		Label lbLock = new Label("x\uD83D\uDD12");
		cbAllowExtremeResources.setGraphic(lbLock);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Information about spent PP
		Label hdUnspent = new Label(ResourceI18N.get(RES, "page.resources.unspent"));
		hdUnspent.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox selectedHeading = new HBox(10, hdUnspent, lbPPCurrent, new Label("/"), lbPPMax);
		selection.setSelectedListHead(selectedHeading);


		layout = new OptionalNodePane(selection, bxDescription);
		layout.setId("optional-powers");
//		layout.setStyle("-fx-background-color: yellow");
		setContent(layout);

		setBackContent(cbAllowExtremeResources);
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		// Current Karma
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.xp"));
		backHeaderKarma.setValue(charGen.getModel().getExpFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		selection.refresh();

		backHeaderKarma.setValue(charGen.getModel().getExpFree());

		lbPPCurrent.setText( String.valueOf(charGen.getResourceController().getPointsLeft()) );
		RuleValue val = charGen.getRuleController().getRule(SplittermondRules.CHARGEN_EXTREME_RESOURCES);
		cbAllowExtremeResources.setSelected(charGen.getRuleController().getRuleValueAsBoolean(SplittermondRules.CHARGEN_EXTREME_RESOURCES));
		cbAllowExtremeResources.setDisable(!val.isEditable());
		lbPPMax    .setText( String.valueOf(charGen.getResourceController().getPointsMax()) );
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.DEBUG, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (SpliMoCharacterGenerator) param[0];
			selection.setController(charGen.getResourceController());
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}
}

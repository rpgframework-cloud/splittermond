package org.prelle.splittermond.chargen.jfx.creatures;

import java.lang.System.Logger;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;
import org.prelle.splimo.creature.CreatureTypeValue;
import org.prelle.splimo.creature.SMLifeform;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class CreatureTypeViewPane extends FlowPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;

	private BooleanProperty editable = new SimpleBooleanProperty();
	private Label lblHeading;
	private Button btnEdit;

	//-------------------------------------------------------------------
	public CreatureTypeViewPane(boolean editable) {
		this.editable.set(editable);
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(UI.getString("label.type")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		btnEdit = new Button(null, new SymbolIcon("edit"));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().addAll(lblHeading, btnEdit);

	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.visibleProperty().bind(editable);
		btnEdit.managedProperty().bind(editable);
		btnEdit.setOnAction(ev -> {
			CreatureTypeEditDialog dia = new CreatureTypeEditDialog(model);
			CloseType closed = FlexibleApplication.getInstance().showAndWait(dia);
			refresh();

		});
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading, btnEdit);

		if (model==null) {
			return;
		}
		for (Iterator<CreatureTypeValue> it=model.getCreatureTypes().iterator(); it.hasNext(); ) {
			CreatureTypeValue val = it.next();
			String text = val.getName();
			if (it.hasNext())
				text +=",";
			Text textNode = new Text(text);
			getChildren().add(textNode);
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
		refresh();
		initInteractivity();
	}

}

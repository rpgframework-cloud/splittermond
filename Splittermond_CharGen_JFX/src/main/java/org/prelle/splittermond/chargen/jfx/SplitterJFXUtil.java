package org.prelle.splittermond.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.OptionsTool;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author prelle
 *
 */
public class SplitterJFXUtil {

	public final static Logger logger = System.getLogger("splittermond.jfx");

	//-------------------------------------------------------------------
	public static void openDecisionDialog(ComplexDataItem decideFor, Choice choice, PartialController<? extends DataItem> handler) {
		logger.log(Level.DEBUG, "open decision dialog for "+choice.getChooseFrom()+" from "+decideFor.getTypeString());
		String modText = SplitterTools.getChoiceString(decideFor, choice);
		Decision dec = RPGFrameworkJavaFX.requestDecision(decideFor, choice, modText, handler);

//		switch ((SplittermondReference)choice.getChooseFrom()) {
//		case ATTRIBUTE:
//			openAttributeDecisionDialog(decideFor, choice, handler);
//			return;
//		}
//		logger.error("Not implemented: "+choice.getChooseFrom());
	}

	//-------------------------------------------------------------------
	public static void openDecisionDialog(ComplexDataItem decideFor, ModificationChoice choice, PartialController<? extends DataItem> handler) {
		logger.log(Level.DEBUG, "open decision dialog for ModificationChoice "+choice+" from "+decideFor.getTypeString());
		String modText = SplitterTools.getChoiceString(decideFor, choice, Locale.getDefault());
		Decision dec = RPGFrameworkJavaFX.requestDecision(decideFor, choice, modText, handler);

//		switch ((SplittermondReference)choice.getChooseFrom()) {
//		case ATTRIBUTE:
//			openAttributeDecisionDialog(decideFor, choice, handler);
//			return;
//		}
//		logger.error("Not implemented: "+choice.getChooseFrom());
	}

	//-------------------------------------------------------------------
	private static void openAttributeDecisionDialog(ComplexDataItem decideFor, Choice choice, PartialController<? extends DataItem> handler) {
		if (handler==null)
			throw new NullPointerException("PartialController is NULL");
		if (choice.getDistribute()==null) {
			boolean multi = choice.getCount()>1;
			ListView<Attribute> list = new ListView<>();
			list.getItems().addAll( (List<Attribute>)OptionsTool.calculateOptions(decideFor, choice, handler) );
			list.getSelectionModel().setSelectionMode(multi?SelectionMode.MULTIPLE:SelectionMode.SINGLE);
			list.setCellFactory( lv -> new ListCell<Attribute>() {
				public void updateItem(Attribute item, boolean empty) {
					super.updateItem(item, empty);
					if (empty) {
						setText(null);
					} else {
						setText(item.getName());
					}
				}
			});

			ManagedDialog dialog = new ManagedDialog("Wähle Attribut", list, CloseType.OK, CloseType.CANCEL);
			if (multi) {
				list.getSelectionModel().getSelectedIndices().addListener(new ListChangeListener<>() {
					public void onChanged(Change<? extends Integer> c) {
						dialog.buttonDisabledProperty().put(CloseType.OK, list.getSelectionModel().getSelectedIndices().size()==choice.getCount());
					}});
			} else {
				list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
					dialog.buttonDisabledProperty().put(CloseType.OK, n==null);
				});
			}
			CloseType closed = FlexibleApplication.getInstance().showAndWait(dialog);
			logger.log(Level.INFO, "Closed with "+closed);
			if (closed==CloseType.OK) {
				List<String> chosen = list.getSelectionModel().getSelectedItems().stream().map(attr->attr.name()).collect(Collectors.toList());
				Decision decide = new Decision(choice, String.join(",", chosen));
				DataItem foo = decideFor;
				handler.decide(null, choice.getUUID(), decide);
			}
		}

	}




	//-------------------------------------------------------------------
	public static void showPopupText(double sceneX, double sceneY, MouseEvent ev, String desc) {
		logger.log(Level.INFO, "Show popup text");

		Stage popupWindow = new Stage(StageStyle.UNDECORATED);
		popupWindow.focusedProperty().addListener( (ov,o,n) -> {
			if (n==Boolean.FALSE) {
				popupWindow.hide();
			}
		});
		popupWindow.initModality(Modality.APPLICATION_MODAL);
		popupWindow.setTitle("Foo");
		TextFlow lbl = new TextFlow();
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(lbl, desc);
		lbl.setStyle("-fx-pref-width: 25em");
		VBox box = new VBox(lbl);
		box.setStyle("-fx-padding: 0.5em");
		Scene scene = new Scene(box);
		popupWindow.setScene(scene);
		popupWindow.setX(sceneX+ev.getSceneX());
		popupWindow.setY(sceneY+ev.getSceneY());
		popupWindow.showAndWait();
	}

	//-------------------------------------------------------------------
	public static void showPopupNode(Node node) {
		logger.log(Level.INFO, "Show popup node");

		Stage popupWindow = new Stage(StageStyle.UNDECORATED);
		popupWindow.focusedProperty().addListener( (ov,o,n) -> {
			if (n==Boolean.FALSE) {
				popupWindow.hide();
			}
		});
		popupWindow.initModality(Modality.APPLICATION_MODAL);
		popupWindow.setTitle("Foo");
		VBox box = new VBox(node);
		box.setStyle("-fx-padding: 0.5em");
		Scene scene = new Scene(box);
		popupWindow.setScene(scene);
		popupWindow.showAndWait();
	}
}

package org.prelle.splittermond.chargen.jfx;

import java.util.function.Function;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class SMReferenceTypeConverter<T> implements Function<ModifiedObjectType, String> {

	private final static MultiLanguageResourceBundle CORE = SplitterMondCore.getI18nResources();

	//-------------------------------------------------------------------
	private static String toString(SplittermondReference ref) {
		// Value of NULL means DataItemPane will not show it
		if (ref==SplittermondReference.CULTURE_LORE) return null;
		if (ref==SplittermondReference.BACKGROUND) return null;
		String key = "reference."+ref.name().toLowerCase()+".name";
		return CORE.getString(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.util.function.Function#apply(java.lang.Object)
	 */
	@Override
	public String apply(ModifiedObjectType t) {
		return toString((SplittermondReference) t);
	}

}

/**
 *
 */
package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.CharacterGeneratorRegistry;
import org.prelle.splittermond.chargen.gen.GeneratorWrapper;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.jfx.wizard.WizardPageGenerator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WizardPageGeneratorSM extends WizardPage {

	private final static Logger logger = System.getLogger("splittermond.gen.wizard");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageGenerator.class.getName());

	private SpliMoCharacter model;
	private GeneratorWrapper wrapper;
	private ListView<Class<? extends SpliMoCharacterGenerator>> options;
	private Label descHeading;
	private Label description;
	private VBox descBox;
	private AnchorPane layout;

	//--------------------------------------------------------------------
	public WizardPageGeneratorSM(Wizard wizard, GeneratorWrapper model) {
		super(wizard);
		this.wrapper = model;
		this.model = new SpliMoCharacter();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		if (!options.getItems().isEmpty()) {
			options.getSelectionModel().select(model.getWrapped().getClass());
		}
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		options= new ListView<Class<? extends SpliMoCharacterGenerator>>();
		options.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		options.setCellFactory(new Callback<ListView<Class<? extends SpliMoCharacterGenerator>>, ListCell<Class<? extends SpliMoCharacterGenerator>>>() {
			public ListCell<Class<? extends SpliMoCharacterGenerator>> call(ListView<Class<? extends SpliMoCharacterGenerator>> arg0) {
				return new CharacterGeneratorListCell();
			}
		});
		options.getItems().setAll(CharacterGeneratorRegistry.getGenerators());
		int lines = Math.max(4, options.getItems().size());
		options.setStyle("-fx-pref-width: 15em; -fx-pref-height: "+(lines*3.5)+"em");

		descHeading = new Label();
		description = new Label();
		description.setWrapText(true);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		descBox = new VBox(20);
		descBox.getChildren().addAll(descHeading, description);
		description.setStyle("-fx-pref-width: 20em");

		layout = new AnchorPane();
//		FlowPane content = new FlowPane();
//		content.setHgap(20);
//		content.setVgap(20);
//		content.getChildren().addAll(options, descBox);
		super.setContent(layout);
		updateLayout();
	}

	//-------------------------------------------------------------------
	private void updateLayout() {
		layout.getChildren().clear();
		switch (ResponsiveControlManager.getCurrentMode()) {
		case MINIMAL:
			VBox helpV = new VBox(20, options, descBox);
			layout.getChildren().add(helpV);
			break;
		default:
			HBox helpH = new HBox(20, options, descBox);
			layout.getChildren().add(helpH);
			break;
		}
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		descHeading.getStyleClass().add("text-small-subheader");
		description.getStyleClass().add("text-body");
//		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		options.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Class<? extends SpliMoCharacterGenerator>>() {
			public void changed(ObservableValue<? extends Class<? extends SpliMoCharacterGenerator>> observable,
					Class<? extends SpliMoCharacterGenerator> oldValue, Class<? extends SpliMoCharacterGenerator> newValue) {
					update(newValue);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		updateLayout();
	}

	//-------------------------------------------------------------------
	private void update(Class<? extends SpliMoCharacterGenerator> charGen) {
		if (charGen!=null) {
			String key = "generator."+charGen.getSimpleName();
			String name = ResourceI18N.get(RES, key);
			String desc = ResourceI18N.get(RES, key+".desc");
			descHeading.setText(name);
			description.setText(desc);

			logger.log(Level.DEBUG, "Change generator selection to "+charGen.getSimpleName());
			try {
				Constructor<? extends SpliMoCharacterGenerator> cons = charGen.getConstructor(SpliMoCharacter.class, CharacterHandle.class);
				SpliMoCharacterGenerator useGen = cons.newInstance(model, null);
				wrapper.setWrapped(useGen);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			descHeading.setText(null);
			description.setText(null);
		}
	}

}

class CharacterGeneratorListCell extends ListCell<Class<? extends SpliMoCharacterGenerator>> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageGenerator.class.getName());

	private VBox box;
	private Label lblHeading;
	private Label lblHardcopy;

	//--------------------------------------------------------------------
	public CharacterGeneratorListCell() {
		lblHeading = new Label();
		lblHeading.getStyleClass().add("base");
		lblHardcopy = new Label();
		box = new VBox(5);
		box.getChildren().addAll(lblHeading, lblHardcopy);
	}


	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Class<? extends SpliMoCharacterGenerator> item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(box);

			String key = "generator."+item.getSimpleName();
			String name = ResourceI18N.get(RES, key);
			String desc = ResourceI18N.get(RES, key+".line2");
			lblHeading.setText(name);
			lblHardcopy.setText(desc);
			lblHardcopy.setUserData(item);
		}
	}
}
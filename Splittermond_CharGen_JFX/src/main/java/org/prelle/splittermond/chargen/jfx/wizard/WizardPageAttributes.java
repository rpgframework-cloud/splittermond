package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Race;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SpliMoPrimaryAttributeTable;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.rules.AttributeTable.AttributeColumn;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

/**
 * @author prelle
 *
 */
public class WizardPageAttributes extends WizardPage implements ResponsiveControl, ControllerListener {

	private final static Logger logger = System.getLogger("splittermond.gen.wizard");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageAttributes.class.getName());

	private Text txtPoints;
	private AttributeController control;
	private RaceController raceControl;
	private SpliMoPrimaryAttributeTable table;
	private Button btnRaceChoice;
	private Label lblRaceChoice;
	private Label lblWarning;

	private Label hdAttrib;
	private TextFlow tfDescr;
	private Pane descNode;

	private OptionalNodePane contentWithOptional;

	//-------------------------------------------------------------------
	public WizardPageAttributes(Wizard wizard, AttributeController control, RaceController raceControl) {
		super(wizard);
		if (control==null)
			throw new NullPointerException("AttributeController may not be null");
		this.control = control;
		this.raceControl = raceControl;
		setTitle(ResourceI18N.get(RES, "page.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initComponents() {
		txtPoints = new Text("?");
		txtPoints.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		txtPoints.setStyle("-fx-font-size: larger");

		table = new SpliMoPrimaryAttributeTable();
		table.setModel(control.getModel());
		table.setController(control);
		initModificationColumn();

		btnRaceChoice = new Button("?");
		lblRaceChoice = new Label("Choice");
		lblWarning    = new Label("Warn");
		lblWarning.setStyle("-fx-text-fill: red");

		tfDescr = new TextFlow();
		hdAttrib = new Label();
		hdAttrib.getStyleClass().add(JavaFXConstants.STYLE_HEADING3);
	}

	//-------------------------------------------------------------------
	private void initModificationColumn() {
		BiFunction<RuleSpecificCharacterObject<Attribute,?,?,?>, Attribute, Object> valueFact = (model, key) -> {
//			logger.log(Level.INFO, "---Mods of "+key+" = "+model.getAttribute(key).getModifications());
			if (model.getAttribute(key).getModifier()!=0)
				return model.getAttribute(key).getModifier();
			return null;
		};
		BiFunction<Attribute, Object, Node> compFact = (key, value) -> {
			if (value==null) return null;
			Label ret = new Label(String.valueOf(value));
			ret.setAlignment(Pos.CENTER);
			ret.setTextAlignment(TextAlignment.CENTER);
			GridPane.setFillWidth(ret, true);
			return ret;
		};

		AttributeColumn<Attribute> col1 = new AttributeColumn<Attribute>(valueFact, compFact);
		col1.setShowBeforeColumn(true);
		col1.setTitle(ResourceI18N.get(RES, "column.species"));
		table.getColumns().add(col1);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		TextFlow preLine = new TextFlow(
				new Text(ResourceI18N.get(RES, "preline.pre")+" "),
				txtPoints,
				new Text(" "+ResourceI18N.get(RES, "preline.post"))
				);


		HBox line1 = new HBox(10, btnRaceChoice, lblRaceChoice);
		line1.setMaxWidth(Region.USE_COMPUTED_SIZE);

		table.setMaxWidth(Region.USE_COMPUTED_SIZE);
		HBox.setHgrow(table, Priority.NEVER);
		HBox goo = new HBox(table);
		goo.setAlignment(Pos.CENTER);

		VBox content = new VBox(5);
		content.setAlignment(Pos.TOP_CENTER);
		content.getChildren().addAll(preLine, goo, new VBox(10, line1, lblWarning));
		content.setFillWidth(false);
		HBox.setHgrow(content, Priority.NEVER);
		content.setMaxWidth(Region.USE_COMPUTED_SIZE);

		descNode = new VBox(10, hdAttrib, tfDescr);
		descNode.getStyleClass().addAll("detail-card");

		ScrollPane spDesc = new ScrollPane(descNode);
		spDesc.setFitToWidth(true);
		spDesc.setStyle("-fx-pref-width: 30em");

		content.getStyleClass().addAll("detail-card");
		HBox.setMargin(content, new Insets(0, 10, 0, 0));

		contentWithOptional = new OptionalNodePane(content, spDesc);
		contentWithOptional.setContentGrow(Priority.SOMETIMES);
		contentWithOptional.setUseScrollPane(false);
		setContent(contentWithOptional);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initInteractivity() {
		table.selectedAttributeProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Selection changed: "+n);
			if (n!=null) {
				hdAttrib.setText(((Attribute)n).getName() );
				RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(tfDescr, ((Attribute)n).getDescription() );
			}
		});

		btnRaceChoice.setOnAction(ev -> {
			SplitterJFXUtil.openDecisionDialog(getRace(), getRaceChoice(getRace()), raceControl);
			updateChoice();
			refresh();
		});
		setOnExtraActionHandler( button -> onExtraAction(button));
	}

	//-------------------------------------------------------------------
	/**
	 */
	void refresh() {
		logger.log(Level.WARNING, "refresh");
		table.refresh();
		txtPoints.setText(String.valueOf(control.getPointsLeft()));

		List<ToDoElement> todos = new ArrayList<>(raceControl.getToDos());
		todos.addAll(control.getToDos());
		List<String> tmp = todos.stream().map(k -> k.getMessage()).collect(Collectors.toList());
		lblWarning.setText( String.join(", ", tmp));

		// Find modification with choice from race
		updateChoice();
	}

	//-------------------------------------------------------------------
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			control = ((SpliMoCharacterGenerator) param[0]).getAttributeController();
			refresh();
			return;
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			refresh();
		}
	}

	//-------------------------------------------------------------------
	private Race getRace() {
		SpliMoCharacter model = (SpliMoCharacter) control.getModel();
		if (model.getRace()==null)
			return null;
		return SplitterMondCore.getItem(Race.class, model.getRace());
	}

	//-------------------------------------------------------------------
	private Choice getRaceChoice(Race item) {
		if (item==null) return null;
		DataItemModification mod = null;
		Choice choice = null;
		for (Modification m : item.getOutgoingModifications()) {
			if (m instanceof DataItemModification) {
				mod = (DataItemModification) m;
				if (mod.getReferenceType()==SplittermondReference.ATTRIBUTE && mod.getConnectedChoice()!=null) {
					choice = item.getChoice(mod.getConnectedChoice());
				} else {
					mod=null;
				}
			}
		}
		return choice;
	}

	//-------------------------------------------------------------------
	private DataItemModification getRaceAttributeModification(Race item) {
		DataItemModification mod = null;
		Choice choice = null;
		for (Modification m : item.getOutgoingModifications()) {
			if (m instanceof DataItemModification) {
				mod = (DataItemModification) m;
				if (mod.getReferenceType()==SplittermondReference.ATTRIBUTE && mod.getConnectedChoice()!=null) {
					return mod;
				}
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void updateChoice() {
		SpliMoCharacter model = (SpliMoCharacter) control.getModel();
		Race item = getRace();
		Choice choice = getRaceChoice(item);

		if (choice==null) {
			// No choice found - this should not happen
			btnRaceChoice.setVisible(false);
			lblRaceChoice.setVisible(false);
		} else {
			btnRaceChoice.setVisible(true);
			lblRaceChoice.setVisible(true);
			Decision dec = model.getDecision(choice.getUUID());
			logger.log(Level.INFO, "Dec = "+dec);
			if (dec==null) {
				logger.log(Level.INFO, "No decision yet: "+SplitterTools.getChoiceString(item, choice));
				lblRaceChoice.setText(SplitterTools.getChoiceString(item, choice));
			} else {
				logger.log(Level.INFO, "Decision = "+SplitterTools.getDecisionString(item, choice, dec));
				lblRaceChoice.setText(SplitterTools.getDecisionString(item, choice, dec));
				Race race = getRace();
				DataItemModification mod = getRaceAttributeModification(race);
				List<Modification> modList = GenericRPGTools.decisionToModifications(mod, choice, dec);
				List<String> modListS = modList.stream().map(mb -> SplitterTools.getModificationString(race,mb, Locale.getDefault()))
						.collect(Collectors.toList());
				lblRaceChoice.setText(String.join(", ", modListS));
			}
		}
//			Decision dec = model.getDecision(mod.getConnectedChoice());
//		if (dec == null) {
//			decText.setText("");
//		} else {
//			SplitterTools.getModificationString(contentPane.getSelectedItem(),m)
//			List<Modification> modList = GenericRPGTools.decisionToModifications(mod,
//					item.getChoice(mod.getConnectedChoice()), dec);
//			List<String> modListS = modList.stream().map(mb -> getSkinnable().getModificationConverter().apply(mb))
//					.collect(Collectors.toList());
//			decText.setText(" (" + String.join(", ", modListS) + ")");
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.log(Level.INFO, "Mode "+value);
		descNode.setVisible(  value!=WindowMode.MINIMAL );
		descNode.setManaged(  value!=WindowMode.MINIMAL );
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		refresh();
	}

	//-------------------------------------------------------------------
	private void onExtraAction(CloseType button) {
		logger.log(Level.INFO, "onExtraAction {0}", button);
		switch (button) {
		case RANDOMIZE:
			control.roll();
			refresh();
			break;
		default:
			logger.log(Level.WARNING, "ToDo: handle "+button);
		}
	}

}

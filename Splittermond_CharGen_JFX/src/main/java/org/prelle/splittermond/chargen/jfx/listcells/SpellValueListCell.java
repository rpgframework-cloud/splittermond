package org.prelle.splittermond.chargen.jfx.listcells;

import java.util.Locale;
import java.util.function.Supplier;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 *
 */
public class SpellValueListCell extends ComplexDataItemValueListCell<Spell, SpellValue> implements ResponsiveControl {

	private Supplier<SpliMoCharacterController> fullCtrlProv;

	private HBox content;
	private Label lbValue;
	private Label lbCost;

	//-------------------------------------------------------------------
	/**
	 * @param ctrlProv
	 */
	public SpellValueListCell(Supplier<SpliMoCharacterController> ctrlProv) {
		super(null);
		this.fullCtrlProv = ctrlProv;

		lbCost = new Label();
		lbValue = new Label();
		Label hdValue = new Label("Wert:");
		hdValue.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		Label hdCost  = new Label("Kosten:");
		hdCost.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		content = new HBox(5, hdValue, lbValue, hdCost, lbCost);
		HBox.setMargin(hdCost, new Insets(0, 0, 0, 5));
	}

	//-------------------------------------------------------------------
	protected Parent getContentNode(SpellValue ref) {
		return content;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		// TODO Auto-generated method stub
		content.setVisible(value!=WindowMode.MINIMAL);
		content.setManaged(value!=WindowMode.MINIMAL);
	}

	//-------------------------------------------------------------------
	protected void updateDecisionLine(SpellValue item, ComplexDataItemController<Spell, SpellValue> ctrl) {
		decision.setText( (item!=null)?(item.getSkill().getName(Locale.getDefault())):null);
		super.updateDecisionLine(item, ctrl);
		decisionLine.setVisible( item!=null );
		decisionLine.setManaged( item!=null );
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SpellValue item, boolean empty) {
		super.updateItem(item, empty);

		if (item!=null) {
			SMSkill skill = item.getSkill();
			super.controlProvider = () -> fullCtrlProv.get().getSpellController(skill);
		} else {
			super.controlProvider = () -> fullCtrlProv.get().getSpellController();
		}
		decisionLine.setVisible( item!=null );
		decisionLine.setManaged( item!=null );
		if (fullCtrlProv.get()!=null && item!=null) {
			lbValue.setText( String.valueOf( SplitterTools.getSpellValue(fullCtrlProv.get().getModel(), item)) );
			lbCost.setText( SplitterTools.getSpellCostString(fullCtrlProv.get().getModel(), item) );
		}
	}

}

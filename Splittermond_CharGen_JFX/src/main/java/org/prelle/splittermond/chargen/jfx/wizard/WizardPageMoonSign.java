package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.MoonSignPane;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author Stefan
 *
 */
public class WizardPageMoonSign extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageMoonSign.class.getPackageName()+".moonsign");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageMoonSign.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	protected MoonSignPane selection;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	protected NumberUnitBackHeader backHeaderKarma;

	//-------------------------------------------------------------------
	public WizardPageMoonSign(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.moonsigns.title"));
		initComponents();
		initLayout();
		initBackHeader();
		initInteractivity();

		charGen.addListener(this);
	}
	//-------------------------------------------------------------------
	protected void initComponents() {
		selection = new MoonSignPane();
		bxDescription = new GenericDescriptionVBox(null,null);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new OptionalNodePane(selection, bxDescription);
		layout.setId("optional-moonsign");
		layout.setContentGrow(Priority.NEVER);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		// Current Karma
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.xp"));
		backHeaderKarma.setValue(charGen.getModel().getExpFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.selectedProperty().addListener( (ov,o,n) -> {
			charGen.getModel().setMoonsign(n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		selection.selectedProperty().setValue(charGen.getModel().getMoonsign());
		backHeaderKarma.setValue(charGen.getModel().getExpFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.DEBUG, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (SpliMoCharacterGenerator) param[0];
			refresh();
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMItemFlag;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.ItemTemplateFilterNode;
import org.prelle.splittermond.chargen.jfx.listcells.CarriedItemListCell;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class WizardPageEquipment extends WizardPage implements ResponsiveControl, ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageEquipment.class.getPackageName()+".equip");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageEquipment.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	private Label lbDescr;
	private Label lbLunare, lbTelare;

	protected ComplexDataItemControllerNode<ItemTemplate, CarriedItem<ItemTemplate>> selection;
	protected GenericDescriptionVBox<ItemTemplate> bxDescription;
	protected OptionalNodePane layout;
	protected NumberUnitBackHeader backHeaderKarma;

	//-------------------------------------------------------------------
	public WizardPageEquipment(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.startequip.title"));
		initComponents();
		initLayout();
		initBackHeader();
		initInteractivity();

		charGen.addListener(this);
	}
	//-------------------------------------------------------------------
	protected void initComponents() {
		lbDescr  = new Label(ResourceI18N.get(RES, "page.startequip.desc"));
		lbDescr.setWrapText(true);
		lbLunare = new Label("?");
		lbTelare     = new Label("?");

		selection = new ComplexDataItemControllerNode<>(charGen.getEquipmentController());
		selection.setFilterNode(new ItemTemplateFilterNode(
				RES,
				selection,
				ItemType.WEAPON_CLOSE,
				ItemType.values()
				));

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.startequip.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.startequip.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<ItemTemplate>( () -> charGen.getEquipmentController()));
//		selection.setSelectedCellFactory(lv -> new ComplexDataItemValueListCell<ItemTemplate,CarriedItem<ItemTemplate>>( () -> charGen.getEquipmentController()));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		selection.setAvailableStyle("-fx-max-width: 22em");
//		selection.setSelectedStyle("-fx-max-width: 22em");
		selection.setSelectedCellFactory(lv -> new CarriedItemListCell(
				()->charGen.getEquipmentController(),
				SMItemFlag.FREE_WEAPON
				));
		bxDescription = new GenericDescriptionVBox<ItemTemplate>(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault())
				);

	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Information about spent PP
		Label hdUnspent = new Label(ResourceI18N.get(RES, "page.startequip.unspent"));
		hdUnspent.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox selectedHeading = new HBox(10, hdUnspent, lbLunare, new Label("/"), lbTelare);
		selection.setSelectedListHead(selectedHeading);

		VBox.setVgrow(selectedHeading, Priority.ALWAYS);
		layout = new OptionalNodePane(new VBox(20,lbDescr,selection), bxDescription);
		layout.setId("optional-toolsoftrade");

		Region buf = new Region();
		buf.setMaxHeight(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.SOMETIMES);
		setContent(new HBox(buf, layout));
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		// Current Karma
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.xp"));
		backHeaderKarma.setValue(charGen.getModel().getExpFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		selection.refresh();

		backHeaderKarma.setValue(charGen.getModel().getExpFree());

		lbLunare.setText( String.valueOf(charGen.getEquipmentController().getMoneyRemaining()/100) +"L" );
		lbTelare.setText( String.valueOf(charGen.getEquipmentController().getMoneyRemaining()%100) +"T" );
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.DEBUG, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (SpliMoCharacterGenerator) param[0];
			selection.setController(charGen.getEquipmentController());
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}
}

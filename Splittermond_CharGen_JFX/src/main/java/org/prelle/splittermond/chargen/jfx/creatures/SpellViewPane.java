package org.prelle.splittermond.chargen.jfx.creatures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.creature.SMLifeform;

import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class SpellViewPane extends FlowPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;
	
	private Label lblHeading;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellViewPane() {
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(UI.getString("label.spells")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().add(lblHeading);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading);
		
		Map<SMSkill, List<SpellValue>> data = new HashMap<>();
		/*
		 * Collect data
		 */
		for (SMSkillValue sVal : model.getSkills(SkillType.MAGIC)) {
			// Ignore all skills with no points
			if (sVal.getValue()<1 && sVal.getModifier()<1)
				continue;
			SMSkill skill = sVal.getSkill();
			List<SpellValue> spells = new ArrayList<SpellValue>();
			data.put(skill, spells);
			for (SpellValue spVal : model.getSpells()) {
				if (spVal.getSkill()!=skill)
					continue;
				spells.add(spVal);
			}
		}
		
		/*
		 * Format data
		 */
		List<SMSkill> skills = new ArrayList<>(data.keySet());
		Collections.sort(skills);
		for (Iterator<SMSkill> it=skills.iterator(); it.hasNext(); ) {
			SMSkill skill = it.next();
			Text textNode = new Text(skill.getName()+" ");
			getChildren().add(textNode);
			
			List<SpellValue> spells = data.get(skill);
			int lastLevel = -1;
			for (Iterator<SpellValue> it2=spells.iterator(); it2.hasNext(); ) {
				SpellValue spell = it2.next();
				if (spell.getSpellLevel()>lastLevel) {
					textNode = new Text(spell.getSpellLevel()+": ");
					getChildren().add(textNode);					
				}
				
				String text = spell.getResolved().getName();
				if (it2.hasNext())
					text +=",";
				textNode = new Text(text);
				getChildren().add(textNode);
			}
			
			if (it.hasNext()) {
				textNode = new Text(", ");
				getChildren().add(textNode);					
			}
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
//		logger.info("setData "+model.dump());
		refresh();
		initInteractivity();
	}

}

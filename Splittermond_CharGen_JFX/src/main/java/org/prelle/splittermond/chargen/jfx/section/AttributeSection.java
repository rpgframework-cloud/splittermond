package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Section;
import org.prelle.splimo.Attribute;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.jfx.rules.AttributeTable;
import de.rpgframework.jfx.rules.AttributeTable.AttributeColumn;
import de.rpgframework.jfx.rules.AttributeTable.Mode;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class AttributeSection extends Section {

	private final static Logger logger = System.getLogger(AttributeSection.class.getPackageName());

	private AttributeTable<Attribute> table;
	private Label lbToDo;

	private SpliMoCharacterController control;

	//-------------------------------------------------------------------
	public AttributeSection(String title) {
		super(title, null);
		setId("attributes");

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbToDo = new Label("?");
		lbToDo.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbToDo.setStyle("-fx-text-fill: highlight");

		table = new AttributeTable<Attribute>(Attribute.primaryValues());
		table.setMode(Mode.GENERATE);
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		setContent(new VBox(10, lbToDo, table));

		CheckBox cb1 = new CheckBox("Configuration Setting 1");
		CheckBox cb2 = new CheckBox("Configuration Setting 2");
		VBox back = new VBox(5, cb1, cb2);
		setSecondaryContent(back);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		table.prefWidthProperty().bind(prefWidthProperty());
//		if (getSettingsButton()!=null)
//			getSettingsButton().setOnAction(ev -> onSettings());
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController control) {
		this.control = control;
		table.setController(control.getAttributeController());
		table.setModel(control.getModel());

		table.getColumns().add(new AttributeColumn<Attribute>( (m,a) -> {
			return m.getAttribute(a).getStart();
		}, null));
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.log(Level.DEBUG, "refresh: {0}",table.getSkin());
		if (control!=null) {
			table.setController(control.getAttributeController());
			lbToDo.setText(GenericRPGTools.getToDoString(control.getAttributeController().getToDos(), Locale.getDefault()));
		}

		table.refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<IAttribute> showHelpForProperty() {
		return table.selectedAttributeProperty();
	}

//	//-------------------------------------------------------------------
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getAttributeController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

}

package org.prelle.splittermond.chargen.jfx;

import org.prelle.splimo.Attribute;

import de.rpgframework.jfx.rules.AttributeTable;

/**
 * @author stefa
 *
 */
public class SpliMoPrimaryAttributeTable extends AttributeTable {

	//-------------------------------------------------------------------
	public SpliMoPrimaryAttributeTable() {
		super(Attribute.primaryValues());
		super.setMode(Mode.GENERATE);
	}
	
}

package org.prelle.splittermond.chargen.jfx.pane;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SplitterTools;

import de.rpgframework.ResourceI18N;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author stefa
 *
 */
public class SpellDescriptionPane extends ADescriptionPane<Spell> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SpellDescriptionPane.class.getName());

	private Label lblType;
	private Label lblDiff;
	private Label lblCost;
	private Label lblDur;
	private Label lblRange;
	private Label lblSpDur;
	private TextFlow tfDescr;
	private Label lblEnhCost;
	private Label lblSuccess;
	private Label lblEnhan;

	public SpellDescriptionPane() {
		// Type
		Label headType = new Label(ResourceI18N.get(UI,"label.spell.type")+": ");
		lblType  = new Label();
		lblType.setWrapText(true);
		headType.getStyleClass().add("base");

		// Difficulty
		Label headDiff = new Label(ResourceI18N.get(UI,"label.spell.difficulty")+": ");
		lblDiff  = new Label();
		headDiff.getStyleClass().add("base");

		// Cost
		Label headCost = new Label(ResourceI18N.get(UI,"label.spell.cost")+": ");
		lblCost  = new Label();
		headCost.getStyleClass().add("base");

		// Cast duration
		Label headDur  = new Label(ResourceI18N.get(UI,"label.spell.castduration")+": ");
		lblDur   = new Label();
		headDur.getStyleClass().add("base");

		// Range
		Label headRange= new Label(ResourceI18N.get(UI,"label.spell.castrange")+": ");
		lblRange = new Label();
		headRange.getStyleClass().add("base");

		// Spell Duration
		Label headSpDur= new Label(ResourceI18N.get(UI,"label.spell.duration")+": ");
		lblSpDur = new Label();
		headSpDur.getStyleClass().add("base");

		// Effect
		tfDescr = new TextFlow();

		// Spell Duration
		Label headSuccess= new Label(ResourceI18N.get(UI,"label.spell.success")+": ");
		lblSuccess = new Label();
		lblSuccess.setWrapText(true);
		headSuccess.getStyleClass().add("base");

		// Enhancement
		Label headEnhan= new Label(ResourceI18N.get(UI,"label.spell.enhanced")+": ");
		lblEnhCost = new Label();
		lblEnhan = new Label();
		lblEnhan.setWrapText(true);
		headEnhan.getStyleClass().add("base");

		HBox boxEnhan = new HBox(headEnhan, lblEnhCost);
		getChildren().setAll(
				descTitle,
				descSources,
				new HBox(headType, lblType),
				new HBox(headDiff, lblDiff),
				new HBox(headCost, lblCost),
				new HBox(headDur , lblDur ),
				new HBox(headRange, lblRange),
				new HBox(headSpDur, lblSpDur),
				tfDescr,
				headSuccess, lblSuccess,
				boxEnhan,
				lblEnhan);
		VBox.setMargin(tfDescr, new Insets(20,0,0,0));
		VBox.setMargin(headSuccess, new Insets(20,0,0,0));

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	public void setData(Spell spell) {
		descTitle.setText(spell.getName(Locale.getDefault()));
		descSources.setText(RPGFrameworkJavaFX.createSourceText(spell));

		// Type
		StringBuffer buf = new StringBuffer();
		for (Iterator<SpellType> it=spell.getTypes().iterator(); it.hasNext(); ) {
			buf.append(it.next().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		lblType.setText(buf.toString());

		// Difficulty
		lblDiff.setText(String.valueOf(spell.getDifficultyString()));

		// Cost
		lblCost.setText(SplitterTools.getFocusString(spell.getCost()));

		// Cast duration
		lblDur.setText(spell.getCastDurationString());

		// Range
		lblRange.setText(spell.getCastRangeString());

		// Spell Duration
		lblSpDur.setText(spell.getSpellDurationString());

		// Effect
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(tfDescr, spell.getDescription(Locale.getDefault()));

		// Erfolgsgrade
		List<String> items = new ArrayList<>();
		items.add("Auslösezeit");
		if (spell.getCost().getExhausted()>1)
			items.add("Erschöpfter Fokus");
		if (spell.getCost().getChannelled()>1)
			items.add("Kanalisierter Fokus");
		if (spell.getCastRange()>0)
			items.add("Reichweite");
		if (spell.getTypes().contains(SpellType.DAMAGE) && spell.getEffectRange()==null)
			items.add("Schaden");
		if (spell.getCost().getConsumed()>1)
			items.add("Verzehrter Fokus");
		if (spell.getEffectRange()!=null)
			items.add("Wirkungsbereich");
		if (spell.hasSpellDuration())
			items.add("Wirkungsdauer");
		lblSuccess.setText(String.join(", ", items));

		// Enhancement
		lblEnhCost.setText(spell.getEnhancementString());
		lblEnhan.setText(spell.getEnhancementDescription());

	}

}

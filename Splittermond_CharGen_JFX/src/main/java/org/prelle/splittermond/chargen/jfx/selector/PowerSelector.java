package org.prelle.splittermond.chargen.jfx.selector;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.splimo.Power;
import org.prelle.splimo.PowerValue;
import org.prelle.splittermond.chargen.charctrl.PowerController;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class PowerSelector extends Selector<Power, PowerValue> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public PowerSelector(PowerController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		super(ctrl, resolver, modResolver, null); //new FilterPowers());
		listPossible.setCellFactory( lv -> new ComplexDataItemListCell( () -> ctrl));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(Power selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

}

package org.prelle.splittermond.chargen.jfx.listcells;

import java.util.function.Function;
import java.util.function.Supplier;

import org.prelle.splimo.Spell;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;

/**
 * 
 */
public class SpellListCell extends ComplexDataItemListCell<Spell> {

	//-------------------------------------------------------------------
	/**
	 */
	public SpellListCell() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param controlProv
	 */
	public SpellListCell(
			Supplier<ComplexDataItemController<Spell, ? extends ComplexDataItemValue<Spell>>> controlProv) {
		super(controlProv);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param controlProv
	 * @param req
	 */
	public SpellListCell(Supplier<ComplexDataItemController<Spell, ? extends ComplexDataItemValue<Spell>>> controlProv,
			Function<Requirement, String> req) {
		super(controlProv, req);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param controlProv
	 * @param req
	 * @param selector
	 */
	public SpellListCell(Supplier<ComplexDataItemController<Spell, ? extends ComplexDataItemValue<Spell>>> controlProv,
			Function<Requirement, String> req, Selector<Spell, ? extends ComplexDataItemValue<Spell>> selector) {
		super(controlProv, req, selector);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param costStringGetter
	 */
	public SpellListCell(Function<Spell, String> costStringGetter) {
		super(costStringGetter);
		// TODO Auto-generated constructor stub
	}

}

package org.prelle.splittermond.chargen.jfx;

import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SplitterTools;

import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * @author stefa
 *
 */
public class SpellDescriptionPageController {
	
	@FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    // Only for pages , not for pane
    @FXML
    private Label descTitle;

    // Only for pages , not for pane
    @FXML
    private Label descSources;

    @FXML
    private Label type;

    @FXML
    private Label difficulty;

    @FXML
    private Label cost;

    @FXML
    private Label castduration;

    @FXML
    private Label castrange;

    @FXML
    private Label duration;

    @FXML
    private Label description;

    @FXML
    private Label enhanced;

    @FXML
    private Label enhanceDescr;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellDescriptionPageController() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@FXML
    void initialize() {
        assert type != null : "fx:id=\"type\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert difficulty != null : "fx:id=\"difficulty\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert cost != null : "fx:id=\"cost\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert castduration != null : "fx:id=\"castduration\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert castrange != null : "fx:id=\"castrange\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert duration != null : "fx:id=\"duration\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert description != null : "fx:id=\"description\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert enhanced != null : "fx:id=\"enhanced\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        assert enhanceDescr != null : "fx:id=\"enhanceDescr\" was not injected: check your FXML file 'SpellDescription.fxml'.";
        
        description.setStyle("-fx-max-width: 30em");

    }

	//-------------------------------------------------------------------
	public void setData(Spell spell) {
		if (descTitle!=null)
			descTitle.setText(spell.getName());
		if (descSources!=null)
			descSources.setText(RPGFrameworkJavaFX.createSourceText(spell));

		
		// Spell Type
		StringBuffer buf = new StringBuffer();
		for (Iterator<SpellType> it=spell.getTypes().iterator(); it.hasNext(); ) {
			buf.append(it.next().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		type.setText(buf.toString());

		// Difficulty
		difficulty.setText(String.valueOf(spell.getDifficultyString()));		
		// Cost
		cost.setText(SplitterTools.getFocusString(spell.getCost()));
		// Cast duration
		castduration.setText(spell.getCastDurationString());
		// Range
		castrange.setText(spell.getCastRangeString());
		// Spell Duration
		duration.setText(spell.getSpellDurationString());
		// Effect
		description.setText(spell.getDescription());
		enhanced.setText(spell.getEnhancementString());
		enhanceDescr.setText(spell.getEnhancementDescription());

	}

}

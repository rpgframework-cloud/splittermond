package org.prelle.splittermond.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splittermond.chargen.charctrl.SkillController;

import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class SpecializationSelector extends Selector<SkillSpecialization<SMSkill>, SkillSpecializationValue<SMSkill>> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public SpecializationSelector(SkillController ctrl, SMSkillValue data,Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		super(ctrl.getSkillSpecializationController(data), resolver, modResolver, null); //new FilterPowers());
		listPossible.setCellFactory( lv -> new ComplexDataItemListCell( () -> ctrl.getSkillSpecializationController(data)));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(SkillSpecialization<SMSkill> selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

}

package org.prelle.splittermond.chargen.jfx;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.prelle.splimo.Moonsign;
import org.prelle.splimo.SplitterMondCore;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author Stefan
 *
 */
public class MoonSignPane extends HBox {

	private static Logger logger = System.getLogger(MoonSignPane.class.getPackageName());

	private Map<ImageView, Moonsign> mapping;
	private ObjectProperty<Moonsign> selected;
	private GridPane content;

	//--------------------------------------------------------------------
	public MoonSignPane() {
		super(20);
		selected = new SimpleObjectProperty<>();
		initComponents();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initComponents() {

		content = new GridPane();

		mapping = new HashMap<ImageView, Moonsign>();
		// Set data
		List<Moonsign> data = SplitterMondCore.getItemList(Moonsign.class);
		// Sort alphabetically
		Collections.sort(data, new Comparator<Moonsign>() {
			@Override
			public int compare(Moonsign o1, Moonsign o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		int i=0;
		for (Moonsign tmp : data) {
			int x= i%3;
			int y= i/3;
			Image img = null;
			String fname = "images/moonsign/"+tmp.getId().toLowerCase()+".png";
			logger.log(Level.DEBUG, "Load "+fname);
			InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
			} else
				logger.log(Level.WARNING,"Missing image at "+fname);
			ImageView iView = new ImageView(img);
			iView.setOnMouseClicked(event -> {
					logger.log(Level.DEBUG, "Foo");
					ImageView view = (ImageView) event.getSource();
					selected.set(mapping.get(view));
					for (ImageView temp : mapping.keySet())
						if (temp!=view)
							temp.setEffect(null);
					// Mark selected
					logger.log(Level.INFO, "Moonsplinter now "+selected);
			});

			Label label = new Label(tmp.getName());
//			label.setPrefWidth(110);

			VBox foo = new VBox();
			foo.setAlignment(Pos.CENTER);
			foo.getChildren().addAll(iView, label);
			mapping.put(iView, tmp);
			content.add(foo, x, y);

			i++;
		}

		getChildren().addAll(content);

	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		selected.addListener( (ov,o,n) -> {
			logger.log(Level.ERROR, "Selected from "+o+" to "+n);
			for (Entry<ImageView, Moonsign> entry : mapping.entrySet()) {
				ImageView view = entry.getKey();
				if (entry.getValue()==n) {
					view.setEffect(new DropShadow(10, Color.RED));
				} else {
					view.setEffect(null);
				}
			}
		});
	}

	//--------------------------------------------------------------------
	public ObjectProperty<Moonsign> selectedProperty() {
		return selected;
	}

}

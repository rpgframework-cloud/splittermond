package org.prelle.splittermond.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.section.AttributeSection;
import org.prelle.splittermond.chargen.jfx.section.BasicDataSection;
import org.prelle.splittermond.chargen.jfx.section.DerivedAttributeSection;
import org.prelle.splittermond.chargen.jfx.section.PowerSection;
import org.prelle.splittermond.chargen.jfx.section.ResourceSection;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.section.AppearanceSection;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class BasicDataPage extends Page {

	private final static Logger logger = System.getLogger(BasicDataPage	.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkillPage.class.getPackageName()+".Pages");

	private SpliMoCharacterController ctrl;

	private BasicDataSection secBaseData;
	private AppearanceSection secPortrait;
	private AttributeSection secAttribPrimary;
	private DerivedAttributeSection secAttribDerived;
	private PowerSection secPowers;
	private ResourceSection secResources;
	private FlexGridPane flex;
	private OptionalNodePane layout ;
	private GenericDescriptionVBox descBox;

	//-------------------------------------------------------------------
	public BasicDataPage() {
		super(ResourceI18N.get(RES, "page.basics.title"));
		// Flow 1
		initBaseData();
		initPortrait();
		initAttributes();
		initPowers();
		initResources();

		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secBaseData, secPortrait,secAttribPrimary, secPowers, secResources, secAttribDerived);


		descBox = new GenericDescriptionVBox(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault())
				);
		layout = new OptionalNodePane(flex, null);
		layout.setTitle("Erklärung");
		setContent(layout);
//		setTitle("Basics");
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initBaseData() {
		secBaseData = new BasicDataSection();
		secBaseData.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secBaseData, 4);
		FlexGridPane.setMinHeight(secBaseData, 5);
		FlexGridPane.setMediumWidth(secBaseData, 5);
		FlexGridPane.setMediumHeight(secBaseData, 4);
	}

	//-------------------------------------------------------------------
	private void initPortrait() {
		secPortrait = new AppearanceSection();
		FlexGridPane.setMinWidth(secPortrait, 4);
		FlexGridPane.setMediumWidth(secPortrait, 7);
		FlexGridPane.setMinHeight(secPortrait, 7);
		FlexGridPane.setMediumHeight(secPortrait, 4);
	}

	//-------------------------------------------------------------------
	private void initAttributes() {
		secAttribPrimary = new AttributeSection(ResourceI18N.get(RES, "section.attributes.title"));
		secAttribDerived = new DerivedAttributeSection(ResourceI18N.get(RES, "section.derived.title"), ctrl);
		FlexGridPane.setMinWidth(secAttribPrimary, 4);
		FlexGridPane.setMinHeight(secAttribPrimary, 6);
		FlexGridPane.setMinWidth(secAttribDerived, 4);
		FlexGridPane.setMinHeight(secAttribDerived, 4);
		secAttribPrimary.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
		secAttribDerived.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void initPowers() {
		secPowers = new PowerSection(ResourceI18N.get(RES, "section.powers.title"));
		FlexGridPane.setMinWidth(secPowers, 4);
		FlexGridPane.setMinHeight(secPowers, 6);
		FlexGridPane.setMediumWidth(secPowers, 5);
		FlexGridPane.setMediumHeight(secPowers, 7);
		secPowers.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void initResources() {
		secResources = new ResourceSection(ResourceI18N.get(RES, "section.resources.title"));
		FlexGridPane.setMinWidth(secResources, 4);
		FlexGridPane.setMinHeight(secResources, 6);
		FlexGridPane.setMediumWidth(secResources, 5);
		FlexGridPane.setMediumHeight(secResources, 7);
		secResources.showHelpForProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void showDescription(DataItemValue n) {
		if (n==null) {
			layout.setOptional(null);
		} else {
			descBox.setData((ComplexDataItem) n.getModifyable());
			layout.setOptional(descBox);
		}
	}

	//-------------------------------------------------------------------
	private void showDescription(IAttribute n) {
		if (n==null) {
			layout.setOptional(null);
		} else {
			layout.setOptional(new Label(n.getName()));
		}
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
//	 */
//	@Override
//	public void setResponsiveMode(WindowMode value) {
//		super.setResponsiveMode(value);
//		if (value==WindowMode.MINIMAL) {
//			secAttribPrimary.prefWidthProperty().unbind();
//			secAttribPrimary.prefWidthProperty().bind(widthProperty().subtract(20));
//		} else {
//			secAttribPrimary.prefWidthProperty().unbind();
//			secAttribPrimary.setMinWidth(600);
//			requestLayout();
//		}
//	}

	//-------------------------------------------------------------------
	public void refresh() {
		secBaseData.refresh();
		secPortrait.refresh();
		secAttribPrimary.refresh();
		secAttribDerived.refresh();
		secPowers.refresh();
		secResources.refresh();
	}

	//-------------------------------------------------------------------
	public void setController(SpliMoCharacterController ctrl) {
		logger.log(Level.INFO, "setController");
		if (ctrl==null)
			throw new NullPointerException("controller is null");
		this.ctrl = ctrl;
		secBaseData.updateController(ctrl);
		secAttribPrimary.updateController(ctrl);
		secAttribDerived.updateController(ctrl);
		secPortrait.updateController(ctrl);
		secPowers.updateController(ctrl);
		secResources.updateController(ctrl);
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	public SpliMoCharacterController getCharacterController() {
		return ctrl;
	}
}

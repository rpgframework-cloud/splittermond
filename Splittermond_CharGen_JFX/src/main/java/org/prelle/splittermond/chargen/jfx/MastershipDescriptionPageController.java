package org.prelle.splittermond.chargen.jfx;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.SplitterTools;

import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * @author stefa
 *
 */
public class MastershipDescriptionPageController {

	@FXML
    private ResourceBundle resources;
    @FXML
    private URL location;


    @FXML
    private Label descTitle;
    // Only for pages , not for pane
    @FXML
    private Label descSources;
    @FXML
    private Label description;
    @FXML
    private Label requires;

	//-------------------------------------------------------------------
	/**
	 */
	public MastershipDescriptionPageController() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@FXML
    void initialize() {
        assert description != null : "fx:id=\"description\" was not injected: check your FXML file 'SpellDescription.fxml'.";
    }

	//-------------------------------------------------------------------
	public void setData(Mastership data) {
		if (descTitle!=null)
			descTitle.setText(data.getName());
		if (descSources!=null)
			descSources.setText(RPGFrameworkJavaFX.createSourceText(data));

		descTitle.setText(data.getName());
		description.setText(data.getDescription());
		List<String> list = new ArrayList<>();
		for (Requirement req : data.getRequirements()) {
			list.add(SplitterTools.getRequirementString(req, Locale.getDefault()));
		}
		if (list.isEmpty())
			requires.setText("-");
		else
			requires.setText(String.join(", ", list));
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.BackdropWizardPage;
import org.prelle.javafx.Wizard;
import org.prelle.splimo.Background;
import org.prelle.splimo.Education;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SMReferenceTypeConverter;
import org.prelle.splittermond.chargen.jfx.SpliMoCharacterViewLayout;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.DataItemPane;
import de.rpgframework.jfx.RecommendationStateNameConverter;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageEducation extends BackdropWizardPage {

	private final static Logger logger = System.getLogger(WizardPageEducation.class.getPackageName()+".edu");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageEducation.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	private DataItemPane<Education> contentPane;
	private CheckBox cbIgnoreRace;

	private boolean updating;

	//-------------------------------------------------------------------
	public WizardPageEducation(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		if (charGen==null)
			throw new NullPointerException("CharGen is NULL");
		setTitle(ResourceI18N.get(RES, "page.education.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		contentPane = new DataItemPane<Education>(
				r -> SplitterTools.requirementResolver(Locale.getDefault()).apply(r),
				m -> SplitterTools.modificationResolver(Locale.getDefault()).apply(m)
				);
		contentPane.setShowModificationsInDescription(false);
		contentPane.setImageConverter(new Callback<Education,Image>(){
			public Image call(Education value) {
				if (value==null) return null;
				String fullID = (value.getVariantOf()!=null)?value.getVariantOf()+"_"+value.getId():value.getId();
				String safeID = value.getId();
				InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/education/"+fullID+".png");
				if (in==null)
					in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/education/"+safeID+".png");
				if (in!=null)
					return new Image(in);
				in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/education/Dummy.gif");
				if (in!=null)
					return new Image(in);
				logger.log(Level.ERROR, "Missing resource "+SpliMoCharacterViewLayout.class.getName()+" + images/education/"+fullID+".png");
				return null;
			}});
		contentPane.setModificationConverter((m) -> SplitterTools.getModificationString(contentPane.getSelectedItem(),m, Locale.getDefault()));
		contentPane.setUseForChoices(charGen.getModel().getEducation());
		contentPane.setReferenceTypeConverter(new SMReferenceTypeConverter<>());
		triggerNameConverter();
		contentPane.setChoiceConverter((c) -> SplitterTools.getChoiceString(contentPane.getSelectedItem(), c));
		contentPane.setItems(charGen.getEducationController().getAvailable());
		contentPane.setDecisionHandler( (r,c) -> {
			logger.log(Level.WARNING, "ToDo: make decision: "+r);
			SplitterJFXUtil.openDecisionDialog(r, c, charGen.getEducationController());
		});
		contentPane.showDecisionColumnProperty().set(false);

		/* Configuration options */
		cbIgnoreRace = new CheckBox(ResourceI18N.get(RES, "config.ignoreRace"));
		cbIgnoreRace.setSelected(charGen.getCultureController().isAllowUnusualCultures());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(contentPane);

		VBox config = new VBox(10, cbIgnoreRace);
		config.setStyle("-fx-padding: 0 0 1em 0;");
		setBackContent(config);
		setBackHeader(new Label("Filter Kulturen und Gottheiten"));
	}

	//-------------------------------------------------------------------
	/**
	 * This triggers the StringConverter of the ChoiceBox to refresh values
	 */
	private void triggerNameConverter() {
		contentPane.setNameConverter( new RecommendationStateNameConverter<>(charGen.getEducationController()) {
			protected String getBaseName(Education data) {
				if (data==null) return "-";
				if (data.getVariantOf()!=null)
					return "- "+data.getName(Locale.getDefault());
				else
					return data.getName(Locale.getDefault());
			}
		});
	}

	//-------------------------------------------------------------------
	private void updateBackHeader() {
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbIgnoreRace.selectedProperty().addListener( (ov,o,n) -> {
			EducationController ctrl = charGen.getEducationController();
			ctrl.setAllowUnusualEducations(n);
			refresh();
		});

		contentPane.selectedItemProperty().addListener( (ov2,o2,n2) -> {
			logger.log(Level.INFO, "Selection changed");
			if (n2==null) return;
			charGen.getEducationController().select(n2);
		});
	}

	//-------------------------------------------------------------------
	void refresh() {
		if (updating) return;
		updating = true;
		try {
			updateBackHeader();

			EducationController eduCtrl = charGen.getEducationController();
			// Update list of available backgrounds
			contentPane.setItems(eduCtrl.getAvailable());
			// Update recommendations
			contentPane.setNameConverter( new RecommendationStateNameConverter<>(eduCtrl));
			// Update selection
			SpliMoCharacter model = charGen.getModel();
			ComplexDataItemValue<Education> selected = model.getEducation();
			if (selected==null || selected.getResolved()==null) {
				eduCtrl.select(eduCtrl.getAvailable().get(0));
				selected = model.getEducation();
			}
			contentPane.setSelectedItem( (selected!=null)?selected.getResolved():null);
		} finally {
			updating=false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		refresh();
	}

}

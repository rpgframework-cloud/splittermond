package org.prelle.splittermond.chargen.jfx.pane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.EnhancementType;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 *
 */
public class FilterEnhancements extends AFilterInjector<Enhancement> {

	private ChoiceBox<EnhancementType> cbType;
	private List<EnhancementType> allowed;

	//-------------------------------------------------------------------
	public FilterEnhancements() {
		this(EnhancementType.values());
	}

	//-------------------------------------------------------------------
	public FilterEnhancements(EnhancementType...allowed) {
		this.allowed = List.of(allowed);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.IRefreshableList, javafx.scene.layout.Pane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		cbType = new ChoiceBox<EnhancementType>();
		List<EnhancementType> availSchools = new ArrayList<EnhancementType>();
		availSchools.add(null);
		availSchools.addAll(allowed);
		cbType.getItems().addAll(availSchools);
		Collections.sort(cbType.getItems(), new Comparator<EnhancementType>() {
			public int compare(EnhancementType o1, EnhancementType o2) {
				if (o1==null) return -1;
				if (o2==null) return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.setConverter(new StringConverter<EnhancementType>() {
			public String toString(EnhancementType val) {
				if (val==null) return "alle Arten";
				return val.getName();
			}
			public EnhancementType fromString(String string) { return null; }
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		filterPane.getChildren().add(cbType);

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<Enhancement> available) {
		// TODO Auto-generated method stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(java.util.List)
	 */
	@Override
	public List<Enhancement> applyFilter(List<Enhancement> input) {
		// Exist in school
		if (cbType.getValue()!=null) {
			input = input.stream()
					.filter(item -> item.getType()==cbType.getValue())
					.collect(Collectors.toList());
		}
		return input;
	}

}

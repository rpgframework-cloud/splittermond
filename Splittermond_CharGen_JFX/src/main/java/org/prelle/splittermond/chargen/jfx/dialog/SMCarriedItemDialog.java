package org.prelle.splittermond.chargen.jfx.dialog;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.jfx.GenericDescriptionVBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * 
 */
public class SMCarriedItemDialog extends Page {

	public static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SMCarriedItemDialog.class.getPackageName()+".AllPages");

	private final static Logger logger = System.getLogger(SMCarriedItemDialog.class.getPackageName());

	private SpliMoCharacterController control;

	private TextField tfCustomName;
	private TextArea  taCustomDesc;

	private NavigButtonControl btnControl;
	private OptionalNodePane content;
	protected GenericDescriptionVBox description;
	private CarriedItem<ItemTemplate> selectedItem;

	//-------------------------------------------------------------------
	public SMCarriedItemDialog(SpliMoCharacterController ctrl, CarriedItem<ItemTemplate> data) {
		super(ResourceI18N.get(UI,"dialog.editcarrieditem.title"), null);
		this.control = ctrl;
		this.selectedItem    = data;
		btnControl = new NavigButtonControl();

		initCompoments();
		initLayout();
		initInteractivity();
		refresh();

		description.setData(data.getResolved());

	}

	//--------------------------------------------------------------------
	protected void initCompoments() {
		taCustomDesc = new TextArea();
		taCustomDesc.setPromptText(ResourceI18N.get(UI, "dialog.editcarrieditem.custom.description"));
		tfCustomName = new TextField();
		tfCustomName.setPromptText(selectedItem.getResolved().getName());

//		priceAndCount = new ValueInfo(selectedItem, control, getNuyenResolver());

		//view.setSubtitle(selectedItem.getUsedAsSubType().getName());
		description = new GenericDescriptionVBox(null,null, selectedItem.getResolved());
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Layout
		content = new OptionalNodePane(null,description);
		content.setUseScrollPane(false);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		taCustomDesc.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setNotes(null);
			} else {
				selectedItem.setNotes(n);
			}
		});
		tfCustomName.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setCustomName(null);
			} else {
				selectedItem.setCustomName(n);
			}
		});


//		try {
//			view.setOnEditAction(ev -> showImageDialog());
//		} catch (Throwable e) {
//			logger.log(Level.ERROR, "Cannot listen to image change requests: "+e);
//		}

		setOnModeChangeAction(mode -> changedResponsiveMode(mode.getMode()));
	}

	//--------------------------------------------------------------------
	private void changedResponsiveMode(WindowMode value) {
		logger.log(Level.ERROR, "Mode changed {0}",value);
		ResponsiveControlManager.changeModeRecursivly(this, value);
		ResponsiveControlManager.changeModeRecursivly(content, value);
	}

	//--------------------------------------------------------------------
	public void refresh()  {

		logger.log(Level.INFO, "refresh");
//		updateImage();
		description.setData(selectedItem);

		tfCustomName.setText(selectedItem.getCustomName());
		taCustomDesc.setText(selectedItem.getNotes());
	}

}

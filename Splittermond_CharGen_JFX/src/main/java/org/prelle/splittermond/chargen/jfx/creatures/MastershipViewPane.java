package org.prelle.splittermond.chargen.jfx.creatures;

import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.creature.SMLifeform;

import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class MastershipViewPane extends FlowPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;

	private Label lblHeading;

	//-------------------------------------------------------------------
	/**
	 */
	public MastershipViewPane() {
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(UI.getString("label.masterships")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().add(lblHeading);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading);

		for (Iterator<SMSkillValue> it=model.getSkillValues().iterator(); it.hasNext(); ) {
			SMSkillValue val = it.next();
			List<MastershipReference> list = val.getMasterships();
			if (list.isEmpty())
				continue;

			// Print masterships for this skill
			getChildren().add(new Text(val.getSkill().getName()+" ("));
			int lastLevel = 0;
			for (Iterator<MastershipReference> it2=list.iterator(); it2.hasNext(); ) {
				MastershipReference ref = it2.next();
				if (ref.getMastership().getLevel()>lastLevel)
					getChildren().add(new Text(ref.getMastership().getLevel()+": "));
				String text =ref.getMastership().getName();
				if (it2.hasNext())
					text +=",";
				Text textNode = new Text(text);
				getChildren().add(textNode);
			}
			getChildren().add(new Text(")"));
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
		refresh();
		initInteractivity();
	}

}

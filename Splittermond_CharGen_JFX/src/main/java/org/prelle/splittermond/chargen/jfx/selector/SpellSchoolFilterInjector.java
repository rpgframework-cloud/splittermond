package org.prelle.splittermond.chargen.jfx.selector;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.ResourceI18N;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

public class SpellSchoolFilterInjector extends AFilterInjector<Spell> {

	private final static Logger logger = System.getLogger(SpellSchoolFilterInjector.class.getPackageName());

	private ChoiceBox<SMSkill> cbSchool;
	private IRefreshableList page;

	public SpellSchoolFilterInjector(SpliMoCharacter model) {
		cbSchool = new ChoiceBox<>();
		cbSchool.getItems().add(null);
		for (SMSkill school : SplitterMondCore.getSkills(SkillType.MAGIC)) {
			SMSkillValue val = model.getSkillValue(school);
			if (val!=null && val.getModifiedValue()>0) {
				cbSchool.getItems().addAll( school );
			}
		}
//		cbSchool.getItems().addAll(SplitterMondCore.getSkills(SkillType.MAGIC));
		cbSchool.setConverter(new StringConverter<SMSkill>() {

			@Override
			public String toString(SMSkill data) {
				if (data==null) return ResourceI18N.get(SpellSelector.RES,"section.spell.allschools");
				return data.getName(Locale.getDefault());
			}

			@Override
			public SMSkill fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		cbSchool.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
	}

	@Override
	public void updateChoices(List<Spell> available) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		this.page = page;
		filterPane.getChildren().add(cbSchool);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(java.util.List)
	 */
	@Override
	public List<Spell> applyFilter(List<Spell> input) {
		logger.log(Level.INFO, "Start with {0} items", input.size());
		// Match item type
		if (cbSchool.getValue()!=null) {

			input = input.stream()
					.filter(data -> data.getSchools().stream().map(se -> se.getSchool()).toList().contains(cbSchool.getValue()))
//					.filter(data -> data.inDataSets(null))
//					.filter(data -> data.getLanguage()==null || (data.getLanguage().equals(Locale.getDefault().getLanguage())))
					.collect(Collectors.toList());
			logger.log(Level.INFO, "After filter cbSchool={0} remain {1} items", cbSchool.getValue(), input.size());

		}
		return input;
	}

}
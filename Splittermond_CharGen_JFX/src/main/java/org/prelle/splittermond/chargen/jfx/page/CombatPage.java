package org.prelle.splittermond.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.ItemType;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.section.GearSection;
import org.prelle.splittermond.chargen.jfx.section.SkillSection;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.GenericDescriptionVBox;

/**
 * @author prelle
 *
 */
public class CombatPage extends Page {

	private final static Logger logger = System.getLogger(CombatPage	.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkillPage.class.getPackageName()+".Pages");

	private SpliMoCharacterController ctrl;

	private SkillSection secCombat;
	private GearSection secWeapons;
	private FlexGridPane flex;
	private OptionalNodePane layout ;
	private GenericDescriptionVBox descBox;

	//-------------------------------------------------------------------
	public CombatPage() {
		super(ResourceI18N.get(RES, "page.combat.title"));
		// Flow 1
		initCombatSkills();
		initWeapons();
		initInteractivity();

		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secCombat, secWeapons);


		descBox = new GenericDescriptionVBox(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()));
		layout = new OptionalNodePane(flex, null);
		layout.setTitle("Erklärung");
		setContent(layout);
//		setTitle("Basics");
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initCombatSkills() {
		secCombat = new SkillSection(ResourceI18N.get(RES, "section.skills.combat"), SkillType.COMBAT);
		secCombat.setMaxHeight(Double.MAX_VALUE);
		secCombat.getTable().showAttributesProperty().set(false);
		secCombat.getTable().getColumns().remove(secCombat.getTable().getAttribute1Column());
		secCombat.getTable().getColumns().remove(secCombat.getTable().getAttribute2Column());
		FlexGridPane.setMinHeight(secCombat, 5);
		FlexGridPane.setMinWidth(secCombat, 4);
		FlexGridPane.setMediumWidth(secCombat, 10);
		FlexGridPane.setMaxWidth(secCombat, 10);
	}

	//-------------------------------------------------------------------
	private void initWeapons() {
		secWeapons = new GearSection(ResourceI18N.get(RES, "section.weapons"), ItemType.WEAPON_CLOSE, ItemType.WEAPON_RANGED);
		secWeapons.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secWeapons, 4);
		FlexGridPane.setMinHeight(secWeapons, 8);
		FlexGridPane.setMediumWidth(secWeapons, 6);
		FlexGridPane.setMediumHeight(secWeapons, 8);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		secCombat.refresh();
		secWeapons.refresh();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		secCombat.selectedSkillProperty().addListener( (ov,o,n) -> showDescription(n));
	}

	//-------------------------------------------------------------------
	private void showDescription(ComplexDataItemValue n) {
		if (n==null) {
			layout.setOptional(null);
		} else {
			descBox.setData((ComplexDataItem) n.getModifyable());
			layout.setOptional(descBox);
		}
	}

	//-------------------------------------------------------------------
	public void setController(SpliMoCharacterController ctrl) {
		logger.log(Level.INFO, "setController");
		if (ctrl==null)
			throw new NullPointerException("controller is null");
		this.ctrl = ctrl;
//		secBaseData.updateController(ctrl);
//		secAttribPrimary.updateController(ctrl);
//		secAttribDerived.updateController(ctrl);
		secCombat.updateController(ctrl);
		secWeapons.updateController(ctrl);
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	public SpliMoCharacterController getCharacterController() {
		return ctrl;
	}
}

package org.prelle.splittermond.chargen.jfx.creatures;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.creature.ServiceValue;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class ServicesPane extends FlowPane {

	private final static Logger logger = System.getLogger(ServicesPane.class.getPackageName());

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;

	private Label lblHeading;

	//-------------------------------------------------------------------
	/**
	 */
	public ServicesPane() {
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(ResourceI18N.get(UI,"label.services")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().add(lblHeading);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading);

		SummonableCreature summon = (SummonableCreature)model;
		if (summon==null)
			return;

		for (Iterator<ServiceValue> it=summon.getServices().iterator(); it.hasNext(); ) {
			ServiceValue val = it.next();
			String text = val.getNameWithRating();
			if (it.hasNext())
				text +=",";
			Text textNode = new Text(text);
			if (val.isBaseService()) {
				textNode.setStyle("-fx-font-style: italic");
			}
			textNode.setOnMouseClicked(ev -> {
				logger.log(Level.INFO, "Clicked "+val);
				SplitterJFXUtil.showPopupText(getScene().getWindow().getX(), getScene().getWindow().getY(), ev, val.getResolved().getDescription());

			});
			getChildren().add(textNode);
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
//		logger.log(Level.INFO, "setData "+model.dump());
		refresh();
		initInteractivity();
	}

}

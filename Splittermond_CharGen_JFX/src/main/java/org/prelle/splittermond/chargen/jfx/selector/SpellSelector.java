package org.prelle.splittermond.chargen.jfx.selector;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.jfx.listcells.SpellListCell;
import org.prelle.splittermond.chargen.jfx.section.SpellSection;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;

/**
 *
 */
public class SpellSelector extends Selector<Spell, SpellValue> implements ResponsiveControl {

	final static ResourceBundle RES = ResourceBundle.getBundle(SpellSection.class.getPackageName()+".Sections");

	//-------------------------------------------------------------------
	/**
	 * @param ctrl
	 * @param filter
	 */
	public SpellSelector(ComplexDataItemController<Spell, SpellValue> ctrl) {
		super(ctrl,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()),
				new SpellSchoolFilterInjector(ctrl.getModel()));
		listPossible.setCellFactory( (lv) -> new SpellListCell( () -> control, resolver, this));
	}

	//-------------------------------------------------------------------
	/**
	 * @param ctrl
	 * @param baseFilter
	 * @param filter
	 */
	public SpellSelector(ComplexDataItemController<Spell, SpellValue> ctrl, Predicate<Spell> baseFilter,
			AFilterInjector<Spell> filter) {
		super(ctrl, baseFilter,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()),
				filter);
		listPossible.setCellFactory( (lv) -> new SpellListCell( () -> control, resolver, this));
	}

}

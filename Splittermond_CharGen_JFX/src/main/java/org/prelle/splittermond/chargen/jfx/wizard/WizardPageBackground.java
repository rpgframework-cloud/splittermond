package org.prelle.splittermond.chargen.jfx.wizard;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.BackdropWizardPage;
import org.prelle.javafx.Wizard;
import org.prelle.splimo.Background;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SMReferenceTypeConverter;
import org.prelle.splittermond.chargen.jfx.SpliMoCharacterViewLayout;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.DataItemPane;
import de.rpgframework.jfx.RecommendationStateNameConverter;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageBackground extends BackdropWizardPage {

	private final static Logger logger = System.getLogger(WizardPageBackground.class.getPackageName()+".backg");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageBackground.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	private DataItemPane<Background> contentPane;
	private CheckBox cbAllowAllGods;

	private boolean updating;

	//-------------------------------------------------------------------
	public WizardPageBackground(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		if (charGen==null)
			throw new NullPointerException("CharGen is NULL");
		setTitle(ResourceI18N.get(RES, "page.background.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		contentPane = new DataItemPane<Background>(
				r -> SplitterTools.requirementResolver(Locale.getDefault()).apply(r),
				m -> SplitterTools.modificationResolver(Locale.getDefault()).apply(m)
				);
		contentPane.setId("background");
		contentPane.setShowModificationsInDescription(false);
		contentPane.setUseForChoices(charGen.getModel().getBackground());
		contentPane.setImageConverter(new Callback<Background,Image>(){
			public Image call(Background value) {
				if (value==null) return null;
				InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/background/"+value.getId()+".png");
				if (in!=null)
					return new Image(in);
				in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/background/"+value.getId()+".jpg");
				if (in!=null)
					return new Image(in);
				in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/background/Dummy.png");
				if (in!=null)
					return new Image(in);
				logger.log(Level.ERROR, "Missing resource "+SpliMoCharacterViewLayout.class.getName()+" + images/Background/"+value.getId()+".png");
				return null;
			}});
		contentPane.setItems(charGen.getBackgroundController().getAvailable());
		contentPane.setModificationConverter((m) -> SplitterTools.getModificationString(contentPane.getSelectedItem(),m, Locale.getDefault()));
		contentPane.setReferenceTypeConverter(new SMReferenceTypeConverter<>());
		contentPane.setNameConverter( new RecommendationStateNameConverter<>(charGen.getBackgroundController()));
		contentPane.setChoiceConverter((c) -> SplitterTools.getChoiceString(contentPane.getSelectedItem(), c));
		contentPane.setItems(charGen.getBackgroundController().getAvailable());
		contentPane.setDecisionHandler( (r,c) -> {
			logger.log(Level.WARNING, "ToDo: make decision: "+r);
			SplitterJFXUtil.openDecisionDialog(r, c, charGen.getBackgroundController());
		});
		contentPane.showDecisionColumnProperty().set(false);

		/* Configuration options */
		cbAllowAllGods = new CheckBox(ResourceI18N.get(RES, "config.allowAllBackgrounds"));
		cbAllowAllGods.setSelected(charGen.getBackgroundController().isAllowUnusualBackgrounds());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(contentPane);

		VBox config = new VBox(10, cbAllowAllGods);
		config.setStyle("-fx-padding: 0 0 1em 0;");
		setBackContent(config);
		setBackHeader(new Label("Filter Abstammungen"));
	}

	//-------------------------------------------------------------------
	private void updateBackHeader() {
		BackgroundController gen = charGen.getBackgroundController();
		if (gen.isAllowUnusualBackgrounds()) {
			if (gen.isAllowUnusualBackgrounds()) {
				setBackHeader(new Label("Keine Filter"));
			} else {
				setBackHeader(new Label("Nur passende Abstammungen"));
			}
		}
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbAllowAllGods.selectedProperty().addListener( (ov,o,n) -> {
			charGen.getBackgroundController().setAllowUnusualBackgrounds(n);
			refresh();
		});

		contentPane.selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.DEBUG, "Background changed to {0}",n);
			if (n==null) return;
			BackgroundController ctrl = charGen.getBackgroundController();
			ctrl.select(n);
//			refresh();
		});
	}

	//-------------------------------------------------------------------
	void refresh() {
		if (updating) return;
		updating = true;
		try {
			updateBackHeader();

			BackgroundController backCtrl = charGen.getBackgroundController();
			// Update list of available backgrounds
			contentPane.setItems(backCtrl.getAvailable());
			// Update recommendations
			contentPane.setNameConverter( new RecommendationStateNameConverter<>(backCtrl));
			// Update selection
			SpliMoCharacter model = charGen.getModel();
			ComplexDataItemValue<Background> selected = model.getBackground();
			if (selected==null || selected.getResolved()==null) {
				backCtrl.select(backCtrl.getAvailable().get(0));
				selected = model.getBackground();
			}

			contentPane.setSelectedItem( (selected!=null)?selected.getResolved():null);
		} finally {
			updating=false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

}

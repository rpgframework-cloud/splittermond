package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.listcells.SpellValueListCell;
import org.prelle.splittermond.chargen.jfx.selector.SpellSelector;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.jfx.section.ListSection;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SpellSection extends ListSection<SpellValue> {

	private final static Logger logger = System.getLogger(SpellSection.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SpellSection.class.getPackageName()+".Sections");

	private SpliMoCharacterController control;
	private SpliMoCharacter model;

	private ChoiceBox<SMSkill> cbSchool;
	private Label lbToDo;

	//-------------------------------------------------------------------
	public SpellSection(String title) {
		super(title);
		initComponents();
		initLayout();
		initInteractivity();

		list.setCellFactory(lv -> new SpellValueListCell( () -> control));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbToDo = new Label("?");
		lbToDo.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbToDo.setStyle("-fx-text-fill: highlight");

		cbSchool = new ChoiceBox<>();
		cbSchool.getItems().add(null);
		cbSchool.getItems().addAll(SplitterMondCore.getSkills(SkillType.MAGIC));
		cbSchool.setConverter(new StringConverter<SMSkill>() {

			@Override
			public String toString(SMSkill data) {
				if (data==null) return ResourceI18N.get(RES,"section.spell.allschools");
				return data.getName(Locale.getDefault());
			}

			@Override
			public SMSkill fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		});
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		VBox header = new VBox(10, cbSchool, lbToDo);
		setHeaderNode(header);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		cbSchool.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		// TODO Auto-generated method stub
		logger.log(Level.DEBUG, "onAdd");

		SpellSelector selector = new SpellSelector(control.getSpellController());
		NavigButtonControl btnCtrl = new NavigButtonControl();
    	btnCtrl.setDisabled(CloseType.OK, true);
    	selector.setButtonControl(btnCtrl);

    	ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"section.spell.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
    	btnCtrl.initialize(FlexibleApplication.getInstance(), dialog);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			Spell data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected spell: "+data);
//			logger.info("  quality needs choice: "+data.needsChoice()+" for "+data.getSelect());
//			if (data.needsChoice()) {
//				Object choosen = ShadowrunJFXUtils.choose(control, managerProvider, model, data.getSelect(), data.getName());
//				control.getSpellController().select(data, choosen);
//			} else {
				control.getSpellController().select(data);
//			}
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(SpellValue item) {
		logger.log(Level.WARNING, "onDelete");
		if (control.getSpellController(item.getSkill()).deselect(item)) {
			list.getItems().remove(item);
		}
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (SpliMoCharacter) ctrl.getModel();
		refresh();
	}

	//-------------------------------------------------------------------
	private List<ToDoElement> getAllSpellToDos() {
		List<ToDoElement> ret = new ArrayList<>();
		for (SMSkill school : SplitterMondCore.getSkills(SkillType.MAGIC)) {
			SMSkillValue val = model.getSkillValue(school);
			if (val!=null && val.getModifiedValue()>0) {
				ret.addAll( control.getSpellController(school).getToDos() );
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		if (model!=null) {
			SMSkill school = cbSchool.getValue();
			if (school==null) {
				setData(model.getSpells());
				lbToDo.setText(GenericRPGTools.getToDoString(getAllSpellToDos(), Locale.getDefault()));
			} else {
				IPerSkillSpellController ctrl = control.getSpellController(school);
				setData(ctrl.getSelected());
				lbToDo.setText(GenericRPGTools.getToDoString(ctrl.getToDos(), Locale.getDefault()));
			}
		} else
			setData(new ArrayList<>());

		super.refresh();
	}

}

package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.splimo.Power;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.page.SkillPage;
import org.prelle.splittermond.chargen.jfx.selector.PowerSelector;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.section.ListSection;
import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class PowerSection extends ListSection<PowerValue> {

	private final static Logger logger = System.getLogger(PowerSection.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(PowerSection.class.getPackageName()+".Sections");

	private SpliMoCharacterController control;
	private SpliMoCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification,String> modResolver;

	private Label lbToDo;

	//-------------------------------------------------------------------
	public PowerSection(String title) {
		super(title);
		this.requirementResolver = SplitterTools.requirementResolver(Locale.getDefault());
		this.modResolver  = SplitterTools.modificationResolver(Locale.getDefault());
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbToDo = new Label("?");
		lbToDo.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbToDo.setStyle("-fx-text-fill: highlight");

		list.setCellFactory(lv -> new ComplexDataItemValueListCell<Power, PowerValue>( () -> control.getPowerController()));
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		setHeaderNode(lbToDo);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		// TODO Auto-generated method stub
		logger.log(Level.WARNING, "onAdd");
		logger.log(Level.DEBUG, "opening quality selection dialog");

		PowerSelector selector = new PowerSelector(control.getPowerController(), requirementResolver, modResolver);
//		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
//		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"section.power.selector.title"), selector, CloseType.OK, CloseType.CANCEL);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			Power data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected quality: "+data);

			OperationResult<PowerValue> result = null;
			if (data.getChoices().isEmpty()) {
				result = control.getPowerController().select(data);
			} else {
				Decision[] dec = handleChoices(data);
				if (dec!=null) {
					result = control.getPowerController().select(data, dec);
				}
			}
			if (result!=null) {
				if (result.wasSuccessful()) {
					list.getItems().add(result.get());
					list.refresh();
				} else {
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, result.getError());
				}
			}
		}

	}

	//-------------------------------------------------------------------
	protected Decision[] handleChoices(Power data) {
		return new Decision[0];
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(PowerValue item) {
		logger.log(Level.DEBUG, "onDelete");
		if (control.getPowerController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (SpliMoCharacter) ctrl.getModel();
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		logger.log(Level.DEBUG, "refresh");

		PowerController ctrl = control.getPowerController();
		if (model!=null) {
			setData(model.getPowers());
			lbToDo.setText(GenericRPGTools.getToDoString(ctrl.getToDos(), Locale.getDefault()));
//			lbTotal.setText( String.valueOf( ctrl.getMaxPowerPoints() ));
//			lbUnspent.setText( String.valueOf( ctrl.getUnsedPowerPoints() ));
		} else
			setData(new ArrayList<>());
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.CharacterGeneratorRegistry;
import org.prelle.splittermond.chargen.gen.GeneratorWrapper;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl.PageType;
import org.prelle.splittermond.chargen.gen.SplittermondRules;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.DataSetMode;
import de.rpgframework.genericrpg.chargen.RuleInterpretation;
import de.rpgframework.jfx.wizard.WizardPageDatasets;
import de.rpgframework.jfx.wizard.WizardPageGenerator;
import de.rpgframework.jfx.wizard.WizardPageProfiles;
import javafx.util.Callback;

/**
 * @author stefa
 *
 */
public class GenerationWizard extends Wizard implements ControllerListener {

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageRace.class.getPackageName()+".WizardPages");

	private final static Logger logger = System.getLogger(GenerationWizard.class.getPackageName());

	private GeneratorWrapper wrapper;
	private Function<Class<SpliMoCharacterGeneratorImpl>,String[]> nameGetter = gen -> {
		String name = SpliMoCharacterGenerator.RES.getString("chargen."+gen.getSimpleName());
		String desc = SpliMoCharacterGenerator.RES.getString("chargen."+gen.getSimpleName()+".desc");
		return new String[]{name,desc};
	};

	private WizardPageDatasets datasets;
	private WizardPageGenerator<Attribute ,SpliMoCharacter, SpliMoCharacterGeneratorImpl> chargen;
	private WizardPageProfiles profiles;
	private WizardPageRace race;
	private WizardPageCulture culture;
	private WizardPageBackground bground;
	private WizardPageEducation edu;
	private WizardPageAttributes attrib;
	private WizardPageSkills skills;
	private WizardPagePowers powers;
	private WizardPageResources resources;
	private WizardPageMoonSign moonsign;
	private WizardPageToolsTrade toolsTrade;
	private WizardPageEquipment startEquip;

	//-------------------------------------------------------------------
	public GenerationWizard(GeneratorWrapper charGen) {
		super(CloseType.CANCEL, CloseType.PREVIOUS, CloseType.RANDOMIZE, CloseType.NEXT, CloseType.FINISH);
		logger.log(Level.DEBUG, "GenerationWizard<>");
		setTitle("Hello");
		setPlain(false);
		this.wrapper = charGen;
		charGen.runProcessors();

		initPages();
		initInteractivtiy();
		refresh();
	}

	//-------------------------------------------------------------------
	private List<WizardPage> getPageList() {
		List<WizardPage> ret = new ArrayList<>();
		for (PageType type : wrapper.getWrapped().getWizardPages()) {
			switch (type) {
			case ATTRIBUTES : ret.add( attrib); break;
			case BACKGROUND : ret.add(bground); break;
			case CULTURE    : ret.add(culture); break;
			case EDUCATION  : ret.add(    edu); break;
			case RACE       : ret.add(   race); break;
			case RECOMMENDER: ret.add(profiles); break;
			case SKILLS     : ret.add( skills); break;
			case POWERS     : ret.add( powers); break;
			case RESOURCES  : ret.add(resources); break;
			case MOONSIGN   : ret.add(moonsign); break;
			case TOOLS_TRADE: ret.add(toolsTrade); break;
			case START_GEAR : ret.add(startEquip); break;
			default:
				logger.log(Level.ERROR, "Unsupported page type "+type);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private void initPages() {
		datasets = new WizardPageDatasets(this, wrapper, SplitterMondCore.getDataSets(), DataSetMode.ALL, DataSetMode.SELECTED);
		chargen= new WizardPageGenerator(this, wrapper,
				CharacterGeneratorRegistry.getGenerators(),
				SplitterMondCore.getItemList(RuleInterpretation.class),
				SplittermondRules.values(),
				nameGetter);
		race   = new WizardPageRace(this, wrapper);
		culture= new WizardPageCulture(this, wrapper);
		edu    = new WizardPageEducation(this, wrapper);
		bground= new WizardPageBackground(this, wrapper);
		attrib = new WizardPageAttributes(this, wrapper.getAttributeController(), wrapper.getRaceController());
		profiles=new WizardPageProfiles(this, wrapper.getWrapped());
		skills = new WizardPageSkills(this, wrapper);
		powers = new WizardPagePowers(this, wrapper);
		resources= new WizardPageResources(this, wrapper);
		moonsign = new WizardPageMoonSign(this, wrapper);
		toolsTrade= new WizardPageToolsTrade(this, wrapper);
		startEquip= new WizardPageEquipment(this, wrapper);

		getPages().add(chargen);
		getPages().add(datasets);
		getPages().addAll(getPageList());
	}

	//-------------------------------------------------------------------
	private void initInteractivtiy() {
		wrapper.addListener(this);

		canBeFinishedCallback = (wizard) -> wrapper.canBeFinished(); // new Callback<Wizard, Boolean>() {
		setConfirmCancelCallback(new Callback<Wizard, Boolean>() {

			@Override
			public Boolean call(Wizard param) {
				logger.log(Level.WARNING, "ToDo: ask user to confirm cancellation");

				CloseType type = FlexibleApplication.getInstance().showAlertAndCall(AlertType.CONFIRMATION, ResourceI18N.get(RES, "confirm.cancel.title"), ResourceI18N.get(RES, "confirm.cancel.mess"));
				logger.log(Level.WARNING, "User confirmed cancellation: "+type);
				if (type==CloseType.OK || type==CloseType.YES) {
					return Boolean.TRUE;
				}
				return Boolean.FALSE;
			}
		});

		addExtraButton(CloseType.RANDOMIZE, ev -> {
			if (getCurrentPage().getOnExtraActionHandler()!=null) {
				getCurrentPage().getOnExtraActionHandler().accept(CloseType.RANDOMIZE);
			} else {
				logger.log(Level.WARNING, "No handler for RANDOMIZE on page {0}",getCurrentPage());
			}
			});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		logger.log(Level.INFO, "Controller event: "+type);
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			// Remove all pages as event listeners
			for (WizardPage page : getPages()) {
				if (page instanceof ControllerListener) {
					logger.log(Level.INFO, "Remove listening page "+page);
					wrapper.removeListener((ControllerListener) page);
				}
			}

			getPages().retainAll(chargen);
			logger.log(Level.INFO, "Add pages for new generator");
			for (WizardPage page : getPageList()) {
				if (page instanceof ControllerListener) {
					logger.log(Level.INFO, "Add listening page "+page);
					wrapper.addListener((ControllerListener) page);
				}
			}
			getPages().addAll(getPageList());
		}

		if (type==BasicControllerEvents.CHARACTER_CHANGED) {
			for (WizardPage page : getPages()) {
				if (page instanceof ControllerListener && !wrapper.hasListener((ControllerListener) page)) {
					logger.log(Level.WARNING, "Page {0} is a ControllerListener but not registered", page.getClass());
					((ControllerListener)page).handleControllerEvent(type, param);
				}
			}
		}

//		logger.log(Level.DEBUG, "Pages now");
//		for (WizardPage page : getPages()) {
//			logger.log(Level.DEBUG, "- "+page);
//		}
		// Update buttons
		refresh();
	}

}

package org.prelle.splittermond.chargen.jfx;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.CharacterGeneratorRegistry;
import org.prelle.splittermond.chargen.gen.GeneratorWrapper;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.jfx.pages.CharacterViewLayout;
import de.rpgframework.jfx.pages.CharactersOverviewPage;

/**
 * @author prelle
 *
 */
public class SpliMoCharactersOverviewPage extends CharactersOverviewPage {

	private final static Logger logger = System.getLogger(SpliMoCharactersOverviewPage.class.getPackageName());

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharactersOverviewPage#createCharacterGenerator()
	 */
	@Override
	protected CharacterGenerator<?, ?> createCharacterGenerator() {
		logger.log(Level.DEBUG, "ENTER createCharacterGenerator");
		SpliMoCharacter model = new SpliMoCharacter();
		@SuppressWarnings("unchecked")
		Class<SpliMoCharacterGeneratorImpl> clazz = (Class<SpliMoCharacterGeneratorImpl>)CharacterGeneratorRegistry.getGenerators().get(0);
		try {
			SpliMoCharacterGeneratorImpl wrapped = (SpliMoCharacterGeneratorImpl) clazz
					.getConstructor(RuleSpecificCharacterObject.class, CharacterHandle.class)
					.newInstance(model, null);
			GeneratorWrapper ret = new GeneratorWrapper(model, null);
			ret.setWrapped(wrapped);
			return ret;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed creating CharacterGenerator "+clazz,e);
			System.exit(1);
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Depending on the model, create either a Generator or a Leveller
	 */
	protected CharacterController<?, ?> createCharacterController(RuleSpecificCharacterObject<?,?,?,?> rawModel, CharacterHandle handle) {
		logger.log(Level.INFO, "ENTER: create Character Controller");
		SpliMoCharacter model = (SpliMoCharacter) rawModel;
		if (model.isInCareerMode()) {
			logger.log(Level.WARNING, "ToDo: create leveller");
			System.exit(1);
			return null;
		} else {
			logger.log(Level.INFO, "create generator for existing character");
			GeneratorWrapper wrapper = new GeneratorWrapper(model, handle);
			try {
//				String json = model.getChargenSettingsJSON();
//				logger.log(Level.ERROR, "JSON = {0}",json);
//				if (model.getCharGenUsed() == null && json!=null) {
//					if (json.startsWith("{\"modules\"")) {
//						logger.log(Level.ERROR, "Guessing ModuleBasedGenerator as previously used generator");
//						model.setCharGenUsed("prio");
//						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, ResourceI18N.get(RES, "error.no_chargen_in_character")+"\nThere are hints that the priority generator has been used, so I assume this now.");
//					} else if (json.startsWith("{\"characterPoints\"")) {
//						logger.log(Level.ERROR, "Guessing Point Buy as previously used generator");
//						model.setCharGenUsed("pointbuy");
//						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, ResourceI18N.get(RES, "error.no_chargen_in_character")+"\nThere are hints that the point buy generator has been used, so I assume this now.");
//					}
//				}
				if (model.getCharGenUsed() == null) {
					throw new NullPointerException(ResourceI18N.get(RES, "error.no_chargen_in_character"));
				}
				SpliMoCharacterGenerator charGen = CharacterGeneratorRegistry.getGenerator(model.getCharGenUsed(), model,
						handle);
				wrapper.setWrapped(charGen);
				logger.log(Level.INFO, "Return "+wrapper);
				return wrapper;
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error creating generator '" + model.getCharGenUsed(), e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2,
						"Internal error creating character generator instance", e);
				return null;
			}
//			logger.log(Level.INFO, "ToDo: Detect previously used generator: {0}", model.getCharGenUsed());
//			Class<SpliMoCharacterGenerator> clazz = (Class<SpliMoCharacterGenerator>) CharacterGeneratorRegistry.getGenerators().get(0);
//			try {
//				SpliMoCharacterGenerator charGen = (SpliMoCharacterGeneratorImpl) clazz
//						.getConstructor(SpliMoCharacter.class, CharacterHandle.class).newInstance(model, null);
//				wrapper.setWrapped(charGen);
//				logger.log(Level.INFO, "Return "+wrapper);
//				return wrapper;
//			} catch (Exception e) {
//				logger.log(Level.ERROR, "Failed creating CharacterGenerator " + clazz, e);
//				System.exit(1);
//				return null;
//			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharactersOverviewPage#createCharacterAppLayout()
	 */
	@Override
	protected CharacterViewLayout<Attribute,SpliMoCharacter,SpliMoCharacterController> createCharacterAppLayout(CharacterController<?, ?> control) {
		logger.log(Level.DEBUG, "##############createCharacterAppLayout");
		try {
			SpliMoCharacterViewLayout ret = new SpliMoCharacterViewLayout();
			if (control!=null)
				ret.setController((SpliMoCharacterController) control);
			return ret;
		} finally {
			logger.log(Level.DEBUG, "LEAVE: createCharacterViewLayout");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharactersOverviewPage#loadCharacters()
	 */
	@Override
	protected List<CharacterHandle> loadCharacters() {
		try {
			return CharacterProviderLoader.getCharacterProvider().getMyCharacters(RoleplayingSystem.SPLITTERMOND);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed loading characters",e);
		}
		return new ArrayList<CharacterHandle>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharactersOverviewPage#loadRuleSpecific(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	protected RuleSpecificCharacterObject<?,?,?,?> loadRuleSpecific(byte[] raw) throws Exception {
		logger.log(Level.INFO, "ENTER loadRuleSpecific");

		try {
			SpliMoCharacter rawChar = SplitterMondCore.load(raw);
			SplitterTools.resolveChar(rawChar);
			return rawChar;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed parsing XML for char",e);
			StringWriter mess = new StringWriter();
			mess.append("Failed loading character\n\n");
			e.printStackTrace(new PrintWriter(mess));
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, mess.toString());
			return null;
		} finally {
			logger.log(Level.INFO, "LEAVE loadRuleSpecific");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharactersOverviewPage#importCharacter(byte[], java.lang.String)
	 */
	@Override
	protected void importCharacter(byte[] raw, String filename) throws Exception {
		logger.log(Level.WARNING, "Import {0} bytes from {1}",raw.length, filename);

		// Parse
		SpliMoCharacter model = (SpliMoCharacter) loadRuleSpecific(raw);
		SplitterTools.resolveChar(model);
		String name = model.getName();
		CharacterHandle handle = CharacterProviderLoader.getCharacterProvider().createCharacter(name, RoleplayingSystem.SPLITTERMOND);
		CharacterProviderLoader.getCharacterProvider().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, name, raw);
	}

}

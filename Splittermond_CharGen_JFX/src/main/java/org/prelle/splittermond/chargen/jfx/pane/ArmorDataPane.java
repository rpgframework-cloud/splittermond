package org.prelle.splittermond.chargen.jfx.pane;

import java.util.ArrayList;
import java.util.List;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.items.Feature;
import org.prelle.splimo.items.FeatureList;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ValueRequirement;
import de.rpgframework.jfx.NumericalValueField;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

/**
 *
 */
public class ArmorDataPane extends GridPane {

	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvVTD;
	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvSR;
	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvHandi;
	private NumericalValueField<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> nvSpeed;
	private Label lbRequires;
	private FlowPane lbFeatures;

	private CarriedItem<ItemTemplate> item;
	private ICarriedItemController ctrl;

	//-------------------------------------------------------------------
	public ArmorDataPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	public ArmorDataPane(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		this.item = item;
		this.ctrl = ctrl;
		initComponents();
		initLayout();
		setData(item, ctrl);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		nvVTD = new NumericalValueField<>( ()-> item.getAsValue(SMItemAttribute.DEFENSE));
		nvVTD.setController(ctrl.getValueController(SMItemAttribute.DEFENSE));
		nvSR  = new NumericalValueField<>( ()-> item.getAsValue(SMItemAttribute.DAMAGE_REDUCTION) );
		nvHandi = new NumericalValueField<>(()-> item.getAsValue(SMItemAttribute.HANDICAP));
		nvSpeed = new NumericalValueField<>(()-> item.getAsValue(SMItemAttribute.SPEED));
		lbRequires = new Label();
		lbFeatures = new FlowPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbVTD = new Label("VTD+"); lbVTD.setMaxWidth(Double.MAX_VALUE);
		Label lbSR = new Label("SR"); lbSR.setMaxWidth(Double.MAX_VALUE);
		Label lbHandi = new Label("Behind."); lbHandi.setMaxWidth(Double.MAX_VALUE);
		Label lbSpeed = new Label("Tick- zuschlag"); lbSpeed.setMaxWidth(Double.MAX_VALUE);
		Label hdRequires = new Label("Mindest-STÄ"); hdRequires.setMaxWidth(Double.MAX_VALUE);
		Label hdFeatures = new Label("Merkmale"); hdFeatures.setMaxWidth(Double.MAX_VALUE);
		lbVTD .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		lbSR.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		lbHandi.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		lbSpeed.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdRequires.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");
		hdFeatures .getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, "table-head");

		this.add(lbVTD, 0, 0);
		this.add(nvVTD, 0, 1);
		this.add(lbSR , 1, 0);
		this.add(nvSR , 1, 1);
		this.add(lbHandi , 2, 0);
		this.add(nvHandi , 2, 1);
		this.add(lbSpeed , 3, 0);
		this.add(nvSpeed , 3, 1);
		this.add(hdRequires, 4, 0);
		this.add(lbRequires, 4, 1);
		this.add(hdFeatures, 5, 0);
		this.add(lbFeatures, 5, 1);

		GridPane.setMargin(lbVTD, new Insets(5));
		GridPane.setMargin(lbSR, new Insets(5));
		GridPane.setMargin(lbHandi, new Insets(5));
		GridPane.setMargin(hdRequires, new Insets(5));
		GridPane.setMargin(hdFeatures, new Insets(5));
	}

	//-------------------------------------------------------------------
	public void setData(CarriedItem<ItemTemplate> item, ICarriedItemController ctrl) {
		this.item = item;
		this.ctrl = ctrl;

		if (ctrl!=null) nvVTD.setController(ctrl.getValueController(SMItemAttribute.DEFENSE));
		if (ctrl!=null) nvSR.setController(ctrl.getValueController(SMItemAttribute.DAMAGE_REDUCTION));
		if (ctrl!=null) nvHandi.setController(ctrl.getValueController(SMItemAttribute.HANDICAP));
		if (ctrl!=null) nvSpeed.setController(ctrl.getValueController(SMItemAttribute.SPEED));
		List<String> toMerge = new ArrayList<>();
		for (Requirement tmp : item.getRequirements()) {
			if (tmp instanceof ValueRequirement) {
				ValueRequirement req = (ValueRequirement)tmp;
				Attribute attr = Attribute.valueOf( req.getKey() );
				toMerge.add(attr.getShortName()+" "+req.getValue());
			}
		}
		lbRequires.setText( String.join(", ", toMerge));
		// Features
		lbFeatures.getChildren().clear();
		System.err.println("ArmorDataPane: "+ item.getAsObject(SMItemAttribute.FEATURES).getModifiedValue().getClass());
		FeatureList features = item.getAsObject(SMItemAttribute.FEATURES).getModifiedValue();
		for (Feature feat : features) {
			lbFeatures.getChildren().add(new Label(feat.getName()));
		}

		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		nvVTD.refresh();
		nvSR.refresh();
		nvHandi.refresh();
		nvSpeed.refresh();
		requestLayout();
	}

}

package org.prelle.splittermond.chargen.jfx.creatures;

import java.lang.System.Logger.Level;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.creature.SMLifeform;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class SkillViewPane extends FlowPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;

	private BooleanProperty editable = new SimpleBooleanProperty();
	private Label lblHeading;
	private Button btnEdit;
	private boolean ignoreAttributes;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillViewPane(boolean editable, boolean ignoreAttributes) {
		this.editable.set(editable);
		this.ignoreAttributes = ignoreAttributes;
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(UI.getString("label.skills")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		btnEdit = new Button(null, new SymbolIcon("edit"));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().addAll(lblHeading, btnEdit);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.visibleProperty().bind(editable);
		btnEdit.managedProperty().bind(editable);
		btnEdit.setOnAction(ev -> {
			SkillEditDialog dia = new SkillEditDialog(model);
			CloseType closed = FlexibleApplication.getInstance().showAndWait(dia);
			refresh();

		});
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading, btnEdit);

		for (Iterator<SMSkillValue> it=model.getSkillValues().iterator(); it.hasNext(); ) {
			SMSkillValue val = it.next();
			int attrVal = 0;
			if (!ignoreAttributes) {
				if (val.getSkill().getType()!=SkillType.COMBAT) {
					attrVal = model.getAttribute(val.getSkill().getAttribute()).getModifiedValue() + model.getAttribute(val.getSkill().getAttribute2()).getModifiedValue();
				}
			}
			int finVal = attrVal + val.getModifiedValue();
			if (finVal==0)
				continue;
			if (val.getSkill()!=null) {
				String text = val.getSkill().getName()+" "+finVal;
				if (it.hasNext())
					text +=",";
				Text textNode = new Text(text);
				getChildren().add(textNode);
			} else {
				System.getLogger("splittermond.jfx").log(Level.WARNING, "SkillValue without skill in lifeform "+model);
			}
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
//		logger.info("setData "+model.dump());
		refresh();
		initInteractivity();
	}

}

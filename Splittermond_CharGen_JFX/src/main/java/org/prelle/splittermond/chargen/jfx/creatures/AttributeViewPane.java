package org.prelle.splittermond.chargen.jfx.creatures;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.creature.SMLifeform;

import de.rpgframework.genericrpg.data.AttributeValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributeViewPane extends GridPane {

	private SMLifeform model;
	
	private Map<Attribute,Control> tfInput;

	//-------------------------------------------------------------------
	/**
	 */
	public AttributeViewPane(boolean editable) {
		initComponents(editable);
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents(boolean editable) {
		tfInput = new HashMap<>();
		for (Attribute attr : Attribute.values()) {
			if (editable) {
				TextField tf = new TextField();
				tf.setPrefColumnCount(2);
				tf.setStyle("-fx-max-width: 2.5em");
				tf.setUserData(attr);
				tfInput.put(attr, tf);
				
				tf.textProperty().addListener( (ov,o,n) -> {
					if (model!=null && n!=null && n.length()>0) {
						try {
							model.getAttribute(attr).setDistributed(Integer.parseInt(n));
						} catch (NumberFormatException e) {
						}
					}
				});
			} else {
				Label tf = new Label();
				tf.setStyle("-fx-min-width: 2.5em");
				tf.setUserData(attr);
				tfInput.put(attr, tf);
			}
		}
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		
		// Primary attributes
		int index = 0;
		for (Attribute key : Attribute.primaryValues()) {
			Label lbl = new Label(key.getShortName(Locale.getDefault()));
			lbl.setAlignment(Pos.CENTER);
			lbl.getStyleClass().add("table-head");
			lbl.setMaxWidth(Double.MAX_VALUE);
			GridPane.setMargin(lbl, new Insets(1,0,5,0));
			GridPane.setHgrow(lbl, Priority.SOMETIMES);
			GridPane.setHalignment(lbl, HPos.CENTER);
			
			Control tf = tfInput.get(key);
			if (tf instanceof Label) {
				((Label)tf).setAlignment(Pos.CENTER);
				GridPane.setMargin(tf, new Insets(1,2,5,2));
			}
			GridPane.setHgrow(tf, Priority.SOMETIMES);
			GridPane.setHalignment(tf, HPos.CENTER);
//			logger.debug("Add "+tf+" to "+index+",1  "+key);
			this.add(lbl, index, 0);
			this.add(tf , index, 1);
			index++;
		}
		
		// Secondary attributes
		index = 0;
		Attribute[] order = new Attribute[]{Attribute.SIZE,Attribute.SPEED,Attribute.INITIATIVE,Attribute.LIFE,Attribute.FOCUS,Attribute.RESIST_DAMAGE,Attribute.DAMAGE_REDUCTION,Attribute.RESIST_BODY,Attribute.RESIST_MIND};
		for (Attribute key : order) {
			if (key==Attribute.INITIATIVE)
				continue;
			
			Label lbl = new Label(key.getShortName(Locale.getDefault()));
			lbl.setAlignment(Pos.CENTER);
			lbl.getStyleClass().add("table-head");
			lbl.setMaxWidth(Double.MAX_VALUE);
			GridPane.setHgrow(lbl, Priority.SOMETIMES);
			GridPane.setHalignment(lbl, HPos.CENTER);
			
			Control tf = tfInput.get(key);
			if (tf instanceof Label) {
				((Label)tf).setAlignment(Pos.CENTER);
			}
			GridPane.setHalignment(tf, HPos.CENTER);
//			logger.debug("Add "+tf+" to "+index+",3  "+key);
			this.add(lbl, index, 2);
			this.add(tf , index, 3);
			index++;
		}
		
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	void refresh() {
		for (Attribute key: Attribute.values()) {
			Control tf = tfInput.get(key);
			if (tf==null) {
				continue;
			}
			if (key==Attribute.SPLINTER)
				continue;
			try {
				AttributeValue val = model.getAttribute(key);
				if (val==null) {
					continue;
				}
				if (tf instanceof Label) {
					((Label)tf).setText(String.valueOf(val.getModifiedValue()));
				} else {
					((TextField)tf).setText(String.valueOf(val.getModifiedValue()));
				}
			} catch (NullPointerException e) {
			}
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
		refresh();
	}

}

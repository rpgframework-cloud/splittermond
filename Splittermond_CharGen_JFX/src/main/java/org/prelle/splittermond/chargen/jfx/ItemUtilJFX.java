package org.prelle.splittermond.chargen.jfx;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.Availability;
import org.prelle.splimo.items.Complexity;
import org.prelle.splimo.items.Feature;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.items.SMPieceOfGearVariant;
import org.prelle.splimo.persist.MoneyConverter;
import org.prelle.splimo.persist.WeaponDamageConverter;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;
import de.rpgframework.genericrpg.items.ItemAttributeFloatValue;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ItemUtilJFX {

	private final static Logger logger = System.getLogger("de.rpgframework.shadowrun6.jfx");

//	private static AttackRatingArrayConverter arConv = new AttackRatingArrayConverter();
//	private static FireModesConverter firemodeConv = new FireModesConverter();
//	private static AmmunitionConverter ammoConv = new AmmunitionConverter();
	private static WeaponDamageConverter damageConv = new WeaponDamageConverter();

	private final static List<SMItemAttribute> COLUMN_ORDER = List.of(
			SMItemAttribute.AVAILABILITY,
			SMItemAttribute.PRICE,
			SMItemAttribute.LOAD,
			SMItemAttribute.RIGIDITY,
			SMItemAttribute.COMPLEXITY,
			SMItemAttribute.DAMAGE,
			SMItemAttribute.SPEED,
			SMItemAttribute.ATTRIBUTE1,
			SMItemAttribute.DEFENSE,
			SMItemAttribute.DAMAGE_REDUCTION,
			SMItemAttribute.HANDICAP,
			SMItemAttribute.FEATURES

			);

	//-------------------------------------------------------------------
	private static void addColumn(GridPane table, CarriedItem<ItemTemplate> carried, SMItemAttribute attrib, int width) {
		ItemAttributeValue<?> raw = carried.getAttributeRaw(attrib);
		if (raw==null)
			return;

		int x = table.getColumnCount();
		Label header  = new Label(attrib.getShortName(Locale.getDefault()));
		header.getStyleClass().add("table-head");
		header.setMaxWidth(Double.MAX_VALUE);
		header.setAlignment(Pos.CENTER);

		table.getColumnConstraints().add(new ColumnConstraints(width));
		table.add(header, x, 0);

		Label value= getItemAttributeLabel(carried, attrib);
		table.add(value, x, 1);
		GridPane.setHalignment(value, HPos.CENTER);
	}

	//-------------------------------------------------------------------
	private static void addColumn(GridPane table, String title, String value, int width) {
		int x = table.getColumnCount();
		Label header  = new Label(title);
		header.getStyleClass().add("table-head");
		header.setMaxWidth(Double.MAX_VALUE);
		header.setAlignment(Pos.CENTER);

		table.getColumnConstraints().add(new ColumnConstraints(width));
		table.add(header, x, 0);

		Label value2= new Label(value);
		table.add(value2, x, 1);
		GridPane.setHalignment(value2, HPos.CENTER);
	}

	//-------------------------------------------------------------------
	/**
	 * @param detailed Include augmentation, price and availability
	 */
	public static Node getItemInfoNode(CarriedItem<ItemTemplate> item, SpliMoCharacterController ctrl, boolean detailed) {
		logger.log(Level.DEBUG, "create InfoNode for "+item);
		SpliMoCharacter model = null; // ctrl.getModel();
		CarryMode carry = item.getCarryMode();

		GridPane table = new GridPane();

		// Weapons
//		addColumn(table, item, SMItemAttribute.ATTACK_RATING, 90);
		addColumn(table, item, SMItemAttribute.DAMAGE, 50);
//		addColumn(table, item, SMItemAttribute.FIREMODES, 100);
//		addColumn(table, item, SMItemAttribute.AMMUNITION, 50);
		// Armor
		addColumn(table, item, SMItemAttribute.DEFENSE, 40);
		addColumn(table, item, SMItemAttribute.DAMAGE_REDUCTION, 50);
		addColumn(table, item, SMItemAttribute.SPEED, 50);
		// Generic
		if (detailed) {
			// Augmentations
			addColumn(table, item, SMItemAttribute.AVAILABILITY , 40);
			addColumn(table, item, SMItemAttribute.PRICE , 60);
		}


		VBox box = new VBox(10);
		box.setStyle("-fx-spacing:0.5em; ");
		box.setMaxWidth(Double.MAX_VALUE);

		if (!table.getChildren().isEmpty())
			box.getChildren().add(table);
		box.getChildren().add(getAccessoryInfoNode(item, ctrl, detailed));

		return box;
	}

	//-------------------------------------------------------------------
	/**
	 * @param detailed Include augmentation, price and availability
	 */
	public static Node getAccessoryInfoNode(CarriedItem<ItemTemplate> item, SpliMoCharacterController ctrl, boolean detailed) {
		logger.log(Level.DEBUG, "create InfoNode for "+item);

		VBox box = new VBox(10);
		box.setStyle("-fx-spacing:0.5em; ");
		box.setMaxWidth(Double.MAX_VALUE);

		/*
		 * Accessories
		 */
		//		logger.warn(item.dump());
		//		logger.debug("Accessories of "+item+" are "+item.getAccessories());
		if (!item.getEffectiveAccessories().isEmpty()) {
			List<String> accessNames = new ArrayList<String>();
			item.getEffectiveAccessories().forEach(sub -> accessNames.add(sub.getNameWithRating()));
			//			lblAccessories.setText(String.join(", ", item.getAccessories()));
			Label heaModif = new Label(SplitterMondCore.getI18nResources().getString("label.accessories", Locale.getDefault())+": ");
			heaModif.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

			FlowPane flow = new FlowPane();
			flow.getChildren().add(heaModif);
			Iterator<String> it = accessNames.iterator();
			while (it.hasNext()) {
				Label lbl = new Label(it.next());
				if (it.hasNext())
					lbl.setText(lbl.getText()+",  ");
				flow.getChildren().add(lbl);
			}
			box.getChildren().add(flow);
			VBox.setMargin(flow, new Insets(5, 0, 5, 0));
		}

		/*
		 * Modifications
		 */
//		List<Modification> mods = item.getCharacterModifications();
//		if (!mods.isEmpty()) {
//			box.getChildren().add(getModificationsNode(item));
//		}

		return box;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("incomplete-switch")
	public static VBox getItemInfoNode(ItemTemplate item, SpliMoCharacter model, CarryMode carry) {
		VBox box = new VBox(10);
		box.setMaxWidth(Double.MAX_VALUE);
		box.setFillWidth(true);

		int nextCol = 0;

		GridPane table = new GridPane();
		table.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(table, Priority.ALWAYS);

		// Add columns
		for (SMItemAttribute attrib : COLUMN_ORDER) {
			ItemAttributeDefinition def =  item.getAttribute(attrib);
			if (def==null)
				continue;

			// Header
			Label head  = new Label(attrib.getShortName(Locale.getDefault()));
			head.getStyleClass().add("table-head");
			head.setAlignment(Pos.CENTER);
			head.setMinWidth(50);
			head .setMaxWidth(Double.MAX_VALUE);
			table.add(head, nextCol  , 0);

			// Value
			Label value = new Label();
			if (def !=null) {
				value.setText( translateVariables( def.getRawValue(), def.getLookupTable() ));
			}
			value.setAlignment(Pos.CENTER);
			value .setMaxWidth(Double.MAX_VALUE);
			GridPane.setMargin(value, new Insets(0, 5, 0, 5));
			table.add(value, nextCol, 1);

			// Finetuning
			switch (attrib) {
			case ATTRIBUTE1:
				head.setText("Attribute");
				value.setText( ((Attribute)def.getValue()).getShortName(Locale.getDefault())+"+"+ ((Attribute)item.getAttribute(SMItemAttribute.ATTRIBUTE2).getValue()).getShortName(Locale.getDefault()));
				break;
			case AVAILABILITY:
				head.setMinWidth(80);
				value.setText( ((Availability)def.getValue()).getName() );
				break;
			case COMPLEXITY:
				value.setText( ((Complexity)def.getValue()).getID() );
				break;
			case DAMAGE:
				head.setMinWidth(80);
				value.setText( WeaponDamageConverter.toString( (int)def.getValue()) );
				break;
			case FEATURES:
				head.setMinWidth(120);
				List<Feature> feats = def.getValue();
				List<String> names =feats.stream().map(feat -> (String)feat.getName(Locale.getDefault())).collect(Collectors.toList());
				value.setText( String.join(", ", names));
				break;
			case PRICE:
				value.setText( MoneyConverter.toString( (int)def.getValue()) );
				break;
			}

			nextCol++;
		}


		box.getChildren().add(table);

		return box;
	}

	//-------------------------------------------------------------------
	private static String translateVariables(String raw, String[] table) {
		String rtg = SplitterMondCore.getI18nResources().getString("label.rating.short");
		if (raw.equals("$RATING") && table!=null)
			return table[0]+"+";
		if (raw.indexOf("$RATING")>-1)
			raw = raw.replace("$RATING", rtg);
		return raw;
	}

	//-------------------------------------------------------------------
	private static String makeAttributeValueString(ItemTemplate data, SMItemAttribute attrib) {
		ItemAttributeDefinition def = data.getAttribute(attrib);

		String raw = data.getAttribute(attrib).getRawValue();
		String[] table = data.getAttribute(attrib).getLookupTable();
		if (data.requiresVariant()) {
			int min = Integer.MAX_VALUE;
			for (SMPieceOfGearVariant variant : data.getVariants()) {
				if (variant.getAttribute(attrib)!=null) {
					int t = variant.getAttribute(attrib).getDistributed();
					min = Math.min(min, t);
				}
			}
			return String.valueOf(min)+"+";
		}
		String rtg = SplitterMondCore.getI18nResources().getString("label.rating.short");
		if (raw.equals("$RATING") && table!=null)
			return table[0]+"+";
		if (raw.indexOf("$RATING")>-1)
			raw = raw.replace("$RATING", rtg);
		return raw;
	}

	//-------------------------------------------------------------------
	private static Label getItemAttributeLabel(CarriedItem<ItemTemplate> item, SMItemAttribute attr) {
		Label ret = new Label("?");
		if (!item.hasAttribute(attr))
			return ret;

		Object obj = null;
		switch (attr) {
		case AVAILABILITY:
			obj = item.getAsObject(attr).getModifiedValue();
			ret.setText(String.valueOf( (Availability)obj));
			break;
		case PRICE:
			int ny = item.getAsValue(attr).getModifiedValue();
			ret.setText( ny+" \u00A5");
			break;
		case DAMAGE:
			obj = item.getAsObject(attr).getModifiedValue();
			ret.setText(obj.toString());
			break;
		case SPEED:
		case DAMAGE_REDUCTION:
			if (item.getAsValue(attr)==null) {
				ret.setText("-");
			} else {
				ret.setText(String.valueOf(item.getAsValue(attr).getModifiedValue()));
			}
			break;
//		case DEFENSE_PHYSICAL:
//		case DEFENSE_SOCIAL:
//		case DEFENSE_MATRIX:
//			if (item.getAsValue(attr)==null) {
//				ret.setText("-");
//			} else {
//				int v = item.getAsValue(attr).getModifiedValue();
//				if (v<0)
//					ret.setText(String.valueOf(v));
//				else
//					ret.setText("+"+String.valueOf(v));
//			}
//			break;
//
		default:
			logger.log(Level.ERROR, "Don't know how to handle "+attr);
		}

//		if (obj!=null) {
//			if (!item.getAsObject(attr).getModifications().isEmpty()) {
//				ret.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
//				Tooltip tooltip = new Tooltip();
//				ret.setTooltip(tooltip);
//				switch (attr) {
//				case ATTACK_RATING:
//					tooltip.setText("?");
//					break;
//				default:
//					logger.log(Level.WARNING, "Don't know how to make tooltips for "+attr);
//				}
//			}
//		} else {
//			switch (attr) {
//			case ESSENCECOST:
//				if (item.getAsFloat(attr)!=null && !item.getAsFloat(attr).getModifications().isEmpty()) {
//					logger.log(Level.WARNING, "Don't know how to make tooltips for "+attr);
//					ret.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
//					Tooltip tooltip = new Tooltip();
//					ret.setTooltip(tooltip);
//				}
//				break;
//			default:
//				if (item.getAsValue(attr)!=null && !item.getAsValue(attr).getModifications().isEmpty()) {
//					logger.log(Level.WARNING, "Don't know how to make tooltips for "+attr+" / "+item.getAsValue(attr).getModifications());
//					ret.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
//					Tooltip tooltip = new Tooltip();
//					ret.setTooltip(tooltip);
//				}
//			}
//		}

		return ret;
	}

	//-------------------------------------------------------------------
	private static Label getItemAttributeLabel(ItemTemplate item, SMItemAttribute attr) {
		Label ret = new Label("?");
		if (item!=null) {
			Object obj = item.getAttribute(attr).getValue();
			ret.setText(String.valueOf(obj));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private static void addWeaponColumns(CarriedItem<ItemTemplate> carried, SpliMoCharacter model, GridPane table) {
		if (carried==null)
			throw new NullPointerException("Empty item");

//		addColumn(table, carried, SMItemAttribute.ATTACK_RATING, 90);
		addColumn(table, carried, SMItemAttribute.DAMAGE, 50);
//		addColumn(table, carried, SMItemAttribute.FIREMODES, 100);
//		addColumn(table, carried, SMItemAttribute.AMMUNITION, 50);
	}

	//-------------------------------------------------------------------
	private static Label createLabelWithValue(ItemTemplate item, CarryMode carry, AGearData data, SMItemAttribute attrib) {
		Label lbAvail = new Label();
		lbAvail.setAlignment(Pos.CENTER);
		lbAvail .setMaxWidth(Double.MAX_VALUE);
		GridPane.setMargin(lbAvail, new Insets(0, 5, 0, 5));

		ItemAttributeDefinition def = data.getAttribute(attrib);
		if (def==null)
			def = item.getAttribute(attrib);
		if (attrib == SMItemAttribute.DAMAGE) {
			if (def!=null) {
					try {
						lbAvail.setText(damageConv.write( (int)def.getValue()));
					} catch (Exception e) {
						logger.log(Level.ERROR, e);
					}
			}
		} else if (attrib == SMItemAttribute.PRICE) {
			List<String> buf = new ArrayList<>();
			int num = def.getValue();
			if (num%100 >0)
				buf.add( (num%100)+"T");
			num /= 100;
			if (num%100 >0)
				buf.add( (num%100)+"L");
			num /= 100;
			if (num>0)
				buf.add( num+"S");
			lbAvail.setText(String.join(" ", buf));
		} else {
			if (def!=null)
				lbAvail.setText(translateVariables(def.getRawValue(), def.getLookupTable()));
		}
		return lbAvail;
	}

	//-------------------------------------------------------------------
	private static Label createLabelWithValue(CarriedItem<ItemTemplate> item, SMItemAttribute attrib) {
		Label lbAvail = new Label();
		lbAvail.setAlignment(Pos.CENTER);
		lbAvail .setMaxWidth(Double.MAX_VALUE);
		GridPane.setMargin(lbAvail, new Insets(0, 5, 0, 5));

		ItemAttributeValue val = item.getAttributeRaw(attrib);
		if (val==null)
			return lbAvail;
		logger.log(Level.WARNING, "Def1 {0}", val);

		StringBuffer buf = new StringBuffer();
		if (val instanceof ItemAttributeFloatValue) {
			ItemAttributeFloatValue fVal = (ItemAttributeFloatValue)val;
			buf.append( String.format("%2.2f", fVal.getDistributed()) );
			if (fVal.getFloatModifier()!=0f) {
				buf.append(" ("+fVal.getModifiedValue()+")");
			}
		} else if (val instanceof ItemAttributeNumericalValue) {
			ItemAttributeNumericalValue fVal = (ItemAttributeNumericalValue)val;
			buf.append( String.format("%2d", fVal.getDistributed()) );
			if (fVal.getModifier()!=0) {
				buf.append(" ("+fVal.getModifiedValue()+")");
			}
		} else {
			logger.log(Level.WARNING, "ToDo: Deal with "+val.getClass());
		}
		lbAvail.setText(buf.toString());
		return lbAvail;
	}


}

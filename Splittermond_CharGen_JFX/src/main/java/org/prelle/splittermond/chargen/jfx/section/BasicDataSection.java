package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Section;
import org.prelle.javafx.TitledComponent;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Deity;
import org.prelle.splimo.Education;
import org.prelle.splimo.Race;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.ResourceI18N;
import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class BasicDataSection extends Section {

	private final static ResourceBundle RES = ResourceBundle.getBundle(BasicDataSection.class.getName());

	private SpliMoCharacterController control;

	private TextField tfName;
	private ChoiceBox<Race> cbRace;
	private ChoiceBox<Gender> cbGender;
	private TextField tfBackground;
	private Button btnBackground;
	private TextField tfCulture;
	private Button btnCulture;
	private TextField tfProfession;
	private Button btnProfession;
	private ChoiceBox<Deity> cbDeity;

	public BasicDataSection() {
		super(ResourceI18N.get(RES, "section.basics.title"), null);

		initComponents();
//		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
//			toMinimal();
//		} else {
			toNonMinimal();
//		}
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField("Lenkan Todsucher");
		cbGender     = new ChoiceBox<Gender>();
		cbGender.setConverter(new StringConverter<Gender>() {
			public String toString(Gender v) { return (v!=null)?v.getName(Locale.getDefault()):"?"; }
			public Gender fromString(String string) {return null;}
			});
		cbGender.getItems().addAll(Gender.values());
		cbRace = new ChoiceBox<>(FXCollections.observableArrayList(SplitterMondCore.getItemList(Race.class)));
		cbRace.setConverter(new StringConverter<Race>() {
			public String toString(Race value) {
				if (value==null)
					return "-";
				return value.getName(Locale.getDefault());
			}
			public Race fromString(String string) {return null;}
		});
		tfBackground = new TextField();
		tfCulture    = new TextField();
		tfProfession = new TextField();
		cbDeity = new ChoiceBox<>(FXCollections.observableArrayList(SplitterMondCore.getItemList(Deity.class)));
		cbDeity.setConverter(new StringConverter<Deity>() {
			public String toString(Deity value) {
				if (value==null)
					return "-";
				return value.getDescription(Locale.getDefault());
			}
			public Deity fromString(String string) {return null;}
		});

		btnBackground = new Button(" ");
		btnCulture    = new Button(" ");
		btnProfession = new Button(" ");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {control.getModel().setName(n); control.runProcessors();});
		cbGender.getSelectionModel().selectedItemProperty().addListener((ov,o,n) -> control.getModel().setGender(n));
	}

	//-------------------------------------------------------------------
	private void toMinimal() {
		FlowPane layout = new FlowPane(
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.name"),  tfName),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.species"), cbRace),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.gender"), cbGender).setTitleMinWidth(120d),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.background") , new HBox(5,tfBackground, btnBackground)),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.culture")    , new HBox(5,tfCulture, btnCulture)),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.profession"), new HBox(5,tfProfession, btnProfession)),
				new TitledComponent(ResourceI18N.get(RES, "section.basics.label.deity"), cbDeity)
				);
		layout.setPrefWrapLength(300);
		layout.setVgap(5);
		layout.setHgap(10);
		setContent(layout);
		super.requestLayout();
	}

	//-------------------------------------------------------------------
	private void toNonMinimal() {
		GridPane input = new GridPane();
		input.setVgap(5);
		input.setHgap(5);
		input.getColumnConstraints().add(new ColumnConstraints(80,100,200));

		Label lbName       = new Label(ResourceI18N.get(RES, "section.basics.label.name"));
		Label lbSpecies    = new Label(ResourceI18N.get(RES, "section.basics.label.species"));
		Label lbGender     = new Label(ResourceI18N.get(RES, "section.basics.label.gender"));
		Label lbBackground = new Label(ResourceI18N.get(RES, "section.basics.label.background"));
		Label lbCulture    = new Label(ResourceI18N.get(RES, "section.basics.label.culture"));
		Label lbProfession = new Label(ResourceI18N.get(RES, "section.basics.label.profession"));
		Label lbDeity      = new Label(ResourceI18N.get(RES, "section.basics.label.deity"));
		lbName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbSpecies.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbGender.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbBackground.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbCulture.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbProfession.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbDeity.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		input.add(lbName, 0, 0);
		input.add(tfName, 1, 0);
		input.add(lbSpecies, 0, 1);
		input.add(cbRace, 1, 1);
		input.add(lbGender, 0, 2);
		input.add(cbGender, 1, 2);
		input.add(lbBackground, 0, 3);
		input.add(new HBox(5,tfBackground, btnBackground), 1, 3);
		input.add(lbCulture, 0, 4);
		input.add(new HBox(5,tfCulture, btnCulture), 1, 4);
		input.add(lbProfession, 0, 5);
		input.add(new HBox(5,tfProfession, btnProfession), 1, 5);
		input.add(lbDeity, 0, 6);
		input.add(cbDeity, 1, 6);

		input.setMaxHeight(Double.MAX_VALUE);
		setContent(input);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
//	 */
//	@Override
//	public void setResponsiveMode(WindowMode value) {
//		// TODO Auto-generated method stub
//		switch (value) {
//		case MINIMAL:
//			toMinimal();
//			break;
//		default:
//			toNonMinimal();
//		}
//	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		System.getLogger(BasicDataSection.class.getPackageName()).log(Level.INFO, "updateController to "+ctrl);
		this.control = ctrl;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		SpliMoCharacter model = (SpliMoCharacter) control.getModel();
		tfName.setText(model.getName());

		if (model.getRace()!=null) {
			Race data = SplitterMondCore.getItem(Race.class, model.getRace());
			cbRace.setValue(data);
		}
		ComplexDataItemValue<Culture> culture = model.getCulture();
		tfCulture.setText(culture==null?"":culture.getNameWithoutDecisions(Locale.getDefault()));
		ComplexDataItemValue<Background> backgr  = model.getBackground();
		tfBackground.setText(backgr==null?"":backgr.getNameWithoutDecisions(Locale.getDefault()));
		ComplexDataItemValue<Education> educat = model.getEducation();
		tfProfession.setText(educat==null?"":educat.getNameWithoutDecisions(Locale.getDefault()));

		Deity data = SplitterMondCore.getItem(Deity.class, model.getDeity());
		cbDeity.setValue(data);
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.javafx.public_skins.GridPaneTableViewSkin.HeaderLine;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SplittermondSkillTable;
import org.prelle.splittermond.chargen.jfx.pane.SpecializationSelector;
import org.prelle.splittermond.chargen.jfx.section.SkillSection;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class WizardPageSkills extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageSkills.class.getPackageName()+".skill");

	protected static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageSkills.class.getPackageName()+".WizardPages");

	protected SpliMoCharacterGenerator charGen;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	private Label lbPoints, lbMaster;


	protected SplittermondSkillTable tbCombat, tbNormal, tbMagic;
	private NumberUnitBackHeader backHeader;

	// -------------------------------------------------------------------
	public WizardPageSkills(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		if (charGen==null)
			throw new NullPointerException("charGen");
		this.charGen = charGen;
		initComponents();
		initLayout();
		updateTableToController(charGen);
		initInteractivity();
	}

	// -------------------------------------------------------------------
	private void initComponents() {
		tbCombat = new SplittermondSkillTable(charGen, SkillType.COMBAT);
		tbNormal = new SplittermondSkillTable(charGen, SkillType.NORMAL);
		tbMagic  = new SplittermondSkillTable(charGen, SkillType.MAGIC);
		tbCombat.setId("skills-combat");
		tbNormal.setId("skills-normal");
		tbMagic .setId("skills-magic");
		setTitle(ResourceI18N.get(UI, "page.skills.title"));

		lbPoints = new Label("?");
		lbPoints.setStyle("-fx-text-fill: -fx-text-base-color");
		lbPoints.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbMaster = new Label("?");
		lbMaster.setStyle("-fx-text-fill: -fx-text-base-color");
		lbMaster.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		bxDescription = new GenericDescriptionVBox(
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault()));
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		// Init tabs
		ScrollPane scrollCombat = new ScrollPane(tbCombat);
		ScrollPane scrollNormal = new ScrollPane(tbNormal);
		ScrollPane scrollMagic  = new ScrollPane(tbMagic);
		Tab tabCombat = new Tab(ResourceI18N.get(UI, "page.skills.tab.combat"), scrollCombat);
		Tab tabNormal = new Tab(ResourceI18N.get(UI, "page.skills.tab.normal"), scrollNormal);
		Tab tabMagic  = new Tab(ResourceI18N.get(UI, "page.skills.tab.magic" ), scrollMagic);
		TabPane tabs = new TabPane(tabCombat, tabNormal, tabMagic);
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabs.setMaxHeight(Double.MAX_VALUE);

		// Back header
		backHeader = new NumberUnitBackHeader("Exp");
		backHeader.setValue(charGen.getModel().getExpFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
		super.setBackHeader(backHeader);

		// Page layout
		Label hdPoints = new Label(ResourceI18N.get(UI, "head.points")+":");
		Label hdMaster = new Label(ResourceI18N.get(UI, "page.skills.head.master")+":");
		HBox line = new HBox(5, hdPoints, lbPoints, hdMaster, lbMaster);
		VBox content = new VBox(10, line, tabs);
		VBox.setVgrow(tabs, Priority.ALWAYS);
		content.setMaxHeight(Double.MAX_VALUE);
		layout = new OptionalNodePane(content, bxDescription);
		layout.setUseScrollPane(false);
		tabs.setMaxHeight(Double.MAX_VALUE);
		setContent(layout);
	}

	// -------------------------------------------------------------------
	private void updateTableToController(SpliMoCharacterGenerator controller) {
		logger.log(Level.DEBUG, "updateTableToController("+controller+")");
		charGen.addListener(this);

		tbCombat.setController(controller.getSkillController());
		tbNormal.setController(controller.getSkillController());
		tbMagic .setController(controller.getSkillController());

	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		if (charGen!=null)
			charGen.addListener(this);

		tbCombat.selectedItemProperty().addListener( (ov,o,n) -> updateHelpWith(n));
		tbNormal.selectedItemProperty().addListener( (ov,o,n) -> updateHelpWith(n));
		tbMagic .selectedItemProperty().addListener( (ov,o,n) -> updateHelpWith(n));
		tbCombat.setActionCallback(sv -> openActionDialog(sv));
		tbNormal.setActionCallback(sv -> openActionDialog(sv));
		tbMagic .setActionCallback(sv -> openActionDialog(sv));
		tbCombat.setExtraActionCallback(sv -> openMastershipDialog(sv));
		tbNormal.setExtraActionCallback(sv -> openMastershipDialog(sv));
		tbMagic .setExtraActionCallback(sv -> openMastershipDialog(sv));
	}

	//-------------------------------------------------------------------
	private void updateHelpWith(SMSkillValue n) {
		if (n!=null) {
			bxDescription.setData(n.getModifyable());
			layout.setTitle(n.getModifyable().getName(Locale.getDefault()));
			// If in MINIMAL mode, hide title after some time
			// since it covers the final values
			if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
				Thread hideThread = new Thread( () -> {
					try {
						Thread.sleep(4000);
					} catch (Exception e) {
					}
					Platform.runLater( () -> layout.setTitle(null));
				});
				hideThread.start();
			}
		} else {
			bxDescription.setData((DataItem)null);
			layout.setTitle(null);
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.log(Level.DEBUG," refresh");
		tbCombat.setData(charGen.getModel().getSkillValues().stream().filter(sv -> sv.getSkill().getType()==SkillType.COMBAT).toList() );
		tbNormal.setData(charGen.getModel().getSkillValues().stream().filter(sv -> sv.getSkill().getType()==SkillType.NORMAL).toList() );
		tbMagic .setData(charGen.getModel().getSkillValues().stream().filter(sv -> sv.getSkill().getType()==SkillType.MAGIC).toList() );
		tbNormal.refresh();
		backHeader.setValue(charGen.getModel().getExpFree());
		lbPoints.setText( String.valueOf(charGen.getSkillController().getPointsLeft()));
		lbMaster.setText( String.valueOf(charGen.getSkillController().getFreeMastershipPoints()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		// TODO Auto-generated method stub
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (SpliMoCharacterGenerator) param[0];
			updateTableToController(charGen);
			refresh();
			return;
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			refresh();
		}
	}

	//-------------------------------------------------------------------
	private CloseType openActionDialog(SMSkillValue sVal) {
		logger.log(Level.INFO, "openActionDialog({0})", sVal);

		SpecializationSelector selector = new SpecializationSelector(
				charGen.getSkillController(),
				sVal,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault())
				);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.format(SkillSection.RES, "section.skill.specialiations.title", sVal.getSkill().getName()), selector, CloseType.OK, CloseType.CANCEL);
		CloseType close = FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
		if (close==CloseType.OK) {
			logger.log(Level.INFO, "Selector returned with info to select specialization "+selector.getSelected());
			OperationResult<SkillSpecializationValue<SMSkill>> result = charGen.getSkillController().selectSpecialization(sVal, selector.getSelected());
			if (result.wasSuccessful()) {
				refresh();
			} else {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, result.getError());
			}
		}
		return close;
	}

	//-------------------------------------------------------------------
	private CloseType openMastershipDialog(SMSkillValue sVal) {
		logger.log(Level.INFO, "openMastershipDialog({0})", sVal);

		ComplexDataItemController<Mastership, MastershipReference> ctrl = charGen.getSkillController().getMastershipController(sVal);

		Selector<Mastership, MastershipReference> selector =
				new Selector<Mastership, MastershipReference>(
						ctrl,
						SplitterTools.requirementResolver(Locale.getDefault()),
						SplitterTools.modificationResolver(Locale.getDefault()),
						null // FilterInjector
						) {};
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.format(SkillSection.RES, "section.skill.masteries.title", sVal.getNameWithoutRating(Locale.getDefault())), selector, CloseType.OK, CloseType.CANCEL);
		CloseType close = FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
		if (close==CloseType.OK) {
			logger.log(Level.INFO, "Selector returned with info to select specialization "+selector.getSelected());
			OperationResult<MastershipReference> result = ctrl.select(selector.getSelected());
			if (result.wasSuccessful()) {
				refresh();
			} else {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, result.getError());
			}
		}
		return close;
	}
	//-------------------------------------------------------------------
	private void injectHeader(List<SMSkillValue> data) {
		boolean hadCombatHeader = false;
		boolean hadNormalHeader = false;
		boolean hadMagicHeader = false;
		for (int i=0; i<data.size(); i++) {
			SMSkillValue sVal = data.get(i);
			SMSkill skill = sVal.getModifyable();

			if (sVal instanceof SMSkillValueHeader) {
				if (skill.getType()==SkillType.COMBAT) hadCombatHeader=true;
				if (skill.getType()==SkillType.NORMAL) hadNormalHeader=true;
				if (skill.getType()==SkillType.MAGIC) hadMagicHeader=true;
				continue;
			}

			if (skill.getType()==SkillType.COMBAT && !hadCombatHeader) {
				SMSkillValueHeader toAdd = new SMSkillValueHeader(skill.getType().getName(Locale.getDefault()));
				toAdd.setResolved(skill);
				hadCombatHeader=true;
				data.add(i++, toAdd);
				continue;
			}
			if (skill.getType()==SkillType.NORMAL && !hadNormalHeader) {
				SMSkillValueHeader toAdd = new SMSkillValueHeader(skill.getType().getName(Locale.getDefault()));
				toAdd.setResolved(skill);
				data.add(i++, toAdd);
				hadNormalHeader=true;
				continue;
			}
			if (skill.getType()==SkillType.MAGIC && !hadMagicHeader) {
				SMSkillValueHeader toAdd = new SMSkillValueHeader(skill.getType().getName(Locale.getDefault()));
				toAdd.setResolved(skill);
				data.add(i++, toAdd);
				hadMagicHeader=true;
				continue;
			}
		}

	}

}

class SMSkillValueHeader extends SMSkillValue implements HeaderLine {

	private String name;

	public SMSkillValueHeader(String header) {
		this.name = header;
	}

	public String getName() {
		return name;
	}
}


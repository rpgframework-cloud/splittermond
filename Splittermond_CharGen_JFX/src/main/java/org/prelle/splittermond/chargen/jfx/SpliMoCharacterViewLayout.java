package org.prelle.splittermond.chargen.jfx;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.GeneratorWrapper;
import org.prelle.splittermond.chargen.jfx.page.BasicDataPage;
import org.prelle.splittermond.chargen.jfx.page.CombatPage;
import org.prelle.splittermond.chargen.jfx.page.GearPage;
import org.prelle.splittermond.chargen.jfx.page.MagicPage;
import org.prelle.splittermond.chargen.jfx.page.SkillPage;
import org.prelle.splittermond.chargen.jfx.wizard.GenerationWizard;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.pages.CharacterViewLayout;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SpliMoCharacterViewLayout extends CharacterViewLayout<Attribute, SpliMoCharacter, SpliMoCharacterController> implements ControllerListener {

	private final static ResourceBundle UI = ResourceBundle.getBundle(SpliMoCharacterViewLayout.class.getName());

	private final static Logger logger = System.getLogger(SpliMoCharacterViewLayout.class.getPackageName());

	private BasicDataPage pgBasic;
	private SkillPage pgSkills;
	private CombatPage pgCombat;
	private MagicPage pgMagic;
	private GearPage pgGear;

	private Label lbMode;
	private Label lbExp, lbMoney;
	private Button btnWizard;

	private VBox bxHoverToDo;

	//-------------------------------------------------------------------
	public SpliMoCharacterViewLayout() {
		super(RoleplayingSystem.SPLITTERMOND);
		initComponents();
		initPages();

		setOnBackAction(ev -> closeRequested());
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbMode = new Label("Unknown Mode");
		lbMode.getStyleClass().add(JavaFXConstants.STYLE_HEADING3);
		lbMode.setStyle("-fx-text-fill: accent; -fx-font-style: italic");
		lbMode.setMaxWidth(Double.MAX_VALUE);
		lbMode.setAlignment(Pos.CENTER_RIGHT);
		HBox.setMargin(lbMode, new Insets(0, 100, 0, 0));

		pages.setAdditionalHeader(lbMode);
		lbMode.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		lbMode.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		lbExp = new Label("?");
		lbExp.setStyle("-fx-text-fill: accent; -fx-font-weight: bold");
		lbMoney = new Label("?");
		lbMoney.setStyle("-fx-text-fill: accent; -fx-font-weight: bold");
		Label hdKarma = new Label("XP");
		hdKarma.setLabelFor(lbExp);
		Label hdNuyen = new Label("Geld");
		VBox bxCenter = new VBox(5, lbMoney, hdNuyen, lbExp, hdKarma);
		VBox.setMargin(lbMoney, new Insets(10, 0, 0, 0));
		bxCenter.setAlignment(Pos.CENTER);
		extraNodesCenterProperty().add(bxCenter);

		bxHoverToDo = new VBox(0);
		bxHoverToDo.setId("todos");
		bxHoverToDo.setStyle("-fx-max-width: 20em");
		bxHoverToDo.getStyleClass().addAll("hover","todos");

		btnWizard = new Button(null, new SymbolIcon("undo"));
		btnWizard.setTooltip(new Tooltip(ResourceI18N.get(UI, "action.openWizard")));
		btnWizard.setOnAction( ev -> openWizard());
		extraButtonsTopProperty().add(btnWizard);
	}

	//-------------------------------------------------------------------
	private void initPages() {
		pgBasic  = new BasicDataPage();
		pgSkills = new SkillPage();
		pgCombat = new CombatPage();
		pgMagic  = new MagicPage();
		pgGear   = new GearPage();
		getPages().addAll(pgBasic, pgSkills, pgCombat, pgMagic, pgGear);
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#startCreation()
	 */
	@Override
	public void startCreation(CharacterGenerator<?,?> charGen) {
		logger.log(Level.WARNING, "ENTER: Start creation");
		Label tmp = new Label(charGen.getModel().getName());
		tmp.getStyleClass().add(JavaFXConstants.STYLE_HEADING2);
		super.pages.setHeader(tmp);
		GeneratorWrapper wrapper = (GeneratorWrapper)charGen;

		logger.log(Level.ERROR, "Inform "+wrapper.getWrapped());
		handleControllerEvent(BasicControllerEvents.GENERATOR_CHANGED, wrapper);

		logger.log(Level.ERROR, "Create wizard for "+wrapper.getWrapped());
		GenerationWizard wizard = new GenerationWizard(wrapper);
		while (true) {
			CloseType close = FlexibleApplication.getInstance().showAndWait(wizard);
			logger.log(Level.INFO, "Wizard closed via "+close);
			//		controller.refresh();
			if (close==CloseType.FINISH) {
					return;
			}
			if (close==CloseType.CANCEL) {
				//			getApplication().closeAppLayout();
				logger.log(Level.INFO, "call historyBack()");
				dontShowConfirmationDialog = true;
				getApplication().closeScreen(this);
				return;
			}
		}
//		CloseType close = FlexibleApplication.getInstance().showAndWait(wizard);
//		logger.log(Level.INFO, "Wizard closed via "+close);
//		if (close==CloseType.FINISH) {
//			charGen.finish();
//			try {
//				if (charGen.getModel().getName().equals("Unbenannt")) {
//					charGen.getModel().setName("Unbenannt"+(new Random()).nextInt(20));
//				}
//				charGen.save(SplitterMondCore.save(wrapper.getModel()));
//			} catch (CharacterIOException c) {
//				logger.log(Level.ERROR, "Error "+c.getCode()+" : "+c.getValue());
//				logger.log(Level.ERROR, c.toString(),c);
//				super.showCharacterIOException(c, wrapper.getModel());
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		if (close==CloseType.CANCEL) {
//			getApplication().closeScreen(this);
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#continueCreation(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void continueCreation(SpliMoCharacter model, CharacterHandle handle) {
		logger.log(Level.INFO, "ENTER: Continue creation");
		this.handle = handle;

		if (this.control==null)
			throw new IllegalStateException("No controller set");

		try {
			Label tmp = new Label(model.getName());
			tmp.getStyleClass().add(JavaFXConstants.STYLE_HEADING2);
			super.pages.setHeader(tmp);

			control.addListener(this);
			refreshController(true);
			refreshPages();

			logger.log(Level.WARNING, "ToDo: open wizard");
		} finally {
			logger.log(Level.INFO, "LEAVE: Continue creation");
		}
//		logger.log(Level.WARNING, "ToDo: Detect previously used generator");
//		SpliMoCharacterGeneratorImpl gen = new ModuleBasedGenerator();
//		GeneratorWrapper wrapper = new GeneratorWrapper(gen);
//		wrapper.setModel((SpliMoCharacter) model, handle);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#continueCreation(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void resetCharacter(SpliMoCharacter model, CharacterHandle handle) {
		logger.log(Level.DEBUG, "resetCharacter");
		//SplitterTools.runProcessors(model);
		try {
			handle.setCharacter(model);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Failed setting character",e);
		}
		handle.setShortDescription(model.getShortDescription());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#edit(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void edit(SpliMoCharacter model, CharacterHandle handle) {
		logger.log(Level.WARNING, "ENTER: Edit "+model);
		this.handle = handle;
		control.addListener(this);
		extraButtonsTopProperty().remove(btnFinish);
		Label tmp = new Label(control.getModel().getName());
		tmp.getStyleClass().add(JavaFXConstants.STYLE_HEADING2);
		super.pages.setHeader(tmp);

		try {
			refreshController(false);
			refreshSidebar();
			refreshPages();
			refreshToDos();
			control.runProcessors();
		} finally {
			logger.log(Level.WARNING, "LEAVE: Edit " + model);
		}
	}

	//-------------------------------------------------------------------
	private void refreshController(boolean runProcessors) {
		control.setAllowRunProcessor(false);
		pgBasic.setController(control);
		pgSkills.setController(control);
		pgCombat.setController(control);
		pgMagic.setController(control);
		pgGear.setController(control);

		control.setAllowRunProcessor(true);

		refreshPages();
		refreshSidebar();
//		if (control instanceof SpliMoCharacterLeveller) {
//			getPages().addAll(pgCareer);
//		}

		if (runProcessors)
			control.runProcessors();
	}

	//-------------------------------------------------------------------
	private void refreshPages() {
		logger.log(Level.INFO, "refreshPages");
		if (control.getModel().isInCareerMode()) {
			lbMode.setText(ResourceI18N.get(UI, "label.mode.career"));
		} else {
			lbMode.setText(ResourceI18N.get(UI, "label.mode.chargen"));
		}
		super.pages.getHeader().setText(control.getModel().getName());

		pgBasic.refresh();
		pgSkills.refresh();
		pgCombat.refresh();
		pgMagic.refresh();
		pgGear.refresh();
	}

	//-------------------------------------------------------------------
	private void refreshSidebar() {
		if (control==null || control.getModel()==null) return;
		lbExp.setText( String.valueOf( control.getModel().getExpFree() ));
		int nuyen = control.getModel().getMoney();
		if (nuyen>=10000) {
			lbMoney.setText( (nuyen/10000)+"S");
		} else if (nuyen>=100) {
			lbMoney.setText( (nuyen/100)+"L");
		} else {
			lbMoney.setText( String.valueOf(nuyen) );
		}

		if (control instanceof SpliMoCharacterGenerator) {
			// Don't allow finishing, when there is a stopper
			btnFinish.setDisable(control.getToDos().stream().anyMatch(td -> td.getSeverity()==Severity.STOPPER));
			btnWizard.setVisible(true);
		} else {
			btnWizard.setVisible(false);
		}
	}

	//-------------------------------------------------------------------
	private void refreshToDos() {
		logger.log(Level.DEBUG, "refreshToDos");
		bxHoverToDo.getChildren().clear();
		if (control.getToDos().isEmpty()) {
			setHoverNode(null);
		} else {
			ToDoElement.Severity worst = Severity.INFO;
			for (ToDoElement todo : control.getToDos()) {
				if (todo.getSeverity().ordinal()<worst.ordinal())
					worst = todo.getSeverity();
				Label hover = new Label(todo+"");
				hover.setWrapText(true);
				hover.setStyle("-fx-text-fill: textcolor-"+todo.getSeverity().name().toLowerCase());
				bxHoverToDo.getChildren().add(hover);
			}
			bxHoverToDo.getStyleClass().setAll("todos", "todos-"+worst.name().toLowerCase());
			setHoverNode(new Group(bxHoverToDo));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		logger.log(Level.DEBUG, "RCV "+type);
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			control = (SpliMoCharacterController) param[0];
			control.addListener(this);
			refreshController(false);
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED) {
			refreshPages();
		}

		refreshSidebar();
		refreshToDos();
	}

	//-------------------------------------------------------------------
	private void closeRequested() {
		logger.log(Level.ERROR, "ENTER closeRequested");
	}

	//-------------------------------------------------------------------
	private void openWizard() {
		logger.log(Level.DEBUG, "reopen wizard");
		GenerationWizard wizard = new GenerationWizard((GeneratorWrapper) control);
		CloseType close = FlexibleApplication.getInstance().showAndWait(wizard);
		logger.log(Level.DEBUG, "Wizard closed via {0}",close);
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#encodeCharacter(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	protected byte[] encodeCharacter(SpliMoCharacter model) throws CharacterIOException {
		logger.log(Level.DEBUG, "START: encodeCharacter");
		return SplitterMondCore.save(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.pages.CharacterViewLayout#decodeCharacter(byte[])
	 */
	@Override
	protected SpliMoCharacter decodeCharacter(byte[] encoded) throws CharacterIOException {
		try {
			return SplitterMondCore.load(encoded);
		} catch (IOException e) {
			throw new CharacterIOException(ErrorCode.DECODING_FAILED, "Failed decoding XML", e);
		}
	}

}

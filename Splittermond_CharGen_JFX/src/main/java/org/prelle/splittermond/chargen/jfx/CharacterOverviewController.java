package org.prelle.splittermond.chargen.jfx;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.splittermond.chargen.jfx.fxml.ScreenLoader;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class CharacterOverviewController {
	
	private final static Logger logger = System.getLogger(CharacterOverviewController.class.getPackageName());

	private transient SpliMoCharacterViewLayout screen;
	
	private Page pgAttributes;

	//-------------------------------------------------------------------
	public CharacterOverviewController(int x) {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
	}

	//-------------------------------------------------------------------
	public void setComponent(SpliMoCharacterViewLayout screen) {
		this.screen = screen;
//		screen.setController(this);
		logger.log(Level.WARNING, "TODO: set start page");
		try {
			if (pgAttributes==null) {
				pgAttributes = ScreenLoader.loadAttributesPage();
			}
			screen.getApplication().openScreen(new ApplicationScreen(pgAttributes));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		pgAttributes.requestLayout();
	}

	//-------------------------------------------------------------------
	@FXML
	public void navigateAttributes(ActionEvent ev) throws IOException {
		logger.log(Level.DEBUG, "Navigate Attributes");
		if (pgAttributes==null) {
			pgAttributes = ScreenLoader.loadAttributesPage();
		}
		screen.getApplication().openScreen(new ApplicationScreen(pgAttributes));
	}

}

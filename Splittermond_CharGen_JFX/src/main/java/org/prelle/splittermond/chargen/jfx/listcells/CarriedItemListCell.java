package org.prelle.splittermond.chargen.jfx.listcells;

import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.Page;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.items.SMItemFlag;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.common.CommonCarriedItemController;
import org.prelle.splittermond.chargen.jfx.pane.SMEditCarriedItemPaneSkin;
import org.prelle.splittermond.chargen.jfx.selector.ChoiceSelectorDialog;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.jfx.cells.ACarriedItemListCell;
import de.rpgframework.jfx.rules.EditCarriedItemPane;

/**
 * @author prelle
 *
 */
public class CarriedItemListCell extends ACarriedItemListCell<ItemTemplate> {

	protected List<SMItemFlag> flagsToShow;

	//-------------------------------------------------------------------
	public CarriedItemListCell(
			Supplier<ComplexDataItemController<ItemTemplate, CarriedItem<ItemTemplate>>> equipCtrl,
			SMItemFlag...flags
			) {
		super(equipCtrl);
		flagsToShow = List.of(flags);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.cells.ACarriedItemListCell#getSinglePrice(de.rpgframework.genericrpg.items.CarriedItem)
	 */
	@Override
	public int getSinglePrice(CarriedItem<ItemTemplate> item) {
		return item.getAsValue(SMItemAttribute.PRICE).getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.cells.ACarriedItemListCell#getModificationSourceString(java.lang.Object)
	 */
	@Override
	public String getModificationSourceString(Object modSource) {
		return SplitterTools.getModificationSourceString(modSource);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.cells.ComplexDataItemValueListCell#getFlagsStringsToShow(de.rpgframework.genericrpg.data.ComplexDataItemValue)
	 */
	@Override
	public List<String> getFlagsStringsToShow(CarriedItem<ItemTemplate> item) {
		return item.getFlags(SMItemFlag.class).stream()
			.filter( flag -> flagsToShow.contains(flag))
			.map(flag -> flag.getName(Locale.getDefault()))
			.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.cells.ACarriedItemListCell#editClicked(de.rpgframework.genericrpg.items.CarriedItem)
	 */
	@Override
	protected OperationResult<CarriedItem<ItemTemplate>> editClicked(CarriedItem<ItemTemplate> ref) {
		logger.log(Level.WARNING, "TODO: editClicked for "+getClass());

		EnhancementController enhanceCtrl = ((IEquipmentController)super.controlProvider.get()).getEnhancementController(ref);
		CommonCarriedItemController carriedCtrl = (CommonCarriedItemController) ((IEquipmentController)super.controlProvider.get()).getCarriedItemController(ref);
		logger.log(Level.WARNING, "EnhancementController = "+enhanceCtrl);
		EditCarriedItemPane<ItemTemplate, Enhancement> content = new EditCarriedItemPane<ItemTemplate, Enhancement>(ref, enhanceCtrl);
		content.setSkin(new SMEditCarriedItemPaneSkin(content, carriedCtrl));
		content.setOptionCallback( (enh, list) -> {
			ChoiceSelectorDialog<Enhancement, ItemEnhancementValue<Enhancement>> dialog
				= new ChoiceSelectorDialog<Enhancement, ItemEnhancementValue<Enhancement>>(enhanceCtrl, ref);
			return dialog.apply(enh, list);
		});
		Page dialog = new Page(ref.getResolved().getName(), content);

		//dialog.refresh();
		dialog.setAppLayout(FlexibleApplication.getInstance().getAppLayout());
		FlexibleApplication.getInstance().openScreen(new ApplicationScreen(dialog));
		return new OperationResult<>(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem<ItemTemplate> item, boolean empty) {
		super.updateItem(item, empty);

		if (item != null) {
			int price = getSinglePrice(item) * (item.getCount()>1?item.getCount():1);
			lbValue.setText(SplitterTools.telareAsCurrencyString(price));
		}
	}
}

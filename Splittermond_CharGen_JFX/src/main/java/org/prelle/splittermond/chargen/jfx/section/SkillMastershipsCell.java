package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.public_skins.GridPaneTableViewSkin;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splittermond.chargen.jfx.SplittermondSkillTable;

import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.jfx.rules.SkillTable;
import javafx.collections.SetChangeListener;
import javafx.css.PseudoClass;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class SkillMastershipsCell extends TableCell<SMSkillValue, Object> implements ResponsiveControl {

	private final static Logger logger = System.getLogger(SkillTable.class.getPackageName()+".skill");

	private SplittermondSkillTable parent;
	private Button button;
	private Label  lbNames;

	//-------------------------------------------------------------------
	public SkillMastershipsCell(SplittermondSkillTable parent) {
		this.parent = parent;
		button = new  Button(null, new SymbolIcon("edit"));
		button.setVisible(false);
		button.getStyleClass().add("mini-button");
		lbNames = new Label();
		lbNames.setWrapText(true);
		lbNames.setGraphic(button);
		initInteractivity();
		this.setTextAlignment(TextAlignment.LEFT);
		this.setAlignment(Pos.CENTER_LEFT);
		this.alignmentProperty().addListener( (ov,o,n) -> {
			if (n!=Pos.CENTER_LEFT) {
				this.setAlignment(Pos.CENTER_LEFT);
			}
		});

//		super.getProperties().put(GridPaneTableViewSkin.ALIGNMENT_OVERWRITE_PROPERTY, HPos.LEFT);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		button.setOnAction(ev -> {
			Callback<SMSkillValue, CloseType> callback = parent.getExtraActionCallback();
			logger.log(Level.INFO, "Clicked Edit - calling "+callback);
			if (callback!=null) {
				CloseType close = callback.call(getTableRow().getItem());
				logger.log(Level.WARNING, "Returned "+close);
				if (close==CloseType.OK || close==CloseType.APPLY) {
					parent.refresh();
				}
			} else {
				logger.log(Level.WARNING, "No callback to handle click on SkillTable");
			}

		});

		// Only show the edit button, when cursor hovers above the row
		getPseudoClassStates().addListener(new SetChangeListener<PseudoClass>() {
			public void onChanged(Change<? extends PseudoClass> change) {
				if (change.getElementRemoved()!=null && change.getElementRemoved().getPseudoClassName().equals("rowhover")) {
					button.setVisible(false);
				}
				if (change.getElementAdded()!=null && change.getElementAdded().getPseudoClassName().equals("rowhover")) {
					button.setVisible(true);
				}

			}});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void updateItem(Object item , boolean empty) {
		super.updateItem(item, empty);
		List<Object> list = (List<Object>)item;

		if (empty) {
			setGraphic(null);
		} else {
			List<String> allNames = new ArrayList<>();
			for (Object tmp : list) {
				if (tmp instanceof MastershipReference) {
					allNames.add( ((MastershipReference)tmp).getNameWithRating() );
				} else if (tmp instanceof SkillSpecializationValue) {
					allNames.add( ((SkillSpecializationValue)tmp).getNameWithRating() );
				} else if (tmp instanceof SkillSpecialization) {
					allNames.add( ((SkillSpecialization)tmp).getName() );
				} else {
					logger.log(Level.WARNING, "Add "+tmp.getClass()+" : "+tmp);
					allNames.add(String.valueOf(tmp));
				}
			}
			if (ResponsiveControlManager.getCurrentMode() != WindowMode.EXPANDED) {
				if (list.isEmpty())
					lbNames.setText("");
				else
					lbNames.setText(String.valueOf(list.size()));
			} else
				lbNames.setText(String.join(", ", allNames));

			setGraphic(lbNames);

		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		lbNames.setVisible(value!=WindowMode.MINIMAL);
		lbNames.setManaged(value!=WindowMode.MINIMAL);
	}
}

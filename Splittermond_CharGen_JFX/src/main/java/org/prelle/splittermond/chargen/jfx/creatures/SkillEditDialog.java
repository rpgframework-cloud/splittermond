package org.prelle.splittermond.chargen.jfx.creatures;

import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.SMLifeform;

import de.rpgframework.genericrpg.data.AttributeValue;
import javafx.geometry.Orientation;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
public class SkillEditDialog extends ManagedDialog {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());
	
	private static List<SMSkill> ALWAYS = List.of(
			SplitterMondCore.getSkill("melee"),
			SplitterMondCore.getSkill("acrobatics"),
			SplitterMondCore.getSkill("athletics"),
			SplitterMondCore.getSkill("determination"),
			SplitterMondCore.getSkill("stealth"),
			SplitterMondCore.getSkill("perception"),
			SplitterMondCore.getSkill("endurance")
			);
	
	private SMLifeform model;
	private TilePane flow;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillEditDialog(SMLifeform data) {
		super(UI.getString("label.skills"), null, CloseType.CLOSE);
		this.model = data;
		initComponents();
		
//		ScrollPane scroll = new ScrollPane(flow);
//		scroll.setFitToWidth(true);
		setContent(flow);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		flow = new TilePane(10,10);
		flow.setPrefColumns(3);
//		flow.setPrefHeight(700);
		flow.setMaxWidth(800);
		flow.setPrefTileWidth(290);
		flow.setOrientation(Orientation.VERTICAL);
		flow.setStyle("-fx-background-color: lightgrey");
		
		List<SMSkill> list = SplitterMondCore.getItemList(SMSkill.class);
		Collections.sort(list);
		for (SMSkill tmp : list) {
			CreatureSkillValueCell cell = new CreatureSkillValueCell(tmp, ALWAYS.contains(tmp));
			cell.setLifeform(model);

			flow.getChildren().add(cell);
		}
		HBox.setHgrow(flow, Priority.ALWAYS);
	}

}

class CreatureSkillValueCell extends AnchorPane {
	
	private SMSkill skill;
	private SMLifeform model;
	private boolean deletable;
	private SMSkillValue skillVal;
	
	private CheckBox cbActive;
	private Label lbName;
	private Label lbAttrib;
	private Label lbPoints;
	private TextField tfValue;
	
	public CreatureSkillValueCell(SMSkill skill, boolean undeletable) {
		this.skill = skill;
		deletable  = !undeletable;
		initComponents();
		initInteractivity();
	}

	private void initComponents() {
		cbActive = new CheckBox();
		cbActive.setDisable(!deletable);

		lbName   = new Label(skill.getName(Locale.getDefault()));
		lbAttrib = new Label();
		lbPoints = new Label();
		tfValue  = new TextField();
		tfValue.setPrefColumnCount(2);
		tfValue.setStyle("-fx-pref-width: 2.5em");
		
		HBox bar = new HBox(5, cbActive, lbName);
		HBox foo = new HBox(5, lbAttrib, lbPoints, tfValue);
		
		getChildren().addAll(bar, foo);
		AnchorPane.setLeftAnchor(bar, 0.0);
		AnchorPane.setRightAnchor(foo, 0.0);
		
	}

	private void initInteractivity() {
		cbActive.selectedProperty().addListener( (ov,o,n) -> {
			skillVal = model.getSkillValue(skill);
			if (!n) {
				tfValue.setText(null);
			} else {
				AttributeValue<Attribute> a1 = model.getAttribute(skill.getAttribute());
				AttributeValue<Attribute> a2 = model.getAttribute(skill.getAttribute2());
				int attr = 0;
				if (a1!=null) attr+= a1.getModifiedValue();
				if (a2!=null) attr+= a2.getModifiedValue();
				tfValue.setText(String.valueOf(attr+skillVal.getValue()));
			}
		});
		tfValue.textProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				try {
					int val = Integer.parseInt(n);
					AttributeValue<Attribute> a1 = model.getAttribute(skill.getAttribute());
					AttributeValue<Attribute> a2 = model.getAttribute(skill.getAttribute2());
					int attr = 0;
					if (a1!=null) attr+= a1.getModifiedValue();
					if (a2!=null) attr+= a2.getModifiedValue();
					int points = val-attr;
					System.getLogger(this.getClass().getSimpleName()).log(Level.INFO, "val="+val+"  attr="+attr+"  points="+points);
					skillVal = model.getSkillValue(skill);
					skillVal.setDistributed(points);
					skillVal.setValue(val);
					lbPoints.setText(points+" = ");
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	public void setLifeform(SMLifeform model) {
		this.model = model;
		skillVal = model.getSkillValue(skill);
		if (skillVal!=null && skillVal.getDistributed()==0)
			skillVal =null;
		cbActive.setSelected(skillVal!=null);
		tfValue.editableProperty().bind(cbActive.selectedProperty());
		
		AttributeValue<Attribute> a1 = model.getAttribute(skill.getAttribute());
		AttributeValue<Attribute> a2 = model.getAttribute(skill.getAttribute2());
		int attr = 0;
		if (a1!=null) attr+= a1.getModifiedValue();
		if (a2!=null) attr+= a2.getModifiedValue();
		lbAttrib.setText(attr+" + ");
		if (skillVal!=null ) {
			lbPoints.setText(skillVal.getDistributed()+" = ");
			tfValue.setText(String.valueOf(skillVal.getModifiedValue()));
		} else {
			lbPoints.setText("0 = ");
			tfValue.setText(null);
		}
	}

}

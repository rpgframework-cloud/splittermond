package org.prelle.splittermond.chargen.jfx.creatures;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.creature.CreatureTypeValue;
import org.prelle.splimo.creature.SMLifeform;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
public class CreatureTypeEditDialog extends ManagedDialog {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());
	
	private SMLifeform model;
	private TilePane flow;
	
	private Map<CreatureType, Spinner<Integer>> spinnerByType;
	
	//-------------------------------------------------------------------
	/**
	 */
	public CreatureTypeEditDialog(SMLifeform data) {
		super(UI.getString("label.type"), null, CloseType.CLOSE);
		this.model = data;
		spinnerByType = new HashMap<>();
		
		initComponents();
		setContent(flow);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		flow = new TilePane(10,10);
		flow.setPrefColumns(2);
//		flow.setPrefWidth(360);
//		flow.setMaxWidth(500);
		flow.setOrientation(Orientation.VERTICAL);
		
		List<CreatureType> list = SplitterMondCore.getItemList(CreatureType.class);
		Collections.sort(list, new Comparator<CreatureType>() {
			public int compare(CreatureType arg0, CreatureType arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}});
		for (CreatureType tmp : list) {
			CheckBox check = new CheckBox(tmp.getName());
			check.setUserData(tmp);

			HBox box = new HBox(5, check);
			if (tmp.hasLevel()) {
				Spinner<Integer> spinner = new Spinner<>(1, 6, 1);
				spinner.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
				spinner.setMaxWidth(80);
				spinner.setManaged(tmp.hasLevel());
				spinner.setVisible(tmp.hasLevel());
				spinner.valueProperty().addListener((ov, o, n) -> setValue(tmp, n));
				spinnerByType.put(tmp, spinner);
				refreshSpinner(tmp);
				box.getChildren().add(spinner);
			}

			flow.getChildren().add(box);
			TilePane.setAlignment(box, Pos.CENTER_LEFT);
			// Value
			boolean selected = model.getCreatureTypes().stream().anyMatch(val -> val.getType()==tmp);
			check.setSelected(selected);
			
			check.selectedProperty().addListener( (ov,o,n) -> toggle(tmp));
		}
	}

	//-------------------------------------------------------------------
	private void refreshSpinner(CreatureType tmp) {
		Spinner<Integer> spinner = spinnerByType.get(tmp);
		CreatureTypeValue present = model.getCreatureType(tmp);
		if (present!=null && spinner!=null) {
			spinner.getValueFactory().setValue(present.getLevel());
		}
	}

	//-------------------------------------------------------------------
	private void toggle(CreatureType tmp) {
		CreatureTypeValue prev = null;
		for (CreatureTypeValue val : model.getCreatureTypes()) {
			if (val.getType()==tmp)
				prev = val;
		}
		
		if (prev!=null) {
			if (model instanceof Creature) {
				((Creature)model).removeCreatureType(prev);
			}
		} else {
			prev = new CreatureTypeValue(tmp);
			if (model instanceof Creature) {
				((Creature)model).addCreatureType(prev);
			}
		}
		refreshSpinner(tmp);
	}

	//-------------------------------------------------------------------
	private void setValue(CreatureType tmp, int value) {
		CreatureTypeValue prev = null;
		System.out.println("CreatureTypeEditDialog.setValue("+tmp+", "+value+")");
		for (CreatureTypeValue val : model.getCreatureTypes()) {
			if (val.getType()==tmp)
				prev = val;
		}
		
		if (prev!=null) {
			prev.setLevel(value);
			final CreatureTypeValue val = prev;
			Spinner<Integer> spinner = spinnerByType.get(tmp);
			spinner.getValueFactory().setValue(value);
			spinner.valueProperty().addListener( (ov,o,n) -> {
				val.setLevel(n);
			});
		}
	}
}

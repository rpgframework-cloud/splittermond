package org.prelle.splittermond.chargen.jfx.pane;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.FlowItemList;
import org.prelle.javafx.ItemListBase;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.EnhancementType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.common.CommonCarriedItemController;
import org.prelle.splittermond.chargen.jfx.SpliMoCharacterViewLayout;
import org.prelle.splittermond.chargen.jfx.section.PowerSection;
import org.prelle.splittermond.chargen.jfx.selector.EnhancementSelector;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.jfx.rules.EditCarriedItemPane;
import javafx.collections.MapChangeListener;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 *
 */
public class SMEditCarriedItemPaneSkin extends SkinBase<EditCarriedItemPane<ItemTemplate,Enhancement>> {

	private final static Logger logger = System.getLogger(SMEditCarriedItemPaneSkin.class.getName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(PowerSection.class.getPackageName()+".Sections");

	private CommonCarriedItemController carriedCtrl;
	private ImageView iView;
	private CarriedItemBasicDataPane paneBasic;
	private ArmorDataPane paneArmor;
	private WeaponDataPane paneWeapon;
	private CarriedItemEnhancementsPane paneEnhance;
	private ItemListBase<ItemEnhancementValue<Enhancement>> paneEnhance2;

	private MapChangeListener<Object, Object> propertiesMapListener = c -> {
        if (! c.wasAdded()) return;
        if (c.getKey().toString().equals("RECREATE")) {
        	refresh();
            getSkinnable().requestLayout();
            getSkinnable().getProperties().remove(c.getKey());
        }
    };

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public SMEditCarriedItemPaneSkin(EditCarriedItemPane<ItemTemplate,Enhancement> control, CommonCarriedItemController ctrl) {
		super(control);
		this.carriedCtrl = ctrl;
		if (ctrl==null) throw new NullPointerException("ICarriedItemController is null");
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		iView = new ImageView();
		iView.setFitWidth(250);

		CarriedItem<ItemTemplate> ci = getSkinnable().getItem();
		paneBasic = new CarriedItemBasicDataPane(ci, carriedCtrl);
		paneArmor = new ArmorDataPane(ci, carriedCtrl);
		paneWeapon= new WeaponDataPane(ci, carriedCtrl);
		paneEnhance = new CarriedItemEnhancementsPane(EnhancementType.NORMAL);

		paneEnhance2 = new FlowItemList<ItemEnhancementValue<Enhancement>>("Verbesserungen") {
			protected void onAdd() { onAddEnhancement();}
			protected void onDelete(ItemEnhancementValue<Enhancement> item) {
				// TODO Auto-generated method stub

			}};
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox accord = new VBox(20);
		accord.getChildren().addAll(paneBasic,paneArmor, paneWeapon,paneEnhance,paneEnhance2);
		ScrollPane scroll = new ScrollPane(accord);

		HBox imgPlusData = new HBox(20, iView, scroll);
		imgPlusData.getStyleClass().add("carried-item-detail");
		imgPlusData.setStyle("-fx-background-color: white; -fx-background-radius: 20px;");

		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox layout = new HBox(imgPlusData,buf);
		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		carriedCtrl.addListener( ci -> refresh());

		//paneEnhance2.setOn
	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.log(Level.ERROR, "refresh");
		CarriedItem<ItemTemplate> item = getSkinnable().getItem();

		String imageFile = "images/gear/"+item.getKey()+".png";
		InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream(imageFile);
		if (in!=null)
			iView.setImage(new Image(in));
		else
			iView.setImage(null);

		boolean wideEnough = ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL;
		iView.setManaged(wideEnough);
		iView.setVisible(wideEnough);

//		paneBasic.setData(getSkinnable().getItem(), carriedCtrl);
//		paneArmor.setData(getSkinnable().getItem(), carriedCtrl);
//		paneWeapon.setData(getSkinnable().getItem(), carriedCtrl);
//		paneEnhance.setData(getSkinnable().getItem(), carriedCtrl.getEnhancementController());

		logger.log(Level.ERROR, "refresh2");
		paneBasic.refresh();
		paneArmor.refresh();
		paneWeapon.refresh();
		paneEnhance.refresh();
		paneEnhance2.getItems().setAll(item.getEnhancements());
	}

	private void onAddEnhancement() {
		// TODO Auto-generated method stub
		logger.log(Level.INFO, "onAdd");
		EnhancementSelector selector = new EnhancementSelector(carriedCtrl.getEnhancementController());
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"section.power.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			Enhancement data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected enhancement: "+data);

			OperationResult<ItemEnhancementValue<Enhancement>> result = null;
			if (data.getChoices().isEmpty()) {
				result = carriedCtrl.getEnhancementController().select(data);
			} else {
				Decision[] dec = handleChoices(data);
				if (dec!=null) {
					result = carriedCtrl.getEnhancementController().select(data, dec);
				}
			}
			if (result!=null) {
				if (result.wasSuccessful()) {
					paneEnhance.refresh();
				} else {
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, result.getError());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	protected Decision[] handleChoices(Enhancement data) {
		logger.log(Level.WARNING, "TODO: handleChoices for {0}",data);
		return new Decision[0];
	}
}

package org.prelle.splittermond.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.Section;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.SplittermondSkillTable;
import org.prelle.splittermond.chargen.jfx.pane.SpecializationSelector;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.rules.SkillTable;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SkillSection extends Section {

	public static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getPackageName()+".Sections");

	private final static Logger logger = System.getLogger(SkillSection.class.getPackageName());

	private SpliMoCharacterController control;
	private SkillType type;

	private SplittermondSkillTable table;

	//-------------------------------------------------------------------
	public SkillSection(String title, SkillType type) {
		super(title, null);
		this.type = type;
		setId("skills-"+type.name().toLowerCase());
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		table = new SplittermondSkillTable(null, type);
		table.setMaxHeight(Double.MAX_VALUE);
		table.setActionCallback( sVal -> openActionDialog(sVal));
		table.setExtraActionCallback( sVal -> openMastershipDialog(sVal));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(table);

		CheckBox cb1 = new CheckBox("Configuration Setting 1");
		CheckBox cb2 = new CheckBox("Configuration Setting 2");
		VBox back = new VBox(5, cb1, cb2);
		setSecondaryContent(back);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		if (control!=null) {
			List<SMSkillValue> list = control.getModel().getSkillValues().stream()
				.filter(sval -> sval.getResolved().getType()==type)
				.toList();
			table.setData(list);
		}
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<SMSkillValue> selectedSkillProperty() {
		return table.getSelectionModel().selectedItemProperty();
	}

	//-------------------------------------------------------------------
	public void updateController(SpliMoCharacterController ctrl) {
		this.control = ctrl;
		table.setModel(ctrl.getModel());
		table.setController(ctrl.getSkillController());
//		table.setExtraCellFactory( col -> {
//			return new SkillMastershipsCell(table, null);
//		});
		refresh();
	}

	//-------------------------------------------------------------------
	public SkillTable<Attribute,SMSkill,SMSkillValue> getTable() {
		return table;
	}

	//-------------------------------------------------------------------
	private CloseType openActionDialog(SMSkillValue sVal) {
		logger.log(Level.INFO, "openActionDialog({0})", sVal);

		SpecializationSelector selector = new SpecializationSelector(
				control.getSkillController(),
				sVal,
				SplitterTools.requirementResolver(Locale.getDefault()),
				SplitterTools.modificationResolver(Locale.getDefault())
				);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.format(RES, "section.skill.specialiations.title", sVal.getSkill().getName()), selector, CloseType.OK, CloseType.CANCEL);
		CloseType close = FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
		if (close==CloseType.OK) {
			logger.log(Level.INFO, "Selector returned with info to select specialization "+selector.getSelected());
			OperationResult<SkillSpecializationValue<SMSkill>> result = control.getSkillController().selectSpecialization(sVal, selector.getSelected());
			if (result.wasSuccessful()) {
				refresh();
			} else {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, result.getError());
			}
		}
		return close;
	}

	//-------------------------------------------------------------------
	private CloseType openMastershipDialog(SMSkillValue sVal) {
		logger.log(Level.INFO, "openMastershipDialog({0})", sVal);

		ComplexDataItemController<Mastership, MastershipReference> ctrl = control.getSkillController().getMastershipController(sVal);

		Selector<Mastership, MastershipReference> selector =
				new Selector<Mastership, MastershipReference>(
						ctrl,
						SplitterTools.requirementResolver(Locale.getDefault()),
						SplitterTools.modificationResolver(Locale.getDefault()),
						null // FilterInjector
						) {};
//		SpecializationSelector selector = new SpecializationSelector(
//				control.getSkillController(),
//				sVal,
//				SplitterTools.requirementResolver(Locale.getDefault()),
//				SplitterTools.modificationResolver(Locale.getDefault())
//				);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.format(RES, "section.skill.masteries.title", sVal.getNameWithoutRating(Locale.getDefault())), selector, CloseType.OK, CloseType.CANCEL);
		CloseType close = FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
		if (close==CloseType.OK) {
			logger.log(Level.INFO, "Selector returned with info to select specialization "+selector.getSelected());
			OperationResult<MastershipReference> result = ctrl.select(selector.getSelected());
			if (result.wasSuccessful()) {
				refresh();
			} else {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, result.getError());
			}
		}
		return close;
	}

}

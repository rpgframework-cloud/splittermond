package org.prelle.splittermond.chargen.jfx.page;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.javafx.Mode;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.Section;
import org.prelle.javafx.TitledComponent;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModule.Category;
import org.prelle.splimo.creature.CreatureModule.Type;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.ServiceValue;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splittermond.chargen.companion.summon.SummonableController;
import org.prelle.splittermond.chargen.jfx.creatures.LifeformPane;
import org.prelle.splittermond.chargen.jfx.selector.ChoiceSelectorDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.SelectedValuesControllerNode;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.cells.SelectedValueListCell;
import de.rpgframework.jfx.rules.SkillTable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SummonableCreatorPage extends Page {

	private final static Logger logger = System.getLogger(SummonableCreatorPage.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(SummonableCreatorPage.class.getPackageName()+".Pages");

	private SummonableController ctrl;

	private ChoiceBox<CreatureModule> cbBody;
	private ChoiceBox<CreatureModule> cbType;

	private ComplexDataItemControllerNode<CreatureModule, CreatureModuleValue> ctrlOptions;
	private SelectedValuesControllerNode<ServiceValue> ctrlServices;
	private SkillTable<Attribute,SMSkill,SMSkillValue> skillTable;

	private LifeformPane paneStats;
	private TabPane tasGenerator;

	private Section secGenerator;
	private Section secStats;

	private FlexGridPane flex;

	//-------------------------------------------------------------------
	public SummonableCreatorPage() {
		super(ResourceI18N.get(RES, "page.creator.summonable.title"));

		ctrl = new SummonableController(null, 0);

		initComponents();
		initLayout();
		initInteractivity();
		OptionalNodePane layout = new OptionalNodePane(flex, new Label("Select something to get a description"));
		setContent(layout);
		super.setMode(Mode.REGULAR);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		initStats();
		initGenerator();
	}

	//-------------------------------------------------------------------
	private void initStats() {
		paneStats = new LifeformPane(false);
		secStats = new Section("Werte", paneStats);
		secStats.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(secStats, 4);
		FlexGridPane.setMinHeight(secStats, 10);
		FlexGridPane.setMediumWidth(secStats, 7);
	}

	//-------------------------------------------------------------------
	private void initGenerator() {
		initGeneratorComponents();
		initSkillTab();

		TitledComponent tcBody = new TitledComponent("Korpus", cbBody);
		TitledComponent tcType = new TitledComponent("Typ", cbType);
		tcBody.setTitleMinWidth(100.0);
		tcType.setTitleMinWidth(100.0);


		VBox content = new VBox(20, new VBox(10,tcBody, tcType,ctrlOptions));

		Tab tab1 = new Tab("Module", content);
		Tab tab2 = new Tab("Dienste", ctrlServices);
		Tab tab3 = new Tab("Meisterschaften", skillTable);
		tasGenerator = new TabPane(tab1, tab2, tab3);
		tasGenerator.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		secGenerator = new Section("Baukasten", tasGenerator);
		FlexGridPane.setMinWidth(secGenerator, 4);
		FlexGridPane.setMinHeight(secGenerator, 10);
		FlexGridPane.setMediumWidth(secGenerator, 7);
		FlexGridPane.setMaxWidth(secGenerator, 8);
	}

	//-------------------------------------------------------------------
	private void initGeneratorComponents() {
		cbBody = new ChoiceBox<>();
		cbBody.getItems().addAll(
				SplitterMondCore.getItemList(CreatureModule.class).stream()
				.filter(m -> m.getCategory()==Category.SUMMONABLE)
				.filter(m -> m.getType()==Type.BODY)
				.collect(Collectors.toList())
				);
		Collections.sort( cbBody.getItems(), new Comparator<CreatureModule>() {
			public int compare(CreatureModule o1, CreatureModule o2) {
				return Integer.compare(o1.getCost(), o2.getCost());
			}
		});
		cbBody.setConverter(new StringConverter<CreatureModule>() {
			public String toString(CreatureModule value) {
				return (value!=null)?value.getName():"-";
			}
			public CreatureModule fromString(String string) {return null;}
		});
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(
				SplitterMondCore.getItemList(CreatureModule.class).stream()
				.filter(m -> m.getCategory()==Category.SUMMONABLE)
				.filter(m -> m.getType()==Type.TYPE)
				.collect(Collectors.toList())
				);
		cbType.setConverter(new StringConverter<CreatureModule>() {
			public String toString(CreatureModule value) {
				return (value!=null)?value.getName():"-";
			}
			public CreatureModule fromString(String string) {return null;}
		});

		ctrlOptions = new ComplexDataItemControllerNode<>(ctrl.getExtraModuleController());
		ctrlOptions.setAvailableCellFactory(lv -> new ComplexDataItemListCell<>());
		ctrlOptions.setSelectedCellFactory(lv -> new ComplexDataItemValueListCell<CreatureModule,CreatureModuleValue>( ()->ctrl.getExtraModuleController() ));
		ctrlOptions.setOptionCallback( (mod,choices) -> requestDecisions(mod,choices));

		ctrlServices = new SelectedValuesControllerNode<ServiceValue>(ctrl.getServicesController());
		ctrlServices.setCellFactory(lv -> new SelectedValueListCell<ServiceValue>( ()->ctrl.getServicesController(), null ));
	}

	//-------------------------------------------------------------------
	private void initSkillTab() {
		skillTable = new SkillTable<Attribute,SMSkill,SMSkillValue>();
		skillTable.setData(ctrl.getModel().getSkillValues());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		flex = new FlexGridPane();
		flex.setSpacing(20);
		flex.getChildren().addAll(secGenerator,secStats);

	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbBody.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Selected "+n);
			ctrl.selectBody(n);
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Selected "+n);
			ctrl.selectType(n);

			paneStats.setData(ctrl.getModel());
		});

		ctrl.addListener(new ControllerListener() {
			@Override
			public void handleControllerEvent(ControllerEvent type, Object... param) {
				if (type==BasicControllerEvents.CREATURE_CHANGED) {
					logger.log(Level.DEBUG, "CREATURE_CHANGED");
					paneStats.setData(ctrl.getModel());
					refresh();
				}
			}
		});
	}

	//-------------------------------------------------------------------
	private Decision[] requestDecisions(CreatureModule mod, List<Choice> choices) {
		logger.log(Level.WARNING, "TODO: ask user for decisions for "+mod);
		ChoiceSelectorDialog<CreatureModule, CreatureModuleValue> dialog = new ChoiceSelectorDialog<CreatureModule, CreatureModuleValue>(ctrl.getExtraModuleController());
		return dialog.apply(mod, choices);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public void refresh() {
		logger.log(Level.WARNING, "refresh");
		ctrlServices.refresh();
		ctrlOptions.refresh();
		paneStats.refresh();
		System.err.println("SummonableCreaturePage: skill values = "+ctrl.getModel().getSkillValues());
		skillTable.setData(ctrl.getModel().getSkillValues());
	}

}

package org.prelle.splittermond.chargen.jfx.wizard;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.BackdropWizardPage;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.NodeWithTitle;
import org.prelle.javafx.Wizard;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Deity;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SMReferenceTypeConverter;
import org.prelle.splittermond.chargen.jfx.SpliMoCharacterViewLayout;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.DataItemPane;
import de.rpgframework.jfx.RecommendationStateNameConverter;
import de.rpgframework.jfx.cells.RecommendingDataItemListCell;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageCulture extends BackdropWizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageCulture.class.getPackageName()+".cult");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageCulture.class.getPackageName()+".WizardPages");

	private SpliMoCharacterGenerator charGen;

	private DataItemPane<Culture> contentPane;
	private CheckBox cbIgnoreRace;
	private CheckBox cbAllowAllGods;

	private ComboBox<Deity> cbDeity;
	private Button btnRoll;
	private TextField tfName;
	private FlowPane customNode1;
	
	private boolean updating;

	//-------------------------------------------------------------------
	public WizardPageCulture(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		if (charGen==null)
			throw new NullPointerException("CharGen is NULL");
		setTitle(ResourceI18N.get(RES, "page.culture.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		contentPane = new DataItemPane<Culture>(
				r -> SplitterTools.requirementResolver(Locale.getDefault()).apply(r),
				m -> SplitterTools.modificationResolver(Locale.getDefault()).apply(m)
				);
		contentPane.setId("culture");
		contentPane.setShowModificationsInDescription(false);
		contentPane.setImageConverter(new Callback<Culture,Image>(){
			public Image call(Culture value) {
				if (value==null) return null;
				InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/culture/"+value.getId()+".png");
				if (in!=null)
					return new Image(in);
				in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/culture/"+value.getId()+".jpg");
				if (in!=null)
					return new Image(in);
				in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/culture/Dummy.gif");
				if (in!=null)
					return new Image(in);
				logger.log(Level.ERROR, "Missing resource "+SpliMoCharacterViewLayout.class.getName()+" + images/culture/"+value.getId()+".png");
				return null;
			}});
		contentPane.setUseForChoices(charGen.getModel().getCulture());
		contentPane.setModificationConverter((m) -> SplitterTools.getModificationString(contentPane.getSelectedItem(),m, Locale.getDefault()));
		contentPane.setReferenceTypeConverter(new SMReferenceTypeConverter<>());
		contentPane.setNameConverter( new RecommendationStateNameConverter<>(charGen.getCultureController()));
		contentPane.setChoiceConverter((c) -> SplitterTools.getChoiceString(contentPane.getSelectedItem(), c));
		contentPane.setItems(charGen.getCultureController().getAvailable());
		contentPane.setDecisionHandler( (r,c) -> {
			logger.log(Level.ERROR, "openDecisionDialog for {0}",r);
			SplitterJFXUtil.openDecisionDialog(r, c, charGen.getCultureController());
			logger.log(Level.ERROR, "done making decision");
			refresh();
		});
		contentPane.setModDecisionHandler( (r,c) -> {
			logger.log(Level.WARNING, "ToDo: make decision: "+r);
			SplitterJFXUtil.openDecisionDialog(r, c, charGen.getCultureController());
			logger.log(Level.WARNING, "done making decision");
			refresh();
		});
		contentPane.showDecisionColumnProperty().set(false);

		/* Configuration options */
		cbIgnoreRace = new CheckBox(ResourceI18N.get(RES, "page.culture.config.ignoreRace"));
		cbIgnoreRace.setSelected(charGen.getCultureController().isAllowUnusualCultures());
		cbAllowAllGods = new CheckBox(ResourceI18N.get(RES, "page.culture.config.allowAllGods"));
		cbAllowAllGods.setSelected(charGen.getCultureController().isAllowUnusualDeities());

		/*
		 * Custom node
		 */
		btnRoll  = new Button(ResourceI18N.get(RES, "page.culture.button.roll"));
		btnRoll.setStyle("-fx-background-color: -fx-accent; -fx-text-fill: white");
		cbDeity = new ComboBox<>();
		cbDeity.getItems().addAll(charGen.getCultureController().getAvailableDeities());
//		cbDeity.setConverter(new StringConverter<Deity>() {
//			public String toString(Deity key) {return (key!=null)?key.getName(Locale.getDefault()):"null";}
//			public Deity fromString(String key) {return null;}
//		});
		cbDeity.setCellFactory( (lv) -> new RecommendingDataItemListCell<Deity>(charGen.getCultureController().getDeityRecommender()));
		tfName   = new TextField();
	}

	//-------------------------------------------------------------------
	private void addToCustom(Node node, String prop) {
		Label ret = new Label(ResourceI18N.get(RES, prop));
		ret.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		customNode1.getChildren().add(new VBox(5, ret, node));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(contentPane);

		VBox config = new VBox(10, cbIgnoreRace, cbAllowAllGods);
		config.setStyle("-fx-padding: 0 0 1em 0;");
		setBackContent(config);
		setBackHeader(new Label("Filter Kulturen und Gottheiten"));

		// Custom
		customNode1 = new FlowPane(10, 10);
		addToCustom(tfName, "page.culture.label.name");
		addToCustom(cbDeity, "page.culture.label.deity");

		VBox cust = new VBox(10, btnRoll, customNode1);
		cust.setId("wizard-culture-data");
		contentPane.setCustomNode1(new NodeWithTitle(ResourceI18N.get(RES,"page.culture.tab.custom"), cust));
	}

	//-------------------------------------------------------------------
	private void updateBackHeader() {
		CultureController gen = charGen.getCultureController();
		if (gen.isAllowUnusualCultures()) {
			if (gen.isAllowUnusualDeities()) {
				setBackHeader(new Label("Keine Filter"));
			} else {
				setBackHeader(new Label("Nur passende Gottheiten, alle Kulturen"));
			}
		} else {
			if (gen.isAllowUnusualDeities()) {
				setBackHeader(new Label("Nur passende Kulturen, alle Gottheiten"));
			} else {
				setBackHeader(new Label("Nur passende Kulturen und Gottheiten"));
			}
		}
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbIgnoreRace.selectedProperty().addListener( (ov,o,n) -> {
			CultureController ctrl = charGen.getCultureController();
			ctrl.setAllowUnusualCultures(n);
			refresh();
		});
		cbAllowAllGods.selectedProperty().addListener( (ov,o,n) -> {
			charGen.getCultureController().setAllowUnusualDeities(n);
			refresh();
		});

		contentPane.selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.ERROR, "Culture changed to {0}",n);
			if (n==null) return;
			CultureController ctrl = charGen.getCultureController();
//			logger.log(Level.ERROR, "vor select(): "+charGen.getModel().getCulture().getResolved());
			ctrl.select(n);
//			logger.log(Level.ERROR, "nach select(): "+charGen.getModel().getCulture().getResolved());
//			refresh();
		});

		btnRoll.setOnAction(ev -> {
			logger.log(Level.INFO, "Roll");
			CultureController ctrl = charGen.getCultureController();
			ctrl.rollName();
			ctrl.rollDeity();
			refresh();
		});

		cbDeity.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				charGen.getModel().setDeity(n.getId());
		});
		tfName.textProperty().addListener( (ov,o,n) -> {
			charGen.getModel().setName(n);
		});
	}

	//-------------------------------------------------------------------
	void refresh() {
		if (updating) return;
		updating = true;
		try {
			updateBackHeader();
			
			CultureController cultCtrl = charGen.getCultureController();
			// Update list of available cultures
			contentPane.setItems(charGen.getCultureController().getAvailable());
			// Update recommendations
			contentPane.setNameConverter( new RecommendationStateNameConverter<>(cultCtrl));
			// Update selection
			SpliMoCharacter model = charGen.getModel();
			ComplexDataItemValue<Culture> selected = model.getCulture();
			contentPane.setSelectedItem( (selected!=null)?selected.getResolved():null);
			// Update deities
			cbDeity.getItems().clear();
			cbDeity.getItems().addAll(cultCtrl.getAvailableDeities());
			if (model.getDeity()!=null) {
				cbDeity.setValue(SplitterMondCore.getItem(Deity.class, model.getDeity()));
			} else {
				cbDeity.getSelectionModel().clearSelection();
			}
			tfName.setText(charGen.getModel().getName());
		} finally {
			updating=false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (SpliMoCharacterGenerator) param[0];
			refresh();
			return;
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
//			refresh();
		}
	}

}

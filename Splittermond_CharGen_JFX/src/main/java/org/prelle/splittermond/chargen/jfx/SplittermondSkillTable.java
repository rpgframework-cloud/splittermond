package org.prelle.splittermond.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.public_skins.GridPaneTableViewSkin;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.jfx.section.SkillMastershipsCell;

import de.rpgframework.jfx.rules.SkillTable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class SplittermondSkillTable extends SkillTable<Attribute, SMSkill, SMSkillValue> {

	private final static Logger logger = System.getLogger(SplittermondSkillTable.class.getPackageName());

	private SpliMoCharacterController charCtrl;
	private Collection<SkillType> allowedTypes;

	private ObjectProperty<Consumer<List<SMSkillValue>>> selectedListModifier = new SimpleObjectProperty<>();

    /**
     * Callback to open an edit masteries dialog
     */
	private ObjectProperty<Callback<SMSkillValue, CloseType>> extraActionCallback = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public ObjectProperty<Callback<SMSkillValue, CloseType>> extraActionCallbackProperty() { return extraActionCallback; }
	public Callback<SMSkillValue, CloseType> getExtraActionCallback() { return extraActionCallback.get(); }
	public SplittermondSkillTable setExtraActionCallback(Callback<SMSkillValue, CloseType> value) { extraActionCallback.set(value); return this; }

	//-------------------------------------------------------------------
	public SplittermondSkillTable(SpliMoCharacterController ctrl, SkillType...types) {
		this.charCtrl = ctrl;
		if (ctrl!=null)
			super.setController(ctrl.getSkillController());
		setAttributeMode(Mode.TWO_ATTRIB);
		//setAlwaysShowExtraColumn(true);
		showAttributesProperty().set(true);
		allowedTypes = List.of(types);

		getStyleClass().addAll("detail-card");

		setExtraCellValueFactory( new Callback<TableColumn.CellDataFeatures<SMSkillValue,Object>, ObservableValue<Object>>() {
			public ObservableValue<Object> call(CellDataFeatures<SMSkillValue, Object> param) {
				List<Object> all = new ArrayList<>();
				all.addAll(param.getValue().getSpecializations());
				all.addAll(param.getValue().getMasterships());
				return new SimpleObjectProperty<Object>(all);
			}
		} );
		setExtraCellFactory( col -> {
			return new SkillMastershipsCell(this);
		});
		getNameColumn().setPrefWidth(210);
		getAttribute1Column().setText("A1");
		getAttribute2Column().setText("A2");
		getExtraColumn().setText("Meisterschaften");
		getExtraColumn().setPrefWidth(400);
		getExtraColumn().setMaxWidth(400);
		getExtraColumn().getProperties().put(GridPaneTableViewSkin.ALIGNMENT_OVERWRITE_PROPERTY, HPos.LEFT);

		getValueColumn().setText("Punkte");
		getFinalValueColumn().setPrefWidth(60);
		getFinalValueColumn().setText("Wert");
		setActionCallback( sVal -> openActionDialog(sVal));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
//	@Override
//	public Skin<?> createDefaultSkin() {
//		return (new SplittermondSkillTableSkin(this, true)).setCenterAfter(1);
//	}

	//-------------------------------------------------------------------
	public ObjectProperty<Consumer<List<SMSkillValue>>> selectedListModifierroperty() { return selectedListModifier; }
	public Consumer<List<SMSkillValue>> getSelectedListModifier() { return selectedListModifier.get(); }
	public SplittermondSkillTable setSelectedListModifier(Consumer<List<SMSkillValue>> value) { selectedListModifier.set(value); return this; }

	//-------------------------------------------------------------------
	public Collection<SkillType> getAllowedTypes() { return allowedTypes;}

	//-------------------------------------------------------------------
	private CloseType openActionDialog(SMSkillValue sVal) {
		logger.log(Level.INFO, "openActionDialog({0})", sVal);

//		SRSkillSettingsPane pane = new SRSkillSettingsPane(sVal, control.getSkillController());
//		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES, "dialog.specializations.title"), pane, CloseType.OK);
//		CloseType close = FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
//		return close;
		return null;
	}

}

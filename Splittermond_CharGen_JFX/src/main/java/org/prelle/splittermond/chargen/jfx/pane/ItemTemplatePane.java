package org.prelle.splittermond.chargen.jfx.pane;

import java.io.InputStream;
import java.util.Locale;
import java.util.function.Function;

import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.jfx.ItemUtilJFX;

import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.GenericDescriptionVBox;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author prelle
 *
 */
public class ItemTemplatePane extends GenericDescriptionVBox<ItemTemplate> {

	private Node perTypeStats;
	private ImageView imgView;
	private CarryMode carry;

	// -------------------------------------------------------------------
	public ItemTemplatePane(Function<Requirement, String> requirementResolver, CarryMode carry) {
		super(requirementResolver, SplitterTools.modificationResolver(Locale.getDefault()));
		this.carry = carry;
	}

	//-------------------------------------------------------------------
	public ItemTemplatePane(Function<Requirement, String> requirementResolver, ItemTemplate item, CarryMode carry) {
		super(requirementResolver, SplitterTools.modificationResolver(Locale.getDefault()), item);
		this.carry = carry;
		setData(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.GenericDescriptionVBox#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(ItemTemplate data) {
		super.setData(data);

		if (perTypeStats != null) {
			super.inner.getChildren().remove(perTypeStats);
		}
		if (imgView != null) {
			super.inner.getChildren().remove(imgView);
		}

		if (data != null) {
			perTypeStats = ItemUtilJFX.getItemInfoNode((ItemTemplate)data, null, carry);
			if (perTypeStats != null) {
				super.inner.getChildren().add(2, perTypeStats);
			}
			// Add image if user has license
			if (data.hasLicense(Locale.getDefault())) {
				InputStream ins = data.getResource("images/"+data.getId()+".png");
				if (ins!=null) {
					Image img = new Image(ins);
					int width = Math.min( (int)img.getWidth(), 600 );
					if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
						width = Math.min(400, width);
					}
					imgView = new ImageView(img);
					imgView.setPreserveRatio(true);
					imgView.setFitWidth(width);
					super.inner.getChildren().add(0, imgView);
				}
			}
		}
	}

}

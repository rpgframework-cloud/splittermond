package org.prelle.splittermond.chargen.jfx.creatures;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class CreatureFeatureViewPane extends FlowPane {

	private final static Logger logger = System.getLogger(CreatureFeatureValue.class.getPackageName());

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LifeformPane.class.getName());

	private SMLifeform model;

	private Label lblHeading;

	//-------------------------------------------------------------------
	/**
	 */
	public CreatureFeatureViewPane() {
		initComponents();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lblHeading = new Label(UI.getString("label.features")+":");
		lblHeading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setHgap(3);
		setVgap(3);
		getChildren().add(lblHeading);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	void refresh() {
		getChildren().retainAll(lblHeading);

		for (Iterator<CreatureFeatureValue> it=model.getFeatures().iterator(); it.hasNext(); ) {
			CreatureFeatureValue val = it.next();
			String text = val.getNameWithRating();
			if (it.hasNext())
				text +=",";
			Text textNode = new Text(text);
			textNode.setOnMouseClicked(ev -> {
				logger.log(Level.INFO, "Clicked "+val);
				SplitterJFXUtil.showPopupText(getScene().getWindow().getX(), getScene().getWindow().getY(), ev, val.getResolved().getDescription());

			});
			getChildren().add(textNode);
		}
	}

	//--------------------------------------------------------------------
	public void setData(SMLifeform model) {
		this.model = model;
//		logger.log(Level.INFO, "setData "+model.dump());
		refresh();
		initInteractivity();
	}

}

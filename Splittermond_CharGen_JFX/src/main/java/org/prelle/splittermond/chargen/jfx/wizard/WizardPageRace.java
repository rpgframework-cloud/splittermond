package org.prelle.splittermond.chargen.jfx.wizard;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.NodeWithTitle;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.splimo.Race;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.jfx.SMReferenceTypeConverter;
import org.prelle.splittermond.chargen.jfx.SpliMoCharacterViewLayout;
import org.prelle.splittermond.chargen.jfx.SplitterJFXUtil;

import de.rpgframework.ResourceI18N;
import de.rpgframework.classification.Gender;
import de.rpgframework.jfx.DataItemPane;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author stefa
 *
 */
public class WizardPageRace extends WizardPage implements ResponsiveControl {

	private final static Logger logger = System.getLogger("splittermond.gen.wizard");

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageRace.class.getName());

	private SpliMoCharacterGenerator charGen;

	private DataItemPane<Race> contentPane;

	private ChoiceBox<Gender> cbGender;
	private Button btnRoll;
	private TextField tfSize;
	private TextField tfWeight;
	private TextField tfSkin;
	private TextField tfHair;
	private TextField tfEyes;
	private FlowPane customNode1;
	
	private boolean updating;

	//-------------------------------------------------------------------
	public WizardPageRace(Wizard wizard, SpliMoCharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		contentPane = new DataItemPane<Race>(
				r -> SplitterTools.requirementResolver(Locale.getDefault()).apply(r),
				m -> SplitterTools.modificationResolver(Locale.getDefault()).apply(m)
				);
		contentPane.setShowModificationsInDescription(false);
		contentPane.setId("species");
		contentPane.setUseForChoices(charGen.getModel());
		contentPane.setImageConverter(new Callback<Race,Image>(){
			public Image call(Race value) {
				InputStream in = SpliMoCharacterViewLayout.class.getResourceAsStream("images/race_"+value.getId()+".png");
				if (in!=null)
					return new Image(in);
				logger.log(Level.ERROR, "Missing resource "+SpliMoCharacterViewLayout.class.getName()+" + images/race"+value.getId()+".png");
				return null;
			}});
		contentPane.setModificationConverter((m) -> SplitterTools.getModificationString(contentPane.getSelectedItem(),m, Locale.getDefault()));
		contentPane.setReferenceTypeConverter(new SMReferenceTypeConverter<>());
		contentPane.setNameConverter( species -> {
			if (species==null) return "-";
			return species.getName();
		});
		contentPane.setChoiceConverter((c) -> SplitterTools.getChoiceString(contentPane.getSelectedItem(), c));
		contentPane.setDecisionHandler( (r,c) -> {
			logger.log(Level.WARNING, "ToDo: make decision");
			SplitterJFXUtil.openDecisionDialog(r, c, charGen.getRaceController());
			logger.log(Level.WARNING, "done making decision");
		});
		contentPane.setItems(SplitterMondCore.getItemList(Race.class));
		contentPane.showDecisionColumnProperty().set(false);

		/*
		 * Custom node
		 */
		btnRoll  = new Button(ResourceI18N.get(RES, "button.roll"));
		btnRoll.setStyle("-fx-background-color: -fx-default-button;");
		cbGender = new ChoiceBox<>();
		cbGender.getItems().addAll(Gender.values());
		cbGender.setConverter(new StringConverter<Gender>() {
			public String toString(Gender key) {return ResourceI18N.get(RES,"gender."+key.name().toLowerCase());}
			public Gender fromString(String key) {return Gender.valueOf(key.toUpperCase());}
		});
		tfSize   = new TextField();
		tfSize.setPrefColumnCount(3);
		tfWeight = new TextField();
		tfWeight.setPrefColumnCount(3);
		tfSkin   = new TextField();
		tfEyes   = new TextField();
		tfHair   = new TextField();
	}

	//-------------------------------------------------------------------
	private void addToCustom(Node node, String prop) {
		if (node instanceof HBox)
			((HBox)node).setAlignment(Pos.CENTER);

		Label ret = new Label(ResourceI18N.get(RES, prop));
		ret.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		customNode1.getChildren().add(new VBox(3, ret, node));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(contentPane);
		customNode1 = new FlowPane(10, 10);
		addToCustom(cbGender, "label.gender");
		addToCustom(new HBox(5, tfSize, new Label("cm")), "label.size");
		addToCustom(new HBox(5, tfWeight, new Label("kg")), "label.weight");
		addToCustom(tfHair, "label.hair");
		addToCustom(tfEyes, "label.eyes");
		addToCustom(tfSkin, "label.skin");

		VBox cust = new VBox(10, btnRoll, customNode1);
		cust.setId("wizard-race-data");
		contentPane.setCustomNode1(new NodeWithTitle(ResourceI18N.get(RES,"tab.custom"), cust));

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			customNode1.setStyle("-fx-max-width: 40em");
		} else {
			customNode1.setStyle("-fx-max-width: 12em");
		}

	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		contentPane.selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.DEBUG, "Race changed to {0}",n);
			if (updating) return;
			RaceController ctrl = charGen.getRaceController();
			ctrl.select(n);
			ctrl.rollGender();
			refresh();
		});

		btnRoll.setOnAction(ev -> {
			logger.log(Level.INFO, "Roll");
			RaceController ctrl = charGen.getRaceController();
			ctrl.rollEyes();
			ctrl.rollGender();
			ctrl.rollHair();
			ctrl.rollSizeAndWeight();
			refresh();
		});

		cbGender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> charGen.getModel().setGender(n));
		tfSize.textProperty().addListener( (ov,o,n) -> {
			try {
				int size = Integer.parseInt(n);
				charGen.getModel().setSize(size);
				tfSize.getStyleClass().remove("invalid");
			} catch (NumberFormatException e) {
				if (!tfSize.getStyleClass().contains("invalid"))
					tfSize.getStyleClass().add("invalid");
			}
		});
	}

	//-------------------------------------------------------------------
	void refresh() {
		if (updating) return;
		updating=true;
		try {
			SpliMoCharacter model = charGen.getModel();
			contentPane.setItems(charGen.getRaceController().getAvailable());
			if (model.getRace() != null) {
				Race race = SplitterMondCore.getItem(Race.class, model.getRace());
				contentPane.setSelectedItem(race);
			}
			cbGender.setValue(model.getGender());
			tfHair.setText(charGen.getModel().getHairColor());
			tfEyes.setText(charGen.getModel().getEyeColor());
			tfSkin.setText(charGen.getModel().getSkinColor());
			try {
				tfSize.setText(String.valueOf(model.getSize()));
				tfWeight.setText(String.valueOf(model.getWeight()));
			} catch (Exception e) {
				logger.log(Level.WARNING, "Found invalid data in textfields: "+e);
			}
		} finally {
			updating=false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		super.setResponsiveMode(value);

		if (value==WindowMode.MINIMAL) {
			customNode1.setStyle("-fx-max-width: 40em");
		} else {
			customNode1.setStyle("-fx-max-width: 12em");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}
}

package de.rpgframework.eden.foundry.splimo;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.Power;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.ItemTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.foundry.Language;
import de.rpgframework.eden.foundry.Module;
import de.rpgframework.eden.foundry.Pack;
import de.rpgframework.foundry.ActorData;
import de.rpgframework.foundry.ItemData;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
public class SplittermondCompendiumFactory {
	
	private final static String VALIDCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private final static Random RANDOM = new Random();
	
	private final static Logger logger = System.getLogger("foundry.splittermond");
	public static String IMGROOT = "src/main/resources";

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	//-------------------------------------------------------------------
	public final static String createRandomID() {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<16; i++) {
			buf.append(VALIDCHARS.charAt(RANDOM.nextInt(VALIDCHARS.length())));
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public static String createSourceText(DataItem item) {
		List<String> elements = new ArrayList<>();
		boolean shorted = item.getPageReferences().size()>2;
		String language = Locale.getDefault().getLanguage();
		for (PageReference ref : item.getPageReferences()) {
			if (!ref.getLanguage().equals(language))
				continue;
			if (shorted) {
				elements.add( ref.getProduct().getShortName(Locale.getDefault())+" "+ref.getPage() );				
			} else {
				elements.add( ref.getProduct().getName(Locale.getDefault())+" "+ref.getPage() );
			}
		}

		return String.join(", ", elements);
	}

	//-------------------------------------------------------------------
	public static Module createCompendium(Player player, String hostport, Collection<DataSet> sets, Function<Collection<PageReference>,Locale[]> localeCallback, boolean shallow) throws IOException {
		Module module = new Module();
		module.setName("splittermond-data");
		module.setTitle("Splittermond Daten");
		if (shallow) {
			module.setVersion("0.0.3"); // For JSON
		} else {
			module.setVersion("0.0.2");  // For ZIP
		}
		module.setMinimumCoreVersion("0.8.0");
		module.setCompatibleCoreVersion("9.251");
		module.setAuthor("Stefan Prelle, Simon Reuter");

		module.fos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(module.fos);

		createSpells(module, zipOut, shallow);
		createFeatureTypes(module, zipOut, shallow);
		createCreatureFeatureTypes(module, zipOut, shallow);
		createCreatures(module, zipOut, shallow);
		createItems(module, zipOut, shallow);
		createPowers(module, zipOut, shallow);
		createMasterships(module, zipOut, shallow);

		for (Language lang : module.getLanguages()) {
			ZipEntry zipEntry = new ZipEntry(lang.getPath());
			zipOut.putNextEntry(zipEntry);
			zipOut.write(gson.toJson(lang.keys).getBytes(Charset.forName("UTF-8")));
		}
		//		Language lang= new Language();
		//		lang.setName("splittermond-translations");
		//		lang.setLang("de");
		//		lang.setPath("./lang/de.json");
		//		module.getLanguages().add(lang);

        // module.json
		if (hostport==null) hostport="localhost";
		if (player!=null) {
			module.setManifest("http://"+player.getLogin()+":"+player.getPassword()+"@"+hostport+"/api/foundry/splittermond/compendium/module.json");
		} else {
			module.setManifest("http://"+hostport+"/api/foundry/splittermond/compendium/module.json");
		}
		module.setDownload(module.getManifest().substring(0, module.getManifest().length()-12)+".zip");
		String json = gson.toJson(module);
		ZipEntry zipEntry = new ZipEntry("module.json");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(json.getBytes());

		zipOut.close();
		module.fos.close();
		return module;
	}

	//-------------------------------------------------------------------
	private static void createSpells(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-spells");
		pack.setLabel("Zauber");
		pack.setEntity("Item");
		pack.setPath("packs/spells.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Spell spell : SplitterMondCore.getItemList(Spell.class)) {

			module.addTranslation("de", "spell."+spell.getId()+".desc", spell.getDescription(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".name", spell.getName(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".enhan", spell.getEnhancementDescription(Locale.GERMAN));
			
			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			ItemData<de.rpgframework.splittermond.foundry.Spell> entry = Converter.convert(spell, Locale.GERMAN);
			entry._id  = createRandomID();
//			entry.name = spell.getName(Locale.GERMAN);
//			entry.type = "spell";
//			entry.img  = "icons/svg/mystery-man.svg";
//
//			de.rpgframework.splittermond.foundry.Spell data = new de.rpgframework.splittermond.foundry.Spell();
//			data.genesisId   = spell.getId();
////			data.description = spell.getDescription(Locale.GERMAN);
//			data.difficulty = spell.getDifficultyString();
//			data.costs = SplitterTools.getFocusString(spell.getCost());
//			data.castDuration = spell.getCastDurationString();
//			data.range = spell.getCastRange()+"m";
//			data.castRangeMeter = spell.getCastRange();
//			data.castTicks = spell.getCastDurationTicks();
//			if (spell.getEffectRange()!=null) {
//				data.effectArea = spell.getEffectRangeString();
//			}
//			data.types = spell.getTypes().stream().map( s -> s.name()).collect(Collectors.toList());
//
//			List<String> availableIn = new ArrayList<>();
//			for (SpellSchoolEntry entry2 : spell.getSchools()) {
//				availableIn.add( entry2.getSchool().getId()+" "+entry2.getLevel());
//			}
//			data.availableIn = String.join(",", availableIn);
//			data.enhancementCosts = spell.getEnhancementString();
//			data.enhancementDescription = spell.getEnhancementDescription(Locale.GERMAN);
//			for (SpellEnhancementType type : spell.getEnhancementtype()) {
//				switch (type) {
//				case CHANNELIZED_FOCUS: data.degreeOfSuccessOptions.channelizedFocus=true; break;
//				case EXHAUSTED_FOCUS  : data.degreeOfSuccessOptions.exhaustedFocus=true; break;
//				case CONSUMED_FOCUS   : data.degreeOfSuccessOptions.consumedFocus=true; break;
//				case DAMAGE           : data.degreeOfSuccessOptions.damage=true; break;
//				case RELEASETIME      : data.degreeOfSuccessOptions.castDuration=true; break;
//				case SPELLDURATION    : data.degreeOfSuccessOptions.effectDuration=true; break;
//				case EFFECT_RANGE     : data.degreeOfSuccessOptions.effectrange=true; break;
//				case RANGE            : data.degreeOfSuccessOptions.range=true; break;
//				}
//			}
//			LogManager.getLogger("splittermond").error("Spell '"+spell.getId()+"': "+availableIn);
//			//			if (spell.getCost().getChannelled()>0) {
//			//				data.costK = spell.getCost().getChannelled() - spell.getCost().getConsumed();
//			//			} else {
//			//				data.costE = spell.getCost().getExhausted() - spell.getCost().getConsumed();
//			//			}
//			//			data.costV = spell.getCost().getConsumed();
//			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}
		System.out.println("Habe "+SplitterMondCore.getItemList(Spell.class).size()+" Zauber exportiert");

		ZipEntry zipEntry = new ZipEntry("packs/spells.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
		return;
	}

	//-------------------------------------------------------------------
	private static void addImages(ZipOutputStream zipOut, String type, String id, ActorData<?> entry) throws IOException {
		// Check for image
		String imgPath = "images/"+type+"/" + id + ".webp";
		Path path = Paths.get(IMGROOT, imgPath);
		if (Files.exists(path)) {
//			logger.log(Level.WARNING, "Found image "+path);
			FileInputStream fins = new FileInputStream(path.toFile());
			entry.img = "modules/splittermond-data/" + imgPath;
			ZipEntry zipEntry = new ZipEntry(imgPath);
			zipOut.putNextEntry(zipEntry);
			zipOut.write(fins.readAllBytes());
			fins.close();
		} else {
			logger.log(Level.WARNING, "Missing image "+path);
		}
		
		// Check for token
		String tokPath = "tokens/"+type+"/" + id + ".png";
		Path token = Paths.get(IMGROOT, tokPath);
		if (Files.exists(token)) {
//			logger.log(Level.WARNING, "Found token "+tokPath);
			FileInputStream fins = new FileInputStream(token.toFile());
			entry.token.img = "modules/splittermond-data/" + tokPath;
			ZipEntry zipEntry = new ZipEntry(tokPath);
			zipOut.putNextEntry(zipEntry);
			zipOut.write(fins.readAllBytes());
			fins.close();
		} else {
			logger.log(Level.WARNING, "Missing token "+tokPath);
			if (Files.exists(path)) {
				logger.log(Level.WARNING, "Use image alternative "+path);
				FileInputStream fins = new FileInputStream(path.toFile());
				entry.token.img = "modules/splittermond-data/" + path;
				ZipEntry zipEntry = new ZipEntry(tokPath);
				zipOut.putNextEntry(zipEntry);
				zipOut.write(fins.readAllBytes());
				fins.close();
			}
		}
		
	}

	//-------------------------------------------------------------------
	private static void createCreatures(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-creatures");
		pack.setLabel("Kreaturen");
		pack.setEntity("Actor");
		pack.setPath("packs/creatures.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Creature tmp : SplitterMondCore.getItemList(Creature.class)) {
			module.addTranslation("de", "creature."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "creature."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ActorData<de.rpgframework.splittermond.foundry.Lifeform> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id  = createRandomID();
			
			addImages(zipOut, "creature", tmp.getId(), entry);
			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry("packs/creatures.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
		return;
	}

	//-------------------------------------------------------------------
	private static void createItems(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-weapons");
		pack.setLabel("Ausrüstung/Waffen");
		pack.setEntity("Item");
		pack.setPath("packs/equip-weapons.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);
		pack = new Pack();
		pack.setName("splittermond-armor");
		pack.setLabel("Ausrüstung/Rüstungen");
		pack.setEntity("Item");
		pack.setPath("packs/equip-armors.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);
		pack = new Pack();
		pack.setName("splittermond-equip");
		pack.setLabel("Ausrüstung/Sonstiges");
		pack.setEntity("Item");
		pack.setPath("packs/equip-other.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer bufWeapon = new StringBuffer();
		StringBuffer bufArmor  = new StringBuffer();
		StringBuffer bufOther  = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (ItemTemplate tmp : SplitterMondCore.getItemList(ItemTemplate.class)) {
			module.addTranslation("de", "item."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "item."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ItemData<de.rpgframework.splittermond.foundry.Gear> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id = createRandomID();

			switch (tmp.getType()) {
			case WEAPON_CLOSE: case WEAPON_RANGED:
//				logger.log(Level.DEBUG, (new GsonBuilder()).setPrettyPrinting().create().toJson(entry) );
				bufWeapon.append(gson.toJson(entry));
				bufWeapon.append('\n');
				break;
			case ARMOR:
				bufArmor.append(gson.toJson(entry));
				bufArmor.append('\n');
				break;
			default:
				bufOther.append(gson.toJson(entry));
				bufOther.append('\n');
			}
		}

		ZipEntry zipEntry = new ZipEntry("packs/equip-weapons.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufWeapon.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();

		zipEntry = new ZipEntry("packs/equip-armors.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufArmor.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
		
		zipEntry = new ZipEntry("packs/equip-other.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufOther.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
	}

	//-------------------------------------------------------------------
	private static void createPowers(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-powers");
		pack.setLabel("Stärken");
		pack.setEntity("Item");
		pack.setPath("packs/strengths.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Power tmp : SplitterMondCore.getItemList(Power.class)) {
			module.addTranslation("de", "strength."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "strength."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ItemData<de.rpgframework.splittermond.foundry.Power> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id  = createRandomID();

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry("packs/strengths.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
	}

	//-------------------------------------------------------------------
	private static void createMasterships(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-mastership");
		pack.setLabel("Meisterschaften");
		pack.setEntity("Item");
		pack.setPath("packs/masterships.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		List<Mastership> list = SplitterMondCore.getItemList(Mastership.class);
		Collections.sort(list, new Comparator<Mastership>() {

			@Override
			public int compare(Mastership o1, Mastership o2) {
				int cmp1 = o1.getAssignedSkill().compareTo(o2.getAssignedSkill());
				if (cmp1!=0) return cmp1;
				return o1.getId().compareTo(o2.getId());
			}
		});
		for (Mastership tmp : list) {
//			System.out.println(tmp.getAssignedSkill()+"/"+ tmp.getId());
			String desc =  tmp.getDescription(Locale.GERMAN);
			module.addTranslation("de", "mastery."+tmp.getId()+".desc", desc);
			module.addTranslation("de", "mastery."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ItemData<de.rpgframework.splittermond.foundry.Mastership> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id  = createRandomID();

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}
		System.out.println("Habe "+SplitterMondCore.getItemList(Mastership.class).size()+" Meisterschaften exportiert");

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
	}

	//-------------------------------------------------------------------
	private static void createFeatureTypes(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-featuretypes");
		pack.setLabel("Merkmale");
		pack.setEntity("Item");
		pack.setPath("packs/featuretypes.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (FeatureType tmp : SplitterMondCore.getItemList(FeatureType.class)) {
			module.addTranslation("de", "featuretype."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "featuretype."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ItemData<de.rpgframework.splittermond.foundry.FeatureType> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id  = createRandomID();
			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
	}

	//-------------------------------------------------------------------
	private static void createCreatureFeatureTypes(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-npcfeature");
		pack.setLabel("Kreaturmerkmale");
		pack.setEntity("Item");
		pack.setPath("packs/npcfeature.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (CreatureFeature tmp : SplitterMondCore.getItemList(CreatureFeature.class)) {
			module.addTranslation("de", "npcfeature."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "npcfeature."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType> entry = Converter.convert(tmp, Locale.GERMAN);
			entry._id  = createRandomID();
			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		zipOut.closeEntry();
	}

}

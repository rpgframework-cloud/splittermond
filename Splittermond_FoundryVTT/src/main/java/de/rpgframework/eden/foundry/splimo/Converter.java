package de.rpgframework.eden.foundry.splimo;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Power;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellEnhancementType;
import org.prelle.splimo.SpellSchoolEntry;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.creature.CreatureWeapon;
import org.prelle.splimo.items.Availability;
import org.prelle.splimo.items.Complexity;
import org.prelle.splimo.items.FeatureList;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.ItemSubType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.persist.WeaponDamageConverter;

import de.rpgframework.foundry.ActorData;
import de.rpgframework.foundry.ItemData;
import de.rpgframework.foundry.TokenData;
import de.rpgframework.foundry.TokenData.TokenBarData;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.splittermond.foundry.Gear;
import de.rpgframework.splittermond.foundry.JSONSkillValue;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Converter {

	private final static Logger logger = System.getLogger(Converter.class.getPackageName());

	private static WeaponDamageConverter dmgConv = new WeaponDamageConverter();


	// -------------------------------------------------------------------
	public static String createSourceText(DataItem item, Locale loc) {
		List<String> elements = new ArrayList<>();
		boolean shorted = item.getPageReferences().size() > 2;
		String language = loc.getLanguage();
		for (PageReference ref : item.getPageReferences()) {
			if (!ref.getLanguage().equals(language))
				continue;
			if (shorted) {
				elements.add(ref.getProduct().getShortName(Locale.getDefault()) + " " + ref.getPage());
			} else {
				elements.add(ref.getProduct().getName(Locale.getDefault()) + " " + ref.getPage());
			}
		}

		if (elements.isEmpty())
			logger.log(Level.WARNING, "No source definition for "+item.getTypeString()+":"+item.getId()+" with "+item.getPageReferences());
		return String.join(", ", elements);
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Spell> convert(Spell item, Locale loc) {
		de.rpgframework.splittermond.foundry.Spell spell = new de.rpgframework.splittermond.foundry.Spell();
		spell.genesisId = item.getId();
		List<String> schools = new ArrayList<>();
		for (SpellSchoolEntry entry2 : item.getSchools()) {
			schools.add(entry2.getSchool().getId() + ":" + entry2.getLevel());
		}

		spell.availableIn = String.join(",", schools);
		spell.difficulty = item.getDifficultyString();
		spell.castDuration = item.getCastDurationString();
		spell.range = item.getCastRange() + "m";
		spell.castRangeMeter = item.getCastRange();
		spell.castTicks = item.getCastDurationTicks();
		spell.costs = SplitterTools.getFocusString(item.getCost());
		spell.effectDuration = item.getSpellDuration();
		spell.enhancementDescription = item.getEnhancementDescription(loc);
		spell.enhancementCosts = item.getEnhancementString();
		if (item.getEffectRange() != null) {
			spell.effRangeMeter = item.getEffectRange();
			spell.effectArea = item.getEffectRange() + "m";
		}
		spell.description = item.getDescription(loc);
		spell.types = item.getTypes().stream().map(s -> s.name()).collect(Collectors.toList());
		for (SpellEnhancementType type : item.getEnhancementtype()) {
			switch (type) {
			case CHANNELIZED_FOCUS:
				spell.degreeOfSuccessOptions.channelizedFocus = true;
				break;
			case EXHAUSTED_FOCUS:
				spell.degreeOfSuccessOptions.exhaustedFocus = true;
				break;
			case CONSUMED_FOCUS:
				spell.degreeOfSuccessOptions.consumedFocus = true;
				break;
			case DAMAGE:
				spell.degreeOfSuccessOptions.damage = true;
				break;
			case RELEASETIME:
				spell.degreeOfSuccessOptions.castDuration = true;
				break;
			case SPELLDURATION:
				spell.degreeOfSuccessOptions.effectDuration = true;
				break;
			case EFFECT_RANGE:
				spell.degreeOfSuccessOptions.effectrange = true;
				break;
			case RANGE:
				spell.degreeOfSuccessOptions.range = true;
				break;
			}
		}

		return new ItemData<de.rpgframework.splittermond.foundry.Spell>(item.getName(loc), "spell", spell);
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Spell> convert(SpellValue val, Locale loc) {
		ItemData<de.rpgframework.splittermond.foundry.Spell> ret = convert(val.getResolved(), loc);
		ret.getData().skill = val.getSkill().getId();

		return ret;
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Mastership> convert(Mastership item, Locale loc) {
		de.rpgframework.splittermond.foundry.Mastership data = new de.rpgframework.splittermond.foundry.Mastership();
		data.genesisId = item.getId();
		data.source = createSourceText(item, loc);
		data.availableIn = item.getSkill().getId();
		data.level = item.getLevel();
		data.description = item.getDescription(loc);

		return new ItemData<de.rpgframework.splittermond.foundry.Mastership>(item.getName(loc), "mastery", data);
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Mastership> convert(MastershipReference val, Locale loc) {
		ItemData<de.rpgframework.splittermond.foundry.Mastership> ret = convert(val.getMastership(), loc);

		return ret;
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Power> convert(Power item, Locale loc) {
		de.rpgframework.splittermond.foundry.Power data = new de.rpgframework.splittermond.foundry.Power();
		data.genesisId = item.getId();
		data.genesisId = item.getId();
		data.source = createSourceText(item, loc);
		data.level = item.hasLevel();
		data.description = item.getDescription(loc);
		switch (item.getSelectable()) {
		case MULTIPLE:
			data.multiSelectable = true;
			break;
		case GENERATION:
			data.onCreationOnly = true;
			break;
		}

		return new ItemData<de.rpgframework.splittermond.foundry.Power>(item.getName(loc), "strength", data);
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Power> convert(PowerValue val, Locale loc) {
		ItemData<de.rpgframework.splittermond.foundry.Power> ret = convert(val.getModifyable(), loc);

		return ret;
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType> convert(CreatureFeature item, Locale loc) {
		de.rpgframework.splittermond.foundry.CreatureFeatureType data = new de.rpgframework.splittermond.foundry.CreatureFeatureType();
		data.genesisId = item.getId();
		data.source = createSourceText(item, loc);
		data.level   = item.hasLevel();
		data.description = item.getDescription(Locale.GERMAN);

		return new ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType>(item.getName(loc), "npcfeature", data);
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType> convert(CreatureFeatureValue val, Locale loc) {
		ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType> ret = convert(val.getModifyable(), loc);

		return ret;
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.FeatureType> convert(FeatureType item, Locale loc) {
		de.rpgframework.splittermond.foundry.FeatureType data = new de.rpgframework.splittermond.foundry.FeatureType();
		data.genesisId = item.getId();
		data.source = createSourceText(item, loc);
		data.level   = item.hasLevel();
		data.description = item.getDescription(Locale.GERMAN);

		return new ItemData<de.rpgframework.splittermond.foundry.FeatureType>(item.getName(loc), "feature", data);
	}

	// -------------------------------------------------------------------
	public static ActorData<de.rpgframework.splittermond.foundry.Lifeform> convert(Creature tmp, Locale loc) {
//		data.genesisId = item.getId();
		de.rpgframework.splittermond.foundry.Lifeform data = new de.rpgframework.splittermond.foundry.Lifeform();
		data.biography = tmp.getDescription(loc);
		if (data.skills==null)
			data.skills = new LinkedHashMap<String,JSONSkillValue>();
		data.attributes.charisma.value = tmp.getAttribute(Attribute.CHARISMA).getModifiedValue();
		data.attributes.agility.value = tmp.getAttribute(Attribute.AGILITY).getModifiedValue();
		data.attributes.intuition.value = tmp.getAttribute(Attribute.INTUITION).getModifiedValue();
		data.attributes.constitution.value = tmp.getAttribute(Attribute.CONSTITUTION).getModifiedValue();
		data.attributes.mystic.value = tmp.getAttribute(Attribute.MYSTIC).getModifiedValue();
		data.attributes.strength.value = tmp.getAttribute(Attribute.STRENGTH).getModifiedValue();
		data.attributes.mind.value = tmp.getAttribute(Attribute.MIND).getModifiedValue();
		data.attributes.willpower.value = tmp.getAttribute(Attribute.WILLPOWER).getModifiedValue();

		data.derivedAttributes.bodyresist.value = tmp.getAttribute(Attribute.BODYRESIST).getModifiedValue();
		data.derivedAttributes.mindresist.value = tmp.getAttribute(Attribute.MINDRESIST).getModifiedValue();
		data.derivedAttributes.defense.value = tmp.getAttribute(Attribute.DEFENSE).getModifiedValue();
		data.derivedAttributes.size.value = tmp.getAttribute(Attribute.SIZE).getModifiedValue();
		data.derivedAttributes.healthpoints.value = tmp.getAttribute(Attribute.LIFE).getModifiedValue();
		data.derivedAttributes.focuspoints.value = tmp.getAttribute(Attribute.FOCUS).getModifiedValue();
		data.derivedAttributes.speed.value = tmp.getAttribute(Attribute.SPEED).getModifiedValue();

		data.level = tmp.getLevelAlone()+"/"+tmp.getLevelGroup();
		data.type  = String.join(", ",tmp.getCreatureTypes().stream().map(f -> f.getName(Locale.GERMAN)).collect(Collectors.toList()));
		data.damageReduction.value = tmp.getAttribute(Attribute.DAMAGE_REDUCTION).getModifiedValue();

		ActorData<de.rpgframework.splittermond.foundry.Lifeform>entry = new ActorData<de.rpgframework.splittermond.foundry.Lifeform>(tmp.getName(loc), "npc", data);
		entry.token= new TokenData();
		entry.token.displayName=20;
		entry.token.displayBars=20;
		entry.token.bar1 = new TokenBarData("data.healthBar");
		entry.token.bar2 = new TokenBarData("data.focusBar");
		entry._id = SplittermondCompendiumFactory.createRandomID();

		// SKills
		for (SMSkillValue sval : tmp.getSkillValues()) {
			SMSkill skill = sval.getSkill();
			JSONSkillValue jVal = new JSONSkillValue();
			jVal.value = sval.getModifiedValue();
//			jVal.points = sval.getDistributed();
			if (skill==null) {
				logger.log(Level.ERROR, "Null skill for "+tmp);
			}
			int a1 = tmp.getAttribute((skill.getAttribute()!=null)?skill.getAttribute():Attribute.AGILITY).getModifiedValue();
			int a2= tmp.getAttribute((skill.getAttribute2()!=null)?skill.getAttribute2():Attribute.STRENGTH).getModifiedValue();
			jVal.points = jVal.value - a1 - a2;
			jVal.sortKey  = skill.getType().ordinal()+"-"+skill.getName();
			data.skills.put(skill.getId(), jVal);

			// Masteries
			for (MastershipReference ref : sval.getMasterships()) {
				entry.addItem(convert(ref, loc));
			}
		}


		// Spells
		for (SpellValue item : tmp.getSpells()) {
			entry.addItem(Converter.convert(item, loc));
		}

		// Creature Features
		for (CreatureFeatureValue item : tmp.getFeatures()) {
			entry.addItem(convert(item, loc));
		}

		// Attacks ( = Equipment)
		for (CreatureWeapon weapon : tmp.getCreatureWeapons()) {
			de.rpgframework.splittermond.foundry.Gear weap = new de.rpgframework.splittermond.foundry.Gear();
			weap.skill = weapon.getSkill().getId();
			weap.weaponSpeed = weapon.getSpeed();
			weap.damage = dmgConv.write(weapon.getDamage());
			weap.weaponSpeed = weapon.getSpeed();

			String name = "Undefiniert";
			if (!weapon.getName().equals("body") & !weapon.getName().equals("Körper")) {
				ItemTemplate item = SplitterMondCore.getItem(ItemTemplate.class, weapon.getName());
				if (item==null) {
					logger.log(Level.ERROR,"Unknown weapon reference '"+weapon.getName()+"' in creature "+tmp.getId());
					name = weapon.getName();
				} else {
					logger.log(Level.ERROR,"Add weapon {0} to "+tmp.getId(), item.getId());
					name = item.getName(Locale.GERMAN);
					weap.hardness = item.getAttribute(SMItemAttribute.RIGIDITY).getDistributed();
					if (item.getAttribute(SMItemAttribute.ATTRIBUTE1)!=null)
						weap.attribute1 = item.getAttribute(SMItemAttribute.ATTRIBUTE1).getRawValue();
					if (item.getAttribute(SMItemAttribute.ATTRIBUTE2)!=null)
						weap.attribute2 = item.getAttribute(SMItemAttribute.ATTRIBUTE2).getRawValue();
					ItemData<Gear> gear = convert(item,loc);
					gear.getData().equipped=true;
					gear._id = SplittermondCompendiumFactory.createRandomID();
					entry.addItem(gear);
					continue;
				}
			} else {
				name = "Körper";
			}
			weap.equipped=true;
			ItemData<de.rpgframework.splittermond.foundry.Gear> gear = new ItemData<de.rpgframework.splittermond.foundry.Gear>(name, "weapon", weap);
			gear._id = SplittermondCompendiumFactory.createRandomID();
			entry.addItem(gear);
		}

		return entry;
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Gear> convert(ItemTemplate tmp, Locale loc) {
		de.rpgframework.splittermond.foundry.Gear data = new de.rpgframework.splittermond.foundry.Gear();
		data.genesisId = tmp.getId();
		data.source = createSourceText(tmp, loc);
		data.description = tmp.getDescription(loc);
		try {
			switch (tmp.getType()) {
			case WEAPON_RANGED:
			case WEAPON_CLOSE:
				if (tmp.getAttribute(SMItemAttribute.SPEED)!=null)
					data.weaponSpeed = tmp.getAttribute(SMItemAttribute.SPEED).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.DAMAGE)!=null)
					data.damage = dmgConv.write(tmp.getAttribute(SMItemAttribute.DAMAGE).getDistributed());
				if (tmp.getAttribute(SMItemAttribute.ATTRIBUTE1)!=null) {
					data.attribute1 = Attribute.valueOf(tmp.getAttribute(SMItemAttribute.ATTRIBUTE1).getRawValue()).name().toLowerCase();
				}
				if (tmp.getAttribute(SMItemAttribute.ATTRIBUTE2)!=null) {
					data.attribute2 = Attribute.valueOf(tmp.getAttribute(SMItemAttribute.ATTRIBUTE2).getRawValue()).name().toLowerCase();
				}
				if (tmp.getAttribute(SMItemAttribute.SKILL)!=null) {
					data.skill = tmp.getAttribute(SMItemAttribute.SKILL).getRawValue();
				}
				break;
			case ARMOR:
				if (tmp.getAttribute(SMItemAttribute.DEFENSE)!=null)
					data.defenseBonus = tmp.getAttribute(SMItemAttribute.DEFENSE).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.DAMAGE_REDUCTION)!=null)
					data.damageReduction = tmp.getAttribute(SMItemAttribute.DAMAGE_REDUCTION).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.SPEED)!=null)
					data.tickMalus = tmp.getAttribute(SMItemAttribute.SPEED).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.HANDICAP)!=null)
					data.handicap = tmp.getAttribute(SMItemAttribute.HANDICAP).getDistributed();
				break;
			default:
			}
			if (tmp.getAttribute(SMItemAttribute.AVAILABILITY)!=null) {
				switch (((Availability)tmp.getAttribute(SMItemAttribute.AVAILABILITY).getValue())) {
				case VILLAGE:    data.availability="village"; break;
				case SMALL_TOWN: data.availability="town"; break;
				case LARGE_TOWN: data.availability="city"; break;
				case CAPITAL   : data.availability="metropolis"; break;
				}
			}
			if (tmp.getAttribute(SMItemAttribute.COMPLEXITY)!=null)
				data.complexity  = ((Complexity)tmp.getAttribute(SMItemAttribute.COMPLEXITY).getValue()).getID();
			if (tmp.getAttribute(SMItemAttribute.LOAD)!=null)
				data.weight        = tmp.getAttribute(SMItemAttribute.LOAD).getDistributed();
			if (tmp.getAttribute(SMItemAttribute.PRICE)!=null)
				data.price        = tmp.getAttribute(SMItemAttribute.PRICE).getDistributed();
			if (tmp.getAttribute(SMItemAttribute.RIGIDITY)!=null)
				data.hardness     = tmp.getAttribute(SMItemAttribute.RIGIDITY).getDistributed();
			if (tmp.getAttribute(SMItemAttribute.FEATURES)!=null) {
				FeatureList list = tmp.getAttribute(SMItemAttribute.FEATURES).getValue();
				List<String> newList = new ArrayList<String>();
				list.forEach( (feat) -> newList.add(feat.getName(Locale.GERMAN)));
				data.features     = String.join(", ", newList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		switch (tmp.getType()) {
		case WEAPON_CLOSE: case WEAPON_RANGED:
			return new ItemData<de.rpgframework.splittermond.foundry.Gear>(tmp.getName(loc), "weapon", data);
		case ARMOR:
			if (tmp.getSubType()==ItemSubType.SHIELD)
				return new ItemData<de.rpgframework.splittermond.foundry.Gear>(tmp.getName(loc), "shield", data);
			return new ItemData<de.rpgframework.splittermond.foundry.Gear>(tmp.getName(loc), "armor", data);
		default:
			return new ItemData<de.rpgframework.splittermond.foundry.Gear>(tmp.getName(loc), "equipment", data);
		}
	}

	// -------------------------------------------------------------------
	public static ItemData<de.rpgframework.splittermond.foundry.Gear> convert(CarriedItem<ItemTemplate> val, Locale loc) {
		ItemData<de.rpgframework.splittermond.foundry.Gear> ret = convert(val.getModifyable(), loc);

		return ret;
	}

}

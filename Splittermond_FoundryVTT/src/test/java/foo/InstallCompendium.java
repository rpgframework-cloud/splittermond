package foo;



import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.splimo.SplitterMondCore;
import de.rpgframework.eden.foundry.Module;

import de.rpgframework.eden.foundry.splimo.SplittermondCompendiumFactory;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.PageReference;

public class InstallCompendium {

    public static void main(String[] args) {
    	Locale.setDefault(Locale.GERMAN);
    	
    	String name = (args.length>0)?args[0]:"unpacked";
    	String setname = (args.length>1)?args[1]:null;
    	
    	SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init();

		// Reduce datasets if requested
		if (setname!=null) {
			SplitterMondCore.getDataSets()
					.stream()
					.filter(set -> !set.getID().equals(setname))
					.forEach( set -> SplitterMondCore.removeDataSet(set));
		}
		
		// Where to export to?
		Path target = Paths.get(name);
        //Path target = Paths.get("/home/prelle/.local/share/FoundryVTT/Data/modules/splittermond-data");

        try {
        	
            unzipFolder(target);
            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void unzipFolder(Path target) throws IOException {
		List<DataSet> sets = SplitterMondCore.getDataSets();
//		sets.remove(1);
		Function<Collection<PageReference>,Locale[]> callback = (references) -> new Locale[] {Locale.GERMAN};
		Module modDeep = SplittermondCompendiumFactory.createCompendium(null, null, sets, callback, false);
		assertNotNull(modDeep);
		byte[] data = modDeep.fos.toByteArray();
		System.out.println("ZIP length = "+data.length);
		Files.write(Paths.get("/tmp/splittermond-data.zip"), data);

        try (ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(data))) {

            // list files in zip
            ZipEntry zipEntry = zis.getNextEntry();

            while (zipEntry != null) {
                boolean isDirectory = false;
                // example 1.1
                // some zip stored files and folders separately
                // e.g data/
                //     data/folder/
                //     data/folder/file.txt
                if (zipEntry.getName().endsWith(File.separator)) {
                    isDirectory = true;
                }

                Path newPath = zipSlipProtect(zipEntry, target);

                
                if (isDirectory) {
                    Files.createDirectories(newPath);
                } else {

                    // example 1.2
                    // some zip stored file path only, need create parent directories
                    // e.g data/folder/file.txt
                    if (newPath.getParent() != null) {
                        if (Files.notExists(newPath.getParent())) {
                            Files.createDirectories(newPath.getParent());
                        }
                    }

                    // copy files, nio
                    long sum= Files.copy(zis, newPath, StandardCopyOption.REPLACE_EXISTING);
                   	System.out.println("Extract "+zipEntry+" with "+sum+" bytes to "+newPath);

                    // copy files, classic
//                    try (FileOutputStream fos = new FileOutputStream(newPath.toFile())) {
//                        byte[] buffer = new byte[1024];
//                        int sum=0;
//                        int len;
//                        while ((len = zis.read(buffer)) > 0) {
//                            fos.write(buffer, 0, len);
//                            sum+=len;
//                         }
//                    	System.out.println("Extract "+zipEntry+" with "+sum+" bytes to "+newPath);
//                    }
                }

                zipEntry = zis.getNextEntry();

            }
            zis.closeEntry();

        }

    }

    // protect zip slip attack
    public static Path zipSlipProtect(ZipEntry zipEntry, Path targetDir)
        throws IOException {

        // test zip slip vulnerability
        // Path targetDirResolved = targetDir.resolve("../../" + zipEntry.getName());

        Path targetDirResolved = targetDir.resolve(zipEntry.getName());

        // make sure normalized file still has targetDir as its prefix
        // else throws exception
        Path normalizePath = targetDirResolved.normalize();
        if (!normalizePath.startsWith(targetDir)) {
            throw new IOException("Bad zip entry: " + zipEntry.getName());
        }

        return normalizePath;
    }

}

package example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.InputStream;
import java.util.Locale;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.DataSet;

public class DataLoadTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		DataSet core = new DataSet(this,RoleplayingSystem.SPLITTERMOND, "CORE", "foo", Locale.GERMAN);
		InputStream ins = getClass().getClassLoader().getResourceAsStream("example/skills.xml");
		assertNotNull("XML resource missing",ins);
		SplitterMondCore.loadSkills(core, ins);
//		SplittermondCore.loadDataItems(SkillList.class, Skill.class, core, ins);
		
		assertNotNull(SplitterMondCore.getSkill("athletics"));
		assertEquals("skill",SplitterMondCore.getSkill("athletics").getTypeString());
	}

}

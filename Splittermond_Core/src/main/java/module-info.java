open module splittermond.core {
	exports org.prelle.splimo.items;
	exports org.prelle.splimo.creature;
	exports org.prelle.splimo.modifications;
	exports org.prelle.splimo.persist;
	exports org.prelle.splimo.proc;
	exports org.prelle.splimo.companion.proc;
	exports org.prelle.splimo;

	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.rules;
	requires java.xml;
	requires simple.persist;
	requires com.google.gson;
}
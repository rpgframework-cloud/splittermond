/**
 *
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.ModifyableNumericalValue;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class PowerValue extends ComplexDataItemValue<Power>  implements ModifyableNumericalValue<Power> {

	/** Cannot be deselected by user (e.g. because it originates from race) */
	@Attribute
	private boolean fixed;

	//-------------------------------------------------------------------
	public PowerValue() {
		value = 0;
	}

	//-------------------------------------------------------------------
	public PowerValue(Power power) {
		this();
		this.ref = power.getId();
		resolved = power;
	}

	//-------------------------------------------------------------------
	public PowerValue(Power power, int count) {
		this(power);
		this.value = count;
		resolved = power;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (getResolved()==null)
			return "Unresolved:"+getKey()+" "+value;
		return getResolved().toString()+" "+value+"/"+getModifier()+" ("+outgoingModifications+")";
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof PowerValue) {
			PowerValue other = (PowerValue)o;
			if (!ref.equals(other.getKey())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	@Override
	public int hashCode() {
		return ref.hashCode();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the fixed
	 */
	public boolean isFixed() {
		return fixed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param fixed the fixed to set
	 */
	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableNumericalValue#getModifiedValue()
	 */
//	@Override
//	public int getModifiedValue() {
//		return getDistributed() + getModifier();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableNumericalValue#getModifier()
	 */
	@Override
	public int getModifier() {
		return getModifier(ValueType.NATURAL);
	}

	//--------------------------------------------------------------------
	private List<DataItemModification> getModificationsExceptRace() {
		List<DataItemModification> ret = new ArrayList<>();
		for (Modification mod : getIncomingModifications()) {
			if (mod.getSource()!=null && mod.getSource() instanceof Race)
				continue;
			if (mod instanceof DataItemModification) {
				ret.add((DataItemModification) mod);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public int getCostPoints() {
		if (!isAutoAdded()) {
			return resolved.hasLevel()?(getModifiedValue()*resolved.getCost()):resolved.getCost();
		}
		List<DataItemModification> list = getModificationsExceptRace();
		if (resolved.hasLevel()) {
			int mVal = list
					.stream()
					.collect(Collectors
							.summingInt(item -> (item instanceof ValueModification)?((ValueModification)item).getValue():1));
			return (mVal+value)*resolved.getCost();
		} else {
			if (!list.isEmpty()) return resolved.getCost();
			return 0;
		}
	}

}

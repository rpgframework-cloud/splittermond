/**
 *
 */
package org.prelle.splimo;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.persist.SkillConverter;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
@Root(name = "spellval")
public class SpellValue extends ComplexDataItemValue<Spell> {

	@Attribute(name="school")
	@AttribConvert(SkillConverter.class)
	private SMSkill skill;
	/** Spell was selected by reaching skill threshold for reaching this level */
	@Attribute(required=false)
	private Integer free;

	//-------------------------------------------------------------------
	public SpellValue() {
		free = 0;
	}

	//-------------------------------------------------------------------
	public SpellValue(Spell spell, SMSkill skill) {
		super(spell);
		this.skill = skill;
		free = -1;
	}

	//-------------------------------------------------------------------
	public SpellValue clone() {
		SpellValue ret = new SpellValue(resolved, skill);
		ret.setFreeLevel(free);
		return ret;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getKey()+" ("+skill+")("+free+")";
	}

	//-------------------------------------------------------------------
	public String getIDWithSchool() {
		return skill.getId()+":"+getKey();
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof SpellValue) {
			SpellValue other = (SpellValue)o;
			if (!ref.equals( other.getKey())) return false;
			return skill==other.getSkill();
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public SMSkill getSkill() {
		return skill;
	}

	//--------------------------------------------------------------------
	public boolean wasFree() {
		return free!=null && free>-1;
	}

	//--------------------------------------------------------------------
	/**
	 * Get skill level threshold reached which free selection was used
	 * for this spell
	 */
	public Integer getFreeLevel() {
		return free;
	}

	//--------------------------------------------------------------------
	public void setFreeLevel(Integer free) {
		this.free = free;
	}

	//--------------------------------------------------------------------
	public int getSpellLevel() {
		return resolved.getLevelInSchool(skill);
	}

}

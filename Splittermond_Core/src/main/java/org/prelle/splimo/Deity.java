/**
 * 
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splimo.persist.AspectConverter;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;
import de.rpgframework.genericrpg.modification.RecommendationModification;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="deity")
public class Deity extends DataItem {

	@ElementList(type=Aspect.class,convert=AspectConverter.class,entry="aspect")
	private List<Aspect> aspects;
	@Element
	private ModificationList modifications;
	@Element
	private CultureList cultures;

	//-------------------------------------------------------------------
	/**
	 */
	public Deity() {
		aspects = new ArrayList<>();
		modifications = new ModificationList();
		cultures = new CultureList();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public List<Aspect> getAspects() {
		return new ArrayList<Aspect>(aspects);
	}

	//-------------------------------------------------------------------
	public List<SMSkill> getFavoredSMSkills() {
		List<SMSkill> ret = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof RecommendationModification) {
				RecommendationModification sMod = (RecommendationModification)mod;
				if (sMod.getReferenceType()==SplittermondReference.SKILL) {
					SMSkill skill = SplitterMondCore.getSkill(sMod.getKey());
					if (!sMod.isNotRecommended())
						ret.add(skill);
				}
			} else {
				System.err.println("Deity "+id+" contains a modification I don't support: "+mod.getClass().getSimpleName());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public List<SMSkill> getUnusualSMSkills() {
		List<SMSkill> ret = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof RecommendationModification) {
				RecommendationModification sMod = (RecommendationModification)mod;
				if (sMod.getReferenceType()==SplittermondReference.SKILL) {
					SMSkill skill = SplitterMondCore.getSkill(sMod.getKey());
					if (sMod.isNotRecommended())
						ret.add(skill);
				}
			} else {
				System.err.println("Deity "+id+" contains a modification I don't support: "+mod.getClass().getSimpleName());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public List<Education> getFavoredEducations() {
		List<Education> ret = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof RecommendationModification) {
				RecommendationModification sMod = (RecommendationModification)mod;
				if (sMod.getReferenceType()==SplittermondReference.EDUCATION) {
					Education data = SplitterMondCore.getItem(Education.class, sMod.getKey());
					if (sMod.isNotRecommended())
						ret.add(data);
				}
			} else {
				System.err.println("Deity "+id+" contains a modification I don't support: "+mod.getClass().getSimpleName());
			}
		}
		return ret;
	}
	//-------------------------------------------------------------------
	public CultureList getFavoredCultures() {
		CultureList list = new CultureList();
		list.addAll(cultures);
		return list;
	}

}

/**
 *
 */
package org.prelle.splimo;

import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;

/**
 * @author prelle
 *
 */
public enum Attribute implements de.rpgframework.genericrpg.data.IAttribute {

	CHARISMA,
	AGILITY,
	INTUITION,
	CONSTITUTION,
	MYSTIC,
	STRENGTH,
	MIND,
	WILLPOWER,
	SPLINTER,

	SIZE,
	SPEED,
	INITIATIVE,
	LIFE,
	FOCUS,
	RESIST_DAMAGE,
	DAMAGE_REDUCTION,
	RESIST_MIND,
	RESIST_BODY,

	/** Aktive Abwehr */
	DEFENSE_DAMAGE,
	/** Zähigkeit */
	DEFENSE_BODY,
	/** Entschlossenheit */
	DEFENSE_MIND,

	// Für Stufe z.B. von Beschwörbaren Wesen, damit diese übr Formeln abrufbar ist
	LEVEL
	;

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(Attribute.class, Locale.GERMAN);

	private StringValueConverter<? extends Object> converter;

	//-------------------------------------------------------------------
	private Attribute() {
		// Having a default int converter breaks ENUMs
		//converter = new IntegerConverter();
	}

	//-------------------------------------------------------------------
	private Attribute(StringValueConverter<? extends Object> conv) {
		converter = conv;
	}

	//-------------------------------------------------------------------
	public String getShortName() {
		return getShortName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public String getShortName(Locale loc) {
		return ResourceI18N.get(RES, loc, "attribute."+this.name().toLowerCase()+".short");
	}

    //-------------------------------------------------------------------
    public String getName() {
        return getName(Locale.getDefault());
    }

    //-------------------------------------------------------------------
    public String getName(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "attribute."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
    }

    //-------------------------------------------------------------------
    public String getDescription() {
        return getDescription(Locale.getDefault());
    }

    //-------------------------------------------------------------------
    public String getDescription(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "attribute."+this.name().toLowerCase()+".desc");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
    }

	//-------------------------------------------------------------------
	public static Attribute[] primaryValues() {
		return new Attribute[]{CHARISMA,AGILITY,INTUITION,CONSTITUTION,MYSTIC,STRENGTH,MIND,WILLPOWER};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValues() {
		return new Attribute[]{SIZE,SPEED,INITIATIVE,LIFE,FOCUS,RESIST_DAMAGE,DAMAGE_REDUCTION,RESIST_BODY,RESIST_MIND,DEFENSE_DAMAGE,DEFENSE_BODY,DEFENSE_MIND};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValuesWithoutDR() {
		return new Attribute[]{SIZE,SPEED,INITIATIVE,LIFE,FOCUS,RESIST_DAMAGE,RESIST_BODY,RESIST_MIND,DEFENSE_DAMAGE,DEFENSE_BODY,DEFENSE_MIND};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValuesWithoutDRAndDefense() {
		return new Attribute[]{SIZE,SPEED,INITIATIVE,LIFE,FOCUS,RESIST_DAMAGE,RESIST_BODY,RESIST_MIND};
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		for (Attribute key : primaryValues())
			if (this==key) return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.IAttribute#isDerived()
	 */
	@Override
	public boolean isDerived() {
		return List.of(secondaryValues()).contains(this);
	}

	//-------------------------------------------------------------------
	public <T> StringValueConverter<T> getConverter() {
		return (StringValueConverter<T>) converter;
	}

}

package org.prelle.splimo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.prelle.simplepersist.SerializationException;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.persist.ReferenceException;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.DataStorage;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.items.GearTool;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ResolvableRequirement;
import de.rpgframework.splittermond.log.Logging;

/**
 * @author prelle
 *
 */
public class SplitterMondCore extends GenericCore {

	private static MultiLanguageResourceBundle i18NResources;

	//-------------------------------------------------------------------
	static {
		i18NResources = new MultiLanguageResourceBundle(SplitterMondCore.class, Locale.GERMAN);
		GearTool.setPerRPGStatsPhase1(RoleplayingSystem.SPLITTERMOND, SMGearTool.SM_PHASE1_STEPS);
		GearTool.setPerRPGStatsPhase2(RoleplayingSystem.SPLITTERMOND, SMGearTool.SM_PHASE2_STEPS);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public SplitterMondCore() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public static MultiLanguageResourceBundle getI18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<SMSkill> loadSkills(DataSet plugin, InputStream in) {
		Logging.logger.log(Level.DEBUG, "Load skills (Plugin="+plugin.getID()+")");
		List<SMSkill> list = new ArrayList<SMSkill>();
		DataStorage<Mastership> storage = getStorage(Mastership.class);
		DataStorage<SMSkillSpecialization> storage2 = getStorage(SMSkillSpecialization.class);
		try {
			list = GenericCore.loadDataItems(SkillList.class, SMSkill.class, plugin, in);
			for (SMSkill tmp : list) {
				for (Mastership master : tmp.getMasterships()) {
					master.assignSkill(tmp);
					master.assignToDataSet(plugin);
					storage.add(master); // This should eventually be removed
					if (!master.isCommon())  {
						storage.add(master, tmp);
					}
				}
				for (SkillSpecialization spec : tmp.getSpecializations()) {
					spec.setSkill(tmp);
					spec.assignToDataSet(plugin);
					storage2.add((SMSkillSpecialization)spec, tmp);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	//-------------------------------------------------------------------
	/**
	 * Allgemeine Meisterschaften werden unter ihrer ID gespeichert
	 * Normale Meisterschaften mit <skill>/<id>
	 * Alle Meisterschaften führen eine Liste von Skills mit, in denen sie gelten
	 */
	public static List<Mastership> loadMasterships(DataSet plugin, InputStream in) {
		Logging.logger.log(Level.DEBUG, "Load masterships (Plugin="+plugin.getID()+")");
		List<Mastership> ret = new ArrayList<Mastership>();
		try {
			DataStorage<SMSkill> storageSkills = getStorage(SMSkill.class);
			DataStorage<Mastership> storage = getStorage(Mastership.class);
			MastershipList toAdd = serializer.read(MastershipList.class, in);
			Logging.logger.log(Level.DEBUG, "Successfully loaded masterships list");
			// If not empty, add the set
			if (!sets.contains(plugin)) {
				sets.add(plugin);
			}

			// Set translation
			int count=0;
			int countCommon=0;
			for (Mastership master : toAdd) {
				if (master.isCommon())
					countCommon++;
				else
					count++;
				// Try to find skill
				List<SMSkill> skills = new ArrayList<>();
				String assignTo = master.getAssignedSkill();
				SMSkill skill = storageSkills.getItem(assignTo);
				if (skill!=null) {
					skills.add(skill);
				} else if (assignTo.contains(",")) {
					for (String key : assignTo.split(",")) {
						skill = storageSkills.getItem(key.trim());
						if (skill!=null) {
							skills.add(skill);
						} else {
							Logging.logger.log(Level.ERROR, "Mastership '"+master.getId()+"' lists unknown skill '"+key+"' as assignedTo");
						}
					}
				} else if ("ALL".equals(assignTo)) {
					skills = getItemList(SMSkill.class);
				} else if ("COMBAT".equals(assignTo)) {
					skills = getSkills(SkillType.COMBAT);
				} else if ("MAGIC".equals(assignTo)) {
					skills = getSkills(SkillType.MAGIC);
				} else {
					Logging.logger.log(Level.ERROR, "Unknown skill reference: "+assignTo);
					System.exit(1);
				}

				logger.log(Level.DEBUG, "Add {0} to {1}", master.getId(), skills);
				for (SMSkill skl: skills) {
					master.assignSkill(skl);
					skl.addMastership(master);
					if (master.isCommon()) {
						storage.add(master);
					} else {
						storage.add(master, skl);
					}
				}
				master.assignToDataSet(plugin);
				master.validate();
				ret.add(master);

				master.getName();
				if (master.getDescription().endsWith(".desc")) {
					Logging.logger.log(Level.ERROR, "Missing "+master.getDescription());
				}
			} // for master

			// Now validate
			for (Mastership master : toAdd) {
				// Validate requirements
				for (Requirement req : master.getRequirements()) {
					try {
						if (req instanceof ResolvableRequirement && !((ResolvableRequirement)req).resolve()) {
							Logging.logger.log(Level.ERROR, "Plugin: "+plugin.getID()+",  mastership="+master.getId()+"  has an unresolvable requirement "+req);
							System.exit(1);
						}
					} catch (ReferenceException e) {
						Logging.logger.log(Level.ERROR, "Plugin: "+plugin.getID()+",  mastership="+master.getId()+"  has an unresolvable reference to "+e.getMessage());
						System.exit(1);
					}
				}
				/*
				 * For grouped skills multiply masterships "journeyman", "expert" and "master" with all
				 * skill specializations
					 */
//					if (tmp.isGrouped() && ("journeyman".equals(master.getId()) || "expert".equals(master.getId()) || "master".equals(master.getId()))) {
//						for (SkillSpecialization focus : tmp.getSpecializations()) {
//							Mastership add = new Mastership(master, (SMSkillSpecialization)focus);
//							tmp.addMastership(add);
//							Logging.logger.log(Level.ERROR, "Created mastership "+add.getName());
//							System.exit(0);
//						}
//					}
//					storage.add(master);
				}

			Logging.logger.log(Level.DEBUG, "Successfully loaded {0} common and {1} regular masterships from {2}",countCommon,count,plugin.getID());
		} catch (Exception e) {
			Logging.logger.log(Level.ERROR, "Failed deserializing skills",e);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static List<Education> loadEducations(DataSet plugin, InputStream in) {
		Logging.logger.log(Level.DEBUG, "Load educations (Plugin="+plugin.getID()+")");
		List<Education> list = new ArrayList<Education>();
		DataStorage<Education> storage = getStorage(Education.class);
		try {
			list = GenericCore.loadDataItems(EducationList.class, Education.class, plugin, in);

			/*
			 * Fix hierarchy
			 */
			for (Education item : list) {
				if (item.getVariantOf()==null) {
					// Not a variant
					storage.add(item);
				} else {
					// Variant
					Education parent = storage.getItem(item.getVariantOf());
					if (parent==null) {
						Logging.logger.log(Level.WARNING, "Education "+item.getId()+" claims to be a variant of the unknown education '"+item.getVariantOf()+"'");
					} else {
						parent.addVariant(item);
//						Logging.logger.log(Level.DEBUG, "  "+item.getId()+" is a variant of "+parent.getId());
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	//
//	//-------------------------------------------------------------------
//	public static <E extends DataItem> void loadDataItems(Class<? extends List<E>> cls, DataSet plugin, InputStream in) {
//		Logging.logger.log(Level.DEBUG, "Load skills (Plugin="+plugin.getID()+")");
//		try {
//			List<E> addSkills = serializer.read(cls, in);
//			Logging.logger.log(Level.INFO, "Successfully loaded "+addSkills.size()+" skills");
//			addSkills.forEach(skill -> skill.assignToDataSet(plugin));
//			addSkills.forEach(skill -> skills.add((Skill) skill));
//		} catch (Exception e) {
//			Logging.logger.fatal("Failed deserializing skills",e);
//			System.exit(0);
//			return;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public static void loadSkills(DataSet plugin, InputStream in) {
//		loadDataItems(SkillList.class, plugin, in);
//		Logging.logger.log(Level.DEBUG, "Load skills (Plugin="+plugin.getID()+")");
//		try {
//			SkillList addSkills = serializer.read(SkillList.class, in);
//			Logging.logger.log(Level.INFO, "Successfully loaded "+addSkills.size()+" skills");
//			addSkills.forEach(skill -> skill.assignToDataSet(plugin));
//			addSkills.forEach(skill -> skills.add(skill));
//		} catch (Exception e) {
//			Logging.logger.fatal("Failed deserializing skills",e);
//			System.exit(0);
//			return;
//		}
//	}

	//-------------------------------------------------------------------
	public static Background getBackground(String key) {
		return getItem(Background.class, key);
	}

	//-------------------------------------------------------------------
	public static SMSkill getSkill(String key) {
		return getItem(SMSkill.class, key);
	}

	//-------------------------------------------------------------------
	public static List<SMSkill> getSkills(SkillType type) {
		List<SMSkill> ret = getItemList(SMSkill.class).stream().filter(sk -> sk.getType()==type).collect(Collectors.toList());
		Collections.sort(ret, new Comparator<SMSkill>() {
			public int compare(SMSkill arg0, SMSkill arg1) {
				return Collator.getInstance().compare(arg0.getName(),  arg1.getName());
			}
		});
		return ret;
	}

	//-------------------------------------------------------------------
	public static Spell getSpell(String key) {
		return getItem(Spell.class, key);
	}

	//-------------------------------------------------------------------
	public static CreatureType getCreatureType(String key) {
		return getItem(CreatureType.class, key);
	}

	//-------------------------------------------------------------------
	public static List<FeatureType> getFeatureTypes() {
		return getItemList(FeatureType.class);
	}

	//-------------------------------------------------------------------
	public static FeatureType getFeatureType(String key) {
		return getItem(FeatureType.class, key);
	}

	//-------------------------------------------------------------------
	public static CreatureFeature getCreatureFeatureType(String key) {
		return getItem(CreatureFeature.class, key);
	}

	//-------------------------------------------------------------------
	public static List<Creature> getCreatures() {
		return getItemList(Creature.class);
	}

	//-------------------------------------------------------------------
	public static List<Mastership> getMasterships() {
		List<Mastership> ret = new ArrayList<>();
		for (SMSkill skill : getItemList(SMSkill.class)) {
			ret.addAll(skill.getMasterships());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static SpliMoNameTable loadNameTable(DataSet plugin, InputStream in) {
		DataStorage<SpliMoNameTable> storage = getStorage(SpliMoNameTable.class);
		Logging.logger.log(Level.INFO, "Load name table (Plugin="+plugin.getID()+")");
		try {
			SpliMoNameTable toAdd = serializer.read(SpliMoNameTable.class, in);
			Logging.logger.log(Level.INFO, "Successfully loaded name table for culture {0}",toAdd.getCulture());
			toAdd.assignToDataSet(plugin);
			storage.add(toAdd);
			return toAdd;
		} catch (Exception e) {
			Logging.logger.log(Level.ERROR, "Failed loading name table: "+e,e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	public static byte[] save(SpliMoCharacter character) {
//		// Ensure that all custom items are added to char
//		// - and eliminate all others
//		character.clearCustomItems();
//		for (CarriedItem item : character.getItems())
//			if (item.getItem().isCustom())
//				character.addCustomItem(item.getItem());
//
//		// Ensure that all custom creatures are added to char
//		// - and eliminate all others
//		character.clearCustomCreatures();
//		for (CreatureReference item : character.getCreatures())
//			if (item.getTemplate()!=null && item.getTemplate().isCustom())
//				character.addCustomCreature(item.getTemplate());


		try {
			StringWriter out = new StringWriter();
			serializer.write(character, out);
			return out.toString().getBytes(Charset.forName("UTF-8"));
		} catch (IOException e) {
			Logging.logger.log(Level.ERROR, "Failed generating XML for char",e);
			StringWriter mess = new StringWriter();
			mess.append("Failed saving character\n\n");
			e.printStackTrace(new PrintWriter(mess));
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, mess.toString());
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacter load(byte[] raw) throws IOException {
		return load(new StreamSource( new ByteArrayInputStream( raw ) ));
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacter load(InputStream ins) throws IOException {
		return load(new StreamSource( ins ));
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacter load(Path path) throws IOException {
		String buf = new String(Files.readAllBytes(path));
		return load(new StreamSource( new StringReader( buf ) ));
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacter load(Source source) throws IOException {
		try {
			SpliMoCharacter ret = serializer.read(SpliMoCharacter.class, source);
			Logging.logger.log(Level.INFO, "Character successfully loaded: "+ret);

//			SplitterTools.applyAfterLoadModifications(ret);
//			loadEquipmentModifications(ret);
			return ret;
		} catch (Exception e) {
			Logging.logger.log(Level.ERROR, "Failed loading character: "+e,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacter decode(byte[] rawData) throws CharacterIOException {
		String data = new String(rawData, Charset.forName("UTF-8"));
		try {
			return serializer.read(SpliMoCharacter.class, data);
		} catch (SerializationException e) {
			logger.log(Level.ERROR, "Failed deocding XML from char, line "+e.getLine()+":"+e.getColumn(),e);
			throw new CharacterIOException(ErrorCode.DECODING_FAILED, "Failed decoding XML: "+e.getMessage(), e);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Failed deocding XML from char",e);
			throw new CharacterIOException(ErrorCode.DECODING_FAILED, "Failed decoding XML: "+e.getMessage(), e);
		}
	}

}

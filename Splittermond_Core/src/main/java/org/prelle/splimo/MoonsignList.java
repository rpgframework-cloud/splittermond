/**
 *
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="moonsigns")
@ElementList(entry="moonsign",type=Moonsign.class,inline=true)
public class MoonsignList extends ArrayList<Moonsign> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public MoonsignList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MoonsignList(Collection<? extends Moonsign> c) {
		super(c);
	}

	//-------------------------------------------------------------------
	public List<Moonsign> getResources() {
		return this;
	}
}

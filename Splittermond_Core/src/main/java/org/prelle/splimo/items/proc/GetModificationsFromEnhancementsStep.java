package org.prelle.splimo.items.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.items.formula.FormulaTool;
import de.rpgframework.genericrpg.items.formula.VariableResolver;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 *
 */
public class GetModificationsFromEnhancementsStep implements CarriedItemProcessor {

	final static Logger logger = SMGearTool.logger;

	//-------------------------------------------------------------------
	/**
	 */
	public GetModificationsFromEnhancementsStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(boolean, de.rpgframework.genericrpg.modification.ModifiedObjectType, de.rpgframework.genericrpg.data.Lifeform, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform charac,
			CarriedItem<?> model, List<Modification> unprocessed) {

		for (ItemEnhancementValue<AItemEnhancement> val : model.getEnhancements()) {
			List<Modification> realMods = val.getOutgoingModifications();
			if (val.getOutgoingModifications().isEmpty()) {
				Enhancement enh = (Enhancement) val.getResolved();
				logger.log(Level.WARNING, "Handle "+enh);
				for (Modification mod : enh.getOutgoingModifications()) {
					Modification realMod = SplitterTools.instantiateModification(mod, val, 1, (SpliMoCharacter) charac);
					val.addOutgoingModification(realMod);
				}
				realMods = val.getOutgoingModifications();
			}

			for (Modification realMod : realMods) {
				logger.log(Level.DEBUG, "  inject {0}", realMod);
				unprocessed.add(realMod);
			}
		}
		return new OperationResult<> (unprocessed);
	}

}

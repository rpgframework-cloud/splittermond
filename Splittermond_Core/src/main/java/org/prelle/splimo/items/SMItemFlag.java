package org.prelle.splimo.items;

import java.util.Locale;

import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.items.ItemFlag;

/**
 * @author prelle
 *
 */
public enum SMItemFlag implements ItemFlag {

	// Is marked to be the primary item of its kind by the user
	PRIMARY,
	// Item is considered "Handwerkszeug"
	TOOL_OF_THE_TRADE,
	// Item is payed with 10L free weapon
	FREE_WEAPON
	;


	private static MultiLanguageResourceBundle RES = SplitterMondCore.getI18nResources();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.ItemFlag#getName()
	 */
	@Override
	public String getName() {
		return getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.ItemFlag#getName(java.util.Locale)
	 */
	@Override
	public String getName(Locale loc) {
		return RES.getString("itemflag."+this.name().toLowerCase(), loc);
	}


}

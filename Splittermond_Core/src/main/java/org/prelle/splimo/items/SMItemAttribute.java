package org.prelle.splimo.items;

import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;
import de.rpgframework.genericrpg.persist.EnumConverter;
import de.rpgframework.genericrpg.persist.IntegerConverter;

/**
 * @author prelle
 *
 */
public enum SMItemAttribute implements IItemAttribute {

	AMMUNITION_PRICE,
	ARTIFACT_QUALITY,
	ATTRIBUTE1( new EnumConverter(Attribute.class)),
	ATTRIBUTE2( new EnumConverter(Attribute.class)),
	AVAILABILITY( new EnumConverter(Availability.class)),
	COMPLEXITY( new EnumConverter(Complexity.class)),
	DAMAGE,
	DEFENSE,
	DAMAGE_REDUCTION,
	FEATURES( (StringValueConverter)null),
	HANDICAP,
	LOAD,
	MATERIAL( new EnumConverter(Material.class)),
	PRICE,
	RANGE,
	RATING,
	RIGIDITY,
	SIZE,
	SKILL( (StringValueConverter)null),
	SPECIALIZATION,
	SPEED,
	SUBTYPE( new EnumConverter(ItemSubType.class)),
	TYPE( new EnumConverter(ItemType.class)),
	;

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SplitterMondCore.class, "SplitterMondCore", Locale.GERMAN);

	private StringValueConverter<? extends Object> converter;

	//-------------------------------------------------------------------
	private SMItemAttribute() {
		converter = new IntegerConverter();
	}

	//-------------------------------------------------------------------
	private SMItemAttribute(StringValueConverter<? extends Object> conv) {
		converter = conv;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getName(java.util.Locale)
	 */
	@Override
	public String getName(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "itemattribute."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getName()
	 */
	@Override
	public String getName() {
		return getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getShortName(java.util.Locale)
	 */
	@Override
	public String getShortName(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "itemattribute."+this.name().toLowerCase()+".short");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getConverter()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> StringValueConverter<T> getConverter() {
		return (StringValueConverter<T>) converter;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#calculateModifiedValue(java.lang.Object, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	public <T> T calculateModifiedValue(Object base, List<Modification> mods) {
		switch (this) {
		case ATTRIBUTE1: case ATTRIBUTE2:
//		case TYPE:
			Attribute retAttr = (Attribute)base;
			for (Modification tmp : mods) {
				if (tmp instanceof ValueModification) {
					retAttr = Attribute.valueOf( ((ValueModification)tmp).getRawValue() );
				}
			}
			return (T)retAttr;
		case AVAILABILITY:
			Availability retAvail = (Availability)base;
			for (Modification tmp : mods) {
				if (tmp instanceof ValueModification) {
					retAvail = Availability.valueOf( ((ValueModification)tmp).getRawValue() );
				}
			}
			return (T)retAvail;
		case TYPE:
			ItemType retType = (ItemType)base;
			for (Modification tmp : mods) {
				if (tmp instanceof ValueModification) {
					retType = ItemType.valueOf( ((ValueModification)tmp).getRawValue() );
				}
			}
			return (T)retType;
		}
		System.err.println("SMItemAttribute: Don't know how to calculate modified value for "+this);
		return (T)base;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#resolve(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public SMItemAttribute resolve(String key) {
		return SMItemAttribute.valueOf(key);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.items.IItemAttribute#resolve(java.lang.String)
//	 */
//	@Override
//	public <T> T resolve(String key) {
////		try {
////			if (converter!=null)
////				return (T) converter.read(key);
////		} catch (Exception e1) {
////			throw new IllegalArgumentException(e1);
////		}
//		try {
//			return (T)Integer.valueOf(key);
//		} catch (NumberFormatException e) {
//			return (T)key;
//		}
//	}
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.items.IItemAttribute#toString(java.lang.Object)
//	 */
//	@Override
//	public <E extends Object> String toString(E value) {
////		if (converter!=null) {
////			try {
////				return converter.write(bla);
////			} catch (Exception e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////		}
//		// TODO Auto-generated method stub
//		return String.valueOf(value);
//	}

}

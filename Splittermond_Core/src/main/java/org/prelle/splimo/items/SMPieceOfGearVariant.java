package org.prelle.splimo.items;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.IgnoreMissingAttributes;

import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.items.Hook;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;

/**
 * @author prelle
 *
 */
@IgnoreMissingAttributes("id")
@DataItemTypeKey(id="variant")
public class SMPieceOfGearVariant extends PieceOfGearVariant<SMVariantMode> {

	@Element
	private WeaponData weapon;
	@Element(name = "rangedweapon")
	private RangedWeaponData rangeWeapon;

	//-------------------------------------------------------------------
	/**
	 */
	public SMPieceOfGearVariant() {
		mode = SMVariantMode.CARRIED;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.PieceOfGearVariant#getTypeData()
	 */
	@Override
	public List<? extends IGearTypeData> getTypeData() {
		return new ArrayList<>();
	}

}

package org.prelle.splimo.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.proc.ApplyModificationsStep;
import org.prelle.splimo.items.proc.GetModificationsFromEnhancementsStep;
import org.prelle.splimo.items.proc.SMResolveTemplatesStep;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.items.GearTool;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class SMGearTool extends GearTool {

	public final static Logger logger = System.getLogger(SMGearTool.class.getPackageName());

	public static CarriedItemProcessor[] SM_PHASE1_STEPS = new CarriedItemProcessor[] {
//			new SR6ModeDetailsStep(),
			new GetModificationsFromEnhancementsStep(),
			new ApplyModificationsStep(),
//			new SR6ResolveFormulasStep(),
//			new ApplyAmmunitionTypeStep(),
//			new CalculateVehicleSlots(),
//			new AddMissingStandardSlots(),
//			new ApplyStockModificationsStep(),
			new SMResolveTemplatesStep(),
//			new CreateAlternatesStep()
	};

	public static CarriedItemProcessor[] SM_PHASE2_STEPS = new CarriedItemProcessor[] {
//			new DeriveCapacityAttributeStep(),
//			new CalculateAccessorySizes(),
//			new InsertDefaultAccessories(),
//			new HandleAugmentationGradeStep(),
//			new ApplyItemFlagsStep(),
//			new AddUpPricesStep(),
//			new AddChemicalPriceStep()
	};

	//-------------------------------------------------------------------
	/**
	 */
	public SMGearTool() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public static <I extends IItemAttribute> OperationResult<List<Modification>> recalculate(String indent, Lifeform user, CarriedItem<ItemTemplate> item) {
		logger.log(Level.DEBUG, "recalculate {0} of {1}",item.getKey(), (user!=null)?user.getName():null);
//		if (item.getUuid().equals(ItemTemplate.UUID_UNARMED))
//			return new OperationResult<>(new ArrayList<>());
		if (item.getResolved()==null) {
			ItemTemplate temp = SplittermondReference.GEAR.resolve(item.getTemplateID());
			if (temp==null)
				logger.log(Level.ERROR, "No such ItemTemplate {0}", item.getTemplateID());
			else
				item.setResolved(temp);
		}

		// Resolve accessories if necessary
		for (CarriedItem<ItemTemplate> tmp : item.getAccessories()) {
			if (tmp.getResolved()==null) {
				tmp.setResolved(SplitterMondCore.getItem(ItemTemplate.class, tmp.getKey()));
				if (tmp.getVariantID()!=null && tmp.getVariant()==null) {
					tmp.setVariant( tmp.getResolved().getVariant(tmp.getCarryMode()) );
				}
			}
		}

			try {
				item.setDirty(false);
				return GearTool.recalculate(indent, SplittermondReference.ITEM_ATTRIBUTE, user, item);
			} catch (DataErrorException e) {
				item.setDirty(true);
				if (e.getReferenceError()!=null) e.getReferenceError().setType(SplittermondReference.GEAR);
				throw e;
			}
	}

}

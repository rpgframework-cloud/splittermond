package org.prelle.splimo.items;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.persist.EnhancementMaxConverter;
import org.prelle.splimo.persist.ItemTypeConverter;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;
import de.rpgframework.genericrpg.items.formula.FormulaImpl;
import de.rpgframework.genericrpg.items.formula.NumberElement;
import de.rpgframework.genericrpg.items.formula.ObjectElement;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * A wrapper for profane modifications made by the item.
 *
 * @author prelle
 *
 */
@Root(name="enhancement")
@DataItemTypeKey(id = "enhancement")
public class Enhancement extends AItemEnhancement implements Comparable<Enhancement> {

	public final static int MAX_ONCEPERLOAD = -1;
	public final static int MAX_HALFLOAD = -2;

	@Attribute
	@AttribConvert(EnhancementMaxConverter.class)
	private int max;
	@Attribute
	private EnhancementType type;
	@Attribute(name="items",required=false)
	@AttribConvert(ItemTypeConverter.class)
	private Collection<ItemType> types;

	@ElementList(entry = "attrdef", type = ItemAttributeDefinition.class, inline = true)
	protected List<ItemAttributeDefinition> attributes;

	/* Combination of direct and shortcut attributes */
	protected transient Map<IItemAttribute, ItemAttributeDefinition> cache;

	//--------------------------------------------------------------------
	public Enhancement() {
		cache = new HashMap<>();
		attributes = new ArrayList<>();
		modifications = new ModificationList();
		type  = EnhancementType.NORMAL;
		types = new ArrayList<>(Arrays.asList(ItemType.values()));
	}

	//--------------------------------------------------------------------
	public Enhancement(String id, int size, Modification mod, EnhancementType type) {
		this();
		this.id   = id;
		this.size = size;
		this.type = type;
		modifications.add(mod);
	}

	//--------------------------------------------------------------------
	public Enhancement(String id, int size, EnhancementType type) {
		this();
		this.id   = id;
		this.size = size;
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Enhancement other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return How often can this enhancement be applied
	 */
	public int getMaxApplyCount() {
		return max;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public EnhancementType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the types
	 */
	public Collection<ItemType> getItemTypeLimitations() {
		return types;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		if (id==null)
			throw new NullPointerException("id-Attribute not set for "+this);
//		for (IGearTypeData shortcut : getTypeData()) {
//			shortcut.copyToAttributes(this);
//		}
		for (ItemAttributeDefinition tmp : attributes) {
			cache.put(tmp.getModifyable(), tmp);
		}
	}

	//-------------------------------------------------------------------
	public Collection<ItemAttributeDefinition> getAttributes() {
		return cache.values();
	}

	//-------------------------------------------------------------------
	public Collection<ItemAttributeDefinition> getAttributes(String variantID) {
		return cache.values();
	}

	//-------------------------------------------------------------------
	public ItemAttributeDefinition getAttribute(IItemAttribute attrib) {
		return cache.get(attrib);
	}

	//-------------------------------------------------------------------
	public void setAttribute(ItemAttributeDefinition def) {
		cache.put(def.getModifyable(), def);
	}

	//-------------------------------------------------------------------
	public void setAttribute(IItemAttribute attrib, Formula value) {
		if (value==null)
			throw new NullPointerException("Cannnot set NULL for "+attrib);
		ItemAttributeDefinition val = cache.get(attrib);
		if (val==null) {
			val = new ItemAttributeDefinition(attrib);
			cache.put(attrib, val);
		}
		val.setRawValue(String.valueOf(value));
		val.setFormula(value);
//		if (value.getClass()==Integer.class || value.getClass()==int.class) {
//			val.setValue( (int)value );
//		} else {
//			val.setValue(value);
//		}
	}

	//-------------------------------------------------------------------
	public void setAttribute(IItemAttribute attrib, int value) {
		FormulaImpl form = new FormulaImpl();
		form.addElement(new NumberElement(value, -1));

		ItemAttributeDefinition val = cache.get(attrib);
		if (val==null) {
			val = new ItemAttributeDefinition(attrib);
			cache.put(attrib, val);
		}
		val.setRawValue(String.valueOf(value));
		val.setFormula(form);
	}

	//-------------------------------------------------------------------
	public void setAttribute(IItemAttribute attrib, Object value) {
		if (value instanceof Integer) {
			setAttribute(attrib, (int)value);
			return;
		}
		FormulaImpl form = new FormulaImpl();
		form.addElement(new ObjectElement(value));

		ItemAttributeDefinition val = cache.get(attrib);
		if (val==null) {
			val = new ItemAttributeDefinition(attrib);
			cache.put(attrib, val);
		}
		if (value instanceof DataItem) {
			val.setRawValue( ((DataItem)value).getId() );
		} else {
			val.setRawValue(String.valueOf(value));
		}
		val.setFormula(form);
	}

}

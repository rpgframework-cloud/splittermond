package org.prelle.splimo.items;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;

import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.items.AlternateUsage;
import de.rpgframework.genericrpg.items.IGearTypeData;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="usage")
public class SMAlternateUsage extends AlternateUsage<SMUsageMode> {
	
	@Attribute
	private ItemType type;
	@Attribute
	private ItemSubType subtype;

	@ElementListUnion({
		@ElementList(entry="weapon", type = WeaponData.class, inline = true),
		@ElementList(entry="rangedweapon", type=RangedWeaponData.class, inline=true),
		@ElementList(entry="armor", type=Armor.class, inline=true),
		@ElementList(entry="shield", type=ShieldData.class, inline=true),
	})
	private List<IGearTypeData> shortcuts; 

	//-------------------------------------------------------------------
	public SMAlternateUsage() {
		shortcuts = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public SMAlternateUsage(SMUsageMode mode) {
		this();
		this.mode = mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.AGearData#getTypeData()
	 */
	@Override
	public List<? extends IGearTypeData> getTypeData() {
		return shortcuts;
	}

}

package org.prelle.splimo.items;

import de.rpgframework.genericrpg.items.AGearData;

/**
 * @author prelle
 *
 */
public class RangedWeaponData extends WeaponData {

	//-------------------------------------------------------------------
	/**
	 */
	public RangedWeaponData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IGearTypeData#copyToAttributes(de.rpgframework.genericrpg.items.AGearData)
	 */
	@Override
	public void copyToAttributes(AGearData copyTo) {
		super.copyToAttributes(copyTo);
	}

}

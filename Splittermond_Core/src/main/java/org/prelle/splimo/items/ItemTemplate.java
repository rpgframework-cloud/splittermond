package org.prelle.splimo.items;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.IgnoreMissingAttributes;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.persist.ComplexityConverter;

import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.PieceOfGear;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "item")
@IgnoreMissingAttributes("id,load,robust,avail")
public class ItemTemplate extends PieceOfGear<SMVariantMode,SMUsageMode,SMPieceOfGearVariant,SMGearUsage> {

	public final static UUID UUID_RATING = UUID.fromString("c2d17c87-1cfe-4355-9877-a20fe09c170d");

	@Attribute(required=true)
	private int load;
	@Attribute(name="robust",required=true)
	private int rigidity;
	@Attribute(name="avail",required=true)
	private Availability availability;
	@Attribute(name="cplx",required=false)
	@AttribConvert(ComplexityConverter.class)
	private Complexity complexity;
	@Attribute(required=false)
	private Material material;
	@Attribute(required=true)
	private ItemType type;
	@Attribute(required=true)
	private ItemSubType subtype;

	@Attribute(required=false)
	private String skill;
	@Attribute(name="spec",required=false)
	private String specialization;

//	@ElementListUnion({
//		@ElementList(entry="weapon", type=WeaponData.class),
//	})
//	private List<PieceOfGearUsage<SMUsageMode>> usages;

	@ElementListUnion({
		@ElementList(entry="weapon", type = WeaponData.class, inline = true),
		@ElementList(entry="rangedweapon", type = WeaponData.class, inline = true),
		@ElementList(entry="armor", type=Armor.class, inline=true),
		@ElementList(entry="shield", type=Armor.class, inline=true)
	})
	private List<IGearTypeData> shortcuts;

	//-------------------------------------------------------------------
	public ItemTemplate() {
		shortcuts = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	@Override
	public void validate() {
		attributes.clear();

		if (availability!=null) setAttribute(SMItemAttribute.AVAILABILITY, availability);
		if (  complexity!=null) setAttribute(SMItemAttribute.COMPLEXITY, complexity);
		if (        load!= 0  ) setAttribute(SMItemAttribute.LOAD, load);
		if (    material!=null) setAttribute(SMItemAttribute.MATERIAL, material);
		if (       price!= 0  ) setAttribute(SMItemAttribute.PRICE, super.price);
		if (    rigidity!= 0  ) setAttribute(SMItemAttribute.RIGIDITY, rigidity);
		if (       skill!=null) setAttribute(SMItemAttribute.SKILL, SplitterMondCore.getSkill(skill));
		if (        type!=null) setAttribute(SMItemAttribute.TYPE, type);
		if (     subtype!=null) setAttribute(SMItemAttribute.SUBTYPE, subtype);

		/* If there is no USAGE assume a NORMAL mode and no slot */
//		if (variants.isEmpty()) {
//			SMPieceOfGearVariant add = new SMPieceOfGearVariant();
//			variants.add(add);
//		}

		super.validate();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.AGearData#getTypeData()
	 */
	@Override
	public List<? extends IGearTypeData> getTypeData() {
		return shortcuts;
	}

	//-------------------------------------------------------------------
	/**
	 * @deprecated Use getItemType(CarryMode)
	 */
	public ItemType getType() { return type; }
	public void setType(ItemType type) { this.type = type; }

	//-------------------------------------------------------------------
	public ItemType getItemType(CarryMode carry) {
		if (getUsage(carry)!=null) return type;
		SMPieceOfGearVariant variant = (SMPieceOfGearVariant) getVariant(carry);
		if (variant!=null && variant.getUsage(carry)!=null) {
			if (variant.getAttribute(SMItemAttribute.TYPE)!=null)
				return variant.getAttribute(SMItemAttribute.TYPE).getValue();
			return type;
		}
		if (usages==null || usages.isEmpty()) return type;
		return null;
	}

	//-------------------------------------------------------------------
	public ItemSubType getSubType() { return subtype; }
	public void setSubType(ItemSubType type) { this.subtype = type; }

	//-------------------------------------------------------------------
	public ItemSubType getItemSubtype(CarryMode carry) {
		if (getUsage(carry)!=null) return subtype;
		SMPieceOfGearVariant variant = (SMPieceOfGearVariant) getVariant(carry);
		if (variant!=null && variant.getUsage(carry)!=null) {
			if (variant.getAttribute(SMItemAttribute.SUBTYPE)!=null)
				return variant.getAttribute(SMItemAttribute.SUBTYPE).getValue();
			return subtype;
		}
		if (usages==null || usages.isEmpty()) return subtype;
		return null;
	}

	//-------------------------------------------------------------------
	public AGearData getMainOrVariant(CarryMode carry) {
		if (getUsage(carry)!=null) return this;
		SMPieceOfGearVariant variant = (SMPieceOfGearVariant) getVariant(carry);
		if (variant!=null && variant.getUsage(carry)!=null) {
			return variant;
		}
		return this;
	}

}

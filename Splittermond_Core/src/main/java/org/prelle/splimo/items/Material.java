package org.prelle.splimo.items;

/**
 * 
 */
public enum Material {
	
	METAL,
	WOOD,
	FABRIC,
	LEATHER,
	OTHER

}

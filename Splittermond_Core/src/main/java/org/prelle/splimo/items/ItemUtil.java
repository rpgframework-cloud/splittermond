package org.prelle.splimo.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splimo.persist.WeaponDamageConverter;

import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeObjectValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.requirements.AnyRequirement;
import de.rpgframework.genericrpg.requirements.ExistenceRequirement;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ValueRequirement;

/**
 * 
 */
public class ItemUtil {
	
	public static class CarriedItemQuality {
		public int itemQuality;
		public int artifactQuality;
		public int legendaryQuality;
		public int minQualityByMaterial;
		public int getRelicLevel() {
			int sum = itemQuality + artifactQuality + legendaryQuality;
			if (sum<minQualityByMaterial)
				sum=minQualityByMaterial;
			// If there is special material, increase relic level
			if (minQualityByMaterial>0)
				sum++;
			return sum;
		}
	}

	private final static Logger logger = System.getLogger(ItemUtil.class.getPackageName());
	
	private static WeaponDamageConverter DMG_CONV = new WeaponDamageConverter();

	//-------------------------------------------------------------------
	public static ItemType getItemType(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<?> val = item.getAsObject(SMItemAttribute.TYPE);
		if (val==null) return null;
		return (ItemType)val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static ItemSubType getItemSubType(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<SMItemAttribute> val = item.getAsObject(SMItemAttribute.SUBTYPE);
		if (val==null) return ItemSubType.NOT_SET;
		return (ItemSubType)val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static Attribute getAttribute1(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<SMItemAttribute> val = item.getAsObject(SMItemAttribute.ATTRIBUTE1);
		if (val==null) return null;
		return (Attribute)val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static Attribute getAttribute2(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<SMItemAttribute> val = item.getAsObject(SMItemAttribute.ATTRIBUTE2);
		if (val==null) return null;
		return (Attribute)val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getLoad(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.LOAD);
		if (val==null) return -1;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getSpeed(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.SPEED);
		if (val==null) return -1;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getDefense(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.DEFENSE);
		if (val==null) return -1;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getHandicap(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.HANDICAP);
		if (val==null) return -1;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getDamageReduction(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.DAMAGE_REDUCTION);
		if (val==null) return -1;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static SMSkill getSkill(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<SMItemAttribute> val = item.getAsObject(SMItemAttribute.SKILL);
		if (val==null) return null;
		return (SMSkill)val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static List<Feature> getFeatures(CarriedItem<ItemTemplate> item) {
		ItemAttributeObjectValue<SMItemAttribute> val = item.getAsObject(SMItemAttribute.FEATURES);
		if (val==null) return List.of();
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static int getDamage(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.DAMAGE);
		if (val==null) return 0;
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public static String getDamageString(CarriedItem<ItemTemplate> item) {
		ItemAttributeNumericalValue<SMItemAttribute> val = item.getAsValue(SMItemAttribute.DAMAGE);
		if (val==null) return "?";
		return DMG_CONV.write( val.getModifiedValue() );
	}

	//-------------------------------------------------------------------
	/**
	 * @return Requirement not met
	 */
	public static Requirement areRequirementsMet(CarriedItem<ItemTemplate> container, ItemTemplate item, SMPieceOfGearVariant variant) {
		for (Requirement tmp : item.getRequirements()) {
			if (!isRequirementMet(container, item, tmp))
				return tmp;
		}
		if (variant!=null) {
			for (Requirement tmp : variant.getRequirements()) {
				if (!isRequirementMet(container, item, tmp))
					return tmp;
			}
		}

		return null;
	}

	//-------------------------------------------------------------------
	public static boolean isRequirementMet(CarriedItem<ItemTemplate> container, ItemTemplate item, Requirement tmp) {
		if (tmp.getApply()!=ApplyTo.DATA_ITEM && !(tmp instanceof AnyRequirement)) return true;
		if (tmp instanceof AnyRequirement) {
			AnyRequirement req = (AnyRequirement)tmp;
			for (Requirement part : req.getOptionList()) {
				if (isRequirementMet(container, item, part)) return true;
			}
			return false;
		}
		SplittermondReference type = (SplittermondReference) tmp.getType();
		String key = tmp.getKey();
		if (tmp instanceof ExistenceRequirement) {
			ExistenceRequirement req = (ExistenceRequirement)tmp;
			switch (type) {
			case GEAR:
				return container.getModifyable().getId().equals(key);
			case ITEMTYPE:
				return container.getAsObject(SMItemAttribute.TYPE).getValue()==type.resolve(key);
			case ITEMSUBTYPE:
				return container.getAsObject(SMItemAttribute.SUBTYPE).getValue()==type.resolve(key);
			default:
				System.err.println("ItemUtil: TODO: check existence of "+req.getType());
				logger.log(Level.ERROR, "TODO: check existence of "+req.getType());
			}
			return false;
		}
		if (tmp instanceof ValueRequirement) {
			ValueRequirement req = (ValueRequirement)tmp;
			switch (type) {
			case ITEM_ATTRIBUTE:
				SMItemAttribute iAttr = SMItemAttribute.valueOf(req.getKey());
				if (!container.hasAttribute(iAttr))
					return false;
				ItemAttributeValue<?> rawAttr = container.getAttributeRaw(iAttr);
				if (rawAttr==null)
					return false;
				switch (iAttr) {
//				case AMMUNITION_CLASS:
//					AmmunitionClass ammoCls = ((ItemAttributeObjectValue<SMItemAttribute>)rawAttr).getModifiedValue();
//					if (ammoCls==null) {
//						logger.log(Level.ERROR, "No valud AMMUNICATION_CLASS value in "+container);
//						return false;
//					}
//					return ammoCls.name().equals(req.getRawValue());
//				case SOFTWARE_TYPES:
//					Object x = ((ItemAttributeObjectValue)rawAttr).getValue();
//					if (x instanceof List)
//						return ((List<SoftwareTypes>)x).contains( SoftwareTypes.valueOf( req.getRawValue() ));
//					else
//						return ((SoftwareTypes)x)==SoftwareTypes.valueOf( req.getRawValue() );
				default:
					logger.log(Level.ERROR, "TODO: support requirement check for item attribut {0}", iAttr);
				}
				return false;
			}
		}
		logger.log(Level.ERROR, "TODO: check "+tmp+"/"+tmp.getClass());
		System.err.println("TODO: support requirement check for "+tmp+"/"+tmp.getClass());
		return false;
	}
	
	//-------------------------------------------------------------------
	public static CarriedItemQuality getItemQuality(CarriedItem<ItemTemplate> model) {
		CarriedItemQuality ret = new CarriedItemQuality();
		// TODO: Material
		
		for (ItemEnhancementValue<AItemEnhancement> ref : model.getEnhancements()) {
			Enhancement enhance = (Enhancement) ref.getResolved();
			switch (enhance.getType()) {
			case NORMAL:
				ret.itemQuality+=enhance.getSize();
				break;
			case MAGIC:
			case HIGHARTIFACT:
				ret.artifactQuality+=enhance.getSize();
				break;
			case RELIC:
				ret.legendaryQuality+=enhance.getSize();
				break;
			}
		}
		
		return ret;
	}

}

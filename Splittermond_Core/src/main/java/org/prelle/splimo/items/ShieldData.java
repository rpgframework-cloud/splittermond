package org.prelle.splimo.items;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.requirements.RequirementList;

/**
 * @author prelle
 *
 */
public class ShieldData implements IGearTypeData {

	@Attribute(name="def")
	protected int defense;
	@Attribute(name="hc")
	protected int handicap;
	@Attribute
	protected int tickMalus;
	@Element
	protected RequirementList requires;
	@Element
	protected FeatureList features;

	@Attribute(name="dr")
	private int damageReduction;

	//-------------------------------------------------------------------
	/**
	 */
	public ShieldData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IGearTypeData#copyToAttributes(de.rpgframework.genericrpg.items.AGearData)
	 */
	@Override
	public void copyToAttributes(AGearData copyTo) {
		if (   defense!= 0  ) copyTo.setAttribute(SMItemAttribute.DEFENSE, defense);
		if ( tickMalus!= 0  ) copyTo.setAttribute(SMItemAttribute.SPEED, tickMalus);
		if (  handicap!= 0  ) copyTo.setAttribute(SMItemAttribute.HANDICAP, handicap);
		if (  requires!=null) copyTo.getRequirements().addAll(requires);
		if (  features!=null) copyTo.setAttribute(SMItemAttribute.FEATURES, features);
		if (damageReduction!= 0  ) copyTo.setAttribute(SMItemAttribute.DAMAGE_REDUCTION, damageReduction);
	}

}

package org.prelle.splimo.items.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.modification.AllowModification;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 *
 */
public class ApplyModificationsStep implements CarriedItemProcessor {

	final static Logger logger = SMGearTool.logger;

	//-------------------------------------------------------------------
	/**
	 */
	public ApplyModificationsStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(boolean, de.rpgframework.genericrpg.modification.ModifiedObjectType, de.rpgframework.genericrpg.data.Lifeform, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform charac,
			CarriedItem<?> model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		for (Modification tmp : previous) {
			if (tmp instanceof AllowModification) {
				unprocessed.add(tmp);
			} else if (tmp instanceof DataItemModification) {
				applyDataItemMod(model, (DataItemModification)tmp);
			} else {
				logger.log(Level.INFO, "Unprocessed "+tmp);
				unprocessed.add(tmp);
			}
		}

		return new OperationResult<> (unprocessed);
	}

	//-------------------------------------------------------------------
	public static void applyDataItemMod(CarriedItem<?> model, DataItemModification mod) {
		switch ((SplittermondReference) mod.getReferenceType()) {
		case ITEM_ATTRIBUTE:
			logger.log(Level.INFO, "Add modification {0} to {1}", mod, mod.getKey());
			SMItemAttribute iAttr = SMItemAttribute.valueOf(mod.getKey());
			if (model.hasAttribute(iAttr))
				model.getAttributeRaw(iAttr).addIncomingModification(mod);
			else
				logger.log(Level.WARNING, "Modification {0}, but model has no attribute {1}", mod, iAttr);
			break;
		default:
			logger.log(Level.WARNING, "Don't know how to apply "+mod.getReferenceType()+" of "+mod);
			//System.err.println("ApplyModificationsGeneric: Don't know how to apply "+tmp.getReferenceType()+" of "+tmp);
		}
	}

}

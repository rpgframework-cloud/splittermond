package org.prelle.splimo.items;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Element;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.persist.WeaponDamageConverter;

import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.requirements.RequirementList;

/**
 * @author prelle
 *
 */
public class WeaponData implements IGearTypeData {

	@org.prelle.simplepersist.Attribute
	protected int speed;
	@org.prelle.simplepersist.Attribute(name="att1")
	protected Attribute attribute1;
	@org.prelle.simplepersist.Attribute(name="att2")
	protected Attribute attribute2;
	/**
	 * Coded XXYYZZ
	 * XX = Number of dice
	 * YY = Dice type
	 * ZZ = Modifier
	 */
	@org.prelle.simplepersist.Attribute
	@AttribConvert(WeaponDamageConverter.class)
	protected int damage;
	@org.prelle.simplepersist.Attribute(required=false)
	protected String skill;
	@Element
	protected RequirementList requires;
	@Element
	protected FeatureList features;

	@org.prelle.simplepersist.Attribute
	private int range;
	@org.prelle.simplepersist.Attribute(name="price_amo",required=false)
	private int ammuPrice;

	//-------------------------------------------------------------------
	/**
	 */
	public WeaponData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IGearTypeData#copyToAttributes(de.rpgframework.genericrpg.items.AGearData)
	 */
	@Override
	public void copyToAttributes(AGearData copyTo) {
		if (attribute1!=null) copyTo.setAttribute(SMItemAttribute.ATTRIBUTE1, attribute1);
		if (attribute2!=null) copyTo.setAttribute(SMItemAttribute.ATTRIBUTE2, attribute2);
		if (     skill!=null) copyTo.setAttribute(SMItemAttribute.SKILL, SplitterMondCore.getSkill(skill));
		if (    damage!= 0  ) copyTo.setAttribute(SMItemAttribute.DAMAGE, damage);
		if (     speed!= 0  ) copyTo.setAttribute(SMItemAttribute.SPEED, speed);
		if (  requires!=null) copyTo.getRequirements().addAll(requires);
		if (  features!=null) copyTo.setAttribute(SMItemAttribute.FEATURES, features);
		if (    range!= 0  ) copyTo.setAttribute(SMItemAttribute.RANGE, range);
		if (ammuPrice!= 0  ) copyTo.setAttribute(SMItemAttribute.AMMUNITION_PRICE, ammuPrice);
	}

}

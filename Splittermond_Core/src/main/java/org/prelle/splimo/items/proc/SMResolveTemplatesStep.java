package org.prelle.splimo.items.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMPieceOfGearVariant;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class SMResolveTemplatesStep implements CarriedItemProcessor {

	final static Logger logger = SMGearTool.logger;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.data.Lifeform, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType ref, Lifeform charac, CarriedItem<?> model,
			List<Modification> unprocessed) {

		boolean changed = false;
		ItemTemplate resolved = (ItemTemplate) model.getResolved();
		SMPieceOfGearVariant variant = (SMPieceOfGearVariant) model.getVariant();
		if (resolved == null) {
			resolved = SplitterMondCore.getItem(ItemTemplate.class, model.getKey());
			if (resolved == null) {
				logger.log(Level.ERROR, "Item {0} refers to unknown item template ''{1}''", model.getUuid(),
						model.getKey());
				return new OperationResult<>();
			}
			changed = true;
		}
		if (model.getVariantID()!=null && model.getVariant()==null) {
			variant = (SMPieceOfGearVariant) resolved.getVariant(model.getVariantID());
			if (variant==null) {
				logger.log(Level.ERROR, "Item {0} refers to unknown variant ''{1}'' of template {2}", model.getUuid(),
						model.getVariantID(), model.getKey());
				return new OperationResult<>();
			}
			changed = true;
		}

		// Depth first
		// Since setResolved() triggers recalculation, resolve children first
		for (CarriedItem<ItemTemplate> child : ((CarriedItem<ItemTemplate>)model).getAccessories()) {
			OperationResult<List<Modification>> sub = this.process(strict, ref, charac, child, unprocessed);
		}

		if (changed) {
//			logger.log(Level.WARNING, "Resolve "+model.getKey()+"/"+model.getVariantID()+" to "+resolved+"/"+variant);
			if (variant!=null) {
				((CarriedItem<ItemTemplate>)model).setResolved(resolved, variant);
			} else {
				((CarriedItem<ItemTemplate>)model).setResolved(resolved);
			}
		}

		return new OperationResult<> (unprocessed);
	}

}

package org.prelle.splimo.items;

import java.util.Locale;

import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.items.Hook;

/**
 * 
 */
public enum EnhancementType implements Hook {
	
	NORMAL,
	HIGHARTIFACT,
	RELIC,
	MAGIC,
	ALCHEMY,
	DIVINE,
	SAINT	
	;

	private static MultiLanguageResourceBundle RES = SplitterMondCore.getI18nResources();

	boolean hasCapacity = false;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Hook#getName()
	 */
	@Override
	public String getName() {
		return RES.getString("enhancementtype."+this.name().toLowerCase());
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		return RES.getString("enhancementtype."+this.name().toLowerCase(), loc);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Hook#hasCapacity()
	 */
	@Override
	public boolean hasCapacity() {
		return hasCapacity;
	}

}

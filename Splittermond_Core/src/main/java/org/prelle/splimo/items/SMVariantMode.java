package org.prelle.splimo.items;

import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.items.IVariantMode;

/**
 * @author prelle
 *
 */
public enum SMVariantMode implements IVariantMode {
	
	CARRIED
	;
	
	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SplitterMondCore.class, "SplitterMondCore", Locale.GERMAN);

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IUsageMode#getName(java.util.Locale)
	 */
	@Override
	public String getName(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "equipmode."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IUsageMode#getName()
	 */
	@Override
	public String getName() {
		return getName(Locale.getDefault());
	}

}

package org.prelle.splimo.items;

import org.prelle.splimo.SplitterMondCore;

/**
 * @author prelle
 *
 */
public enum ItemSubType {
	
	NOT_SET,
	
	MELEE,
	BLADES,
	SLASHING,
	STAFFS,
	CHAINS,
	SHOOTING,
	THROWING,
	
	BODYARMOR,
	SHIELD,
	
	// PROFESSION
	SAGE_HEALER,
	SHADY,
	CRAFTING,
	ARTISTS,
	
	// DAILY
	CLOTHING,
	TRAVEL,
	CONTAINER,
	
	// ALCHEMY
	POTION,
	POISON,
	HERB
	;
	
	//-------------------------------------------------------------------
	public String getName() {
		return SplitterMondCore.getI18nResources().getString("itemsubtype."+this.name().toLowerCase());
	}

}

/**
 * 
 */
package org.prelle.splimo.items;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.persist.ItemTypeConverter;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="feature")
public class FeatureType extends DataItem {

	@Attribute(name="types")
	@AttribConvert(ItemTypeConverter.class)
	private Collection<ItemType> applyableTypes;
	@Attribute
	private boolean level;
	@Attribute
	private boolean remark;

    //-------------------------------------------------------------------
    public static FeatureType getByName(String name) {
    	if (name.equals("Wurftauglich")) name="Wurffähig";
		for (FeatureType tmp : SplitterMondCore.getFeatureTypes()) {
			if (tmp.getName().equalsIgnoreCase(name)) {
				return tmp;
			}
		}
		throw new IllegalArgumentException("'"+name+"'");
    }
	
    //-------------------------------------------------------------------
    public FeatureType() {
    	applyableTypes = new ArrayList<ItemType>();
    }

	//-------------------------------------------------------------------
	public boolean hasLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * The feature type is not a "Feature Type" as defined in the rules, but just
	 * something added by an enhancement that is memorized this way
	 */
	public boolean isRemark() {
		return remark;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the applyableTypes
	 */
	public Collection<ItemType> getApplyableItemTypes() {
		return applyableTypes;
	}

}

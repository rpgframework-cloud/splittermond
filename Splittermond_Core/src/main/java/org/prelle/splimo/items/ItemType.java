/**
 * 
 */
package org.prelle.splimo.items;

import java.util.Locale;

import org.prelle.splimo.SplitterMondCore;

/**
 * @author prelle
 *
 */
public enum ItemType {
	
	WEAPON_CLOSE,
	WEAPON_RANGED,
	PROJECTILE,
	ARMOR,
	SHIELD,
	PROFESSION,
    DAILY,
    ALCHEMY,
    TOOLS,
    ANIMALS,
    LIGHT,
	OTHER,
	
	ENHANCEMENT,
    ;

    public String getName() {
        return SplitterMondCore.getI18nResources().getString("itemtype."+name().toLowerCase());
    }

    public String getName(Locale loc) {
        return SplitterMondCore.getI18nResources().getString("itemtype."+name().toLowerCase(),loc);
    }

}

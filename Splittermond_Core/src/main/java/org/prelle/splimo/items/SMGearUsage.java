package org.prelle.splimo.items;

import java.util.List;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.IgnoreMissingAttributes;

import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.AlternateUsage;

/**
 * @author prelle
 *
 */
@IgnoreMissingAttributes("id")
@DataItemTypeKey(id="usage")
public class SMGearUsage extends AlternateUsage<SMUsageMode> {

	@Element
	private WeaponData weapon;
	@Element(name = "rangedweapon")
	private RangedWeaponData rangeWeapon;

	//-------------------------------------------------------------------
	public SMGearUsage() {
	}

	//-------------------------------------------------------------------
	public SMGearUsage(SMUsageMode mode) {
		this.mode = mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.AGearData#getTypeData()
	 */
	@Override
	public List<? extends IGearTypeData> getTypeData() {
		// TODO Auto-generated method stub
		return null;
	}

}

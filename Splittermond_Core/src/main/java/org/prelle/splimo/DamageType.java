/**
 * 
 */
package org.prelle.splimo;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public enum DamageType {

	PROFANE,
	
	FIRE,
	WATER,
	STONE,
	AIR,
	
	LIGHT,
	SHADOW,
	;
	
	public String getName() {
		return SplitterMondCore.getI18nResources().getString("damagetype."+name().toLowerCase());
	}
	
	public String getName(Locale loc) {
		return SplitterMondCore.getI18nResources().getString("damagetype."+name().toLowerCase(), loc);
	}
}

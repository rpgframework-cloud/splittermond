package org.prelle.splimo;

import de.rpgframework.genericrpg.data.CheckInfluence;

/**
 *
 */
public enum SMCheckInfluence implements CheckInfluence {

	/** Erfolgsgrad */
	SUCCESS,
	/** Bonus auf den Fertigkeitswert */
	BONUS,
	/** Reduzierter Aufschlag */
	REDUCE_MALUS,
	OTHER

}

/**
 * 
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="terraintypes")
@ElementList(entry="terraintype",type=TerrainType.class)
public class TerrainTypeList extends ArrayList<TerrainType> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public TerrainTypeList() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public TerrainTypeList(Collection<? extends TerrainType> c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

}

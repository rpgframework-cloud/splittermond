/**
 * 
 */
package org.prelle.splimo;

/**
 * @author prelle
 *
 */
public interface MastershipOrSpecialization extends Comparable<MastershipOrSpecialization> {

	//-------------------------------------------------------------------
	public String getName();
	
	//-------------------------------------------------------------------
	public SMSkill getSkill();
	
	//-------------------------------------------------------------------
	public int getLevel();
	
}

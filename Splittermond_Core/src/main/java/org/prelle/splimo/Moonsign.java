/**
 *
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="moonsign")
public class Moonsign extends DataItem {

	//--------------------------------------------------------------------
	public String getLevelText(int level) {
		String baseKey = getTypeString()+"."+id.toLowerCase();
		String key = baseKey+".noLevel";
		switch (level){
		case 1: key = baseKey + ".level1"; break;
		case 2: key = baseKey + ".level2"; break;
		case 4: key = baseKey + ".level4"; break;
		}
		List<String> keys = new ArrayList<>();
		if (parentItem!=null) {
			keys.add(parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key);
		}
		keys.add(key);
		String result = getLocalizedString(Locale.GERMAN, keys);
		if ("?No_Key_Found?".equals(result)) return null;
		return result;
	}

}

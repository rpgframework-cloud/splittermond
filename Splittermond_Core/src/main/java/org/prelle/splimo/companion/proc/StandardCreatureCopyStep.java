package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureWeapon;
import org.prelle.splimo.creature.SMLifeform.LifeformType;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class StandardCreatureCopyStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public StandardCreatureCopyStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		logger.log(Level.INFO, "process for {0} vs {1}",model, model);
		if (model.getLifeformType()!=LifeformType.CREATURE)
			return new OperationResult<List<Modification>>(unprocessed);

		if (model.getStandardCreature()==null) 
			return new OperationResult<List<Modification>>(unprocessed);

		Creature standard = SplitterMondCore.getItem(Creature.class, model.getStandardCreature());
		if (standard==null) {
			logger.log(Level.WARNING, "Creature {0} references unknown standard creature {1}", model.getName(), model.getStandardCreature());
			return new OperationResult<List<Modification>>(unprocessed);
		}
		
		if (model.getName()==null)
			model.setName(standard.getName());
		
		for (Attribute key : Attribute.values()) {
			AttributeValue<Attribute> aVal = standard.getAttribute(key);
			if (aVal!=null) {
				model.setAttribute(key, aVal.getDistributed());
			}
		}

		for (SMSkillValue sVal : standard.getSkillValues()) {
			model.setSkill(sVal);
		}
		
		// Types
		standard.getCreatureTypes().forEach(ct -> model.addCreatureType(ct));
		standard.getFeatures().forEach(f -> model.addCreatureFeature(f));

		for (CreatureWeapon weapon : standard.getCreatureWeapons()) {
			CreatureWeapon cloned = weapon.clone();
			model.addWeapon(cloned);
		}

		return new OperationResult<List<Modification>>(unprocessed);
	}

}

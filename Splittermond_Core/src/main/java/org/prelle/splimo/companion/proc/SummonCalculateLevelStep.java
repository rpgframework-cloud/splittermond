package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.creature.SummonableCreature;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SummonCalculateLevelStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public SummonCalculateLevelStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(unprocessed);

		SummonableCreature result = model.getSummonable();
		int cost = 0;
		if (result.getBody()!=null) cost+=result.getBody().getResolved().getCost();
		if (result.getType()!=null) cost+=result.getType().getResolved().getCost();
		for (CreatureModuleValue mod : result.getModules()) {
			cost += 1;
		}
		model.setAttribute(Attribute.LEVEL, cost);
		CreatureFeatureValue val = model.addCreatureFeature(SplitterMondCore.getCreatureFeatureType("summonable"));
		val.setDistributed(cost);
		logger.log(Level.INFO, "Level is {0}",cost);
		return new OperationResult<List<Modification>>(unprocessed);
	}

}

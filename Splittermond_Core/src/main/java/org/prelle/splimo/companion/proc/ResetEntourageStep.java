package org.prelle.splimo.companion.proc;

import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.creature.Companion;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class ResetEntourageStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public ResetEntourageStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		
		for (Attribute attr : Attribute.primaryValues()) {
			model.setAttribute(attr, 2);
		}
				
		return new OperationResult<>(unprocessed);
	}

}

package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.CreatePoints;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureWeapon;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class SummonableWeaponStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public SummonableWeaponStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	private boolean applyModification(Modification mod) {
		if (mod instanceof ModificationChoice) {
			return false;
		}

		switch ((SplittermondReference)mod.getReferenceType()) {
		case CREATION_POINTS:
			ValueModification vMod = (ValueModification)mod;
			CreatePoints type = mod.getReferenceType().resolve(vMod.getKey());
			if (type==CreatePoints.ATTACK) {
				applyAttackModification(vMod);
				return true;
			}
			break;
		}

		return false;
	}

	//-------------------------------------------------------------------
	private void applyAttackModification(ValueModification vMod) {
		logger.log(Level.WARNING, "Add {0} to attack", vMod.getValue());
		for (CreatureWeapon weapon : getModel().getCreatureWeapons()) {
			weapon.setValue( weapon.getValue() + vMod.getValue());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> previous) {
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(previous);

		SummonableCreature result = model.getSummonable();
		List<Modification> unprocessed = new ArrayList<>();
		for (Modification mod : previous) {
			if (!applyModification(mod)) {
				unprocessed.add(mod);
			}
		}
		return new OperationResult<List<Modification>>(unprocessed);
	}

}

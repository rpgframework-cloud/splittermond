package org.prelle.splimo.companion.proc;

import java.lang.System.Logger;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.creature.Companion;

import de.rpgframework.character.ExtendedProcessingStep;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * 
 */
public abstract class APartialLifeformController implements ExtendedProcessingStep,PartialLifeformController<Companion> {

	protected final static Logger logger = System.getLogger(APartialLifeformController.class.getPackageName());
	
	protected Companion model;
	protected List<ToDoElement> todos;

	//-------------------------------------------------------------------
	/**
	 */
	public APartialLifeformController(Companion model) {
		this.model = model;
		todos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.companion.proc.PartialLifeformController#getModel()
	 */
	@Override
	public Companion getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.companion.proc.PartialLifeformController#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

}

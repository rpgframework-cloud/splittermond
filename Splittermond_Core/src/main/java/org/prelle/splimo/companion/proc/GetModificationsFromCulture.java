package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Culture;
import org.prelle.splimo.Race;
import org.prelle.splimo.creature.Companion;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromCulture extends APartialLifeformController implements PartialLifeformController<Companion> {

	//-------------------------------------------------------------------
	public GetModificationsFromCulture(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.SpliMoCharacter, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		todos.clear();
		logger.log(Level.WARNING, "ENTER: process");
		try {
			ComplexDataItemValue<Culture> value = model.getCulture();
			// Apply modifications by race
			if (value!=null) {
				Culture race = value.getResolved();
				//updateChoices(race);
				logger.log(Level.DEBUG, "1. Apply modifications from culture "+value+" = "+race.getOutgoingModifications());
				for (Modification tmp : race.getOutgoingModifications()) {
					if (tmp.getSource()==null)
						tmp.setSource(race);

					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						// Does this modification require a choice?
						if (dMod.getConnectedChoice()!=null) {
							Choice choice = race.getChoice(dMod.getConnectedChoice());
							if (choice==null) {
								logger.log(Level.ERROR, "Modification "+dMod+" of "+race.getTypeString()+" references unknown modification "+dMod.getConnectedChoice());
								continue;
							}
							Decision dec = value.getDecision(dMod.getConnectedChoice());
							if (dec==null) {
								// User has not yet decided
								logger.log(Level.ERROR, "No decision yet for choice "+dMod.getConnectedChoice());
								todos.add(new ToDoElement(Severity.STOPPER, "Noch fehlende Entscheidung(en) für Kultur"));
							} else {
								// User already made a decision
								List<Modification> list = GenericRPGTools.decisionToModifications(dMod, choice, dec);
								logger.log(Level.ERROR, "Convert decision of "+choice.getUUID()+" into "+list.size()+" modifications");
								unprocessed.addAll(list);
							}
							continue;
						}
					}
					logger.log(Level.INFO, "Inject modification {0}",tmp);
					unprocessed.add(tmp);
				}
			} else {
				logger.log(Level.WARNING, "Culture not selected yet");
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		OperationResult<List<Modification>> result = new OperationResult<List<Modification>>(unprocessed);
		todos.forEach(todo -> result.addMessage(todo));
		return result;
	}

}

package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureWeapon;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.creature.SummonableCreature;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SummonableBodyStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public SummonableBodyStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		logger.log(Level.INFO, "process for {0} vs {1}",model, model);
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(unprocessed);

		SummonableCreature result = model.getSummonable();
		if (result.getBody()==null) {
			todos.add(new ToDoElement(Severity.STOPPER, "Noch kein Basis Körper gewählt"));
			return new OperationResult<List<Modification>>(unprocessed);
		}
		CreatureModule body = result.getBody().getResolved();
		logger.log(Level.INFO, "Apply body {0}", body.getId());

		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue<Attribute> aVal = body.getAttribute(key);
			model.setAttribute(aVal.getModifyable(), aVal.getDistributed());
		}
		for (Attribute key : Attribute.secondaryValues()) {
			AttributeValue<Attribute> aVal = body.getAttribute(key);
			if (aVal==null) {
				logger.log(Level.WARNING, "No value for attribute {0} in {1}", key, body.getId());
				continue;
			}
			model.setAttribute(aVal.getModifyable(), aVal.getDistributed());
		}

			for (CreatureWeapon weapon : body.getCreatureWeapons()) {
				CreatureWeapon cloned = weapon.clone();
				model.addWeapon(cloned);
			}


		// Add modifications from module
		unprocessed.addAll(body.getOutgoingModifications());

		return new OperationResult<List<Modification>>(unprocessed);
	}

}

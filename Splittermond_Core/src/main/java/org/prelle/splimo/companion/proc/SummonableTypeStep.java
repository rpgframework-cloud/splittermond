package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.creature.SummonableCreature;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SummonableTypeStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public SummonableTypeStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(unprocessed);

		SummonableCreature result = model.getSummonable();
		if (result.getType()==null) {
			todos.add(new ToDoElement(Severity.STOPPER, "Noch kein Typus gewählt"));
			return new OperationResult<List<Modification>>(unprocessed);
		}
		CreatureModule module = result.getType().getResolved();
		logger.log(Level.INFO, "Get from type {0}", module.getId());

		// Add modifications from module
		unprocessed.addAll(module.getOutgoingModifications());

		return new OperationResult<List<Modification>>(unprocessed);
	}

}

package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.ServiceValue;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splimo.creature.SummonableCreature;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SummonableServiceStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public SummonableServiceStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> previous) {
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(previous);

		List<Modification> unprocessed = new ArrayList<>();
		SummonableCreature result = model.getSummonable();
//		for (Modification mod : previous) {
//			if (mod.getReferenceType()==SplittermondReference.SERVICE) {
//				DataItemModification sMod = (DataItemModification)mod;
//				ServiceValue service = new ServiceValue(sMod.getResolvedKey());
//				sMod.getDecisions().forEach(d -> service.addDecision(d));
//
//				if (canAddService(service)) {
//					logger.log(Level.TRACE, "Add service {0}", service.getKey());
//					available.add(service);
//				} else {
//					logger.log(Level.TRACE, "Avoid duplicate {0}", service.getKey());
//				}
//			} else
//				unprocessed.add(mod);
//		}

		return new OperationResult<List<Modification>>(unprocessed);
	}

}

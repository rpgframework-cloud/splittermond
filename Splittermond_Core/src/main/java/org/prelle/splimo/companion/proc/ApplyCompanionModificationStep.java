package org.prelle.splimo.companion.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.creature.CreatureTypeValue;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.character.ExtendedProcessingStep;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaTool;
import de.rpgframework.genericrpg.items.formula.VariableResolver;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ApplyCompanionModificationStep implements ExtendedProcessingStep {

	private final static Logger logger = System.getLogger(ApplyCompanionModificationStep.class.getPackageName());
	
	private Companion model;

	//-------------------------------------------------------------------
	public ApplyCompanionModificationStep(Companion model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	private void applySkillModification(ValueModification mod) {
		SMSkill skill = mod.getReferenceType().resolve(mod.getKey());
		SMSkillValue sVal = model.getSkillValue(skill);
		if (sVal==null) {
			logger.log(Level.DEBUG, "  Add new skill {0} at value {1}", skill.getId(), mod.getValue());
			sVal = new SMSkillValue(skill, mod.getValue());
			model.addSkill(sVal);
		} else {
			if (mod.isModMin()) {
				int[] modMin = mod.getModMinValues();
				int need = modMin[1] - sVal.getDistributed();
				int get  = Math.max(modMin[0], need);
				logger.log(Level.DEBUG, "  Add {1} to skill {0}", skill.getId(), get);
				sVal.setDistributed( sVal.getDistributed() + get);
			} else {
//				logger.log(Level.DEBUG, "  Add {1} to skill {0}", skill.getId(), mod.getValue());
				sVal.setDistributed( sVal.getDistributed() + mod.getValue());
			}
		}
	}

	//-------------------------------------------------------------------
	private void applySpellModification(DataItemModification mod) {
		String key = mod.getKey();
		logger.log(Level.DEBUG, "resolve {0}",key);
		int pos = key.indexOf(":");
		String schoolID = key.substring(0,pos);
		String spellID  = key.substring(pos+1);
		Spell spell = mod.getReferenceType().resolve(spellID);
		SMSkill school = SplitterMondCore.getSkill(schoolID);
		SpellValue sVal = new SpellValue(spell, school);
		logger.log(Level.DEBUG, "  Add new spell {0} in school{1}", spellID, schoolID);
		model.addSpell(sVal);
	}

	//-------------------------------------------------------------------
	private void applyAttributeModification(ValueModification mod) {
		Attribute key = mod.getReferenceType().resolve(mod.getKey());
		AttributeValue<Attribute> sVal = model.getAttribute(key);
		if (sVal==null) {
			logger.log(Level.ERROR, "TODO: Add new attribute {0} at value {1}", key, mod.getValue());
		} else {
			logger.log(Level.TRACE, "  Add {1} to attribute {0}", key, mod.getValue());
			sVal.setDistributed( sVal.getDistributed() + mod.getValue());
		}
	}

	//-------------------------------------------------------------------
	private boolean applyModification(Modification mod) {
		if (mod instanceof ModificationChoice) {
			ModificationChoice cMod = (ModificationChoice)mod;
			logger.log(Level.ERROR, "There shouldn't be any ModificationChoices up to this point");
			return false;
		}

		switch ((SplittermondReference)mod.getReferenceType()) {
		case ATTRIBUTE:
			applyAttributeModification( (ValueModification)mod );
			return true;
		case CREATURE_FEATURE_TYPE:
			DataItemModification cfMod = ((DataItemModification)mod);
			CreatureFeature cFeat = mod.getReferenceType().resolveAsDataItem(cfMod.getKey());
			CreatureFeatureValue cfVal = model.addCreatureFeature(cFeat);
			if (cFeat.hasLevel()) {
				ValueModification valMod = (ValueModification)cfMod;
				if (valMod.hasFormula()) {
					Formula formula = valMod.getFormula();
					logger.log(Level.WARNING, "Resolve formula "+formula);
					String resolved = FormulaTool.resolve(valMod.getReferenceType(), formula, new VariableResolver(null, model));
					int realVal = Integer.parseInt(resolved);
					cfVal.setLevel(realVal);
				} else {
					cfVal.setLevel(valMod.getValue());
				}
			}
			if (!cfMod.getDecisions().isEmpty()) {
				cfMod.getDecisions().forEach(d -> cfVal.addDecision(d));
			}
			return true;
		case CREATURE_TYPE:
			DataItemModification ctMod = ((DataItemModification)mod);
			CreatureType cType = mod.getReferenceType().resolveAsDataItem(ctMod.getKey());
			model.addCreatureType(new CreatureTypeValue(cType));
			return true;
		case SKILL:
			applySkillModification( (ValueModification)mod );
			return true;
		case SPELL:
			applySpellModification( (DataItemModification) mod );
			return true;
		case SERVICE:
		case CREATION_POINTS:
			return false;
		default:
			logger.log(Level.WARNING, "Don't know how to apply "+mod.getReferenceType());
			return false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		for (Modification mod : previous) {
			if (!applyModification(mod)) {
				unprocessed.add(mod);
			}
		}
		return new OperationResult<>(unprocessed);
	}

}

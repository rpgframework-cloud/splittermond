package org.prelle.splimo.companion.proc;

import java.lang.System.Logger.Level;
import java.util.List;
import java.util.UUID;

import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splimo.creature.SMLifeform.LifeformType;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;

/**
 * @author prelle
 *
 */
public class SummonableExtraModulesStep extends APartialLifeformController {

	//-------------------------------------------------------------------
	public SummonableExtraModulesStep(Companion model) {
		super(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		logger.log(Level.INFO, "process for {0} vs {1}",model, model);
		if (model.getLifeformType()!=LifeformType.SUMMONABLE)
			return new OperationResult<List<Modification>>(unprocessed);

		SummonableCreature result = model.getSummonable();

		for (CreatureModuleValue module : result.getModules()) {
			logger.log(Level.INFO, "Get from type {0}", module.getKey());

			// Clear previous modifications
			module.clearOutgoingModifications();

			// Check that all expected decisions are present
			for (Choice choice : module.getResolved().getChoices()) {
				Decision dec = module.getDecision(choice.getUUID());
				if (dec==null) {
					logger.log(Level.WARNING, "Missing decision {0} for {1}", choice.getUUID(), module.getKey());
				}
			}

			// Build new
			for (Modification mod : module.getResolved().getOutgoingModifications()) {
				if (mod instanceof DataItemModification) {
					Modification newMod = resolveModification(module, (DataItemModification)mod);
					if (newMod!=null) {
						module.addOutgoingModification( newMod );
					}
				} else if (mod instanceof ModificationChoice) {
					UUID uuid = ((ModificationChoice)mod).getUUID();
					Modification newMod = resolveModification(module, (ModificationChoice)mod );
					if (newMod!=null) {
						logger.log(Level.DEBUG, "<selmod uuid=\"{0}\"> was {1}", uuid, newMod);
						module.addOutgoingModification( newMod );
					} else {
						logger.log(Level.DEBUG, "No decision found for <selmod uuid=\"{0}\">",uuid);
					}
				} else
					module.addOutgoingModification(mod);
			}

			// Add modifications from module
			unprocessed.addAll(module.getOutgoingModifications());
		}

		return new OperationResult<List<Modification>>(unprocessed);
	}

	//-------------------------------------------------------------------
	private Modification resolveModification(CreatureModuleValue val, ModificationChoice mod) {
		// Obtain the decision made for this choice
		UUID choiceUUID = mod.getUUID();
		if (choiceUUID==null) {
			logger.log(Level.ERROR, "<selmod> without a uuid-Attribute in "+mod.getSource());
			return null;
		}

		// Find decision for this modification choice
		Decision dec = val.getDecision(choiceUUID);
		if (dec==null) {
			logger.log(Level.WARNING, "<selmod uuid={0}> from {1} not decided yet ", choiceUUID, val.getKey());
			return null;
		}

		// The decision value is expected to be a UUID that references an option in the <selmod>
		UUID decisionUUID = UUID.fromString(dec.getValue());
		return mod.getModification(decisionUUID);
	}

	private Modification resolveModification(CreatureModuleValue val, DataItemModification mod) {
		if (!  "CHOICE".equalsIgnoreCase(mod.getKey())) {
			return mod;
		}
		CreatureModule module = val.getResolved();
		UUID choiceUUID = mod.getConnectedChoice();
		Choice choice = module.getChoice( choiceUUID );
		if (choice==null) {
			logger.log(Level.WARNING, "Unknown modification {0} referenced in modification {1} from {2}", choiceUUID, mod, module.getId());
			return null;
		}
		// Locale decision for choice
		Decision dec = val.getDecision(choiceUUID);
		if (dec==null) {
			logger.log(Level.WARNING, "Decision for choice {0} missing in {1}", choiceUUID, mod);
			return null;
		}

		Modification newMod = SplitterTools.instantiateModification(mod, dec);

		return newMod;
	}

}

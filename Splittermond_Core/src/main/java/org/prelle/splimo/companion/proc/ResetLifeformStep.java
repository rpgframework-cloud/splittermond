package org.prelle.splimo.companion.proc;

import java.util.List;

import org.prelle.splimo.creature.Companion;

import de.rpgframework.character.ExtendedProcessingStep;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ResetLifeformStep implements ExtendedProcessingStep {
	
	private Companion model;

	//-------------------------------------------------------------------
	public ResetLifeformStep(Companion model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(List<Modification> unprocessed) {
		model.clearCalculations();
		
		
		return new OperationResult<>(unprocessed);
	}

}

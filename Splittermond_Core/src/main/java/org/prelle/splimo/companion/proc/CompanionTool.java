package org.prelle.splimo.companion.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.splimo.creature.Companion;

import de.rpgframework.character.ExtendedProcessingStep;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class CompanionTool {
	
	private final static Logger logger = System.getLogger(CompanionTool.class.getPackageName());
	
	private final static List<Class<? extends ExtendedProcessingStep>> ENTOURAGE_STEPS = Arrays.asList(
			ResetEntourageStep.class,
			GetModificationsFromRace.class,
			GetModificationsFromCulture.class,
			GetModificationsFromBackground.class,
			ApplyCompanionModificationStep.class
			);
	
	private final static List<Class<? extends ExtendedProcessingStep>> SUMMONABLE_STEPS = Arrays.asList(
			ResetLifeformStep.class,
			SummonableBodyStep.class,
			SummonableTypeStep.class,
			SummonableExtraModulesStep.class,
			SummonableServiceStep.class,
			SummonCalculateLevelStep.class,
			SummonableWeaponStep.class,
			ApplyCompanionModificationStep.class
			);
	
	private final static List<Class<? extends ExtendedProcessingStep>> CREATURE_STEPS = Arrays.asList(
			ResetLifeformStep.class,
			StandardCreatureCopyStep.class,
			ApplyCompanionModificationStep.class
			);

	//-------------------------------------------------------------------
	public static void recalculate(Companion companion) {
		List<ExtendedProcessingStep> steps = new ArrayList<>();
		switch (companion.getLifeformType()) {
		case ENTOURAGE:
			recalculate(companion, instantiate(ENTOURAGE_STEPS, companion));
			break;
		case SUMMONABLE:
			recalculate(companion, instantiate(SUMMONABLE_STEPS, companion));
			break;
		case CREATURE:
			recalculate(companion, instantiate(CREATURE_STEPS, companion));
			break;
		default:
			logger.log(Level.ERROR, "Don't know how to "+companion.getLifeformType());
			System.err.println("CompanionTool: Don't know how to "+companion.getLifeformType());
		}
	}
	
	//--------------------------------------------------------------------
	private static List<ExtendedProcessingStep> instantiate(List<Class<? extends ExtendedProcessingStep>> classes, Companion model) {
		List<ExtendedProcessingStep> steps = new ArrayList<>();
		for (Class<? extends ExtendedProcessingStep> clazz : classes) {
			try {
				Constructor<? extends ExtendedProcessingStep> con = clazz.getConstructor(Companion.class);
				steps.add( con.newInstance(model) );
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error instantiating "+clazz,e);
			}
		}
		return steps;
	}
	
	//--------------------------------------------------------------------
	/**
	 * - Copy resolved stats to attributes
	 * - Process decisions
	 * - Process modifications from "Modifications" (Permanent changes made to the base item)
	 * - Copy unresolved stats to attributes
	 *
	 * - Process modifications from accessories
	 */
	private static OperationResult<List<Modification>> recalculate(Companion model, List<ExtendedProcessingStep> steps) {
		logger.log(Level.INFO, "recalculate {0}", model.getName());

		// Recalculate accessories
//		for (CarriedItem acc : item.getAccessories()) {
//			recalculate(indent+"", refType, user, acc);
//		}

		OperationResult<List<Modification>> unprocessed = new OperationResult(new ArrayList<>());
		try {
			for (ExtendedProcessingStep step : steps) {
				if (logger.isLoggable(Level.TRACE))
					logger.log(Level.TRACE, "  run "+step.getClass().getSimpleName());
				OperationResult<List<Modification>> result = step.process(unprocessed.get());
				if (result.hasError()) {
						logger.log(Level.WARNING, "Error recalculating companion {0}: {1}",model.getLinkUUID(), result.getMessages());
//						return unprocessed;
				} 
					// Replace previous data with current
					unprocessed.set( result.get() );
				
				// Copy all warnings to summarized object
				unprocessed.getMessages().addAll(result.getMessages());
				if (logger.isLoggable(Level.TRACE))
					logger.log(Level.TRACE, "  after "+step.getClass().getSimpleName()+" = "+unprocessed);
//				logger.log(Level.TRACE, prefix+"  after "+step.getClass().getSimpleName()+"\n"+item.dump());
			}
			logger.log(Level.DEBUG, "  unprocessed = "+unprocessed);
			logger.log(Level.DEBUG, "  messages    = "+unprocessed.getMessages());
			//unprocessed.get().forEach( tmp -> item.addModification(tmp));
//		logger.log(Level.DEBUG, prefix+item.dump());
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error recalculating companion",e);
		}

		//item.setLastRecalculateResult(unprocessed);
		return unprocessed;
	}

}

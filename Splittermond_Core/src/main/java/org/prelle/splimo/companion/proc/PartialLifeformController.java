package org.prelle.splimo.companion.proc;

import java.util.List;

import org.prelle.splimo.creature.SMLifeform;

import de.rpgframework.character.ExtendedProcessingStep;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author prelle
 *
 */
public interface PartialLifeformController<L extends SMLifeform> extends ExtendedProcessingStep {

	//-------------------------------------------------------------------
	public L getModel();

	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<ToDoElement> getToDos();

}

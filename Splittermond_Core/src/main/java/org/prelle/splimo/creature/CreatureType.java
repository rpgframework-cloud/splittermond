/**
 * 
 */
package org.prelle.splimo.creature;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="creature.type")
public class CreatureType extends DataItem {
	
	@Attribute(name="levels",required=false)
	private boolean levels;
	@Attribute(name="special",required=false)
	private boolean special;

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public boolean hasLevel() {
		return levels;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the special
	 */
	public boolean isSpecial() {
		return special;
	}

}

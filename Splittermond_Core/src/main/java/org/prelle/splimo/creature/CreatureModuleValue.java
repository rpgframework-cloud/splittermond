package org.prelle.splimo.creature;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class CreatureModuleValue extends ComplexDataItemValue<CreatureModule> {

	//-------------------------------------------------------------------
	/**
	 */
	public CreatureModuleValue() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 */
	public CreatureModuleValue(CreatureModule data) {
		super(data);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 * @param val
	 */
	public CreatureModuleValue(CreatureModule data, int val) {
		super(data, val);
		// TODO Auto-generated constructor stub
	}

}

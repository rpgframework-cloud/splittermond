/**
 *
 */
package org.prelle.splimo.creature;

import java.util.List;
import java.util.UUID;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.genericrpg.data.Lifeform;

/**
 * @author prelle
 *
 */
public interface SMLifeform extends Lifeform<Attribute,SMSkill,SMSkillValue> {
	
	public static enum LifeformType {
		ENTOURAGE,
		CREATURE,
		SUMMONABLE,
		GOLEM
		;
	    public String getName() {
	        return SplitterMondCore.getI18nResources().getString("lifeform."+name().toLowerCase());
	    }
	}

	//-------------------------------------------------------------------
	public LifeformType getLifeformType();

	//-------------------------------------------------------------------
	/**
	 * Returns either required level in KREATUR, GEFOLGE, GOLEM bzw. Beschwörbares Wesen
	 */
	public int getLevel();

	//-------------------------------------------------------------------
	public UUID getLinkUUID();

	//-------------------------------------------------------------------
	public byte[] getImage();

	//-------------------------------------------------------------------
	public List<CreatureTypeValue> getCreatureTypes();

	//-------------------------------------------------------------------
	public CreatureTypeValue getCreatureType(CreatureType key);

	//-------------------------------------------------------------------
    /**
     * Returns a list of skills of a given type.
     * @param type SkillType
     * @return List of skills of the given type, sorted alphabetically
     */
    public List<SMSkillValue> getSkills(SkillType type);

	//-------------------------------------------------------------------
	public boolean hasSkill(SMSkill skill);

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells();

	//--------------------------------------------------------------------
	public boolean hasSpell(SpellValue spell);

	//-------------------------------------------------------------------
	/**
	 * Get the value for a specific spell, including skill value and
	 * eventually existing spell type specializations
	 */
	public int getSpellValueFor(SpellValue spellVal);

	//-------------------------------------------------------------------
	public List<CreatureWeapon> getCreatureWeapons();

	//-------------------------------------------------------------------
	public List<CreatureFeatureValue> getFeatures();

}

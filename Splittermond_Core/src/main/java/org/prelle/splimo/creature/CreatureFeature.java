/**
 *
 */
package org.prelle.splimo.creature;

import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "creaturefeature")
public class CreatureFeature extends ComplexDataItem {

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s(lvl=%s, sel=%s)", id, hasLevel, choices);
	}

	//-------------------------------------------------------------------
	/**
	 * Convenience method
	 * @return the select
	 */
	public SplittermondReference getSelect() {
		if (choices.isEmpty())
			return null;
		return (SplittermondReference) choices.get(0).getChooseFrom();
	}

}

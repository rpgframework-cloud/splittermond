package org.prelle.splimo.creature;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.Service;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class ServiceValue extends ComplexDataItemValue<Service> {

	@Attribute(name="base")
	private boolean baseService;

	//-------------------------------------------------------------------
	public ServiceValue() {
	}

	//-------------------------------------------------------------------
	public ServiceValue(Service data) {
		super(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the baseService
	 */
	public boolean isBaseService() {
		return baseService;
	}

	//-------------------------------------------------------------------
	/**
	 * @param baseService the baseService to set
	 */
	public void setBaseService(boolean baseService) {
		this.baseService = baseService;
	}

}

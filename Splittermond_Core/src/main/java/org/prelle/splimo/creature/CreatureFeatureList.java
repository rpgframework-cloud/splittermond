/**
 * 
 */
package org.prelle.splimo.creature;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="creaturefeatures")
@ElementList(entry="creaturefeature",type=CreatureFeature.class)
public class CreatureFeatureList extends ArrayList<CreatureFeature> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	public CreatureFeatureList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public CreatureFeatureList(Collection<? extends CreatureFeature> c) {
		super(c);
	}

}

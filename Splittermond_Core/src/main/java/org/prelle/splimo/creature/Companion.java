package org.prelle.splimo.creature;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Race;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillSpecialization.SkillSpecializationType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.persist.WeaponDamageConverter;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 *
 */
@Root(name = "companion")
@DataItemTypeKey(id = "companion")
public class Companion extends DataItem implements SMLifeform {

	public static class CustomCreature {
		@org.prelle.simplepersist.Attribute
		protected String base;
		@Element
		protected ComplexDataItemValue<CreatureModule> role;
//		@ElementList(type=CreatureModule.class,entry="option",convert=CreatureModuleConverter.class)
		protected List<ComplexDataItemValue<CreatureModule>> options;

	}


	@org.prelle.simplepersist.Attribute(required = true)
	private SMLifeform.LifeformType type;
	@Element
	private String name;
	@Element
	private UUID uuid;
	@Element
	private byte[] image;

	/** Valid for predefined creatures */
	@Element
	private String standardCreature;

	/** Valid for custom creatures */
	@Element
	private CustomCreature customCreature;

	/** Valid for predefined and custom creatures */
	@Element
	private ComplexDataItemValue<CreatureModule> training;
	/** Valid for predefined and custom creatures */
	@ElementList(type=MastershipReference.class,entry="master")
	private List<MastershipReference> masterships;

	/** Valid for summonable lifeforms */
	@Element
	private SummonableCreature summonable;

	/** Valid for entourage */
	@Element
	private ComplexDataItemValue<Race> race;
	/** Valid for entourage */
	@Element
	private ComplexDataItemValue<Culture> culture;
	/** Valid for entourage */
	@Element
	private ComplexDataItemValue<Background> background;
	/** Valid for entourage */
	@Element
	private String increasedSkills;
	/** Valid for entourage */
	@Element
	/** Valid for entourage */
	private String freeChoices;
	@Element
	private String boughtChoices;

	protected transient List<AttributeValue<Attribute>> attributes;
	protected transient List<CreatureTypeValue> creaturetypes;
	protected transient int levelAlone;
	protected transient int levelGroup;
	protected transient List<CreatureWeapon> weapons;
	protected transient List<SMSkillValue> skillvals;
	protected transient List<SpellValue> spellvals;
	protected transient List<CreatureFeatureValue> features;

	//-------------------------------------------------------------------
	public Companion() {
		attributes = new ArrayList<>();
		for (Attribute attr : Attribute.values())
			attributes.add(new AttributeValue<Attribute>(attr));
		weapons      = new ArrayList<CreatureWeapon>();
		skillvals    = new ArrayList<SMSkillValue>();
		spellvals    = new ArrayList<SpellValue>();
		creaturetypes= new ArrayList<CreatureTypeValue>();
		features     = new ArrayList<CreatureFeatureValue>();
		masterships  = new ArrayList<MastershipReference>();
	}

	//-------------------------------------------------------------------
	public Companion(LifeformType type) {
		this();
		this.id   = "not_set";
		this.type = type;
//		options      = new ArrayList<CreatureModule>();
		uuid = UUID.randomUUID();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
	public Companion setName(String value) { this.name = value; return this; }

	//-------------------------------------------------------------------
	public void clearCalculations() {
		weapons.clear();
		skillvals.clear();
		spellvals.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getLevel()
	 */
	@Override
	public int getLevel() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getLinkUUID()
	 */
	@Override
	public UUID getLinkUUID() {
		return uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getLifeformType()
	 */
	@Override
	public LifeformType getLifeformType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getImage()
	 */
	@Override
	public byte[] getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the race
	 */
	public ComplexDataItemValue<Race> getRace() {
		return race;
	}

	//-------------------------------------------------------------------
	/**
	 * @param race the race to set
	 */
	public void setRace(ComplexDataItemValue<Race> race) {
		this.race = race;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the culture
	 */
	public ComplexDataItemValue<Culture> getCulture() {
		return culture;
	}

	//-------------------------------------------------------------------
	/**
	 * @param culture the culture to set
	 */
	public void setCulture(ComplexDataItemValue<Culture> culture) {
		this.culture = culture;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the background
	 */
	public ComplexDataItemValue<Background> getBackground() {
		return background;
	}

	//-------------------------------------------------------------------
	/**
	 * @param background the background to set
	 */
	public void setBackground(ComplexDataItemValue<Background> background) {
		this.background = background;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the increasedSkills
	 */
	public String getIncreasedSkills() {
		return increasedSkills;
	}

	//-------------------------------------------------------------------
	/**
	 * @param increasedSkills the increasedSkills to set
	 */
	public void setIncreasedSkills(String increasedSkills) {
		this.increasedSkills = increasedSkills;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the freeChoices
	 */
	public String getFreeChoices() {
		return freeChoices;
	}

	//-------------------------------------------------------------------
	/**
	 * @param freeChoices the freeChoices to set
	 */
	public void setFreeChoices(String freeChoices) {
		this.freeChoices = freeChoices;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the boughtChoices
	 */
	public String getBoughtChoices() {
		return boughtChoices;
	}

	//-------------------------------------------------------------------
	/**
	 * @param boughtChoices the boughtChoices to set
	 */
	public void setBoughtChoices(String boughtChoices) {
		this.boughtChoices = boughtChoices;
	}
	//-------------------------------------------------------------------
	public String dump() {
		WeaponDamageConverter dmgConvert = new WeaponDamageConverter();
		StringBuffer buf = new StringBuffer();
		buf.append("Name  : "+name);

		for (Attribute attr : Attribute.values()) {
			buf.append("\n"+attr.getShortName()+": "+getAttribute(attr)+"  \t");
		}

		buf.append("\n\nWaffen:\n=============");
		for (CreatureWeapon tmp : weapons) {
//			buf.append("\n "+tmp.getName()+" : "+tmp.getValue()+"  "+dmgConvert.write(tmp.getDamage())+" "+tmp.getSpeed()+"  "+tmp.getInitiative()+"  "+tmp.getFeatures());
			buf.append("\n "+tmp.getName()+" : "+tmp.getValue()+"  "+dmgConvert.write(tmp.getDamage())+" "+tmp.getSpeed()+"  "+tmp.getInitiative());
		}

		buf.append("\n\nFertigkeiten:\n=============");
		for (SMSkillValue tmp : skillvals) {
			buf.append("\n "+tmp.getSkill().getName()+" : "+tmp.getModifiedValue());
		}

		buf.append("\n\nZauber:\n=============");
		for (SpellValue tmp : spellvals) {
			buf.append("\n "+tmp.getResolved().getName()+" : "+tmp);
		}

		buf.append("\n\nMerkmale:\n=============");
		for (CreatureFeatureValue tmp : features) {
			buf.append("\n "+tmp.getNameWithRating());
		}

		return buf.toString();
	}

	//-------------------------------------------------------------------
	public void setAttribute(Attribute key, int value) {
		for (AttributeValue<Attribute> pair : attributes) {
			if (pair.getModifyable()==key) {
				pair.setDistributed(value);
				return;
			}
		}

		AttributeValue<Attribute> toAdd = new AttributeValue<Attribute>(key, value);
		attributes.add(toAdd);
	}

	//-------------------------------------------------------------------
	public AttributeValue<Attribute> getAttribute(String key) {
		return getAttribute(Attribute.valueOf(key));
	}

	//-------------------------------------------------------------------
	public AttributeValue<Attribute> getAttribute(Attribute key) {
		for (AttributeValue<Attribute> pair : attributes) {
			if (pair.getModifyable()==key) {
				return pair;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void setSkill(SMSkillValue skillVal) {
		if (!skillvals.contains(skillVal.getSkill()))
			skillvals.add(skillVal);
	}

	//-------------------------------------------------------------------
	public int getSkillPoints(SMSkill key) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==key)
				return tmp.getModifiedValue();
		return 0;
	}

	//-------------------------------------------------------------------
    /**
     * Returns a list of skills of a given type.
     * @param type SkillType
     * @return List of skills of the given type, sorted alphabetically
     */
    public List<SMSkillValue> getSkills(SkillType type) {
        List<SMSkillValue> ret = new ArrayList<SMSkillValue>();
        for (SMSkillValue tmp : skillvals)
            if (tmp.getSkill().getType()==type)
                ret.add(tmp);
        // Sort alphabetically
//        Collections.sort(ret);
        return ret;
    }

	//-------------------------------------------------------------------
    public void addSkill(SMSkillValue sval) {
    	for (SMSkillValue check : skillvals) {
    		if (check.getSkill()==sval.getSkill())
    			throw new IllegalArgumentException("Already exists");
    	}
    	skillvals.add(sval);
    }

	//-------------------------------------------------------------------
    public void removeSkill(SMSkillValue sval) {
    	if (skillvals.remove(sval))
    		return;

    	for (SMSkillValue check : skillvals) {
    		if (check.getSkill()==sval.getSkill()) {
    			skillvals.remove(check);
    			return;
    		}

    	}
    }

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.genericrpg.data.Lifeform#getSkillValues()
     */
    public List<SMSkillValue> getSkillValues() {
        List<SMSkillValue> ret = new ArrayList<SMSkillValue>(skillvals);
//        Collections.sort(ret);
        return ret;
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#hasSkill(org.prelle.splimo.SMSkill)
	 */
	@Override
    public boolean hasSkill(SMSkill skill) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==skill && tmp.getModifiedValue()>0)
				return true;
    	return false;
    }

	//-------------------------------------------------------------------
	public SMSkillValue getSkillValue(SMSkill skill) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==skill)
				return tmp;

		SMSkillValue sVal = new SMSkillValue(skill, 0);
		skillvals.add(sVal);
		return sVal;
//		throw new NoSuchElementException();
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue tmp) {
		spellvals.add(tmp);
	}

	//--------------------------------------------------------------------
	public void removeSpell(SpellValue spell) {
		spellvals.remove(spell);
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells() {
		return new ArrayList<SpellValue>(spellvals);
	}

	//--------------------------------------------------------------------
	public boolean hasSpell(SpellValue spell) {
		return getSpells().contains(spell);
	}

	//-------------------------------------------------------------------
	public void clearSpells() {
		spellvals.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * Get the value for a specific spell, including skill value and
	 * eventually existing spell type specializations
	 */
	public int getSpellValueFor(SpellValue spellVal) {
		SMSkillValue skillVal = getSkillValue(spellVal.getSkill());
		// Calculate value for skill, including attributes
		int val = skillVal.getValue();
		val += getAttribute(skillVal.getSkill().getAttribute()).getModifiedValue();
		val += getAttribute(skillVal.getSkill().getAttribute2()).getModifiedValue();

		for (SpellType type : spellVal.getResolved().getTypes()) {
			SMSkillSpecialization spec = new SMSkillSpecialization();
			spec.setType(SkillSpecializationType.SPELLTYPE);
			spec.setId(type.name());
			val += skillVal.getSpecializationLevel(spec);
		}

		return val;
	}

	//-------------------------------------------------------------------
    public List<CreatureFeatureValue> getFeatures() {
        return new ArrayList<>(features);
    }

	//-------------------------------------------------------------------
    /**
     * @return Value >0 if feature is present, -1 otherwise
     */
    public int getCreatureFeatureLevel() {
        for (CreatureFeatureValue feat : features) {
        	if (feat.getResolved().getId().equals("CREATURE"))
        		return feat.getLevel();
        }
        return -1;
    }

	//-------------------------------------------------------------------
   public CreatureFeatureValue getCreatureFeature(CreatureFeature key) {
        for (CreatureFeatureValue feat : features) {
        	if (feat.getResolved()==key)
        		return feat;
        }
        return null;
    }

   //-------------------------------------------------------------------
   public CreatureFeatureValue addCreatureFeature(CreatureFeature key) {
	   for (CreatureFeatureValue feat : features) {
		   if (feat.getResolved()==key)
			   return feat;
	   }
	   CreatureFeatureValue ret = new CreatureFeatureValue(key);
	   features.add(ret);
	   return ret;
   }

   //-------------------------------------------------------------------
   public void addCreatureFeature(CreatureFeatureValue val) {
	   features.add(val);
   }

   //-------------------------------------------------------------------
   public void removeCreatureFeature(CreatureFeature key) {
	   for (CreatureFeatureValue feat : features) {
		   if (feat.getResolved()==key) {
			   features.remove(feat);
			   return;
		   }
	   }
   }

	//-------------------------------------------------------------------
    public void addCreatureType(CreatureTypeValue cType) {
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==cType.getType()) {
    			if (val.getLevel()==cType.getLevel())
    				return;
    			val.setLevel(Math.max(val.getLevel(), cType.getLevel()));
    			return;
    		}
    	}

    	creaturetypes.add(cType);
    }

	//-------------------------------------------------------------------
    public void removeCreatureType(CreatureTypeValue cType) {
    	if (cType==null)
    		throw new NullPointerException();
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==cType.getType()) {
    			creaturetypes.remove(val);
    			return;
    		}
    	}
    }

	//-------------------------------------------------------------------
    public List<CreatureTypeValue> getCreatureTypes() {
        return new ArrayList<>(creaturetypes);
    }

	//-------------------------------------------------------------------
	public CreatureTypeValue getCreatureType(CreatureType key) {
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==key) {
    			return val;
    		}
    	}
    	return null;
	}

	//-------------------------------------------------------------------
    public void addWeapon(CreatureWeapon data) {
//    	for (CreatureWeapon val : weapons) {
//    		if (val.
//    			if (val.getLevel()==cType.getLevel())
//    				return;
//    			val.setLevel(Math.max(val.getLevel(), cType.getLevel()));
//    			return;
//    		}
//    	}

    	weapons.add(data);
    }

	//-------------------------------------------------------------------
	public void removeWeapon(CreatureWeapon data) {
		weapons.remove(data);
	}

	//-------------------------------------------------------------------
    public void removeCreatureType(CreatureWeapon data) {
    	weapons.remove(data);
    }

	//-------------------------------------------------------------------
    public List<CreatureWeapon> getCreatureWeapons() {
        return new ArrayList<>(weapons);
    }

	//-------------------------------------------------------------------
	/**
	 * @return the levelAlone
	 */
	public int getLevelAlone() {
		return levelAlone;
	}

	//-------------------------------------------------------------------
	/**
	 * @param levelAlone the levelAlone to set
	 */
	public void setLevelAlone(int levelAlone) {
		this.levelAlone = levelAlone;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the levelGroup
	 */
	public int getLevelGroup() {
		return levelGroup;
	}

	//-------------------------------------------------------------------
	/**
	 * @param levelGroup the levelGroup to set
	 */
	public void setLevelGroup(int levelGroup) {
		this.levelGroup = levelGroup;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the summonable
	 */
	public SummonableCreature getSummonable() {
		return summonable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param summonable the summonable to set
	 */
	public void setSummonable(SummonableCreature summonable) {
		this.summonable = summonable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the standardCreature
	 */
	public String getStandardCreature() {
		return standardCreature;
	}

	//-------------------------------------------------------------------
	/**
	 * @param standardCreature the standardCreature to set
	 */
	public void setStandardCreature(String standardCreature) {
		this.standardCreature = standardCreature;
	}

}

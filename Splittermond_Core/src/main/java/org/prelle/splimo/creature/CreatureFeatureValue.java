package org.prelle.splimo.creature;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
@Root(name="creatfeatval")
public class CreatureFeatureValue extends ComplexDataItemValue<CreatureFeature> {

	@Attribute(required=false)
	private int level;

	//-------------------------------------------------------------------
	public CreatureFeatureValue() {
	}

	//--------------------------------------------------------------------
	public CreatureFeatureValue(CreatureFeature type) {
		super(type);
	}

	//--------------------------------------------------------------------
	public CreatureFeatureValue(CreatureFeature type, int lvl) {
		super(type);
		this.level = lvl;
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof CreatureFeatureValue) {
			CreatureFeatureValue other = (CreatureFeatureValue)o;
			if (ref!=other.getKey()) return false;
			return level==other.getLevel();
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}

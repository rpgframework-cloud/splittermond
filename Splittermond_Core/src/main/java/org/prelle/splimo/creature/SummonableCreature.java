package org.prelle.splimo.creature;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.splimo.Attribute;

import de.rpgframework.genericrpg.data.AttributeValue;

/**
 * @author prelle
 *
 */
public class SummonableCreature {

	@Element
	private CreatureModuleValue body;
	@Element
	private CreatureModuleValue type;

	@ElementList(entry = "module", type = CreatureModuleValue.class)
	private List<CreatureModuleValue> modules;

	@ElementList(entry="service",type = ServiceValue.class)
	private List<ServiceValue> services;
	
	@Element
	private CreatureModuleValue enhanced;

	//-------------------------------------------------------------------
	public SummonableCreature() {
		modules = new ArrayList<>();
		services = new ArrayList<>();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splimo.creature.Creature#getLifeformType()
//	 */
//	@Override
//	public LifeformType getLifeformType() {
//		return LifeformType.SUMMONABLE;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.data.DataItem#getName(java.util.Locale)
//	 */
//	@Override
//	public String getName(Locale locale) {
//		return "No Name Yet";
//	}

	//--------------------------------------------------------------------
	public String getDescription(Locale locale) {
		return null;
	}

	//-------------------------------------------------------------------
	public int getLevel() {
		int cost = 0;
		if (body!=null) cost+= body.getResolved().getCost();
		if (type!=null) cost+= type.getResolved().getCost();
		for (CreatureModuleValue m : modules)
			cost+=m.getResolved().getCost();
		return cost;
	}

	//-------------------------------------------------------------------
//	public void reset() {
//		attributes.clear();
//		for (Attribute attr : Attribute.values()) {
//			AttributeValue<Attribute> toAdd = new AttributeValue<Attribute>(attr, 0);
//			attributes.add(toAdd);
//		}
//		skillvals.clear();
//		spellvals.clear();
//		features.clear();
//		weapons.clear();
//	}

	//-------------------------------------------------------------------
	public SummonableCreature setBody(CreatureModuleValue tmp) { body = tmp; return this; }
	public CreatureModuleValue getBody() { return body; }

	//-------------------------------------------------------------------
	public SummonableCreature setType(CreatureModuleValue tmp) { type = tmp; return this; }
	public CreatureModuleValue getType() { return type; }

	//-------------------------------------------------------------------
	public SummonableCreature addModule(CreatureModuleValue tmp) { modules.add(tmp); return this; }
	public SummonableCreature removeModule(CreatureModuleValue tmp) { modules.remove(tmp); return this; }
	public List<CreatureModuleValue> getModules() { return modules; }

	//-------------------------------------------------------------------
	public SummonableCreature addService(ServiceValue tmp) { services.add(tmp); return this; }
	public SummonableCreature removeService(ServiceValue tmp) { services.remove(tmp); return this; }
    public List<ServiceValue> getServices() {
        return new ArrayList<>(services);
    }

}

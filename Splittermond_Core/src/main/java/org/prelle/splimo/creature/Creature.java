/**
 *
 */
package org.prelle.splimo.creature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillSpecialization.SkillSpecializationType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.persist.WeaponDamageConverter;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@Root(name = "creature")
@DataItemTypeKey(id = "creature")
public class Creature extends DataItem implements SMLifeform {

	public final static List<String> DEFAULT_SKILL_NAMES = Arrays.asList("acrobatics", "athletics", "determination",
			"stealth", "hunting","perception", "endurance");

	@ElementList(entry="attr", type=AttributeValue.class)
	protected List<AttributeValue<Attribute>> attributes;
	@ElementList(entry="creaturetype", type=CreatureTypeValue.class)
	protected List<CreatureTypeValue> creaturetypes;
	@org.prelle.simplepersist.Attribute(name="lvl1", required=false)
	protected int levelAlone;
	@org.prelle.simplepersist.Attribute(name="lvl2", required=false)
	protected int levelGroup;
	@ElementList(type=CreatureWeapon.class)
	protected List<CreatureWeapon> weapons;
	@ElementList(entry="skillval", type=SMSkillValue.class)
	protected List<SMSkillValue> skillvals;
	@ElementList(entry="spellval", type=SpellValue.class)
	protected List<SpellValue> spellvals;
	@ElementList(type=CreatureFeatureValue.class)
	protected List<CreatureFeatureValue> features;

	/* For self built creatures */
//	@org.prelle.simplepersist.Attribute
//	@AttribConvert(CreatureModuleConverter.class)
//	protected CreatureModule base;
//	@org.prelle.simplepersist.Attribute
//	@AttribConvert(CreatureModuleConverter.class)
//	protected CreatureModule role;
//	@ElementList(type=CreatureModule.class,entry="option",convert=CreatureModuleConverter.class)
//	protected List<CreatureModule> options;


	//-------------------------------------------------------------------
	public Creature() {
		attributes   = new ArrayList<AttributeValue<Attribute>>();
		weapons      = new ArrayList<CreatureWeapon>();
		skillvals    = new ArrayList<SMSkillValue>();
		spellvals    = new ArrayList<SpellValue>();
		creaturetypes= new ArrayList<CreatureTypeValue>();
		features     = new ArrayList<CreatureFeatureValue>();
//		options      = new ArrayList<CreatureModule>();

		for (Attribute attr : Attribute.values()) {
			if (attr==Attribute.SPLINTER)
				continue;
//			if (attr==Attribute.INITIATIVE)
//				continue;
			AttributeValue<Attribute> toAdd = new AttributeValue<Attribute>(attr, 0);
			attributes.add(toAdd);
		}
	}

	//-------------------------------------------------------------------
	public void initializeDefaultSkills() {
		for (String id : DEFAULT_SKILL_NAMES) {
			SMSkill skill = SplitterMondCore.getSkill(id);
			skillvals.add(new SMSkillValue(skill, 0));
		}
	}

	//-------------------------------------------------------------------
	public Creature(String id) {
		this();
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getLifeformType()
	 */
	@Override
	public LifeformType getLifeformType() {
		return LifeformType.CREATURE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getImage()
	 */
	public byte[] getImage() {
		return null;
	}

	//-------------------------------------------------------------------
	public String dump() {
		WeaponDamageConverter dmgConvert = new WeaponDamageConverter();
		StringBuffer buf = new StringBuffer();
		buf.append("Name  : "+id);

		for (Attribute attr : Attribute.values()) {
			buf.append("\n"+attr.getShortName()+": "+getAttribute(attr)+"  \t");
		}

		buf.append("\n\nWaffen:\n=============");
		for (CreatureWeapon tmp : weapons) {
//			buf.append("\n "+tmp.getName()+" : "+tmp.getValue()+"  "+dmgConvert.write(tmp.getDamage())+" "+tmp.getSpeed()+"  "+tmp.getInitiative()+"  "+tmp.getFeatures());
			buf.append("\n "+tmp.getName()+" : "+tmp.getValue()+"  "+dmgConvert.write(tmp.getDamage())+" "+tmp.getSpeed()+"  "+tmp.getInitiative());
		}

		buf.append("\n\nFertigkeiten:\n=============");
		for (SMSkillValue tmp : skillvals) {
			buf.append("\n "+tmp.getSkill().getName()+" : "+tmp.getModifiedValue());
		}

		buf.append("\n\nZauber:\n=============");
		for (SpellValue tmp : spellvals) {
			buf.append("\n "+tmp.getResolved().getName()+" : "+tmp);
		}

		buf.append("\n\nMerkmale:\n=============");
		for (CreatureFeatureValue tmp : features) {
			buf.append("\n "+tmp.getNameWithRating());
		}

		return buf.toString();
	}

	//-------------------------------------------------------------------
	public boolean isCustom() {
		return id.startsWith("custom");
	}

	//-------------------------------------------------------------------
	public void setAttribute(Attribute key, int value) {
		for (AttributeValue pair : attributes) {
			if (pair.getModifyable()==key) {
				pair.setDistributed(value);
				return;
			}
		}

		AttributeValue toAdd = new AttributeValue(key, value);
		attributes.add(toAdd);
	}

	//-------------------------------------------------------------------
	public AttributeValue<Attribute> getAttribute(String key) {
		return getAttribute(Attribute.valueOf(key));
	}

	//-------------------------------------------------------------------
	public AttributeValue<Attribute> getAttribute(Attribute key) {
		for (AttributeValue pair : attributes) {
			if (pair.getModifyable()==key) {
				return pair;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void setSkill(SMSkillValue skillVal) {
		if (!skillvals.contains(skillVal.getSkill()))
			skillvals.add(skillVal);
	}

	//-------------------------------------------------------------------
	public int getSkillPoints(SMSkill key) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==key)
				return tmp.getModifiedValue();
		return 0;
	}

	//-------------------------------------------------------------------
    /**
     * Returns a list of skills of a given type.
     * @param type SkillType
     * @return List of skills of the given type, sorted alphabetically
     */
    public List<SMSkillValue> getSkills(SkillType type) {
        List<SMSkillValue> ret = new ArrayList<SMSkillValue>();
        for (SMSkillValue tmp : skillvals)
            if (tmp.getSkill().getType()==type)
                ret.add(tmp);
        // Sort alphabetically
//        Collections.sort(ret);
        return ret;
    }

	//-------------------------------------------------------------------
    public void addSkill(SMSkillValue sval) {
    	for (SMSkillValue check : skillvals) {
    		if (check.getSkill()==sval.getSkill())
    			throw new IllegalArgumentException("Already exists");
    	}
    	skillvals.add(sval);
    }

	//-------------------------------------------------------------------
    public void removeSkill(SMSkillValue sval) {
    	if (skillvals.remove(sval))
    		return;

    	for (SMSkillValue check : skillvals) {
    		if (check.getSkill()==sval.getSkill()) {
    			skillvals.remove(check);
    			return;
    		}

    	}
    }

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.genericrpg.data.Lifeform#getSkillValues()
     */
    public List<SMSkillValue> getSkillValues() {
        List<SMSkillValue> ret = new ArrayList<SMSkillValue>(skillvals);
//        Collections.sort(ret);
        return ret;
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#hasSkill(org.prelle.splimo.SMSkill)
	 */
	@Override
    public boolean hasSkill(SMSkill skill) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==skill && tmp.getModifiedValue()>0)
				return true;
    	return false;
    }

	//-------------------------------------------------------------------
	public SMSkillValue getSkillValue(SMSkill skill) {
		for (SMSkillValue tmp : skillvals)
			if (tmp.getSkill()==skill)
				return tmp;

		SMSkillValue sVal = new SMSkillValue(skill, 0);
		skillvals.add(sVal);
		return sVal;
//		throw new NoSuchElementException();
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue tmp) {
		spellvals.add(tmp);
	}

	//--------------------------------------------------------------------
	public void removeSpell(SpellValue spell) {
		spellvals.remove(spell);
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells() {
		return new ArrayList<SpellValue>(spellvals);
	}

	//--------------------------------------------------------------------
	public boolean hasSpell(SpellValue spell) {
		return getSpells().contains(spell);
	}

	//-------------------------------------------------------------------
	public void clearSpells() {
		spellvals.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * Get the value for a specific spell, including skill value and
	 * eventually existing spell type specializations
	 */
	public int getSpellValueFor(SpellValue spellVal) {
		SMSkillValue skillVal = getSkillValue(spellVal.getSkill());
		// Calculate value for skill, including attributes
		int val = skillVal.getValue();
		val += getAttribute(skillVal.getSkill().getAttribute()).getModifiedValue();
		val += getAttribute(skillVal.getSkill().getAttribute2()).getModifiedValue();

		for (SpellType type : spellVal.getResolved().getTypes()) {
			SMSkillSpecialization spec = new SMSkillSpecialization();
			spec.setType(SkillSpecializationType.SPELLTYPE);
			spec.setId(type.name());
			val += skillVal.getSpecializationLevel(spec);
		}

		return val;
	}

	//-------------------------------------------------------------------
    public List<CreatureFeatureValue> getFeatures() {
        return new ArrayList<>(features);
    }

	//-------------------------------------------------------------------
    /**
     * @return Value >0 if feature is present, -1 otherwise
     */
    public int getCreatureFeatureLevel() {
        for (CreatureFeatureValue feat : features) {
        	if (feat.getResolved().getId().equals("CREATURE"))
        		return feat.getLevel();
        }
        return -1;
    }

	//-------------------------------------------------------------------
   public CreatureFeatureValue getCreatureFeature(CreatureFeature key) {
        for (CreatureFeatureValue feat : features) {
        	if (feat.getResolved()==key)
        		return feat;
        }
        return null;
    }

   //-------------------------------------------------------------------
   public CreatureFeatureValue addCreatureFeature(CreatureFeature key) {
	   for (CreatureFeatureValue feat : features) {
		   if (feat.getResolved()==key)
			   return feat;
	   }
	   CreatureFeatureValue ret = new CreatureFeatureValue(key);
	   features.add(ret);
	   return ret;
   }

   //-------------------------------------------------------------------
   public void removeCreatureFeature(CreatureFeature key) {
	   for (CreatureFeatureValue feat : features) {
		   if (feat.getResolved()==key) {
			   features.remove(feat);
			   return;
		   }
	   }
   }

	//-------------------------------------------------------------------
    public void addCreatureType(CreatureTypeValue cType) {
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==cType.getType()) {
    			if (val.getLevel()==cType.getLevel())
    				return;
    			val.setLevel(Math.max(val.getLevel(), cType.getLevel()));
    			return;
    		}
    	}

    	creaturetypes.add(cType);
    }

	//-------------------------------------------------------------------
    public void removeCreatureType(CreatureTypeValue cType) {
    	if (cType==null)
    		throw new NullPointerException();
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==cType.getType()) {
    			creaturetypes.remove(val);
    			return;
    		}
    	}
    }

	//-------------------------------------------------------------------
    public List<CreatureTypeValue> getCreatureTypes() {
        return new ArrayList<>(creaturetypes);
    }

	//-------------------------------------------------------------------
	public CreatureTypeValue getCreatureType(CreatureType key) {
    	for (CreatureTypeValue val : creaturetypes) {
    		if (val.getType()==key) {
    			return val;
    		}
    	}
    	return null;
	}

	//-------------------------------------------------------------------
    public void addWeapon(CreatureWeapon data) {
//    	for (CreatureWeapon val : weapons) {
//    		if (val.
//    			if (val.getLevel()==cType.getLevel())
//    				return;
//    			val.setLevel(Math.max(val.getLevel(), cType.getLevel()));
//    			return;
//    		}
//    	}

    	weapons.add(data);
    }

	//-------------------------------------------------------------------
	public void removeWeapon(CreatureWeapon data) {
		weapons.remove(data);
	}

	//-------------------------------------------------------------------
    public void removeCreatureType(CreatureWeapon data) {
    	weapons.remove(data);
    }

	//-------------------------------------------------------------------
    public List<CreatureWeapon> getCreatureWeapons() {
        return new ArrayList<>(weapons);
    }

	//-------------------------------------------------------------------
	/**
	 * @return the levelAlone
	 */
	public int getLevelAlone() {
		return levelAlone;
	}

	//-------------------------------------------------------------------
	/**
	 * @param levelAlone the levelAlone to set
	 */
	public void setLevelAlone(int levelAlone) {
		this.levelAlone = levelAlone;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the levelGroup
	 */
	public int getLevelGroup() {
		return levelGroup;
	}

	//-------------------------------------------------------------------
	/**
	 * @param levelGroup the levelGroup to set
	 */
	public void setLevelGroup(int levelGroup) {
		this.levelGroup = levelGroup;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#validate()
	 */
	@Override
	public void validate() {
		for (SMSkillValue sVal : skillvals) {
			if (sVal.getSkill()==null) {
				throw new DataErrorException(this, "Creature:"+id+" refers to unset skill with value="+sVal.getDistributed());
			}

			// Resolve masterships
			for (MastershipReference ref : sVal.getMasterships()) {
				if (ref.getResolved()==null) {
					String key = ref.getKey();
					Mastership resolved = SplitterMondCore.getItem(Mastership.class, key);
					if (resolved==null && key.contains("/"))
						resolved = SplitterMondCore.getItem(Mastership.class, key.substring(key.indexOf("/")+1));
					if (resolved==null)
						throw new DataErrorException(this, "Creature: "+id+" refers to unknown mastership="+ref.getKey());
					ref.setResolved(resolved);
				}
			}
		}

		// Resolve creature features
		for (CreatureFeatureValue ref : features) {
			if (ref.getResolved()==null) {
				String key = ref.getKey();
				CreatureFeature resolved = SplitterMondCore.getItem(CreatureFeature.class, key);
				if (resolved==null)
					throw new DataErrorException(this, "Creature: "+id+" refers to unknown creature feature="+ref.getKey());
				ref.setResolved(resolved);
			}
		}

		// Resolve creature features
		for (SpellValue ref : spellvals) {
			if (ref.getResolved()==null) {
				String key = ref.getKey();
				Spell resolved = SplitterMondCore.getItem(Spell.class, key);
				if (resolved==null)
					throw new DataErrorException(this, "Creature: "+id+" refers to unknown spell="+ref.getKey());
				ref.setResolved(resolved);
			}
		}

		if (getDescription()==null || getDescription().endsWith(".desc")) {
			System.err.println("Creature.validate: No description for "+id);
		}
	}

	@Override
	public int getLevel() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.creature.SMLifeform#getLinkUUID()
	 */
	@Override
	public UUID getLinkUUID() {
		return null;
	}

}

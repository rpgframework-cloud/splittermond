package org.prelle.splimo.creature;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "creatmod")
public class CreatureModule extends ComplexDataItem {

	public static enum Category {
		/** Bestienmeister */
		CREATURE,
		/** Beschwörbare Wesen */
		SUMMONABLE
	}

	public static enum Type {
		BODY,
		TYPE,
		ELEMENT,
		EXTRA,
		ROLE
	}

	@Attribute(name="cat", required = true)
	private Category category;
	@Attribute(required = true)
	private Type type;
	@Attribute
	private int cost;

	@ElementList(entry="attr", type=AttributeValue.class)
	protected List<AttributeValue<org.prelle.splimo.Attribute>> attributes;
	@ElementList(type=CreatureWeapon.class)
	protected List<CreatureWeapon> weapons;

	//-------------------------------------------------------------------
	/**
	 */
	public CreatureModule() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public List<AttributeValue<org.prelle.splimo.Attribute>> getAttributes() {
		return attributes;
	}

	//-------------------------------------------------------------------
	public AttributeValue<org.prelle.splimo.Attribute> getAttribute(org.prelle.splimo.Attribute key) {
		for (AttributeValue<org.prelle.splimo.Attribute> tmp : attributes) {
			if (tmp.getModifyable()==key)
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
    public List<CreatureWeapon> getCreatureWeapons() {
        return new ArrayList<>(weapons);
    }

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

}

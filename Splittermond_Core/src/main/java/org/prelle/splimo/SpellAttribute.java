package org.prelle.splimo;

/**
 *
 */
public enum SpellAttribute {

	DIFFICULTY,
	FOCUS_CHAN,
	FOCUS_EXH,
	FOCUS_CONS,
	CAST_DUR,
	EFF_DUR,
	DISTANCE,
	AREA

}

/**
 * 
 */
package org.prelle.splimo;

import java.text.Collator;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="language")
public class Language extends DataItem implements Comparable<Language> {
	
	//-------------------------------------------------------------------
	public Language() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Language o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

}

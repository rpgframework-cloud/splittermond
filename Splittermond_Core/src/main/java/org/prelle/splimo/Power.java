/**
 * 
 */
package org.prelle.splimo;

import java.text.Collator;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="power")
public class Power extends ComplexDataItem implements Comparable<Power> {

	public enum SelectionType {
		ALWAYS,
		/** Multiple times - unlimited */
		MULTIPLE,
		/** Only at generation */
		GENERATION,
		/** To a total of 3 times */
		MAX3,
		/** Once per level */
		LEVEL, 
	}
	
	@Attribute(name="cost",required=false)
	private int cost;
	@Attribute(name="selectable",required=false)
	private SelectionType selectable;

	//-------------------------------------------------------------------
	public Power() {
		cost=1;
		selectable = SelectionType.ALWAYS;
	}

	//-------------------------------------------------------------------
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Power other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the selectable
	 */
	public SelectionType getSelectable() {
		return selectable;
	}

	//--------------------------------------------------------------------
	/**
	 * @param selectable the selectable to set
	 */
	public void setSelectable(SelectionType selectable) {
		this.selectable = selectable;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the selectable
	 */
	public boolean canBeUsedMultipleTimes() {
		return selectable!=null && (selectable==SelectionType.LEVEL || selectable==SelectionType.MAX3 || selectable==SelectionType.MULTIPLE);
	}

	//-------------------------------------------------------------------
	public boolean hasLevel() {
		return canBeUsedMultipleTimes();
	}

}

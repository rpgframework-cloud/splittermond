/**
 *
 */
package org.prelle.splimo;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="background")
public class Background extends ComplexDataItem {

	@Attribute(required=false)
	private String name;

	//-------------------------------------------------------------------
	public Background() {
	}

	//-------------------------------------------------------------------
	public Background(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	public Background(String id, String customName) {
		this.id = id;
		this.name= customName;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (!(o instanceof Background))
			return false;

		Background other = (Background)o;
		if (!id.equals(other.getId())) return false;
		if (other.name!=null && !other.name.equals(name)) return false;

		return modifications.equals(other.getOutgoingModifications());
	}

	//-------------------------------------------------------------------
	public String dump() {
		return id+" = "+modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#getName()
	 */
	@Override
	public String getName() {
		if (name!=null)
			return name;
		return super.getName();
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#getName(java.util.Locale)
	 */
	@Override
	public String getName(Locale loc) {
		if (name!=null)
			return name;
		return super.getName(loc);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#validate()
	 */
	@Override
	public void validate() {
		int resources =0;
		int skills    =0;
		for (Modification tmp : modifications) {
			tmp.setSource(this);
			if (tmp instanceof ValueModification) {
				ValueModification mod = (ValueModification)tmp;
				int val = mod.getValue();
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice()+"\nValid: "+choices);
					if (choice.getDistribute()!=null) {
						for (int i : choice.getDistribute())
							val+=i;
					}
				}
				if (mod.getReferenceType()==SplittermondReference.RESOURCE) {
					resources+=val;
				} else if (mod.getReferenceType()==SplittermondReference.SKILL) {
					skills+=val;
				}
			} else if (tmp instanceof DataItemModification) {
				DataItemModification mod = (DataItemModification)tmp;
				String[] keys = new String[] {mod.getKey()};
				if (mod.getKey().contains(",") || mod.getKey().contains(" ")) {
					keys = mod.getAsKeys();
				}
				String[] ckeys = new String[0];
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification of culture:"+id+" refers to unknown choice "+mod.getConnectedChoice());
					choice.setSource(this);
				}
			}
		}
		if (resources!=4)
			throw new DataErrorException(this, "Abstammung '"+getId()+"' hat "+resources+" Ressourcenpunkte - es sollten 4 sein");
		if (skills!=5)
			throw new DataErrorException(this,"Abstammung '"+getId()+"' hat "+resources+" Fertigkeitspunkte - es sollten 5 sein");
	}
}

/**
 *
 */
package org.prelle.splimo;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
@Root(name = "masterref")
public class MastershipReference extends ComplexDataItemValue<Mastership> implements Comparable<MastershipReference> {

	public final static int LEVEL4 = 4;
	public final static int LEVEL3 = 3;
	public final static int LEVEL2 = 2;
	public final static int LEVEL1 = 1;
	public final static int PAYED_WITH_EXP = 0;
	public final static int GENERATOR_FREE = -1;
	public final static int EXTRAORDINARY_FREE = -2;

	// -1 for free from modules, 1-4 from thresholds
	@Attribute(required=false)
	private int free;

	private transient boolean canBeCleared;
	private transient SMSkill skill;

	//-------------------------------------------------------------------
	public MastershipReference() {
	}

	//-------------------------------------------------------------------
	public MastershipReference(Mastership power) {
		super(power);
		setResolved(power);
	}

	//-------------------------------------------------------------------
	public MastershipReference(Mastership power, SMSkill skill) {
		super(power);
		this.skill = skill;
		setResolved(power);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ResolvableDataItem#setResolved(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setResolved(Mastership value) {
		this.resolved = value;
		ref = (value!=null)?( (value.isCommon())?value.getId():(value.getSkill().getId()+"/"+value.getId())):null;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof MastershipReference) {
			MastershipReference other = (MastershipReference)o;
			if (resolved!=null) return resolved.equals(other.getModifyable());
			return false;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MastershipReference other) {
		return toString().compareTo(other.toString());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the free
	 */
	public int getFree() {
		return free;
	}

	//-------------------------------------------------------------------
	/**
	 * @param free the free to set
	 */
	public void setFree(int free) {
		this.free = free;
	}

	//-------------------------------------------------------------------
	/**
	 * Can the mastership be cleared when running CharacterProcessors?
	 * @return the canBeCleared
	 */
	public boolean canBeCleared() {
		return canBeCleared;
	}

	//-------------------------------------------------------------------
	/**
	 * @param canBeCleared the canBeCleared to set
	 */
	public void setCanBeCleared(boolean canBeCleared) {
		this.canBeCleared = canBeCleared;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @return the free
//	 */
//	public int getLevel() {
//		return (data!=null)?data.getLevel():spec.getLevel();
//	}

	public Mastership getMastership() { return getModifyable(); }
	public String getName(Locale loc) { return getNameWithoutRating(loc); }

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public SMSkill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(SMSkill skill) {
		this.skill = skill;
	}

}

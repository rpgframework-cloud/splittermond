/**
 *
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "education")
public class Education extends ComplexDataItem {

	@Attribute(required=false)
	@Deprecated
	private String name;
	@Attribute(name="variantof",required=false)
	private String variantOf;
	private transient List<Education> variants;

	//-------------------------------------------------------------------
	public Education() {
		variants      = new ArrayList<Education>();
	}

	//-------------------------------------------------------------------
	public Education(String id) {
		this.id   = id;
		variants      = new ArrayList<Education>();
	}

	//-------------------------------------------------------------------
	@Deprecated
	public Education(String id, String variantOf, String customName) {
		this.id   = id;
		this.variantOf= variantOf;
		this.name     = new String(customName);
		variants      = new ArrayList<Education>();
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o==this) return true;
		if (!(o instanceof Education))
			return false;

		Education other = (Education)o;
		if (!id.equals(other.getId())) return false;
		if (name!=null && !name.equals(other.name)) return false;
		if (other.name!=null && !other.name.equals(name)) return false;

		return modifications.equals(other.getOutgoingModifications());
	}

	//-------------------------------------------------------------------
	public String dump() {
		return id+" = "+modifications;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (name!=null)
			return name;
		return super.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variants
	 */
	public List<Education> getVariants() {
		return variants;
	}

	//-------------------------------------------------------------------
	public void addVariant(Education toAdd) {
		if (!variants.contains(toAdd))
			variants.add(toAdd);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variantOf
	 */
	public String getVariantOf() {
		return variantOf;
	}

	//-------------------------------------------------------------------
	/**
	 * @param variantOf the variantOf to set
	 */
	public void setVariantOf(String variantOf) {
		this.variantOf = variantOf;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#validate()
	 */
	@Override
	public void validate() {
		int skills    =0;
		int powers    =0;
		int resources =0;
		int master    =0;
		for (Modification tmp : modifications) {
			tmp.setSource(this);
			if (tmp instanceof ValueModification) {
				ValueModification mod = (ValueModification)tmp;
				int val = mod.getValue();
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice());
					if (choice.getDistribute()!=null) {
						for (int i : choice.getDistribute())
							val+=i;
					}
				}
				if (mod.getReferenceType()==SplittermondReference.SKILL) {
					skills+=val;
				} else if (mod.getReferenceType()==SplittermondReference.RESOURCE) {
					resources+=val;
				}
			} else if (tmp instanceof DataItemModification) {
				DataItemModification mod = (DataItemModification)tmp;
				String[] keys = new String[] {mod.getKey()};
				if (mod.getKey().contains(",") || mod.getKey().contains(" ")) {
					keys = mod.getAsKeys();
				}
				String[] ckeys = new String[0];
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice());
				}

				if (mod.getReferenceType()==SplittermondReference.POWER) {
					powers+=1;
					for (String key : keys) {
						if ("CHOICE".equals(key))
							continue;
						Power result = SplitterMondCore.getItem(Power.class, key);
						if (result==null)
							throw new DataErrorException(this, "Unknown power '"+key+"' in culture '"+id+"'");
					}
					for (String key : ckeys) {
						Power result = SplitterMondCore.getItem(Power.class, key);
						if (result==null)
							throw new DataErrorException(this, "Unknown choice power '"+key+"' in culture '"+id+"'");
					}
				}
				if (mod.getReferenceType()==SplittermondReference.MASTERSHIP) {
					if (mod.getConnectedChoice()!=null) {
						Choice choice = getChoice(mod.getConnectedChoice());
						if (choice==null)
							throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice());
						if (choice.getCount()>0)
							master+=choice.getCount();
						else
							master++;
					} else {
						master++;
					}
				}
				if (mod.getReferenceType()==SplittermondReference.SKILL_SPECIAL) {
					SMSkillSpecialization skillSpec = mod.getResolvedKey();
					if (skillSpec==null) {
						throw new DataErrorException(this, "ValueModification refers to unknown skillspec "+mod.getKey());
					}
					if (mod.getConnectedChoice()!=null) {
						Choice choice = getChoice(mod.getConnectedChoice());
						if (choice==null)
							throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice());
						if (choice.getCount()>0)
							master+=choice.getCount();
						else
							master++;
					} else {
						master++;
					}
				}
			}
		}
		if (skills!=30)
			throw new DataErrorException(this,"Ausbildung '"+getId()+"' hat "+skills+" Fertigkeitspunkte - es sollten 30 sein");
		if (resources!=2)
			throw new DataErrorException(this,"Ausbildung '"+getId()+"' hat "+resources+" Ressourcen - es sollte 2 sein");
		if (master!=2)
			throw new DataErrorException(this,"Ausbildung '"+getId()+"' hat "+master+" Meisterschaften - es sollte 2 sein");
	}

}

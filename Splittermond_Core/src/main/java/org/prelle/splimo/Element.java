package org.prelle.splimo;

/**
 * @author prelle
 *
 */
public enum Element {

	STONE,
	FIRE,
	NATURE,
	LIGHT,
	SHADOW,
	WATER,
	WIND
}

package org.prelle.splimo;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.persist.SpellCostConverter;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="service")
public class Service extends ComplexDataItem {

	@Attribute(name="cost")
	@AttribConvert(SpellCostConverter.class)
	private SpellCost cost;

	//-------------------------------------------------------------------
	public Service() {
		// TODO Auto-generated constructor stub
	}

}

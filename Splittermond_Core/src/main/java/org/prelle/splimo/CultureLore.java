/**
 * 
 */
package org.prelle.splimo;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="culturelore")
public class CultureLore extends DataItem implements Comparable<CultureLore> {
	
	private transient List<Culture> cultures;
	private transient List<Language> languages;
	
	//-------------------------------------------------------------------
	public CultureLore() {
		cultures = new ArrayList<>();
		languages = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CultureLore o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the culture
	 */
	public Collection<Culture> getCultures() {
		return cultures;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @param culture the culture to set
//	 */
//	public void addCulture(Culture culture) {
//		if (!cultures.contains(culture)) {
//			cultures.add(culture);
//			// Find language modification
//			for (Modification mod : culture.getModifications()) {
//				if (mod instanceof DataItemModification) {
//					DataItemModification dMod = (DataItemModification)mod;
//					if (dMod.getReferenceType()==SplittermondReference.LANGUAGE) {
//						Language lang = SplitterMondCore.getItem(Language.class, dMod.getKey());
//						if (lang==null) 
//							throw new RuntimeException("No such language: '"+dMod.getKey()+"' in culture '"+culture.getId()+"'");
//						if (!languages.contains(lang))
//							languages.add(lang);
//					}
//				} else if (mod instanceof ModificationChoice) {
//					for (Modification mod2 : ((ModificationChoice)mod).getOptionList()) {
//						if (mod instanceof DataItemModification) {
//							DataItemModification dMod = (DataItemModification)mod2;
//							if (dMod.getReferenceType()==SplittermondReference.LANGUAGE) {
//								Language lang = SplitterMondCore.getItem(Language.class, dMod.getKey());
//								if (lang==null) 
//									throw new RuntimeException("No such language: '"+dMod.getKey()+"' in culture '"+culture.getId()+"'");
//								if (!languages.contains(lang))
//									languages.add(lang);
//							}
//						}					}
//					
//				}
//			}
//			
//		}
//	}

	//-------------------------------------------------------------------
	public Collection<Language> getLanguages() {
		return languages;
	}

}

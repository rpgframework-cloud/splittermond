/**
 *
 */
package org.prelle.splimo;

import java.text.Collator;
import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.items.ItemTemplate;

import de.rpgframework.genericrpg.data.SkillSpecialization;

/**
 * @author prelle
 *
 */
public class SMSkillSpecialization extends SkillSpecialization<SMSkill> implements MastershipOrSpecialization {

	public enum SkillSpecializationType {
		NORMAL,
		WEAPON,
		SPELLTYPE
		;
	}

	@Attribute(required=false)
	private SkillSpecializationType type = SkillSpecializationType.NORMAL;
	@Attribute(required=false)
	private boolean multi;

	//-------------------------------------------------------------------
	public SMSkillSpecialization() {

	}

	//-------------------------------------------------------------------
	public SMSkillSpecialization(SMSkill skill, SkillSpecializationType type, String id) {
		this.skill = skill;
		this.type  = type;
		this.id    = id;
	}

	//-------------------------------------------------------------------
	public SMSkillSpecialization(SMSkill skill, SpellType stype) {
		this.skill = skill;
		this.type  = SkillSpecializationType.SPELLTYPE;
		this.id    = stype.name();
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof SMSkillSpecialization) {
			SMSkillSpecialization other = (SMSkillSpecialization)o;
			if (type!=other.getType()) return false;
			return id.equals(other.getId());
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return type+": "+skill+"("+id+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillSpecializationType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(SkillSpecializationType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public SMSkill getSkill() {
		return (SMSkill) skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(SMSkill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MastershipOrSpecialization other) {
		return Collator.getInstance().compare(this.getName(), other.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.MastershipOrSpecialization#getLevel()
	 */
	@Override
	public int getLevel() {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multi
	 */
	public boolean canBeSelectedMultiple() {
		return multi;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#getName(java.util.Locale)
	 */
	@Override
	public String getName(Locale locale) {
		switch (type) {
		case SPELLTYPE:
			return SpellType.valueOf(id).getName();
		case WEAPON:
			return SplitterMondCore.getItem(ItemTemplate.class, id).getName(locale);
		default:
			return super.getName(locale);
		}
	}

}

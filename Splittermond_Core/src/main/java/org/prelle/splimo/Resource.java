/**
 *
 */
package org.prelle.splimo;

import java.text.Collator;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="resource")
public class Resource extends ComplexDataItem implements Comparable<Resource> {

	@Attribute(name="base")
	private boolean base;

	//-------------------------------------------------------------------
	public Resource() {
		hasLevel=true;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Resource other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	public boolean isBaseResource() {
		return base;
	}

}

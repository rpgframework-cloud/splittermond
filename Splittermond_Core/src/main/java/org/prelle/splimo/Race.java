/**
 * 
 */
package org.prelle.splimo;

import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="race")
public class Race extends ComplexDataItem {

	@Element
	private ColorDiceTable hair;
	@Element
	private ColorDiceTable eyes;
	@Element
	private Size size;
	
	//-------------------------------------------------------------------
	public Race() {
		hair = new ColorDiceTable();
		eyes = new ColorDiceTable();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "race:"+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the hair
	 */
	public ColorDiceTable getHair() {
		return hair;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the eyes
	 */
	public ColorDiceTable getEyes() {
		return eyes;
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	public String dump() {
		StringBuffer buf = new StringBuffer();
		hair.dump(buf);
		eyes.dump(buf);
		return id+"  "+modifications+"\n"+buf;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the size
	 */
	public Size getSize() {
		return size;
	}

	//-------------------------------------------------------------------
	public void fixModifications() {
		for (Modification mod : modifications)
			mod.setSource(this);
	}

}

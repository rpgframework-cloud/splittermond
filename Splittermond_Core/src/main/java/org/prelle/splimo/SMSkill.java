package org.prelle.splimo;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.TwoAttributeSkill;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="skill")
public class SMSkill extends TwoAttributeSkill<Attribute> implements Comparable<SMSkill> {

	public enum SkillType {
		COMBAT,
		NORMAL,
		MAGIC;

		public String getName(Locale loc) {
			return SplitterMondCore.getI18nResources().getString("skill.type."+name().toLowerCase(), loc);
		}
	}

	@org.prelle.simplepersist.Attribute(required=false)
	private Attribute attr1;
	@org.prelle.simplepersist.Attribute(required=false)
	private Attribute attr2;
	@org.prelle.simplepersist.Attribute(required=false)
	private SkillType type;
	@org.prelle.simplepersist.Attribute(required=false)
	private boolean grouped;
	@ElementList(entry="specialization",type=SMSkillSpecialization.class,inline=true)
	private List<SMSkillSpecialization> specialization;
	@ElementList(entry="mastership",type=Mastership.class,inline=true)
	private List<Mastership> mastership;

	//-------------------------------------------------------------------
	public SMSkill() {
		type = SkillType.NORMAL;
		mastership = new ArrayList<Mastership>();
		specialization = new ArrayList<SMSkillSpecialization>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.OneAttributeSkill#getAttribute()
	 */
	@Override
	public Attribute getAttribute() {
		return attr1;
	}

	//-------------------------------------------------------------------
	/**
	 * @param attr1 the attr1 to set
	 */
	public void setAttribute1(Attribute attr1) {
		this.attr1 = attr1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.TwoAttributeSkill#getAttribute2()
	 */
	@Override
	public Attribute getAttribute2() {
		return attr2;
	}

	//-------------------------------------------------------------------
	/**
	 * @param attr2 the attr2 to set
	 */
	public void setAttribute2(Attribute attr2) {
		this.attr2 = attr2;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SMSkill o) {
		int foo = type.compareTo(o.getType());
		if (foo!=0)
			return foo;
		return Collator.getInstance().compare(getName(), o.getName());

	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public Mastership getMastership(String id) {
		for (Mastership tmp : mastership)
			if (tmp.getId().equals(id))
				return tmp;
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public List<Mastership> getMasterships() {
		return mastership;
	}

	//-------------------------------------------------------------------
	public void addMastership(Mastership toAdd) {
		if (!mastership.contains(toAdd)) {
			mastership.add(toAdd);
			toAdd.assignSkill(this);
		}
	}

	//-------------------------------------------------------------------
	public void removeMastership(Mastership toRemove) {
		mastership.remove(toRemove);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public List<de.rpgframework.genericrpg.data.SkillSpecialization<?>> getSpecializations() {
		List<de.rpgframework.genericrpg.data.SkillSpecialization<?>> ret = new ArrayList<>();
		ret.addAll(specialization);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public SMSkillSpecialization getSpecialization(String id) {
		for (SMSkillSpecialization tmp : specialization) {
			if (tmp.getId().equals(id))
				return tmp;
			if (id.startsWith(tmp.getId()))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addSpecialization(SMSkillSpecialization spec) {
		specialization.add(spec);
		spec.setSkill(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the grouped
	 */
	public boolean isGrouped() {
		return grouped;
	}

}

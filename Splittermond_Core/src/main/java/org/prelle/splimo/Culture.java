/**
 *
 */
package org.prelle.splimo;

import java.text.Collator;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="culture")
public class Culture extends ComplexDataItem implements Comparable<Culture> {

	public enum Continent {
		CUSTOM,
		DRAGOREA,
		FROSTLANDE,
		TAKASADU,
		ARAKEA,
		PASHANAR,
		EXOTENKUESTE,
		BINNENMEER
	}

	@Attribute(required=false)
	private String name;
	@Attribute(name="continent")
	private Continent continent;

	//-------------------------------------------------------------------
	public Culture() {
	}

	//-------------------------------------------------------------------
	public Culture(String id, String customName) {
		this.id = id;
		this.name = customName;
	}

	//-------------------------------------------------------------------
	public String dump() {
		return id+" = "+modifications;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (name!=null)
			return name;
		return super.getName();
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		this.name = name;
	}
	//-------------------------------------------------------------------
	public void setKey(String key) {
		this.id = key;
	}

	//-------------------------------------------------------------------
	public void setModifications(List<Modification> mods) {
		this.modifications = new ModificationList(mods);
	}

//	//-------------------------------------------------------------------
//	public void addModification(ModificationImpl mod) {
//		if (mod instanceof ModificationChoice) {
//			if (((ModificationChoice)mod).getOptions().length>2 )
//				throw new IllegalArgumentException("ModificationChoice without options: "+mod);
//		}
//		modifications.add(mod);
//	}

	//-------------------------------------------------------------------
	public Continent getContinent() {
		return continent;
	}

	//-------------------------------------------------------------------
	public void setContinent(Continent continent) {
		this.continent = continent;
	}

//	//-------------------------------------------------------------------
//	public List<Race> getCommonRaces() {
//		List<Race> ret = new ArrayList<Race>();
//		for (Modification tmp : modifications) {
//			if (tmp instanceof RaceModification) {
//				ret.add( ((RaceModification) tmp).getRace() );
//			}
//		}
//
//		return ret;
//	}

//	//-------------------------------------------------------------------
//	public List<Background> getCommonBackgrounds() {
//		List<Background> ret = new ArrayList<Background>();
//		for (Modification tmp : modifications) {
//			if (tmp instanceof BackgroundModification) {
//				ret.add( ((BackgroundModification) tmp).getBackground() );
//			} else
//			if (tmp instanceof NotBackgroundModification) {
//				ret.addAll(SplitterMondCore.getBackgrounds());
//				for (Background remove : ((NotBackgroundModification) tmp).getBackgrounds())
//					ret.add(remove);
//			}
//		}
//
//		return ret;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Culture o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataItem#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		
		int skills    =0;
		int powers    =0;
		for (Modification tmp : modifications) {
			tmp.setSource(this);
			if (tmp instanceof ValueModification) {
				ValueModification mod = (ValueModification)tmp;
				// Check if reference is valud
				Object resolved = SplittermondReference.resolve((SplittermondReference) mod.getReferenceType(), mod.getKey());
				if (resolved==null && !"CHOICE".equals(mod.getKey())) {
					throw new DataErrorException(this, "ValueModification of culture:"+id+" refers to unknown item "+mod.getReferenceType()+":"+mod.getKey());
				}
				// Define the points value of this modification
				int val = mod.getValue();
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification refers to unknown choice "+mod.getConnectedChoice());
					choice.setSource(this);
					if (choice.getDistribute()!=null) {
						for (int i : choice.getDistribute())
							val+=i;
					}
				}
				if (mod.getReferenceType()==SplittermondReference.SKILL) {
					skills+=val;
				} 
			} else if (tmp instanceof DataItemModification) {
				DataItemModification mod = (DataItemModification)tmp;
				String[] keys = new String[] {mod.getKey()};
				if (mod.getKey().contains(",") || mod.getKey().contains(" ")) {
					keys = mod.getAsKeys();
				}
				String[] ckeys = new String[0];
				if (mod.getConnectedChoice()!=null) {
					Choice choice = getChoice(mod.getConnectedChoice());
					if (choice==null)
						throw new DataErrorException(this, "ValueModification of culture:"+id+" refers to unknown choice "+mod.getConnectedChoice());
					choice.setSource(this);
				}
				
				if (mod.getReferenceType()==SplittermondReference.POWER) {
					powers+=1;
					for (String key : keys) {
						if ("CHOICE".equals(key))
							continue;
						Power result = SplitterMondCore.getItem(Power.class, key);
						if (result==null)
							throw new DataErrorException(this, "Unknown power '"+key+"' in culture '"+id+"'");
					}
					for (String key : ckeys) {
						Power result = SplitterMondCore.getItem(Power.class, key);
						if (result==null)
							throw new DataErrorException(this, "Unknown choice power '"+key+"' in culture '"+id+"'");
					}
				}
			}
		}
		if (skills!=15)
			throw new DataErrorException(this,"Abstammung '"+getId()+"' hat "+skills+" Fertigkeitspunkte - es sollten 15 sein");
		if (powers!=1)
			throw new DataErrorException(this,"Abstammung '"+getId()+"' hat "+powers+" Stärken - es sollte 1 sein");
	}

}

package org.prelle.splimo;

import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;
import de.rpgframework.genericrpg.requirements.RequirementList;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="mastership")
public class Mastership extends ComplexDataItem implements MastershipOrSpecialization {

	private final Collator COLLATOR = Collator.getInstance();

	@Attribute
	private int level;
	/** Max Level */
	@Attribute
	private int max;
	@Attribute(required=false)
	private boolean multiple;
	@Attribute
	private boolean common;
	@Attribute
	private String applyTo;

	private transient SMSkillSpecialization focus;
	private transient List<SMSkill> inSkills = new ArrayList<>();

	//-------------------------------------------------------------------
	public Mastership() {
	}

	//-------------------------------------------------------------------
	public Mastership(String id, int level) {
		this.level = level;
		this.id    = id;
	}

	//-------------------------------------------------------------------
	public Mastership(Mastership toClone, SMSkillSpecialization focus) {
		this.id    = toClone.getId();
		this.level = toClone.getLevel();
		this.multiple = toClone.isMultiple();
		requires = new RequirementList(toClone.getRequirements());
		modifications = new ModificationList(toClone.getOutgoingModifications());
		this.common = toClone.isCommon();
		this.focus = focus;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getId()
	 */
	@Override
	public String getId() {
		if (focus!=null)
			return id+"_"+focus.getId();
		return id;
	}

	//--------------------------------------------------------------------
	public String getDescription(Locale locale) {
		String skillID = "";
		if (inSkills.size()==1)
			skillID = "."+getSkill().getId();
		String key = getTypeString()+skillID+"."+id.toLowerCase()+".desc";
		// Check license
		DataSet set = null;
		int where = 0;
		try {
			set = getFirstParent(locale);
			if (set==null)
				return "?No ParentSet?";
			// Check for a user defined property
			if (CustomResourceManagerLoader.getInstance()!=null) {
				String custom = CustomResourceManagerLoader.getInstance().getProperty(set.getRules(), key, locale);
				if (custom!=null)
					return custom;
			}

			where++;
			if (!LicenseManager.hasLicense(set,locale))
				return GenericRPGTools.RES.format("warning.no_license", set.getName(locale), locale.getDisplayLanguage(locale));

			String foo = set.getResourceString(key,locale);
			return foo;
		} catch (MissingResourceException mre) {
			mre.printStackTrace();
			if (where==0)
				logger.log(Level.ERROR, mre.toString());
			else
				logger.log(Level.ERROR, "Missing resource  "+mre.getKey()+"\t  for locale "+locale+" in "+set.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof Mastership) {
			Mastership other = (Mastership)o;
			if (!getId().equals(other.getId())) return false;
			if (level!=other.getLevel()) return false;
			if (focus!=other.getFocus()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
//		if (modifications!=null && !modifications.isEmpty())
//			return key+" "+modifications;
		return getId();
	}

	//-------------------------------------------------------------------
	public String getAssignedSkill() {
		return applyTo;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.MastershipOrSpecialization#getSkill()
	 */
	@Override
	public SMSkill getSkill() {
		if (!inSkills.isEmpty())
			return inSkills.get(0);
		return null;
	}

	//-------------------------------------------------------------------
	public void assignSkill(SMSkill skill) {
		if (!inSkills.contains(skill))
			inSkills.add(skill);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MastershipOrSpecialization other) {
		int comp = getSkill().compareTo(other.getSkill());
		if (comp!=0) return comp;
		comp = ((Integer)level).compareTo(other.getLevel());
		if (comp!=0) return comp;
		return COLLATOR.compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the common
	 */
	public boolean isCommon() {
		return common;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multiple
	 */
	public boolean isMultiple() {
		return multiple;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @param mReq
//	 * @param mastershipRequirement
//	 */
//	public void replaceRequirement(MastershipRequirement mReq, MastershipRequirement mastershipRequirement) {
//		requires.remove(mReq);
//		requires.add(mastershipRequirement);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the focus
	 */
	public SMSkillSpecialization getFocus() {
		return focus;
	}

	//-------------------------------------------------------------------
	/**
	 * Used in deriving classes to perform validation checks on loading,
	 * if necessary
	 * @return Error message or NULL
	 */
	public void validate() throws DataErrorException {
		if (level==0)
			throw new DataErrorException(this, "Missing level for mastership '"+id+"'");
		if (applyTo==null)
			throw new DataErrorException(this, "Missing applyTo for mastership '"+id+"'");

		for (Modification mod : modifications) {
			mod.setSource(this);
			mod.validate();
		}
	}

}

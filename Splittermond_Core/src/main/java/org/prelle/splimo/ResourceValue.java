/**
 *
 */
package org.prelle.splimo;

import java.lang.System.Logger.Level;
import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.ModifyableNumericalValue;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;
import de.rpgframework.splittermond.log.Logging;

/**
 * @author prelle
 *
 */
public class ResourceValue extends ComplexDataItemValue<Resource>  implements ModifyableNumericalValue<Resource> {
	/**
	 * References some other object in the character. Format is either
	 * "creature:<uniqueid>" or "item:<uniqueid>"
	 */
	@Attribute(required=false)
	private UUID idRef;
	/** Is assigned from a module - and not actively from the user */
	private transient boolean systemAssigned;

	//-------------------------------------------------------------------
	public ResourceValue() {
	}

	//-------------------------------------------------------------------
	public ResourceValue(Resource resource, int value) {
		super(resource);
		if (resource==null)
			throw new NullPointerException("Resource may not be null");
		this.value = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "RSRC:"+getKey()+"(val="+getModifiedValue()+")";
	}

//	//-------------------------------------------------------------------
//	public int getModifier() {
//		int sum = 0;
//		for (Modification mod : modifications) {
//			if (mod.getReferenceType()!=SplittermondReference.RESOURCE)
//				continue;
//			DataItemModification iMod = (DataItemModification)mod;
//			String key = iMod.getKey();
//			if (!ref.equals(key)) {
//				Logging.logger.log(Level.ERROR, "Resource reference of '"+ref+"' contains modification for other resource: "+key);
//				continue;
//			}
//			if (mod instanceof ValueModification) {
//				sum+= ((ValueModification)mod).getValue();
//			} else {
//				sum+=1;
//			}
//		}
//		return sum;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @return the value
//	 */
//	public int getModifiedValue() {
//		return value + getModifier();
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the idref
	 */
	public UUID getIdReference() {
		return idRef;
	}

	//-------------------------------------------------------------------
	/**
	 * @param idref the idref to set
	 */
	public void setIdReference(UUID idref) {
		this.idRef = idref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the systemAssigned
	 */
	public boolean isSystemAssigned() {
		return systemAssigned;
	}

	//-------------------------------------------------------------------
	/**
	 * @param systemAssigned the systemAssigned to set
	 */
	public void setSystemAssigned(boolean systemAssigned) {
		this.systemAssigned = systemAssigned;
	}

}

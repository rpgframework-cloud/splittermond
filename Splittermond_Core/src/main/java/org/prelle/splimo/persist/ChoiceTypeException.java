/**
 * 
 */
package org.prelle.splimo.persist;

import org.prelle.splimo.modifications.SplittermondReference;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class ChoiceTypeException extends RuntimeException {
	
	private SplittermondReference type;
	private String reference;
	private Object context;

	//--------------------------------------------------------------------
	public ChoiceTypeException(SplittermondReference type, String ref) {
		super("Invalid reference to "+type+" '"+ref+"'");
		this.type = type;
		this.reference = ref;
	}
	//--------------------------------------------------------------------
	public ChoiceTypeException(SplittermondReference type, String ref, Object context) {
		super("Invalid reference to "+type+" '"+ref+"' in context "+context);
		this.type = type;
		this.reference = ref;
		this.context = context;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SplittermondReference getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the context
	 */
	public Object getContext() {
		return context;
	}

}

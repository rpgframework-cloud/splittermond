/**
 *
 */
package org.prelle.splimo.persist;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

/**
 * @author prelle
 *
 */
public class MastershipConverter implements StringValueConverter<Mastership> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	public Mastership read(String v) {
		Mastership master = SplitterMondCore.getItem(Mastership.class, v);
		if (master!=null)
			return master;
		StringTokenizer tok = new StringTokenizer(v, "/- ");
		try {
			String skillID = tok.nextToken();
			String masterID = tok.nextToken();
			SMSkill skill = SplitterMondCore.getSkill(skillID);
			if (skill==null) {
				throw new ReferenceException(SplittermondReference.SKILL, skillID);
			}
			master = skill.getMastership(masterID);
			if (master==null) {
				// Search common mastership
				master = SplitterMondCore.getItem(Mastership.class, masterID);
			}
			if (master==null) {
				// Search common mastership
				master = SplitterMondCore.getItem(Mastership.class, v);
			}
			if (master==null) {
				throw new ReferenceException(SplittermondReference.MASTERSHIP, v);
			}
		} catch (NoSuchElementException e) {
			throw new ReferenceException(SplittermondReference.MASTERSHIP, v);
		}
		return master;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Mastership v) throws Exception {
		if (v.isCommon())
			return v.getSkill().getId();
		if (v.getSkill()==null)
			throw new NullPointerException("No skill set in "+v);
		return v.getSkill().getId()+"/"+v.getId();
	}

}

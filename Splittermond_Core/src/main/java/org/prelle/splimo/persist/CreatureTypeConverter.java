package org.prelle.splimo.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.modifications.SplittermondReference;

public class CreatureTypeConverter implements StringValueConverter<CreatureType> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CreatureType read(String v) throws Exception {
		CreatureType item = SplitterMondCore.getCreatureType(v);
		if (item==null) {
			throw new ReferenceException(SplittermondReference.CREATURE_TYPE, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(CreatureType v) throws Exception {
		return v.getId();
	}
	
}
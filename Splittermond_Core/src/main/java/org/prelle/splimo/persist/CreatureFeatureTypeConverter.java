package org.prelle.splimo.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.modifications.SplittermondReference;

public class CreatureFeatureTypeConverter implements StringValueConverter<CreatureFeature> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CreatureFeature read(String v) throws Exception {
		CreatureFeature item = SplitterMondCore.getCreatureFeatureType(v);
		if (item==null) {
			throw new ReferenceException(SplittermondReference.CREATURE_FEATURE_TYPE, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(CreatureFeature v) throws Exception {
		return v.getId();
	}
	
}
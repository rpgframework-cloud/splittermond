package org.prelle.splimo.persist;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

public class SkillSpecializationConverter implements StringValueConverter<SMSkillSpecialization> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public SMSkillSpecialization read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v, "/ ");
		try {
			String skillID   = tok.nextToken();
			SMSkill skill = SplitterMondCore.getSkill(skillID);
			if (skill==null) {
				throw new IllegalArgumentException("No such skill: "+v);
			}
			String specialID = tok.nextToken();
			return skill.getSpecialization(specialID);
		} catch (NoSuchElementException nse) {
			throw new ReferenceException(SplittermondReference.SKILL_SPECIAL, v);			
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(SMSkillSpecialization v) throws Exception {
		if (v==null)
			return null;
		if (v.getSkill()==null)
			throw new NullPointerException("No skill set in specialization "+v);
		String id = v.getSkill().getId()+"/"+v.getId();
		return id;
	}
}
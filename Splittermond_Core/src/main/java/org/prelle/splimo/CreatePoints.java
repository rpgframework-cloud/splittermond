package org.prelle.splimo;

/**
 * @author prelle
 *
 */
public enum CreatePoints {

	/** Distribute to existing skills */
	SKILL,
	/** Distribute to existing or new skills */
	SKILL_NEW,
	ATTACK,
	DAMAGE,
	IMPROVED_ATTACK,
	/** Distribute to existing or new magic schools */
	MAGIC,
	MASTERIES,
	/** free spell */
	SPELL

}

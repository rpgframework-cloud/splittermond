package org.prelle.splimo;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="services")
@ElementList(entry="service",type=Service.class,inline=true)
public class ServiceList extends ArrayList<Service> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public ServiceList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ServiceList(Collection<? extends Service> c) {
		super(c);
	}

}

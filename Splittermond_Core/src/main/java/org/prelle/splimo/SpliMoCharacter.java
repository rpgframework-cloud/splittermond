package org.prelle.splimo;

import java.lang.System.Logger.Level;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.items.ItemTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Lifeform;

/**
 * @author stefa
 *
 */
/**
 * @author prelle
 *
 */
@Root(name="splimochar")
public class SpliMoCharacter extends CommonCharacter<Attribute,SMSkill,SMSkillValue,ItemTemplate> implements Lifeform<Attribute, SMSkill, SMSkillValue> {

	protected static Gson GSON = (new GsonBuilder()).create();

	@Element
	protected String ruleProfile;
    @Element(name="chargenSettings")
    protected String chargenSettingsJSON;
    protected transient Object chargenSettingsObject;

	@org.prelle.simplepersist.Attribute(name="gen")
    private boolean generationMode;
	@org.prelle.simplepersist.Attribute(name="expInv")
	private int expInvested;
	@org.prelle.simplepersist.Attribute(name="exp")
	private int expFree;
	@org.prelle.simplepersist.Attribute(name="money")
	private int money;

    @Element
    private String birthPlace;

    @org.prelle.simplepersist.Attribute(name="race")
	private String race;
	@Element
	private ComplexDataItemValue<Culture> culture;
	@Element
	private ComplexDataItemValue<Background> background;
	@Element
	private ComplexDataItemValue<Education> education;
	@Element
	private String deity;
	@ElementList(entry="powerref", type=PowerValue.class)
	private List<PowerValue> powerrefs;
	@ElementList(entry="resourceref", type=ResourceValue.class)
	private List<ResourceValue> resourcerefs;
	@ElementList(entry="lang", type=String.class)
	private List<String> languages;
	@ElementList(entry="cultLore", type=String.class)
	private List<String> cultLores;
	@ElementList(entry="spell", type=SpellValue.class)
	private List<SpellValue> spells;
	@ElementList(entry="companion", type=Companion.class)
	private List<Companion> companions;
	@Element
	private String moonsign;
	@ElementList(entry="weakness", type=String.class)
	private List<String> weaknesses;

//	@ElementList(type = SMSkillValue.class, entry="skill")
//	private List<SMSkillValue> skills;
	private transient List<Language> autoLanguages;
	private transient List<CultureLore> autoCultLores;

    //-------------------------------------------------------------------
	public SpliMoCharacter() {
		name = "Unbenannt";
		skills       = new ArrayList<>();
		spells       = new ArrayList<>();
		powerrefs    = new ArrayList<>();
		resourcerefs = new ArrayList<>();

		cultLores    = new ArrayList<>();
		languages    = new ArrayList<>();
		autoCultLores    = new ArrayList<>();
		autoLanguages    = new ArrayList<>();
		weaknesses   = new ArrayList<>();
		companions   = new ArrayList<Companion>();
		culture      = new ComplexDataItemValue<Culture>();
		background   = new ComplexDataItemValue<Background>();
		education    = new ComplexDataItemValue<Education>();
		moonsign     = SplitterMondCore.getItemList(Moonsign.class).get(  (new Random()).nextInt(10)).getId();


		for (Attribute attr : Attribute.values()) {
			setAttribute(new AttributeValue(attr));
		}
		for (SMSkill skill : SplitterMondCore.getItemList(SMSkill.class)) {
			addSkill(new SMSkillValue(skill,0));
		}

		race = "human";
		culture.setResolved(SplitterMondCore.getItem(Culture.class, "selenia"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getShortDescription()
	 */
	@Override
	public String getShortDescription() {
		String p1 = (race!=null)?SplitterMondCore.getItem(Race.class, race).getName():"?";
		String p2 = (education!=null)?education.getNameWithoutRating():"?";
		return p1+" "+p2;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SPLITTERMOND;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenSettingsJSON
	 */
	public String getChargenSettingsJSON() {
		return chargenSettingsJSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenSettingsJSON the chargenSettingsJSON to set
	 */
	public void setChargenSettingsJSON(String chargenSettingsJSON) {
		this.chargenSettingsJSON = chargenSettingsJSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenSettingsObject
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> T getCharGenSettings(Class<T> cls) {
		return (T)chargenSettingsObject;
	}

	//-------------------------------------------------------------------
	public boolean hasCharGenSettings(Class<? extends Object> cls) {
		return chargenSettingsObject!=null && cls.isAssignableFrom(chargenSettingsObject.getClass());
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenSettingsObject the chargenSettingsObject to set
	 */
	public void setCharGenSettings(Object chargenSettingsObject) {
		this.chargenSettingsObject = chargenSettingsObject;
		System.err.println("SpliMoCharacter.setCharGenSettings: overwrite with "+chargenSettingsObject);
		System.getLogger(SpliMoCharacter.class.getPackageName()).log(Level.ERROR,"overwrite with "+chargenSettingsObject);
		chargenSettingsJSON = GSON.toJson(chargenSettingsObject);
	}

	//-------------------------------------------------------------------
	public void readCharGenSettings(Class<?> clazz) {
		if (clazz==null) throw new NullPointerException("Class to decode JSON chargen settings not set");
		if (chargenSettingsObject!=null && clazz.isAssignableFrom(chargenSettingsObject.getClass()))
			return;
		try {
			if (chargenSettingsJSON!=null)
				chargenSettingsObject = GSON.fromJson(chargenSettingsJSON, clazz);
			if (chargenSettingsObject==null) {
				try {
					chargenSettingsObject = clazz.getConstructor().newInstance();
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					System.getLogger(getClass().getPackageName()).log(Level.ERROR, "Failed creating settings object",e);
				}
			}
			System.out.println("ShadowrunCharacter.readCharGenSettings ----> settings = "+chargenSettingsObject);
		} catch (JsonSyntaxException e) {
			System.getLogger(getClass().getPackageName()).log(Level.ERROR, "Error decoding JSON\n"+chargenSettingsJSON,e);
			throw e;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getAttribute(A)
	 */
	@Override
	public AttributeValue<Attribute> getAttribute(String key) {
		return super.getAttribute(Attribute.valueOf(key));
	}

	//-------------------------------------------------------------------
	/**
	 * @return the race
	 */
	public String getRace() {
		return race;
	}

	//-------------------------------------------------------------------
	/**
	 * @param race the race to set
	 */
	public void setRace(String race) {
		this.race = race;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the culture
	 */
	public ComplexDataItemValue<Culture> getCulture() {
		return culture;
	}

	//-------------------------------------------------------------------
	/**
	 * @param data the culture to set
	 */
	public void setCulture(Culture data) {
		if (this.culture.getResolved()==data) return;
		this.culture.setResolved(data);
		this.culture.setCustomName(null);
		this.culture.getDecisions().clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the background
	 */
	public ComplexDataItemValue<Background> getBackground() {
		return background;
	}

	//-------------------------------------------------------------------
	/**
	 * @param data the background to set
	 */
	public void setBackground(Background data) {
		if (this.background.getResolved()==data) return;
		this.background.setResolved(data);
		this.background.setCustomName(null);
		this.background.getDecisions().clear();
	}

	//-------------------------------------------------------------------
	public ComplexDataItemValue<Education> getEducation() {
		return education;
	}

	//-------------------------------------------------------------------
	public void setEducation(Education data) {
		if (this.education.getResolved()==data) return;
		this.education.setResolved(data);
		this.education.setCustomName(null);
		this.education.getDecisions().clear();
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("Name  : "+name);
		buf.append("\nRasse : "+race);
		buf.append("\nKultur: "+culture);
		buf.append("\nAbstam: "+background);
		buf.append("\nAusbil: "+education);

		for (AttributeValue aVal : getAttributes()) {
			if (aVal.getModifyable() == Attribute.DAMAGE_REDUCTION) {
				continue;
			}
			buf.append("\n"+aVal.getModifyable()+": "+aVal+"  \t");
		}

//		buf.append("\n\nKulturkunden:\n=============");
//		for (CultureLoreReference data : culturelores) {
//			buf.append("\n"+data.getCultureLore().getName());
//		}
//
//		buf.append("\n\nSprachen:\n=============");
//		for (LanguageReference data : languages) {
//			buf.append("\n"+data.getLanguage().getName());
//		}
//
//		buf.append("\n\nStärken:\n=============");
//		for (PowerReference power : powerrefs) {
//			buf.append("\n"+power.getPower().getName()+" "+power.getCount());
//		}
//
//		buf.append("\n\nSchwächen:\n=============");
//		for (String weak : weaknesses) {
//			buf.append("\n"+weak);
//		}
//
//		buf.append("\n\nFertigkeiten:\n=============");
//		for (SkillValue tmp : skillvals) {
//			buf.append("\n "+tmp.getSkill().getName()+" : "+tmp.getModifiedValue());
//		}
//
//		buf.append("\n\nZauber:\n=============");
//		for (SpellValue tmp : spellvals) {
//			buf.append("\n "+tmp.getSpell().getName()+" : "+tmp);
//		}
//
//		buf.append("\n\nInventar:\n=============");
//		for (CarriedItem tmp : carries) {
//			buf.append("\n "+tmp.getItem().getName());
//		}
//
//		buf.append("\n\nKreaturen:\n=============");
//		for (CreatureReference tmp : creatures) {
//			buf.append("\n "+tmp.getName());
//		}

		return buf.toString();
	}

//	public List<SkillValue> getSkillValues() {
//		return new ArrayList<>(skills);
//	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.data.CommonCharacter#getSkillValue(de.rpgframework.genericrpg.data.ISkill)
//	 */
//	@Override
//	public SMSkillValue getSkillValue(SMSkill key) {
//		for (SMSkillValue sVal : skills) {
//			if (sVal.getModifyable()==(key))
//				return sVal;
//		}
//		return null;
//	}

	public void addSkill(SMSkillValue value) {
		for (SMSkillValue sVal : skills) {
			if (sVal.getModifyable().equals(value.getModifyable())) {
				skills.remove(sVal);
				break;
			}
		}
		skills.add(value);
	}

	//-------------------------------------------------------------------
	public String getBirthplace() { return birthPlace; }
	public void setBirthPlace(String birthPlace) { this.birthPlace = birthPlace;}

	//-------------------------------------------------------------------
	/**
	 * @return the deity
	 */
	public String getDeity() {
		return deity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param deity the deity to set
	 */
	public void setDeity(String deity) {
		this.deity = deity;
	}

	//--------------------------------------------------------------------
	public boolean hasPower(Power power) {
		for (PowerValue ref : powerrefs)
			if (ref.getModifyable()==power)
				return true;
		return false;
	}

	//--------------------------------------------------------------------
	public boolean hasPower(String key) {
		for (PowerValue ref : powerrefs)
			if (ref.getKey().equals(key))
				return true;
		return false;
	}

	//--------------------------------------------------------------------
	public PowerValue getPower(Power power) {
		for (PowerValue ref : powerrefs)
			if (ref.getModifyable()==power)
				return ref;
		return null;
	}

	//-------------------------------------------------------------------
	public PowerValue getPower(String key) {
		for (PowerValue ref : powerrefs) {
			if (ref.getModifyable().getId().equals(key)) {
				return ref;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addPower(PowerValue ref) {
		powerrefs.add(ref);
	}

	//-------------------------------------------------------------------
	public void removePower(PowerValue ref) {
		powerrefs.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<PowerValue> getPowers() {
		return new ArrayList<PowerValue>(powerrefs);
	}

	//-------------------------------------------------------------------
	public void addResource(ResourceValue ref) {
		if (!resourcerefs.contains(ref))
			resourcerefs.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeResource(ResourceValue ref) {
		resourcerefs.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<ResourceValue> getResources() {
		return new ArrayList<ResourceValue>(resourcerefs);
	}

	//-------------------------------------------------------------------
	public ResourceValue getResource(String key) {
		for (ResourceValue ref : resourcerefs) {
			if (ref.getModifyable().getId().equals(key)) {
				return ref;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the expInvested
	 */
	public int getExpInvested() {
		return expInvested;
	}

	//-------------------------------------------------------------------
	/**
	 * @param expInvested the expInvested to set
	 */
	public void setExpInvested(int expInvested) {
		this.expInvested = expInvested;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the expFree
	 */
	public int getExpFree() {
		return expFree;
	}

	//-------------------------------------------------------------------
	/**
	 * @param expFree the expFree to set
	 */
	public void setExpFree(int expFree) {
		this.expFree = expFree;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the languages
	 */
	public List<Language> getLanguages() {
		List<Language> ret = new ArrayList<>();
		ret.addAll(languages.stream().map(cl -> SplitterMondCore.getItem(Language.class, cl)).collect(Collectors.toList()));
		ret.addAll(autoLanguages);
		return ret;
	}
	public void clearAutoLanguage() { autoLanguages.clear(); }
	public void addAutoLanguage(Language value) { autoLanguages.add(value); }
	public List<Language> getAutoLanguages() { return autoLanguages; }

	//-------------------------------------------------------------------
	public void addLanguage(Language value) {
		if (!languages.contains(value.getId()))
			languages.add(value.getId());
	}

	//-------------------------------------------------------------------
	public void removeLanguage(Language value) {
		languages.remove(value.getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cultLores
	 */
	public List<CultureLore> getCultureLores() {
		List<CultureLore> ret = new ArrayList<>();
		ret.addAll(cultLores.stream().map(cl -> SplitterMondCore.getItem(CultureLore.class, cl)).collect(Collectors.toList()));
		ret.addAll(autoCultLores);
		return ret;
	}
	public void clearAutoCultureLore() { autoCultLores.clear(); }
	public void addAutoCultureLore(CultureLore value) { autoCultLores.add(value); }
	public List<CultureLore> getAutoCultureLore() { return autoCultLores; }

	//-------------------------------------------------------------------
	public void addCultureLore(CultureLore value) {
		if (!cultLores.contains(value.getId()))
			cultLores.add(value.getId());
	}

	//-------------------------------------------------------------------
	public void removeCultureLore(CultureLore value) {
		cultLores.remove(value.getId());
	}

	//-------------------------------------------------------------------
	public boolean hasSpell(Spell spell, SMSkill school) {
		return spells.stream().anyMatch(val -> val.getSkill()==school && val.getResolved()==spell);
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue ref) {
		if (!spells.contains(ref))
			spells.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSpell(SpellValue ref) {
		spells.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells() {
		return new ArrayList<SpellValue>(spells);
	}

	//-------------------------------------------------------------------
	public List<SMSkillValue> getSkillValues(SkillType type) {
		return super.getSkillValues().stream().filter(sv -> sv.getResolved().getType()==type).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the money
	 */
	public int getMoney() {
		return money;
	}

	//-------------------------------------------------------------------
	/**
	 * @param money the money to set
	 */
	public void setMoney(int money) {
		this.money = money;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the money
	 */
	public int getLevel() {
		AttributeValue<Attribute> val = getAttribute(Attribute.LEVEL);
		if (val!=null)
			return val.getModifiedValue();
		return 0;
	}

	//-------------------------------------------------------------------
	public void addCompanion(Companion ref) {
		if (!companions.contains(ref))
			companions.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSpell(SMLifeform ref) {
		companions.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<Companion> getCompanions() {
		return new ArrayList<Companion>(companions);
	}

	//-------------------------------------------------------------------
	/**
	 * Returns the highest closecombat SkillValue which is use as shield skill.
	 * @return highest closecombat SkillValue
	 */
	public SMSkillValue getShieldSkillValue(){
		SMSkillValue value = null;
		for (SMSkillValue skillValue : this.getSkillValues(SkillType.COMBAT)) {
			SMSkill skill = skillValue.getSkill();

			if (skill.getId().equals("throwing") || skill.getId().equals("longrange")) {
				continue;
			}

			if (value == null||skillValue.getValue() > value.getValue()) {
				value = skillValue;
			}
		}
		return value;
	}

	//--------------------------------------------------------------------
	public Moonsign getMoonsign() {
		if (moonsign==null) return null;
		return SplitterMondCore.getItem(Moonsign.class, moonsign);
	}

	//--------------------------------------------------------------------
	public void setMoonsign(Moonsign value) {
		this.moonsign = value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the weakness
	 */
	public List<String> getWeaknesses() {
		return weaknesses;
	}

	//-------------------------------------------------------------------
	public void addWeakness(String weakness) {
		weaknesses.add(weakness);
	}

	//-------------------------------------------------------------------
	public void removeWeakness(String weakness) {
		weaknesses.remove(weakness);
	}

}

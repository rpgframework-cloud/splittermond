package org.prelle.splimo.modifications;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Function;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.splimo.Aspect;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.CreatePoints;
import org.prelle.splimo.Culture;
import org.prelle.splimo.CultureLore;
import org.prelle.splimo.DamageType;
import org.prelle.splimo.Education;
import org.prelle.splimo.Language;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Power;
import org.prelle.splimo.Race;
import org.prelle.splimo.Resource;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.Service;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellAttribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.TerrainType;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.ItemSubType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.persist.AttributeConverter;
import org.prelle.splimo.persist.MastershipConverter;
import org.prelle.splimo.persist.ReferenceException;
import org.prelle.splimo.persist.SkillSpecializationConverter;

import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public enum SplittermondReference implements ModifiedObjectType {

	ASPECT(Aspect.class),
	ATTRIBUTE("ATTRIBUTE"),
	BACKGROUND(Background.class),
	CARRIED("CarriedItem"),
	CREATION_POINTS(CreatePoints.class,0),
	CREATURE(Creature.class),
	CREATURE_TYPE(CreatureType.class),
	CREATURE_FEATURE_TYPE(CreatureFeature.class),
	CULTURE(Culture.class),
	CULTURE_LORE(CultureLore.class),
	DAMAGE_TYPE(DamageType.class,0),
	EDUCATION(Education.class),
	ELEMENT(org.prelle.splimo.Element.class,0),
	FEATURE_TYPE(FeatureType.class),
	GEAR(ItemTemplate.class),
	GENDER("GENDER"),
	ITEMTYPE(ItemType.class,0),
	ITEMSUBTYPE(ItemSubType.class,0),
	ITEM_ATTRIBUTE(SMItemAttribute.class,0),
	LANGUAGE(Language.class),
	MAGIC_CREATURE("MAGIC_CREATURE"),
	MASTERSHIP(new MastershipConverter()),
	POISON("POISON"),
	POWER(Power.class),
	RACE(Race.class),
	RESOURCE(Resource.class),
	SERVICE(Service.class),
	SKILL(SMSkill.class),
	SPELL(Spell.class),
	SKILL_SPECIAL("SKILLSPEC"),
	SPELL_ATTRIBUTE(SpellAttribute.class,0),
	SPELLTYPE_SPECIAL("spelltype"),
	SURROUNDING("SURROUNDING"),
	TERRAIN_TYPE(TerrainType.class),
	TEXT("TEXT"),
	;

	Class<? extends DataItem> typeClass;
	String typeId;
	Class<? extends Enum<?>> enumType;
	StringValueConverter<? extends Object> converter;
	Function<String, ? extends DataItem> resolver;

	//-------------------------------------------------------------------
	SplittermondReference(StringValueConverter<? extends Object> conv) {
		converter = conv;
	}

	//-------------------------------------------------------------------
	SplittermondReference(Function<String, ? extends DataItem> resolv) {
		resolver = resolv;
	}

	//-------------------------------------------------------------------
	SplittermondReference(String type) {
		this.typeId = type;
		if ("ATTRIBUTE".equals(type)) {
			converter = new AttributeConverter();
		}
		if ("SKILLSPEC".equals(type)) {
			converter = new SkillSpecializationConverter();
		}
	}

	//-------------------------------------------------------------------
	SplittermondReference(Class<? extends DataItem> cls) {
		this.typeClass = cls;
	}

	//-------------------------------------------------------------------
	SplittermondReference(Class<? extends Enum<?>> enumType, int x) {
		this.enumType = enumType;
	}

	//-------------------------------------------------------------------
	public static <T> T  resolve(SplittermondReference type, String key) {
//		if ("MAGIC".equals(key)) {
//			System.out.println("SplittermondReference.resolve");
//		}
		if (type.typeClass!=null) {
			if (type==SKILL) {
				if ("MAGIC".equals(key)) {
					System.out.println("SplittermondReference.resolve");
				}
			}
			return (T) SplitterMondCore.getItem(type.typeClass, key);
		} else if (type.enumType!=null) {
			try {
				Method valueOf = type.enumType.getMethod("valueOf", String.class);
				return (T) valueOf.invoke(null, key);
			} catch (InvocationTargetException ivte) {
				Throwable ee = ivte.getTargetException();
				if (ee instanceof IllegalArgumentException) {
					throw new ReferenceException(type, key);
				}
				System.err.println(SplittermondReference.class.getSimpleName()+".resolve()-1:");
				ivte.printStackTrace();
			} catch (Exception e) {
				System.err.println(SplittermondReference.class.getSimpleName()+".resolve()-1:");
				e.printStackTrace();
			}
		} else if (type.resolver!=null) {
			return (T)type.resolver.apply(key);
		} else {
			if (type==SplittermondReference.TEXT)
				return (T)key;
			if (type.converter==null) {
				System.err.println("No converter for "+type+" in "+SplittermondReference.class.getSimpleName());
				System.exit(1);
				return null;
			}
			try {
				return (T) type.converter.read(key);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		throw new ReferenceException(type, key);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static <T> T[] resolveAll(SplittermondReference type) {
		switch (type) {
		case ATTRIBUTE:
			return (T[]) org.prelle.splimo.Attribute.primaryValues();
		default:
			throw new IllegalArgumentException("ALL not supported for "+type);
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static <T> T[] resolveVariable(SplittermondReference type, String varName) {
		switch (type) {
		case ATTRIBUTE:
			switch (varName) {
			case "PHYSICAL": return (T[]) new Attribute[] {Attribute.AGILITY, Attribute.STRENGTH, Attribute.CHARISMA, Attribute.CONSTITUTION};
			case "MENTAL": return (T[]) new Attribute[] {Attribute.MIND, Attribute.MYSTIC, Attribute.WILLPOWER, Attribute.INTUITION};
			}
			throw new IllegalArgumentException("Unknown variable "+varName+" for "+type);
		case SKILL:
			switch (varName) {
			case "COMBAT": return (T[]) SplitterMondCore.getSkills(SkillType.COMBAT).toArray();
			case "MAGIC": return (T[]) SplitterMondCore.getSkills(SkillType.MAGIC).toArray();
			}
			throw new IllegalArgumentException("Unknown variable "+varName+" for "+type);
		case MASTERSHIP:
			if (varName.startsWith("SKILL:")) {
				String key = varName.substring(6);
				SMSkill skill = SplitterMondCore.getSkill(key);
				return (T[])skill.getMasterships()
						.stream()
						.filter(m -> m.getLevel()==1)
						.map(m -> new MastershipReference(m, skill))
						.toArray();
			}
			throw new IllegalArgumentException("Don't know how to convert to masterships: "+varName);
		default:
			throw new IllegalArgumentException("Variables of type "+type+" not supported");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#resolve(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T resolve(String key) {
		return (T)SplittermondReference.resolve(this, key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#resolve(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends DataItem> T resolveAsDataItem(String key) {
		return (T)SplittermondReference.resolve(this, key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#resolveAny()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] resolveAny() {
		return (T[])SplittermondReference.resolveAll(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#resolveAny()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] resolveVariable(String varName) {
		return (T[])SplittermondReference.resolveVariable(this, varName);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#instantiateModification(de.rpgframework.genericrpg.modification.Modification, de.rpgframework.genericrpg.data.ComplexDataItemValue, de.rpgframework.genericrpg.data.CommonCharacter)
	 */
	@Override
	public Modification instantiateModification(Modification tmp, ComplexDataItemValue<?> value, int multiplier,
			CommonCharacter<?, ?, ?,?> model) {
		return SplitterTools.instantiateModification(tmp, value, multiplier, (SpliMoCharacter) model );
	}

}

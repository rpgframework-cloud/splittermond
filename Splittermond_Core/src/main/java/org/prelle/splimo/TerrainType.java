/**
 * 
 */
package org.prelle.splimo;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="terrain.type")
public class TerrainType extends DataItem {

}

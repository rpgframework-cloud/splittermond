/**
 * 
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="masterships")
@ElementList(entry="mastership",type=Mastership.class,inline=true)
public class MastershipList extends ArrayList<Mastership> {

	private static final long serialVersionUID = 3532129761175646703L;

	//-------------------------------------------------------------------
	public MastershipList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MastershipList(Collection<? extends Mastership> c) {
		super(c);
	}

}

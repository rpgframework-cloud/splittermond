/**
 *
 */
package org.prelle.splimo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.splimo.modifications.ModificationSource;
import org.prelle.splimo.modifications.SkillModification;

import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
@Root(name = "skillval")
public class SMSkillValue extends ASkillValue<SMSkill> {


	@ElementList(entry="masterref",type=MastershipReference.class,inline=true)
	private List<MastershipReference> masterships;

	private transient int modifierCap;

	//-------------------------------------------------------------------
	public SMSkillValue() {
		masterships     = new ArrayList<MastershipReference>();
	}

	//-------------------------------------------------------------------
	public SMSkillValue(SMSkill skill, int val) {
		super(skill);
		this.value = val;
		masterships = new ArrayList<MastershipReference>();
	}

	//-------------------------------------------------------------------
	public SMSkillValue(SMSkillValue toClone) {
		this.resolved = toClone.getSkill();
		this.value = toClone.getValue();
		outgoingModifications.addAll(toClone.getOutgoingModifications());
		masterships = new ArrayList<MastershipReference>(toClone.getMasterships());
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s = %s (%s)",
				String.valueOf(resolved),
				value+"+"+getModifier(),
				String.valueOf(outgoingModifications)
				);
		//		return skill+"="+value+" ("+masterships+")";
	}

	//-------------------------------------------------------------------
	@Deprecated
	public String getName() {
		return getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		StringBuffer buf = new StringBuffer();
		if (resolved==null)
			buf.append("? ");
		else
			buf.append(resolved.getName(loc)+" ");
		buf.append(String.valueOf(value));
		if (getModifier()>0)
			buf.append("("+getModifiedValue()+")");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof SMSkillValue) {
			SMSkillValue other = (SMSkillValue)o;
			if (resolved!=other.getSkill()) return false;
			if (value!=other.getValue()) return false;
			return masterships.equals(other.getMasterships());
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String getSkillReference() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public SMSkill getSkill() {
		if (resolved==null) {
			resolved = SplitterMondCore.getItem(SMSkill.class, ref);
		}
		return resolved;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(SMSkill skill) {
		this.resolved = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	public List<MastershipReference> getMasterships() {
		return masterships;
	}

	//-------------------------------------------------------------------
	public boolean hasMastership(Mastership master) {
		for (MastershipReference ref : masterships)
			if (ref.getMastership()==master)
				return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @param masterships the masterships to set
	 */
	public void addMastership(MastershipReference mastership) {
		if (!masterships.contains(mastership))
			masterships.add(mastership);
	}

	//-------------------------------------------------------------------
	public void removeMastership(Mastership mastership) {
		for (MastershipReference ref : masterships)
			if (ref.getMastership()==mastership) {
				masterships.remove(ref);
				return;
			}
	}

	//-------------------------------------------------------------------
	public void removeSpecialization(SMSkillSpecialization special) {
		for (SkillSpecializationValue<SMSkill> ref : specializations) {
			if (ref.getModifyable()!=null && ref.getModifyable()==special) {
				specializations.remove(ref);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	public int getSpecializationLevel(SMSkillSpecialization special) {
		if (special==null)
			throw new NullPointerException("SkillSpecialization is null");
		for (SkillSpecializationValue<SMSkill> tmp : specializations) {
			if (tmp.getResolved()==null)
				continue;
			SMSkillSpecialization tmpSpec = (SMSkillSpecialization) tmp.getResolved();
			if (tmpSpec.getId().equals(special.getId()))
				return tmpSpec.getLevel();
		}

		return 0;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue<SMSkill> setSpecializationLevel(SMSkillSpecialization special, int level) {
		// Search for old data
		for (SkillSpecializationValue<SMSkill> tmp : specializations) {
			if (tmp.getResolved()==null)
				continue;
			SMSkillSpecialization tmpSpec = (SMSkillSpecialization) tmp.getResolved();
			if (tmpSpec.getId().equals(special.getId())) {
				tmp.setDistributed(level);
				return tmp;
			}

		}

		SkillSpecializationValue<SMSkill> ref = new SkillSpecializationValue<SMSkill>(special, level);
		specializations.add(ref);
		return ref;
	}

	//-------------------------------------------------------------------
	public int getModifier() {
		int count = 0;
		int countEquip = 0;
		int countMagic = 0;
		for (Modification mod : outgoingModifications) {
			if (mod instanceof ValueModification) {
				ValueModification sMod = (ValueModification)mod;
				count += sMod.getValue();
			} else

			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
//				if (sMod.isConditional())
//					continue;
				System.err.println("SkillValue.getModifier: TODO: isConditional");
				if (sMod.getSkill()==resolved) {
					if (sMod.getModificationSource()==ModificationSource.EQUIPMENT)
						countEquip += sMod.getValue();
					else if (sMod.getModificationSource()==ModificationSource.MAGICAL)
						countMagic += sMod.getValue();
					else
						count += sMod.getValue();
				}
			}
		}

		if (modifierCap>0) {
			count += Math.min(countMagic, modifierCap);
			count += Math.min(countEquip, modifierCap);
		} else {
			count += countMagic;
			count += countEquip;
		}

		return count;
	}

	//-------------------------------------------------------------------
	public void setModifierCap(int modifierCap) {
		this.modifierCap = modifierCap;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public org.prelle.splimo.SMSkill getModifyable() {
		return resolved;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getPoints()
	 */
	@Override
	public int getDistributed() {
		return getValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#setPoints(int)
	 */
	@Override
	public void setDistributed(int points) {
		setValue(points);
	}

}

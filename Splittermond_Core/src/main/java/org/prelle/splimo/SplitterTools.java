package org.prelle.splimo;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.function.Function;

import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.companion.proc.CompanionTool;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.creature.ServiceValue;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.items.SMItemFlag;
import org.prelle.splimo.items.proc.SMResolveTemplatesStep;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splimo.persist.MoneyConverter;
import org.prelle.splimo.persist.ReferenceException;
import org.prelle.splimo.persist.SpellCostConverter;
import org.prelle.splimo.persist.WeaponDamageConverter;
import org.prelle.splimo.proc.ApplyModifications;
import org.prelle.splimo.proc.CalculateAttributePools;
import org.prelle.splimo.proc.CalculateDerivedAttributes;
import org.prelle.splimo.proc.GetModificationsFromMasterships;
import org.prelle.splimo.proc.GetModificationsFromPowers;
import org.prelle.splimo.proc.GetModificationsFromRace;
import org.prelle.splimo.proc.ResetModifications;
import org.prelle.splimo.proc.TrickleDownSpellModifications;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.ProcessingStep;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.GearTool;
import de.rpgframework.genericrpg.items.ItemAttributeObjectValue;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.items.formula.FormulaImpl;
import de.rpgframework.genericrpg.items.formula.FormulaTool;
import de.rpgframework.genericrpg.items.formula.VariableResolver;
import de.rpgframework.genericrpg.modification.CheckModification;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.RelevanceModification;
import de.rpgframework.genericrpg.modification.ValueModification;
import de.rpgframework.genericrpg.requirements.AnyRequirement;
import de.rpgframework.genericrpg.requirements.ExistenceRequirement;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ValueRequirement;
import de.rpgframework.splittermond.log.Logging;

/**
 * @author prelle
 *
 */
public class SplitterTools {

	public static record SummoningCategory(String name, List<SMLifeform> lifeforms) {};

	private final static Logger logger = Logging.logger;

	private final static MultiLanguageResourceBundle RES = SplitterMondCore.getI18nResources();

	public final static List<Class<? extends ProcessingStep>> RECALCULATE_STEPS = Arrays.asList(
		ResetModifications.class,
//		EnsureAttributePresence.class,
		GetModificationsFromRace.class,
		ApplyModifications.class,
		GetModificationsFromPowers.class,
		GetModificationsFromMasterships.class,
//		GetModificationsFromGear.class,
////		new GetModificationsFromMetamagicOrEchoes(),
//		GetModificationsFromFoci.class,
		ApplyModifications.class,
////		new ApplyAdeptPowerModifications(),
//		GetModificationsFromPowers.class,
//		GetModificationsFromTechniques.class,
//		ApplyModificationsGeneric.class,
////		new ApplyCarriedItemModifications(),
////		new DistributeAccessoriesToContainers(),
////		new ApplyAttributeModifications(),
////		new ApplySkillModifications(),
////		new ApplyMemorizedUUIDModifications(),
//////		new ApplySINModifications(),
////		new ConnectSignatureManeuvers(),
////		new ApplyRelevanceAndEdgeMods(),
//		CalculateEssence.class,
//		CalculatePersona.class,
		CalculateDerivedAttributes.class,
		CalculateAttributePools.class,
		TrickleDownSpellModifications.class
//		CalculateSkillPools.class,
//		CalculateMeleeAndUnarmed.class
	);

	public final static String LEVEL4 = "Level 4";
	public final static String LEVEL3 = "Level 3";
	public final static String LEVEL2 = "Level 2";

	//-------------------------------------------------------------------
	public static ComplexDataItem resolveDecision(ComplexDataItem referTo, Decision dec) {
		Choice choice = referTo.getChoice(dec.getChoiceUUID());
		if (choice!=null) {
			String key = dec.getValue();
			try {
				SplittermondReference type = (SplittermondReference)choice.getChooseFrom();
				switch (type) {
				case POWER:
					Power power = SplitterMondCore.getItem(Power.class, key);
					if (power==null) {
						throw new DataErrorException(referTo, "Cannot resolve power '"+key+"' in decision for "+referTo.getTypeString()+":"+referTo.getId());
					}
					return power;
				default:
					Logging.logger.log(Level.ERROR, "Unsupported choice type: "+type+" in item "+referTo);
					throw new DataErrorException(referTo, "Unsupported choice type: "+type);
				}
			} catch (IllegalArgumentException e) {
				Logging.logger.log(Level.ERROR, "Unknown choice type: "+choice.getChooseFrom()+" in item "+referTo);
				throw new DataErrorException(referTo, "Unknown choice type: "+choice.getChooseFrom());
			}
		} else {
			Logging.logger.log(Level.ERROR, "Choice not found: "+dec+" in item "+referTo);
			throw new DataErrorException(referTo, "Choice not found: "+dec+" in item "+referTo);
		}
	}

	//-------------------------------------------------------------------
	private static String getAttributeChoiceName(String id) {
		if (id==null)
			return "ein belieb. Attribut";
		return Attribute.valueOf(id).getName();
	}

	//-------------------------------------------------------------------
	private static String getBackgroundChoiceName(String id) {
		if (id==null)
			return "ein belieb. Abstammung";
		return SplitterMondCore.getItem(Background.class, id).getName();
	}

	//-------------------------------------------------------------------
	private static String getSkillChoiceName(String id) {
		if ("COMBAT".equals(id))
			return "eine Kampffertigkeit";
		if ("MAGIC".equals(id))
			return "eine Magieschule";
		if (id==null || SplitterMondCore.getSkill(id)==null)
			return "?null?";
		return SplitterMondCore.getSkill(id).getName();
	}

	//-------------------------------------------------------------------
	private static List<SMSkill> getSkillChoiceOptions(String id) {
		if ("COMBAT".equals(id))
			return SplitterMondCore.getSkills(SkillType.COMBAT);
		if ("MAGIC".equals(id))
			return SplitterMondCore.getSkills(SkillType.MAGIC);
		if (id==null || SplitterMondCore.getSkill(id)==null)
			return List.of();
		return List.of(SplitterMondCore.getSkill(id));
	}

	//-------------------------------------------------------------------
	private static String getPowerChoiceName(String id) {
		if (id==null)
			return "ein bel. Stärke";
		return SplitterMondCore.getItem(Power.class, id).getName();
	}

	//-------------------------------------------------------------------
	private static String getResourceChoiceName(String id) {
		if (id==null)
			return "ein beliebige Ressource";
		return SplitterMondCore.getItem(Resource.class, id).getName();
	}

	//-------------------------------------------------------------------
	private static String getMastershipChoiceName(String id) {
		if (id==null)
			return "ein bel. Meisterschaft";

		if (id.startsWith("SKILL:")) {
			id = id.substring(6);
			String[] skillIDs = id.split(",");
			List<String> ret = new ArrayList<>();
			for (String skillID : skillIDs) {
				ret.add( getMastershipChoiceName(skillID) );
			}
			return String.join(" oder ", ret);
		}

		StringTokenizer tok = new StringTokenizer(id, "/- ");
		Mastership master;
		try {
			String skillID = tok.nextToken();
			SMSkill skill = SplitterMondCore.getSkill(skillID);
			if (skill==null)
				return "ein bel. Meisterschaft";
			if (!tok.hasMoreTokens())
				return "ein bel. "+skill.getName()+" Meisterschaft";
			String masterID = tok.nextToken();
			if (skill==null) {
				throw new ReferenceException(SplittermondReference.SKILL, skillID);
			}
			master = skill.getMastership(masterID);
			if (master==null) {
				// Search common mastership
				master = SplitterMondCore.getItem(Mastership.class, masterID);
			}
			if (master!=null)
				return master.getName();

			SkillSpecialization<SMSkill> spec = skill.getSpecialization(masterID);
			if (spec!=null)
				return spec.getName();
		} catch (Exception e) {
			e.printStackTrace();
			return "SplitterTools.getMastershipChoiceName:Error";
		}
		return "SplitterTools.getMastershipChoiceName:null";
	}

	//-------------------------------------------------------------------
	private static String buildSentence(ComplexDataItem data, Choice choice, Function<String,String> converter) {
		List<String> converted = new ArrayList<>();
		if (choice != null && choice.getChoiceOptions() != null) {
			for (String key : choice.getChoiceOptions()) {
				converted.add(converter.apply(key));
			}
		}

		if (converted.size()==0)
			return converter.apply(null);
		if (converted.size()==1)
			return converted.get(0);
		if (converted.size()==2)
			return converted.get(0)+" oder "+converted.get(1);

		return String.join(", ", converted.subList(0, converted.size()-2))+" oder "+converted.get(converted.size()-1);
	}

	//-------------------------------------------------------------------
	private static List<?> buildOptionList(ComplexDataItem data, Choice choice, Function<String,Object> converter) {
		List<Object> converted = new ArrayList<>();
		if (choice != null && choice.getChoiceOptions() != null) {
			for (String key : choice.getChoiceOptions()) {
				converted.add(converter.apply(key));
			}
		}

		return converted;
	}

	//-------------------------------------------------------------------
	public static String getChoiceString(ComplexDataItem data, Choice choice) {
		// Find the modification for the choice
		DataItemModification mod = null;
		for (Modification tmp : data.getOutgoingModifications()) {
			if (tmp instanceof DataItemModification) {
				DataItemModification dmod = (DataItemModification)tmp;
				if (dmod.getConnectedChoice()!=null && dmod.getConnectedChoice().equals(choice.getUUID())) {
					mod = dmod;
					break;
				}
			}
		}
		final ValueModification vMod = (mod instanceof ValueModification)?(ValueModification) mod:null;

		SplittermondReference type = (SplittermondReference)choice.getChooseFrom();
		String tmp = null;
		switch (type) {
		case ATTRIBUTE:
			Logging.logger.log(Level.DEBUG, "  choice="+choice);
//			if (choice.getChoiceOptions()==null) {
//				tmp = buildSentence(data, choice, key -> getAttributeChoiceName(key));
//				choice.setChoiceOptions(String.join(",", List.of(Attribute.primaryValues()).stream().map(a -> a.name()).toList()));
//			}
			tmp = buildSentence(data, choice, key -> {
				String foo = getAttributeChoiceName(key);
				if (vMod!=null && vMod.getValue()!=0) {
					if (vMod.getValue()>0)
						foo+=" +"+vMod.getValue();
					else
						foo+=" "+vMod.getValue();
				}
				return foo;
				});
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		case BACKGROUND:
			tmp = buildSentence(data, choice, key -> getBackgroundChoiceName(key));
			if (choice.isNegated())
				tmp="Nicht "+tmp;
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		case POWER:
			tmp = buildSentence(data, choice, key -> getPowerChoiceName(key));
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		case MASTERSHIP:
			tmp = buildSentence(data, choice, key -> getMastershipChoiceName(key));
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		case RESOURCE:
			tmp = buildSentence(data, choice, key -> getResourceChoiceName(key));
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		case SKILL:
			tmp = buildSentence(data, choice, key -> getSkillChoiceName(key));
			Logging.logger.log(Level.DEBUG, "SplitterTools.getChoiceString("+choice+") = "+tmp);
			return tmp;
		}
		Logging.logger.log(Level.ERROR, "getChoiceString("+choice+")");
		System.err.println("getChoiceString("+type+")");

//		switch ((SplittermondReference)choice.getChooseFrom()) {
//		case ATTRIBUTE:
//			if (choice.getChoiceOptions()==null) {
//				if (choice.getDistribute()!=null) {
//					int sum = Arrays.asList(choice.getDistribute()).stream().reduce(0, Integer::sum);
//					return "Verteile "+sum+" Punkte auf "+choice.getDistribute().length+" verschiedene Attribute";
//				}
//				return "Wähle ein beliebiges Attribut";
//			}
//			List<String> attribs = Arrays.asList( choice.getChoiceOptions() );
//			List<String> except  = new ArrayList<>();
//			for (Attribute tmp : Attribute.primaryValues()) {
//				if (!attribs.contains(tmp.name()))
//					except.add(tmp.getName());
//			}
//			return "Wähle ein Attribut außer "+String.join(", ", except);
//		case SKILL:
//			if (choice.getChoiceOptions()==null) {
//				if (choice.getDistribute()!=null) {
//					int sum = Arrays.asList(choice.getDistribute()).stream().reduce(0, Integer::sum);
//					return "Verteile "+sum+" Punkte auf "+choice.getDistribute().length+" verschiedene Fertigkeiten";
//				}
//				return "Wähle ein beliebiges Fertigkeit";
//			}
//			attribs = Arrays.asList( choice.getChoiceOptions() );
//			List<String> positive  = new ArrayList<>();
//			except  = new ArrayList<>();
//			for (String key : choice.getChoiceOptions()) {
//				if ("MAGIC".equals(key)) {
//					positive.add("eine Magieschule");
//					break;
//				}
//				if ("COMBAT".equals(key)) {
//					positive.add("eine Kampffertigkeit");
//					break;
//				}
//				SMSkill skill = SplitterMondCore.getSkill(key);
//				if (skill!=null)
//					positive.add(skill.getName());
//				else {
//					Logging.logger.log(Level.WARNING, "Unknown skill reference '"+key+"' in choice "+choice.getUUID()+" from "+data);
//				}
//			}
//			for (SMSkill tmp : SplitterMondCore.getItemList(SMSkill.class)) {
//				if (!attribs.contains(tmp.getId()))
//					except.add(tmp.getName());
//			}
//			if (except.size()<positive.size())
//				return "Wähle eine Fertigkeit außer "+String.join(", ", except);
//			return String.join(" oder ", positive);
//		}
		return "SplitterTools.getChoiceString("+choice.getChooseFrom()+")";
	}

	//-------------------------------------------------------------------
	public static String getChoiceString(ComplexDataItem data, ModificationChoice choice, Locale loc) {
		return RES.getString("choice.modificationchoice");
	}

	//-------------------------------------------------------------------
	public static String getDecisionString(ComplexDataItem data, Choice choice, Decision dec) {
		return null;
	}

	//-------------------------------------------------------------------
	public static String getModificationSourceString(Object source) {
		if (source==null)
			return "Unknown source";
		if (source instanceof Attribute)
			return ((Attribute)source).getName();
		if (source instanceof SMItemAttribute)
			return ((SMItemAttribute)source).getName();
		if (source instanceof SMSkill)
			return ((SMSkill)source).getName();
		if (source instanceof CarriedItem) {
			return ((CarriedItem)source).getNameWithRating();
		} else if (source instanceof Power) {
			return RES.getString("label.power")+" "+((Power)source).getName();
		} else if (source instanceof ItemTemplate) {
			return ((ItemTemplate)source).getName();
		} else if (source instanceof String) {
			return (String)source;
//		} else if (source instanceof FocusValue) {
//			return ((FocusValue)source).getName();
		} else if (source instanceof DataItem) {
			return ((DataItem)source).getName();
		} else if (source instanceof DataItemValue) {
			DataItem di = ((DataItemValue)source).getResolved();
			return di.getName();
		}
		logger.log(Level.WARNING,"Missing treatment for modification source: "+source.getClass());
		return "?"+source.getClass().getSimpleName();
	}

	//-------------------------------------------------------------------
	public static String getModificationString(Modification mod, Locale loc) {
		String ret = getModificationStringWithoutCond(mod, loc);
		if (ret==null) return null;
		if (mod.isConditional()) {
			return ret+" ("+RES.getString("modification.conditional", loc)+")";
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static String getModificationStringWithoutCond(Modification mod, Locale loc) {
		SplittermondReference type = (SplittermondReference) mod.getReferenceType();
		if (mod instanceof ValueModification) {
			ValueModification valMod = (ValueModification)mod;

			String what = type.name();
			switch (type) {
			case ATTRIBUTE:
				String attrName = null;
				if (valMod.getConnectedChoice()!=null) {
					attrName = RES.getString("modification.choice.attribute", loc);
				} else {
					attrName = Attribute.valueOf(valMod.getKey()).getName(loc);
				}
				if (valMod.getValue()>0) {
					return attrName+" +"+valMod.getValue();
				} else {
					return attrName+" "+valMod.getValue();
				}
			case RESOURCE:
				String resrcName = null;
				if (valMod.getConnectedChoice()!=null) {
					resrcName = RES.getString("modification.choice.resource", loc);
				} else {
					resrcName = SplitterMondCore.getItem(Resource.class, valMod.getKey()).getName(loc);
				}
				if (valMod.getValue()>0) {
					return resrcName+" +"+valMod.getValue();
				} else {
					return resrcName+" "+valMod.getValue();
				}
			case SKILL:
				String skillName = null;
				if (valMod.getConnectedChoice()!=null) {
					skillName = RES.getString("modification.choice.skill", loc);
				} else {
					SMSkill skill = SplitterMondCore.getSkill(valMod.getKey());
					if (skill==null) {
						Logging.logger.log(Level.WARNING, "Found unknown skill '"+valMod.getKey()+"' in valuemod of "+valMod);
						skillName = "Unknown skill '"+valMod.getKey()+"'";
					} else {
						skillName = SplitterMondCore.getSkill(valMod.getKey()).getName(loc);
					}
				}
				if (valMod.getValue()>0) {
					return skillName+" +"+valMod.getValue();
				} else {
					return skillName+" "+valMod.getValue();
				}
			default:
				return "Unknown value type(0) "+type;
			}
		}

		else if (mod instanceof DataItemModification) {
			DataItemModification valMod = (DataItemModification)mod;
			switch (type) {
			case CULTURE_LORE:
				if (valMod.getConnectedChoice()!=null) {
					return "???"+valMod.getConnectedChoice()+"???";
				}

				CultureLore cultlore = type.resolve(valMod.getKey());
				if (cultlore==null) {
					Logging.logger.log(Level.WARNING, "Found unknown cultlore '"+valMod.getKey()+"' in valuemod of "+mod);
					return "Unknown cultlore '"+valMod.getKey()+"'";
				}
				return "Kulturkunde "+cultlore.getName();
			case LANGUAGE:
				if (valMod.getConnectedChoice()!=null) {
					return "???"+valMod.getConnectedChoice()+"???";
				}

				Language lang = type.resolve(valMod.getKey());
				if (lang==null) {
					Logging.logger.log(Level.WARNING, "Found unknown skill '"+valMod.getKey()+"' in valuemod of "+mod);
					return "Unknown language '"+valMod.getKey()+"'";
				}
				return "Sprache "+lang.getName();
			case POWER:
				if (valMod.getConnectedChoice()!=null) {
					return "???"+valMod.getConnectedChoice()+"???";
				}
				Power power = SplitterMondCore.getItem(Power.class, valMod.getKey());
				if (power==null) {
					return "Unbekannte Kraft '"+valMod.getKey()+"'";
				}
				return power.getName(Locale.getDefault());
			}
			return "ToDo: "+type;
		}

		Logging.logger.log(Level.ERROR, "Missing string conversion for "+mod.getClass());
		return mod.toString();
	}

	//-------------------------------------------------------------------
	public static String getModificationString(ComplexDataItem data, Modification mod, Locale loc) {
		SplittermondReference type = (SplittermondReference) mod.getReferenceType();
		if (mod instanceof ValueModification) {
			ValueModification valMod = (ValueModification)mod;
			if ("CHOICE".equals(valMod.getKey())) {
				Choice choice = data.getChoice(valMod.getConnectedChoice());
				if (choice!=null)
					return getChoiceString(data, choice)+" +"+valMod.getValue();
				return "Unknown choice "+valMod.getConnectedChoice();
			}

			String what = type.name();
			switch (type) {
			case ATTRIBUTE:
				if (valMod.getValue()>0) {
					return Attribute.valueOf(valMod.getKey()).getName()+" +"+valMod.getValue();
				} else {
					return Attribute.valueOf(valMod.getKey()).getName()+" "+valMod.getValue();
				}
			case RESOURCE:
			case POWER:
				DataItem item = type.resolve(valMod.getKey());
				if (valMod.getValue()>0) {
					return item.getName()+" +"+valMod.getValue();
				} else {
					return item.getName()+" "+valMod.getValue();
				}
			case SKILL:
				SMSkill skill = SplitterMondCore.getSkill(valMod.getKey());
				if (skill==null) {
					Logging.logger.log(Level.WARNING, "Found unknown skill '"+valMod.getKey()+"' in valuemod of "+data);
					return "Unknown skill '"+valMod.getKey()+"'";
				}
				if (valMod.getValue()>0) {
					return skill.getName()+" +"+valMod.getValue();
				} else {
					return skill.getName()+" "+valMod.getValue();
				}
			default:
				return "Unknown value type(1) "+type;
			}
		}

		else if (mod instanceof DataItemModification) {
			DataItemModification valMod = (DataItemModification)mod;
			String what = type.name();
			if ("CHOICE".equals(valMod.getKey())) {
				Choice choice = data.getChoice(valMod.getConnectedChoice());
				if (choice!=null)
					return getChoiceString(data, choice);
				return "<CHOICE>";
			}
			DataItem resolved = type.resolve(valMod.getKey());
			switch (type) {
			case CULTURE_LORE:
				if (valMod.getConnectedChoice()!=null) {
					Choice choice = data.getChoice(valMod.getConnectedChoice());
					if (choice==null) {
						return "Unknown choice "+valMod.getConnectedChoice();
					}
					return "???"+choice.getChooseFrom()+"???";
				}

				CultureLore cultlore = type.resolve(valMod.getKey());
				if (cultlore==null) {
					Logging.logger.log(Level.WARNING, "Found unknown cultlore '"+valMod.getKey()+"' in valuemod of "+data);
					return "Unknown cultlore '"+valMod.getKey()+"'";
				}
				return "Kulturkunde "+cultlore.getName();
			case LANGUAGE:
				if (valMod.getConnectedChoice()!=null) {
					Choice choice = data.getChoice(valMod.getConnectedChoice());
					if (choice==null) {
						return "Unknown choice "+valMod.getConnectedChoice();
					}
					return "???"+choice.getChooseFrom()+"???";
				}

				Language lang = type.resolve(valMod.getKey());
				if (lang==null) {
					Logging.logger.log(Level.WARNING, "Found unknown skill '"+valMod.getKey()+"' in valuemod of "+data);
					return "Unknown language '"+valMod.getKey()+"'";
				}
				return "Sprache "+lang.getName();
			case MASTERSHIP:
				return "Meisterschaft "+resolved.getName(loc);
			case POWER:
				if (valMod.getConnectedChoice()!=null) {
					Choice choice = data.getChoice(valMod.getConnectedChoice());
					if (choice==null) {
						return "Unknown choice "+valMod.getConnectedChoice();
					}
					return getChoiceString(data, choice);
				}
				Power power = SplitterMondCore.getItem(Power.class, valMod.getKey());
				if (power==null) {
					return "Unbekannte Kraft '"+valMod.getKey()+"'";
				}
				return power.getName(Locale.getDefault());
			case SKILL_SPECIAL:
				return "Spezialisierung "+resolved.getName(loc);
			case CULTURE:
				return "Kultur "+resolved.getName(loc);
			}
			Logging.logger.log(Level.ERROR, "Missing string conversion for {0}: {1}",type,resolved);
			System.err.println("SplitterTools: Missing string conversion for "+type);
			return "ToDo: "+type;
		}

		if (mod instanceof ModificationChoice) {
			ModificationChoice selMod = (ModificationChoice)mod;
			List<String> options = new ArrayList<String>();
			for (Modification tmp : selMod.getModificiations()) {
				options.add( getModificationString(data, tmp, loc));
			}
			return String.join(" oder ", options);
		}

		Logging.logger.log(Level.ERROR, "Missing string conversion for "+mod.getClass());
		System.err.println("SplitterTools: Missing string conversion for "+mod.getClass());
		return mod.toString();
	}

	//-------------------------------------------------------------------
	public static Function<Modification, String> modificationResolver(Locale loc) {
		return (m) -> getModificationString(m, loc);
	}

	//-------------------------------------------------------------------
	public static Function<Requirement, String> requirementResolver(Locale loc) {
		return (r) -> getRequirementString(r, loc);
	}

	//-------------------------------------------------------------------
	public static String getRequirementString(Requirement req, Locale loc) {
		if (req instanceof ValueRequirement) {
			ValueRequirement tmp = (ValueRequirement)req;
			try {
				switch ((SplittermondReference)tmp.getType()) {
				case ATTRIBUTE:
					Attribute attr = Attribute.valueOf(tmp.getKey());
					return attr.getName(loc)+" "+tmp.getValue();
				default:
					Logging.logger.log(Level.ERROR, "Missing string conversion for "+req.getClass()+" type "+tmp.getType());
				}
			} catch (NoSuchElementException e) {
				throw new ReferenceException((SplittermondReference)tmp.getType(), tmp.getKey());
			}
		}
		if (req instanceof ExistenceRequirement) {
			ExistenceRequirement tmp = (ExistenceRequirement)req;
			try {
				switch ((SplittermondReference)tmp.getType()) {
				case MASTERSHIP:
					Mastership master =  SplitterMondCore.getItem(Mastership.class, tmp.getKey());
					if (master==null) {
						return "Unknown "+SplitterMondCore.getI18nResources().getString("label.mastership")+" "+tmp.getKey();
					}
					return master.getName(loc);
				case RACE:
					Race race = SplitterMondCore.getItem(Race.class, tmp.getKey());
					if (race==null) {
						return "Unknown "+SplitterMondCore.getI18nResources().getString("label.mastership")+" "+tmp.getKey();
					}
					return race.getName(loc);
				case SKILL_SPECIAL:
					SMSkillSpecialization special = null;
					StringTokenizer tok = new StringTokenizer(tmp.getKey(), "/- ");
					String skillID = tok.nextToken();
					String masterID = tok.nextToken();
					SMSkill skill = SplitterMondCore.getSkill(skillID);
					if (skill!=null) {
						special = skill.getSpecialization(masterID);
					}
					if (special==null) {
						return "Unknown "+SplitterMondCore.getI18nResources().getString("label.skillspecialization")+" "+tmp.getKey();
					}
					return special.getName(loc);
				default:
					Logging.logger.log(Level.ERROR, "Missing string conversion for "+req.getClass()+" type "+tmp.getType());			}
			} catch (NoSuchElementException e) {
				throw new ReferenceException((SplittermondReference)tmp.getType(), tmp.getKey());
			}
			return req.toString();
		}


		Logging.logger.log(Level.ERROR, "Missing string conversion for "+req.getClass());
		return req.toString();
	}

//	//-------------------------------------------------------------------
//	private static void sort(List<Datable> rewardsAndMods) {
//		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
//			public int compare(Datable o1, Datable o2) {
//				Long time1 = 0L;
//				Long time2 = 0L;
//				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
//				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();
//
//				int cmp = time1.compareTo(time2);
//				if (cmp==0) {
//					if (o1 instanceof RewardImpl && o2 instanceof ModificationImpl) return -1;
//					if (o1 instanceof ModificationImpl && o2 instanceof RewardImpl) return  1;
//				}
//				return cmp;
//			}
//		});
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param aggregate Aggregate history elements with same adventure
//	 */
//	public static List<HistoryElement> convertToHistoryElementList(SpliMoCharacter charac, boolean aggregate) {
//		// Initial reward
//		Logging.logger.log(Level.WARNING, "Sort "+charac.getRewards().size()+" rewards  and "+charac.getHistory().size()+" mods");
//
//		/*
//		 * Build a merged list of rewards and modifications and sort it by time
//		 */
//		List<Datable> rewardsAndMods = new ArrayList<Datable>();
//		rewardsAndMods.addAll(charac.getRewards());
//		for (Modification mod : charac.getHistory()) {
//			rewardsAndMods.add(mod.clone());
//		}
////		rewardsAndMods.addAll(charac.getHistory());
//		sort(rewardsAndMods);
//
//		/*
//		 * Does the creation reward need to be added?
//		 */
//		if (rewardsAndMods.isEmpty() || !(rewardsAndMods.get(0) instanceof Reward)) {
//			RewardImpl creation = new RewardImpl(15, CORE.getString("label.reward.creation"));
//			rewardsAndMods.add(creation);
//			sort(rewardsAndMods);
//		}
//
//		/*
//		 * Now build a list of HistoryElements. Start a new H
//		 */
//		List<HistoryElement> ret = new ArrayList<HistoryElement>();
//		HistoryElementImpl current = null;
//		ProductService sessServ = null;
//		try {
//			sessServ = ProductServiceLoader.getInstance();
//		} catch (Exception e) {
//			Logging.logger.log(Level.ERROR, "Failed loading session service",e);
//		}
//
//		for (Datable item : rewardsAndMods) {
//			if (item instanceof RewardImpl) {
//				RewardImpl reward = (RewardImpl)item;
//				Adventure adv = null;
//				System.err.println("Reward "+reward+" / "+reward.getTitle());
//				if (reward.getId()!=null) {
//					if (sessServ!=null)
//						adv = sessServ.getAdventure(RoleplayingSystem.SPLITTERMOND, reward.getId());
//					if (adv==null) {
//						Logging.logger.log(Level.WARNING, "Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
//					}
//				}
//				// If is same adventure as current, keep same history element
//				if (!aggregate || !(adv!=null && current!=null && adv.getId().equals(current.getAdventureID())) ) {
//					current = new HistoryElementImpl();
//					current.setName(reward.getTitle());
//					if (adv!=null) {
//						current.setName(adv.getTitle());
//						current.setAdventure(adv);
//					}
//					ret.add(current);
//				}
//				current.addGained(reward);
//			} else if (item instanceof ModificationImpl) {
//				if (current==null) {
//					Logging.logger.log(Level.ERROR, "Failed preparing history: Exp spent on modification without previous reward");
//				} else {
//					Modification lastMod = (current.getSpent().isEmpty())?null:current.getSpent().get(current.getSpent().size()-1);
//					if (lastMod!=null && lastMod.getClass()==item.getClass()) {
//						if (item instanceof SkillModification) {
////							Logging.logger.log(Level.DEBUG, "Combine "+lastMod+" with "+item);
//							// Aggregate same skill
//							SkillModification lastSMod = (SkillModification)lastMod;
//							SkillModification newtMod = (SkillModification)item;
//							if (lastSMod.getSkill()==newtMod.getSkill()) {
//								// Same skill
//								lastSMod.setValue(newtMod.getValue());
//								lastSMod.setExpCost(lastSMod.getExpCost() + newtMod.getExpCost());
//							} else {
//								// Different skill
//								current.addSpent((ModificationImpl) item);
//							}
//						} else {
//							current.addSpent((ModificationImpl) item);
//						}
//					} else {
//						current.addSpent((ModificationImpl) item);
//					}
//				}
//			}
//		}
//
//
//		Logging.logger.log(Level.WARNING, "  return "+ret.size()+" elements");
//		return ret;
//	}


//	//-------------------------------------------------------------------
//	/**
//	 * Convert spell cost to a printable string. Also takes into account
//	 * modifications to the spell cost by spell school masterships.
//	 */
//	public static String getModifiedFocusString(SpliMoCharacter model, SpellValue spellVal) {
//		Spell spell = spellVal.getSpell();
//		SpellCost cost = spell.getCost();
//
//		int channelled = cost.getChannelled(); // Kanalisiert
//		int exhausted  = cost.getExhausted();  // Erschöpft
//		int consumed   = cost.getConsumed();   // Verzehrt
//
//		/*
//		 * Modifications
//		 */
//		List<String> modifications = new ArrayList<>();
//		for (Mastership master : spellVal.getSkill().getMasterships()) {
//			if (model.hasMastership(master))
//				modifications.add(master.getKey());
//		}
////		Logging.logger.log(Level.WARNING, "***Spell "+spell.getName()+" mods="+modifications);
//		// Check influential modifications
//		if (modifications.contains("savingcaster")) {
//			exhausted  = (exhausted >1)?(exhausted -1):exhausted;
//			channelled = (channelled>1)?(channelled-1):channelled;
//		}
//		if (modifications.contains("wardinghand")) {  // Bannende Hand
////			Logging.logger.log(Level.WARNING, "*** warding = "+spell.getTypes());
//			if (spell.getTypes().contains(SpellType.BREAKSPELL))
//				exhausted   = Math.max(1, exhausted-2);
//		}
//		if (modifications.contains("effectivemotion")) { // Effektive Bewegung
////			Logging.logger.log(Level.WARNING, "*** effectivemotion = "+spell.getTypes());
//			if (spell.getTypes().contains(SpellType.ENHANCE_MOTION))
//				channelled   = Math.max(1, channelled-2);
//		}
//
//		String chan = SplitterMondCore.getI18nResources().getString("spell.cost.channelled.short");
//		String cons = SplitterMondCore.getI18nResources().getString("spell.cost.consumed.short");
//		StringBuffer buf = new StringBuffer();
//		if (channelled>0)
//			buf.append(chan+channelled);
//		else
//			buf.append(String.valueOf(exhausted));
//
//		if (consumed>0)
//			buf.append(cons+consumed);
//
//		return buf.toString();
//
//	}

	//-------------------------------------------------------------------
	/**
	 * Convert spell cost to a printable string.
	 */
	public static String getFocusString(SpellCost cost) {
		int channelled = cost.getChannelled(); // Kanalisiert
		int exhausted  = cost.getExhausted();  // Erschöpft
		int consumed   = cost.getConsumed();   // Verzehrt

		String chan = SplitterMondCore.getI18nResources().getString("spell.cost.channelled.short");
		String cons = SplitterMondCore.getI18nResources().getString("spell.cost.consumed.short");
		StringBuffer buf = new StringBuffer();
		if (channelled>0)
			buf.append(chan+channelled);
		else
			buf.append(String.valueOf(exhausted));

		if (consumed>0)
			buf.append(cons+consumed);

		return buf.toString();

	}

	// -------------------------------------------------------------------
	public static List<DataItem> getInfluences(Modifyable val) {
		List<DataItem> ret = new ArrayList<>();
		for (Modification mod : val.getIncomingModifications()) {
			if (mod.isConditional() || mod instanceof CheckModification) {
				if (mod.getSource() == null) {
					System.err.println("SplitterTools.getInfluences: No source for Modification " + mod);
				} else if (!(mod.getSource() instanceof DataItem)) {
					System.err.println("SplitterTools.getInfluences: Source of SkillModification " + mod
							+ " is of type " + mod.getSource().getClass());
				} else {
					ret.add((DataItem) mod.getSource());
				}
			}
		}
		return ret;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * Iterate through enhancements. Get their modifications. Apply those that
//	 * refer to the item itself. Return those that needs to be applied to the
//	 * character
//	 */
//	public static void applyToCharacter(SpliMoCharacter model, List<Modification> modifications, Skill skill, SkillSpecialization spec) {
//		Logging.logger.log(Level.DEBUG, "Apply "+modifications.size()+" modifications to character");
//		for (Modification mod : modifications) {
//			if (mod instanceof AttributeModification) {
//				AttributeModification data = (AttributeModification)mod;
//				model.getAttribute(data.getAttribute()).addModification(mod);
//				Logging.logger.log(Level.DEBUG, "Added "+data.getAttribute()+" +"+data.getValue()+"  ... result now "+model.getAttribute(data.getAttribute()));
//			} else if (mod instanceof SkillModification) {
//				SkillModification data = (SkillModification)mod;
//				model.getSkillValue(data.getSkill()).addModification(mod);
//				Logging.logger.log(Level.DEBUG, "Added "+data.getSkill()+" "+data.getValue());
//			} else if (mod instanceof MastershipModification) {
//				MastershipModification data = (MastershipModification)mod;
//				if (data.getSpecialization()!=null) {
//					SkillSpecializationValue special = data.getSpecialization();
//					model.getSkillValue(data.getSkill()).addModification(mod);
//					Logging.logger.log(Level.DEBUG, "Added "+data.getSkill()+" "+special.getLevel());
//				} else if (data.getMastership()==null) {
//					// No specialization, no mastership - must be associated with the skill of the item itself
//					Logging.logger.log(Level.WARNING, "Character has an invalid saved Mastership modification for skill "+skill+". Try to fix it");
//					data.setSpecialization(new SkillSpecializationValue(spec, data.getLevel()));
//					try {
//						Logging.logger.log(Level.WARNING, "Added specializatation  "+model.getSkillValue(skill));
//					} catch (NullPointerException e) {
//						Logging.logger.log(Level.ERROR, "Character has an invalid saved Mastership modification, that could bot be fixed",e);
//					}
//				} else {
//					Logging.logger.log(Level.WARNING, "Don't know how to apply "+data);
//					throw new RuntimeException("Trace");
//				}
//			} else if (mod instanceof ItemModification) {
//				// Can be ignored here
//			} else if (mod instanceof AttributeChangeModification) {
//				// Can be ignored here
//			} else
//				Logging.logger.log(Level.WARNING, "Don't know how to apply to character: "+mod.getClass());
//		}
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Iterate through enhancements. Get their modifications. Apply those that
//	 * refer to the item itself. Return those that needs to be applied to the
//	 * character
//	 */
//	public static void unapplyToCharacter(SpliMoCharacter model, List<Modification> modifications) {
//		Logging.logger.log(Level.DEBUG, "unapply "+modifications.size()+" modifications from character");
//		for (Modification mod : modifications) {
//			if (mod instanceof AttributeModification) {
//				AttributeModification data = (AttributeModification)mod;
//				model.getAttribute(data.getAttribute()).removeModification(mod);
//				Logging.logger.log(Level.DEBUG, "Removed "+data.getAttribute()+" +"+data.getValue());
//			} else if (mod instanceof SkillModification) {
//				SkillModification data = (SkillModification)mod;
//				model.getSkillValue(data.getSkill()).removeModification(mod);
//				Logging.logger.log(Level.DEBUG, "Removed "+data.getSkill()+" "+data.getValue());
//			} else
//				Logging.logger.log(Level.WARNING, "Don't know how to apply to character: "+mod.getClass());
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public static void convertFromV1Chars(CarriedItem item) {
//		Logging.logger.log(Level.DEBUG, "Update V1 item modifications to enhancements for "+item.getName());
//		/*
//		 * Apply Genesis 1.x Magic effects and convert them to enhancements
//		 */
//		Logging.logger.log(Level.DEBUG, "  Mods of "+item+" = "+item.getModifications());
//		for (Modification mod : new ArrayList<Modification>(item.getModifications())) {
//			if (mod instanceof AttributeModification) {
//				AttributeModification aMod = (AttributeModification)mod;
//				Logging.logger.log(Level.WARNING, "Found an attribute modification "+aMod+" -> convert to enhancement");
//				Attribute attrib = aMod.getAttribute();
//				if (attrib==null) {
//					Logging.logger.log(Level.ERROR, String.format("Character has item %s with invalid attribute modification", item.getItem().getID()));
//					continue;
//				}
//				// Convert to enhancement
//				EnhancementReference enh = null;
//				switch (aMod.getAttribute()) {
//				case MINDRESIST:
//					enh = new EnhancementReference(SplitterMondCore.getEnhancement("mindresist"));
//					Logging.logger.log(Level.INFO, "Convert "+aMod+" to "+enh.getName());
//					break;
//				case BODYRESIST:
//					enh = new EnhancementReference(SplitterMondCore.getEnhancement("bodyresist"));
//					Logging.logger.log(Level.INFO, "Convert "+aMod+" to "+enh.getName());
//					break;
//				case CHARISMA:	case AGILITY:  case INTUITION: case CONSTITUTION:
//				case MYSTIC  :  case STRENGTH: case MIND:  case WILLPOWER:
//					enh = new EnhancementReference(SplitterMondCore.getEnhancement("raiseattrib"));
//					Logging.logger.log(Level.INFO, "Convert "+aMod+" to "+enh.getName());
//					break;
//				default:
//					Logging.logger.log(Level.ERROR, "Cannot convert attribute modification "+aMod+" to enhancement");
//					continue;
//				}
//				item.addEnhancement(enh);
//				item.removeModification(mod);
//			}
//		}
//
//	}
//
//	//-------------------------------------------------------------------
//	public static void reward(SpliMoCharacter charac, RewardImpl reward) {
//		Logging.logger.log(Level.INFO, "Add reward "+reward+" to "+charac);
//		charac.addReward(reward);
//
//		charac.setExperienceFree(charac.getExperienceFree() + reward.getExperiencePoints());
//		for (Modification mod : reward.getModifications()) {
//			if (mod instanceof ResourceModification) {
//				Resource res = ((ResourceModification)mod).getResource();
//				int      val = ((ResourceModification)mod).getValue();
//				String title = ((ResourceModification)mod).getResourceName();
//
//				if (res.isBaseResource()) {
//					for (ResourceReference ref : charac.getResources()) {
//						if (ref.getResource()==res) {
//							ref.setValue(ref.getValue() + val);
//							Logging.logger.log(Level.INFO, "Reward existing resource to "+ref);
//							ref.setDescription(title);
//							break;
//						}
//					}
//				} else {
//					ResourceReference ref = new ResourceReference(res, val);
//					ref.setDescription(title);
//					Logging.logger.log(Level.INFO, "Reward new resource "+ref);
//					charac.addResource(ref);
//				}
//				// Add to history
//				charac.addToHistory(mod);
//			} else if (mod instanceof MoneyModification) {
//				int value = ((MoneyModification)mod).getTelare();
//				Logging.logger.log(Level.DEBUG, "Add "+value+" telare");
//				charac.setTelare(charac.getTelare() + value);
//			} else {
//				Logging.logger.log(Level.ERROR, "Unsupported modification: "+mod.getClass());
//			}
//		}
//
//	}
//
//	//-------------------------------------------------------------------
//	public static void calculateDerivedAttribute(SpliMoCharacter model) {
//		model.setAttribute(Attribute.SPEED     , model.getAttribute(Attribute.SIZE).getValue() + model.getAttribute(Attribute.AGILITY).getValue());
//		model.setAttribute(Attribute.INITIATIVE,                  10 - model.getAttribute(Attribute.INTUITION).getValue());
//		model.setAttribute(Attribute.LIFE      , model.getAttribute(Attribute.SIZE).getValue() + model.getAttribute(Attribute.CONSTITUTION).getValue());
//		model.setAttribute(Attribute.FOCUS     , 2*(model.getAttribute(Attribute.MYSTIC).getValue() + model.getAttribute(Attribute.WILLPOWER).getValue()) );
//		model.setAttribute(Attribute.DEFENSE   , 12 + model.getAttribute(Attribute.AGILITY).getValue() + model.getAttribute(Attribute.STRENGTH).getValue());
//		model.setAttribute(Attribute.MINDRESIST, 12 + model.getAttribute(Attribute.MIND).getValue() + model.getAttribute(Attribute.WILLPOWER).getValue());
//		model.setAttribute(Attribute.BODYRESIST, 12 + model.getAttribute(Attribute.CONSTITUTION).getValue() + model.getAttribute(Attribute.WILLPOWER).getValue());
//	}
//
//	//-------------------------------------------------------------------
//	private static void setMaxBonusCap(SpliMoCharacter data, int cap) {
//		Logging.logger.log(Level.INFO, "Set cap for equipment and magic bonus to "+cap);
//
//		// Apply to derived attribute
//		for (Attribute key : Attribute.secondaryValues()) {
//			data.getAttribute(key).setModifierCap(cap);
//		}
//
//		// Apply to skills
//		for (Skill key : SplitterMondCore.getSkills()) {
//			data.getSkillValue(key).setModifierCap(cap);
//		}
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Apply those modifications not saved in the character - e.g. the
//	 * resistance boni depending on level
//	 */
//	public static void applyAfterLoadModifications(SpliMoCharacter data) {
//		Logging.logger.log(Level.INFO, "calculate modifications by level, size, race, powers, etc.");
//
//		/*
//		 * V1 versions saved the size as an attribute. Since V2 it is derived from
//		 * the raze
//		 */
//		data.getAttribute(Attribute.SIZE).setDistributed(0);
//		data.getAttribute(Attribute.SPLINTER).setDistributed(3);
//
//		int cap = 3;
//
//		/*
//		 * Calculate level and add resistances depending on it
//		 */
//        double factor = Preferences.userRoot().node("/de/rpgframework/plugins/splittermond").getDouble("exp_factor", 1.0);
//		// Level 2
//		if (data.getExperienceInvested()>=(int)(100*factor)) {
//			data.setLevel(2);
//			data.getAttribute(Attribute.SPLINTER  ).addModification( new AttributeModification(Attribute.SPLINTER  , 1, SplitterTools.LEVEL2) );
//			data.getAttribute(Attribute.BODYRESIST).addModification( new AttributeModification(Attribute.BODYRESIST, 2, SplitterTools.LEVEL2) );
//			data.getAttribute(Attribute.MINDRESIST).addModification( new AttributeModification(Attribute.MINDRESIST, 2, SplitterTools.LEVEL2) );
//			data.getAttribute(Attribute.DEFENSE   ).addModification( new AttributeModification(Attribute.DEFENSE   , 2, SplitterTools.LEVEL2) );
//			cap = 4;
//		}
//		// Level 3
//		if (data.getExperienceInvested()>=(int)(300*factor)) {
//			data.setLevel(3);
//			data.getAttribute(Attribute.SPLINTER  ).addModification( new AttributeModification(Attribute.SPLINTER  , 1, SplitterTools.LEVEL3) );
//			data.getAttribute(Attribute.BODYRESIST).addModification( new AttributeModification(Attribute.BODYRESIST, 2, SplitterTools.LEVEL3) );
//			data.getAttribute(Attribute.MINDRESIST).addModification( new AttributeModification(Attribute.MINDRESIST, 2, SplitterTools.LEVEL3) );
//			data.getAttribute(Attribute.DEFENSE   ).addModification( new AttributeModification(Attribute.DEFENSE   , 2, SplitterTools.LEVEL3) );
//			cap = 5;
//		}
//		// Level 4
//		if (data.getExperienceInvested()>=(int)(600*factor)) {
//			data.setLevel(4);
//			data.getAttribute(Attribute.SPLINTER  ).addModification( new AttributeModification(Attribute.SPLINTER  , 1, SplitterTools.LEVEL4) );
//			data.getAttribute(Attribute.BODYRESIST).addModification( new AttributeModification(Attribute.BODYRESIST, 2, SplitterTools.LEVEL4) );
//			data.getAttribute(Attribute.MINDRESIST).addModification( new AttributeModification(Attribute.MINDRESIST, 2, SplitterTools.LEVEL4) );
//			data.getAttribute(Attribute.DEFENSE   ).addModification( new AttributeModification(Attribute.DEFENSE   , 2, SplitterTools.LEVEL4) );
//			cap = 6;
//		}
//
//		/*
//		 * Has character power that raises cap
//		 */
//		if (data.getPower(SplitterMondCore.getPower("onestepahead"))!=null)
//			cap++;
//		// Set cap
//		setMaxBonusCap(data, cap);
//
//		/*
//		 * Size depending on race
//		 */
//		for (Modification mod : data.getRace().getModifications()) {
//			if (mod instanceof AttributeModification)  {
//				AttributeModification aMod = (AttributeModification)mod;
//				if (aMod.getAttribute()==Attribute.SIZE)
//					data.getAttribute(Attribute.SIZE).addModification( new AttributeModification(Attribute.SIZE, aMod.getValue(), data.getRace()) );
//			}
//		}
//
//		/*
//		 * Powers
//		 */
//		for (PowerReference powerRef : data.getPowers()) {
//			Logging.logger.log(Level.INFO, "PowerRef "+powerRef);
//			Power power = powerRef.getPower();
//			for (Modification mod : power.getModifications()) {
//				mod.setSource(power);
//				if (mod instanceof AttributeModification) {
//					AttributeModification aMod = (AttributeModification)mod;
//					AttributeModification cloned = aMod.clone();
//					AttributeValue aVal = data.getAttribute(aMod.getAttribute());
//					cloned.setValue(powerRef.getCount()*aMod.getValue());
//					cloned.setSource(powerRef);
//					Logging.logger.log(Level.INFO, "Add "+cloned+"  (already exist = "+aVal.getModifications()+")");
//					aVal.addModification(cloned);
//					powerRef.getModifications().add(cloned);
//				} else if (mod instanceof SkillModification) {
//					SkillModification sMod = (SkillModification)mod;
//					SkillModification cloned = sMod.clone();
//					SkillValue sVal = data.getSkillValue(sMod.getSkill());
//					cloned.setValue(powerRef.getCount()*sMod.getValue());
//					cloned.setSource(powerRef);
//					Logging.logger.log(Level.INFO, "Add "+cloned+"  (already exist = "+sVal.getModifications()+")");
//					sVal.addModification(cloned);
//					Logging.logger.log(Level.INFO, "... sval="+sVal);
//					powerRef.getModifications().add(cloned);
//				} else {
//					Logging.logger.log(Level.WARNING, "Unsupported modification "+mod+" in power "+power);
//				}
//			}
//		}
//
//		/*
//		 * Masterships
//		 */
//		for (Skill skill : SplitterMondCore.getSkills()) {
//			SkillValue sVal = data.getSkillValue(skill);
//			for (MastershipReference ref : sVal.getMasterships()) {
//				// Depending on masterships or skill specialization
//				if (ref.getMastership()!=null) {
//					for (Modification mod : ref.getMastership().getModifications()) {
//						mod.setSource(ref.getMastership());
//						if (mod instanceof AttributeModification) {
//							AttributeModification aMod = (AttributeModification)mod;
//							AttributeValue aVal = data.getAttribute(aMod.getAttribute());
//							aMod.setValue(aMod.getValue());
//							aMod.setSource(ref.getMastership());
//							Logging.logger.log(Level.INFO, "Add "+aMod+"  (already exist = "+aVal.getModifications()+")");
//							aVal.addModification(aMod);
//						} else if (mod instanceof AttitudeModification) {
//						} else if (mod instanceof SkillModification) {
//							SkillModification sMod = (SkillModification)mod;
//							if (sMod.getSource()==null)
//								sMod.setSource(ref.getMastership());
//							SkillValue sVal2 = data.getSkillValue(sMod.getSkill());
//							Logging.logger.log(Level.INFO, "Add "+sMod+"  (already exist = "+sVal2.getModifications()+")");
//							sVal2.addModification(sMod);
//						} else {
//							Logging.logger.log(Level.WARNING, "Unsupported modification "+mod+"//"+mod.getClass()+" in mastership "+ref.getMastership());
//						}
//					}
//				} else if (ref.getSpecialization()!=null) {
//					// Specializations are taken into account only in specific situations
//				}
//			}
//		}
//
//
//		/*
//		 * Modifications depending on size
//		 */
//		int diff = 5 - data.getAttribute(Attribute.SIZE).getValue();
//		if (diff!=0) {
//			data.getAttribute(Attribute.DEFENSE).addModification( new AttributeModification(Attribute.DEFENSE, diff*2, Attribute.SIZE) );
//			data.getSkillValue(SplitterMondCore.getSkill("stealth")).addModification(new SkillModification("stealth", diff, Attribute.SIZE));
//		}
//
//
//		/*
//		 * Calculate derived attributes
//		 */
//		calculateDerivedAttribute(data);
//
//		/*
//		 * Mark items that are relics
//		 */
//		for (ResourceReference ref : data.getResources()) {
//			if (!ref.getResource().getId().equals("relic"))
//				continue;
//			UUID uniqueID = ref.getIdReference();
//			CarriedItem item = (CarriedItem)data.getUniqueObject(uniqueID);
//			if (item==null) {
//				Logging.logger.log(Level.WARNING, "Resource "+ref+" refers to an unknown unique ID");
//			} else {
//				item.setResource(ref);
//				Logging.logger.log(Level.DEBUG, "Mark "+item+" as an relic");
//			}
//		}
//
//		/*
//		 * Mark creatures that are resources
//		 */
//		for (ResourceReference ref : data.getResources()) {
//			if (!ref.getResource().getId().equals("creature"))
//				continue;
//			UUID uniqueID = ref.getIdReference();
//			CreatureReference item = (CreatureReference)data.getUniqueObject(uniqueID);
//			if (item==null) {
//				Logging.logger.log(Level.WARNING, "Resource "+ref+" refers to an unknown unique ID");
//			} else {
//				item.setResource(ref);
//				Logging.logger.log(Level.DEBUG, "Mark "+item+" as an creature");
//			}
//		}
//
//		/*
//		 * Calculate ModuleBasedCreatures
//		 */
//		for (CreatureReference ref : data.getCreatures()) {
//			if (ref.getModuleBasedCreature()!=null) {
//				Logging.logger.log(Level.INFO, "  calculate creature "+ref.getName()+" from modules");
//				try {
//					// Create modifications
//					Logging.logger.log(Level.DEBUG, "    set role modifications: "+ref.getModuleBasedCreature().getRole().getModule().getModifications());
//					ref.getModuleBasedCreature().getRole().setModifications(ref.getModuleBasedCreature().getRole().getModule().getModifications());
//					// Fix originModule
//					Logging.logger.log(Level.DEBUG, "    set role choices: "+ref.getModuleBasedCreature().getRole().getChoices());
//					for (NecessaryChoice choice : ref.getModuleBasedCreature().getRole().getChoices()) {
//						choice.originModule = ref.getModuleBasedCreature().getRole();
//						// Replace original modifications from role with those from choices
//						if (choice.getMadeChoice()!=null) {
//							Logging.logger.log(Level.DEBUG, "    replace option "+choice.getOriginChoice()+" with choice "+choice.getMadeChoice());
//							boolean couldNotReplace = true;
//							for (Modification mod : new ArrayList<Modification>(ref.getModuleBasedCreature().getRole().getModifications())) {
//								if (mod.equals(choice.getOriginChoice())) {
//									Logging.logger.log(Level.INFO, "    Creature "+ref.getName()+", Role "+ref.getModuleBasedCreature().getRole().getModule().getId()+": replace option "+choice.getOriginChoice()+" with choice '"+choice.getMadeChoice()+"'");
//									ref.getModuleBasedCreature().getRole().removeModification(mod);
//									ref.getModuleBasedCreature().getRole().addModification(choice.getMadeChoice());
//									couldNotReplace = false;
//									break;
//								}
//							}
//							if (couldNotReplace) {
//								Logging.logger.log(Level.ERROR, "Creature "+ref.getName()+", Role "+ref.getModuleBasedCreature().getRole().getModule().getId()+": Could not find option "+choice.getOriginChoice()+" to replace decision with");
//							}
//						}
//					}
//					Logging.logger.log(Level.DEBUG, "    process options");
//				for (CreatureModuleReference origin : ref.getModuleBasedCreature().getOptions()) {
//					Logging.logger.log(Level.DEBUG, "* Option "+origin.getModule().getId());
//					for (Modification mod : origin.getModule().getModifications()) {
//						CreatureTools.instantiateModification(ref.getModuleBasedCreature(), origin, mod);
//					}
//
//					Logging.logger.log(Level.INFO, "  START: Calculate values for "+ref);
//					CreatureTools.calculateModuleBasedCreature(ref.getModuleBasedCreature());
//					Logging.logger.log(Level.INFO, "  STOP : Calculate values for "+ref);
////				System.exit(0);
//				}
//				} catch (Exception e) {
//					Logging.logger.log(Level.ERROR, "Error processing modifications of creature "+ref,e);
//					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Fehler beim Berechnen der Daten für die Kreatur "+ref.getName());
//				}
//			}
//			CreatureTools.calculateTrainings(ref);
//		}
//
//		for (CarriedItem carriedItem : data.getItems()) {
//			if (carriedItem.getItem()==null) {
//				Logging.logger.log(Level.WARNING, "Found a carried item with unknown referenced item: "+carriedItem.getName());
//				data.removeItem(carriedItem);
//				continue;
//			}
//			if (carriedItem.getName().equals(carriedItem.getItem().getName())){
//				carriedItem.setCustomName(null);
//			}
//		}
//	}

	//-------------------------------------------------------------------
	/**
	 * @param total Value measured in Silver(Telare)
	 * @return
	 */
	public static String telareAsCurrencyString(int total) {
		return MoneyConverter.toString(total);
	}

	//-------------------------------------------------------------------
	/**
	 * Since distributed points may not be saved in the creature xml,
	 * calculate them from value minus attributes
	 */
	public static void fixCreatureSkillPoints(Creature model) {
		for (SMSkillValue val : model.getSkillValues()) {
			if (val.getSkill().getType()==SkillType.COMBAT)
				continue;
			SMSkill skill = val.getSkill();
			int attrVal = model.getAttribute(skill.getAttribute()).getModifiedValue() + model.getAttribute(skill.getAttribute2()).getModifiedValue();
			int points  = val.getValue() - attrVal;
			val.setValue(points);
		}
	}


//	//-------------------------------------------------------------------
//	public static int getModifiedWeaponSpeed(SpliMoCharacter model, CarriedItem item) {
//		int speed = 0;
//		if (item.isType(ItemType.WEAPON))
//			speed = item.getSpeed(ItemType.WEAPON);
//		else if (item.isType(ItemType.LONG_RANGE_WEAPON))
//			speed = item.getSpeed(ItemType.LONG_RANGE_WEAPON);
//
//		speed += getTickMalusSum(model, true);
//		return speed;
//	}
//
//	//-------------------------------------------------------------------
//	public static int getDefenseSum(SpliMoCharacter character, boolean inclusiveShield) {
//		return getSumForArmorAttribute(character, ArmorAttributeType.DEFENSE, inclusiveShield);
//	}
//
//	//-------------------------------------------------------------------
//	public static int getDamageReductionSum(SpliMoCharacter character) {
//		int base = character.getAttribute(Attribute.DAMAGE_REDUCTION).getValue();
//		return  base + getSumForArmorAttribute(character, ArmorAttributeType.DAMAGE_REDUCTION, false);
//	}
//
//	//-------------------------------------------------------------------
//	public static int getHandicapSum(SpliMoCharacter character, boolean inclusiveShield) {
//		return   getSumForArmorAttribute(character, ArmorAttributeType.HANDICAP, inclusiveShield);
//	}
//
//	//-------------------------------------------------------------------
//	public static int getTickMalusSum(SpliMoCharacter character, boolean inclusiveShield) {
//		return   getSumForArmorAttribute(character, ArmorAttributeType.TICKMALUS, inclusiveShield);
//	}
//
//	//-------------------------------------------------------------------
//	private static int getSumForArmorAttribute(SpliMoCharacter character, ArmorAttributeType type, boolean withShield){
//
//		List<CarriedItem> items = character.getItems();
//		int sum = 0;
//		int sumWithShield = 0;
//		for (CarriedItem carriedItem : items) {
//			if (carriedItem.isType(ItemType.ARMOR)) {
//				if (ItemLocationType.BODY.equals(carriedItem.getLocation())) {
//					switch(type) {
//						case DEFENSE:
//							sum += carriedItem.getDefense(ItemType.ARMOR);
//							break;
//						case DAMAGE_REDUCTION:
//							sum += carriedItem.getDamageReduction(ItemType.ARMOR);
//							break;
//						case HANDICAP:
//							sum += carriedItem.getHandicap(ItemType.ARMOR);
//							break;
//						case TICKMALUS:
//							sum += carriedItem.getTickMalus(ItemType.ARMOR);
//							break;
//					}
//				}
//			}
//			if (withShield && carriedItem.isType(ItemType.SHIELD)) {
//				if (ItemLocationType.BODY.equals(carriedItem.getLocation())){
//					switch(type) {
//						case DEFENSE:
//							sumWithShield += carriedItem.getDefense(ItemType.SHIELD);
//							break;
//						case HANDICAP:
//							sumWithShield += carriedItem.getHandicap(ItemType.SHIELD);
//							break;
//						case TICKMALUS:
//							sumWithShield += carriedItem.getTickMalus(ItemType.SHIELD);
//							break;
//						case DAMAGE_REDUCTION:
//							// does not apply for shields
//							break;
//					}
//				}
//			}
//		}
//
//		// check Meisterschaften in Zähigkeit
//		Skill endurance = SplitterMondCore.getSkill("endurance");
//		List<MastershipReference> masterships = character.getSkillValue(endurance).getMasterships();
//		for (MastershipReference mastership : masterships) {
//			if (mastership.getMastership() != null) {
//				switch (type) {
//					case TICKMALUS:
//						if (sum > 0
//								&& mastership.getMastership().getKey().equals("armour2")) {
//							sum -= 1;
//						}
//						if (withShield
//								&& sumWithShield > 0
//								&& mastership.getMastership().getKey().equals("shield2")) {
//							sumWithShield -= 1;
//						}
//                    	break;
//					case HANDICAP:
//						if (sum > 0
//								&& mastership.getMastership().getKey().equals("armour1")) {
//							sum -= 1;
//						}
//						if (withShield
//								&& sumWithShield > 0
//								&& mastership.getMastership().getKey().equals("shield1")) {
//							sumWithShield -= 1;
//						}
//                    	break;
//					default:
//				}
//			}
//		}
//
//
//		if (withShield) {
//			return sum + sumWithShield;
//		} else {
//			return sum;
//		}
//	}

	private enum ArmorAttributeType{
		DEFENSE,
		DAMAGE_REDUCTION,
		HANDICAP,
		TICKMALUS
	}

	private static SpellCostConverter spellCostConv = new SpellCostConverter();
	private static WeaponDamageConverter weaponDmgConv = new WeaponDamageConverter();

	//--------------------------------------------------------------------
	public static String getWeaponDamageString(int damage) {
		try {
			return weaponDmgConv.write(damage);
		} catch (Exception e) {
			return "";
		}
	}

	//--------------------------------------------------------------------
	public static int parseWeaponDamageString(String damage) {
		try {
			return weaponDmgConv.read(damage);
		} catch (Exception e) {
			return 0;
		}
	}

	//--------------------------------------------------------------------
	public static String encode(String toEncode) {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<toEncode.length(); i++) {
			char c = toEncode.charAt(i);
			switch (c) {
			case '<': buf.append("&lt;"); break;
			case '>': buf.append("&gt;"); break;
			case '"': buf.append("&quot;"); break;
			case '&': buf.append("&amp;"); break;
			default:
				buf.append(c);
			}
		}
		return buf.toString();
	}

	//--------------------------------------------------------------------
	public static String decode(String toDecode) {
		StringBuffer buf = new StringBuffer();
		int pos=0;
		while (pos<toDecode.length()) {
			String sub = toDecode.substring(pos);
			if (sub.startsWith("&lt;")) {
				buf.append("<");
				pos+=4;
			} else if (sub.startsWith("&gt;")) {
				buf.append(">");
				pos+=4;
			} else if (sub.startsWith("&quot;")) {
				buf.append('"');
				pos+=5;
			} else if (sub.startsWith("&amp;")) {
				buf.append('&');
				pos+=5;
			} else {
				buf.append(toDecode.charAt(pos));
				pos++;
			}

		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @param model
	 * @param req
	 * @return
	 */
	public static boolean isRequirementMet(SpliMoCharacter model, Requirement req) {
		if (req.getApply()!=null &&  req.getApply()!=ApplyTo.CHARACTER)
			throw new IllegalArgumentException(req.getApply()+"-Requirement cannot be checked against character");

		SplittermondReference type = (SplittermondReference) req.getType();
		List<String> allowedItemIDs = Arrays.asList(req.getAsKeys());
		List<DataItem> allowedItems = new ArrayList<>();
		for (String id : req.getAsKeys()) {
			allowedItems.add(type.resolve(id));
		}

		if (req instanceof ExistenceRequirement) {
			if (type==SplittermondReference.RACE) {
				boolean matches = (model.getRace()!=null && allowedItemIDs.contains(model.getRace()));
				return ((ExistenceRequirement)req).isNegate()?!matches:matches;
			}
			if (type==SplittermondReference.CULTURE) {
				boolean matches = (model.getCulture()!=null && allowedItemIDs.contains(model.getCulture()));
				return ((ExistenceRequirement)req).isNegate()?!matches:matches;
			}
			Logging.logger.log(Level.ERROR, "Unsupported type for existence requirement: "+type);
			return false;
		}

		if (req instanceof AnyRequirement) {
			AnyRequirement any = (AnyRequirement)req;
			for (Requirement r2 : any.getOptionList()) {
				if (isRequirementMet(model, r2))
					return true;
			}
			return false;
		}
		Logging.logger.log(Level.ERROR, "Unsupported requirement: "+req.getClass());
		return false;
	}

//	//-------------------------------------------------------------------
//	public static List<String> getGMRelevantPowers(SpliMoCharacter model) {
//		List<String> ret = new ArrayList<>();
//		Iterator<PowerReference> itPow = model.getPowers().iterator();
//		while (itPow.hasNext()) {
//			PowerReference ref = itPow.next();
//			if (ref.getPower().getId().equals("addsplinter"))
//				continue;
//			if (ref.getPower().getId().equals("focuspool"))
//				continue;
//			if (ref.getPower().getId().equals("sturdy"))
//				continue;
//			if (ref.getPower().getId().equals("swift"))
//				continue;
//			String name = ref.getPower().getName();
//			if (ref.getCount()>1)
//				name += ""+ref.getCount();
//
//			ret.add(name);
//		}
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	public static List<String> getGMRelevantItems(SpliMoCharacter model) {
//		List<String> ret = new ArrayList<>();
//		Iterator<CarriedItem> itItem = model.getItems().iterator();
//		while (itItem.hasNext()) {
//			CarriedItem ref = itItem.next();
//			int quality = ref.getArtifactQuality() + ref.getItemQuality();
//			if (quality==0)
//				continue;
//			String name = ref.getName();
//			name+=" (";
//			if (ref.isRelic())
//				name+="R ";
//			name += "Q"+quality+")";
//
//			ret.add(name);
//		}
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	public static List<SkillValue> getHighestSkills(SpliMoCharacter model) {
//		List<SkillValue> values = new ArrayList<>(model.getSkills(SkillType.NORMAL));
//		values.addAll(model.getSkills(SkillType.MAGIC));
//		values.remove(model.getSkillValue(SplitterMondCore.getSkill("athletics")));
//		values.remove(model.getSkillValue(SplitterMondCore.getSkill("empathy")));
//		values.remove(model.getSkillValue(SplitterMondCore.getSkill("determination")));
//		values.remove(model.getSkillValue(SplitterMondCore.getSkill("perception")));
//		values.remove(model.getSkillValue(SplitterMondCore.getSkill("endurance")));
////		values.remove(model.getSkillValue(SplitterMondCore.getSkill("locksntraps")));
//
//		Collections.sort(values, new Comparator<SkillValue>() {
//			public int compare(SkillValue o1, SkillValue o2) {
//				return ((Integer)o1.getValue()).compareTo(o2.getValue());
//			}
//		});
//		Collections.reverse(values);
//
//		List<SkillValue> ret = values.subList(0, 6);
//		Collections.sort(ret);
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Get the speed for the assigned weapon, including mastership and
//	 * weapon modifications.
//	 */
//	public static int getWeaponSpeedFor(SpliMoCharacter model, CarriedItem item, ItemType type) {
//		// TODO Pastore 11.04.2017: this is a workaround. The value of the mastership should be a modification.
//		int tickMalus = SplitterTools.getTickMalusSum(model, true);
//		SkillValue skillVal;
//		switch(type) {
//			case WEAPON:
//				Weapon weapon = item.getItem().getType(Weapon.class);
//				skillVal = model.getSkillValue(weapon.getSkill());
//				return (int) Math.max(3, tickMalus + item.getSpeed(ItemType.WEAPON)
//												   - skillVal.getMasterships().stream()
//																			  .map(m -> (m.getMastership()!=null)?m.getMastership().getKey():"")
//																			  .filter(s -> s.equals("dancingblade")).count());
//			case LONG_RANGE_WEAPON:
//				LongRangeWeapon longRangeWeapon = item.getItem().getType(LongRangeWeapon.class);
//				skillVal = model.getSkillValue(longRangeWeapon.getSkill());
//				return (int) (tickMalus + item.getSpeed(ItemType.LONG_RANGE_WEAPON)
//										- skillVal.getMasterships().stream()
//																   .map(m -> (m.getMastership()!=null)?m.getMastership().getKey():"")
//																   .filter(s -> s.startsWith("fastshooter")).count());
//
//			default:
//				Logging.logger.log(Level.ERROR, "Unknown item type of " + item);
//				return Short.MAX_VALUE;
//		}
//
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Get the value for a specific weapon, including skill value and
//	 * eventually existing weapon specializations
//	 */
//	public static int getWeaponValueFor(SpliMoCharacter model, CarriedItem item, ItemType type) {
//		int val = 0;
//		if (!item.isType(type)) {
//			return val;
//		}
//
//		SkillValue skillVal = new SkillValue();
//		SkillSpecialization spec = new SkillSpecialization();
//		switch (type) {
//
//		case WEAPON:
//			Weapon weapon = item.getItem().getType(Weapon.class);
//			skillVal = model.getSkillValue(weapon.getSkill());
//			// Calculate value for skill, including attributes
//			val = skillVal.getModifiedValue();
//			val += model.getAttribute(item.getAttribute1(ItemType.WEAPON)).getValue();
//			val += model.getAttribute(item.getAttribute2(ItemType.WEAPON)).getValue();
//
//			// Check for a skill bonus in the item
//			for (Modification mod : item.getOutgoingModifications()) {
//				Logging.logger.log(Level.INFO, "Mod of "+item.getName()+" = "+mod);
//				if (mod instanceof MastershipModification) {
//					MastershipModification mmod = (MastershipModification)mod;
//					Logging.logger.log(Level.INFO, "  mastermod = "+mmod.getSkill()+" / "+mmod.getSpecialization()+" / "+mmod.getSpecialization().getSpecial()+"  =?= "+item.getItem().getID());
//					if (mmod.getSkill()==weapon.getSkill() && mmod.getSpecialization().getSpecial().getId().equals(item.getItem().getID())) {
//						val += mmod.getSpecialization().getLevel();
//					}
//				}
//			}
//
//			// Check for specialization
//			spec.setType(SkillSpecializationType.WEAPON);
//			spec.setId(item.getItem().getID());
//			break;
//		case LONG_RANGE_WEAPON:
//			LongRangeWeapon longRangeWeapon = item.getItem().getType(LongRangeWeapon.class);
//			skillVal = model.getSkillValue(longRangeWeapon.getSkill());
//			// Calculate value for skill, including attributes
//			val = skillVal.getValue();
//			val += model.getAttribute(item.getAttribute1(ItemType.LONG_RANGE_WEAPON)).getValue();
//			val += model.getAttribute(item.getAttribute2(ItemType.LONG_RANGE_WEAPON)).getValue();
//
//			// Check for a skill bonus in the item
//			for (Modification mod : item.getOutgoingModifications()) {
//				if (mod instanceof MastershipModification) {
//					MastershipModification mmod = (MastershipModification)mod;
//					if (mmod.getSkill()==longRangeWeapon.getSkill() && mmod.getSpecialization().getSpecial().getId().equals(item.getItem().getID())){
//						val += mmod.getSpecialization().getLevel();
//					}
//				}
//			}
//
//			// Check for specialization
//			spec.setType(SkillSpecializationType.WEAPON);
//			spec.setId(item.getItem().getID());
//			break;
//		default:
//			break;
//		}
//
//		val += skillVal.getSpecializationLevel(spec);
//
//
//		return val;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Calculate training potential for a given creature.
//	 * @See Bestienmeister S.15
//	 */
//	public static int getCreaturePotential(SpliMoCharacter model, CreatureReference creature) {
//		int value = 1;
//		// +2 per value in resource "creature"
//		if (creature.getResource()!=null) {
//			value += 2*creature.getResource().getValue();
//		}
//		// TODO: is animal connected via Power "Tiervertrauter" ?
//		// TODO: Meisterschaft "Tierspezialist"
//		// Meiste der Bestien
//		if (model.hasMastership(SplitterMondCore.getSkill("animals").getMastership("lord_of_the_beasts"))) {
//			value += 2;
//		}
//		// TODO: Meisterbändiger
//
//		return value;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * Calculate training potential for a given creature.
//	 */
//	public static int getUsedCreaturePotential(CreatureReference creature) {
//		int value = 0;
//		if (creature != null && !creature.getTrainings().isEmpty()) {
//			List<CreatureModuleReference> trainings = creature.getTrainings();
//			value = trainings.stream().mapToInt(training -> training.getModule().getCost()).sum();
//		}
//		return value;
//	}
//
//	//-------------------------------------------------------------------
//	public static boolean isRequirementMet(SpliMoCharacter model, Requirement requires) {
//		if (requires instanceof AttributeRequirement) {
//			AttributeRequirement req = (AttributeRequirement)requires;
//			return model.getAttribute(req.getAttribute()).getValue()>=req.getValue();
//		} else if (requires instanceof MastershipRequirement) {
//			MastershipRequirement req = (MastershipRequirement)requires;
//			return model.hasMastership(req.getMastership());
//		}
//		Logging.logger.log(Level.WARNING, "I don't know how to check for "+requires.getClass());
//		return true;
//	}

	//-------------------------------------------------------------------
	/**
	 * Walk through all items in the character and resolve them
	 * @param rawChar
	 */
	public static void resolveChar(SpliMoCharacter model) {
		logger.log(Level.ERROR, "\n\nENTER resolveChar({0})", model.getName());
		try {
			if (model.getCulture().getKey()!=null)
				model.setCulture(SplitterMondCore.getItem(Culture.class, model.getCulture().getKey()));
			if (model.getBackground().getKey()!=null)
				model.setBackground(SplitterMondCore.getItem(Background.class, model.getBackground().getKey()));
			if (model.getEducation().getKey()!=null)
				model.setEducation(SplitterMondCore.getItem(Education.class, model.getEducation().getKey()));
			logger.log(Level.DEBUG, "resolve powers");
			for (PowerValue tmp : model.getPowers()) {
				Power resolved = SplitterMondCore.getItem(Power.class, tmp.getKey());
				tmp.setResolved(resolved);
			}

			logger.log(Level.DEBUG, "resolve resources");
			for (ResourceValue tmp : model.getResources()) {
				Resource resolved = SplitterMondCore.getItem(Resource.class, tmp.getKey());
				tmp.setResolved(resolved);
			}

			resolveGenericLifeform(model);

			logger.log(Level.DEBUG, "resolve spells");
			for (SpellValue tmp : model.getSpells()) {
				Spell resolved = SplitterMondCore.getItem(Spell.class, tmp.getKey());
				tmp.setResolved(resolved);
			}

			logger.log(Level.DEBUG, "resolve gear");
			SMResolveTemplatesStep resolver = new SMResolveTemplatesStep();
			for (CarriedItem<ItemTemplate> tmp : model.getCarriedItems()) {
				tmp.setCharacter(model);
				if (tmp.getUuid()==null)
					logger.log(Level.WARNING, "Char {0} Item {1} has no UUID", model.getName(), tmp.getKey());
				if (tmp.getResolved()==null) {
					ItemTemplate resolved = SplitterMondCore.getItem(ItemTemplate.class, tmp.getKey());
					if (resolved==null) {
						logger.log(Level.ERROR, "Char {0} has an unresolvable item: {1}", model.getName(), tmp.getKey());
						System.err.println("Char "+model.getName()+" has an unresolvable item: "+tmp.getKey());
						model.removeCarriedItem(tmp);
					} else {
						tmp.setResolved(resolved);
						if (tmp.getVariantID()!=null && tmp.getVariant()==null) {
							tmp.setVariant( tmp.getResolved().getVariant(tmp.getCarryMode()) );
						}
					}
				}
				// Resolve item enhancements
				for (ItemEnhancementValue<AItemEnhancement> eVal : tmp.getEnhancements()) {
					Enhancement enh = SplitterMondCore.getItem(Enhancement.class, eVal.getKey());
					if (enh==null) {
						logger.log(Level.ERROR, "Char {0} has item {1} with unresolvable enhancement {2}", model.getName(), tmp.getNameWithoutRating(), eVal.getKey());
					} else {
						eVal.setResolved( enh );
					}
				}
				resolver.process(false, SplittermondReference.ITEM_ATTRIBUTE, model, tmp, List.of());
				SMGearTool.recalculate("", model, tmp);
				tmp.setDirty(false);
			}

			logger.log(Level.DEBUG, "resolve lifeforms");
			for (Companion tmp : model.getCompanions()) {
				resolveCompanion(tmp);
			}


		} finally {
			logger.log(Level.INFO, "LEAVE resolveChar({0})", model.getName());
		}
	}

	//-------------------------------------------------------------------
	public static void resolveCompanion(Companion model) {
		logger.log(Level.WARNING, "resolveCompanion {0}", model.getName());
		resolveGenericLifeform(model);

		logger.log(Level.DEBUG, "resolve creature features");
		for (CreatureFeatureValue tmp : model.getFeatures()) {
			CreatureFeature resolved = SplitterMondCore.getItem(CreatureFeature.class, tmp.getKey());
			if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown creature feature '{1}'", model.getName(), tmp.getKey());
			tmp.setResolved(resolved);
		}

		logger.log(Level.DEBUG, "resolve spells");
		for (SpellValue tmp : model.getSpells()) {
			Spell resolved = SplitterMondCore.getItem(Spell.class, tmp.getKey());
			if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown spell '{1}'", model.getName(), tmp.getKey());
			tmp.setResolved(resolved);
		}

		if (model.getSummonable()!=null) {
			SummonableCreature summon = model.getSummonable();
			if (summon.getBody()!=null) {
				summon.getBody().setResolved( SplitterMondCore.getItem(CreatureModule.class, summon.getBody().getKey()));
			}
			if (summon.getType()!=null) {
				summon.getType().setResolved( SplitterMondCore.getItem(CreatureModule.class, summon.getType().getKey()));
			}
			for (CreatureModuleValue tmp : summon.getModules()) {
				CreatureModule resolved = SplitterMondCore.getItem(CreatureModule.class, tmp.getKey());
				if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown creature module '{1}'", model.getName(), tmp.getKey());
				tmp.setResolved(resolved);
			}

			for (ServiceValue tmp : model.getSummonable().getServices()) {
				Service resolved = SplitterMondCore.getItem(Service.class, tmp.getKey());
				if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown creature feature '{1}'", model.getName(), tmp.getKey());
				tmp.setResolved(resolved);
			}
		}

		CompanionTool.recalculate(model);
	}
	//-------------------------------------------------------------------
	private static void resolveGenericLifeform(Lifeform<Attribute, SMSkill, SMSkillValue> model) {
		logger.log(Level.DEBUG, "resolve skills");
		for (SMSkillValue tmp : model.getSkillValues()) {
			SMSkill resolved = SplitterMondCore.getSkill(tmp.getKey());
			if (resolved==null) {
				logger.log(Level.ERROR, "Character {0} contains unknown skill ''{1}''", model.getName(), tmp.getKey());
				continue;
			}
			tmp.setResolved(resolved);

			for (MastershipReference ref : tmp.getMasterships()) {
				Mastership r2 = SplitterMondCore.getItem(Mastership.class, ref.getKey());
				if (r2==null) logger.log(Level.ERROR, "Character {0} contains unknown mastership ''{1}''", model.getName(), ref.getKey());
				ref.setResolved(r2);
			}
			for (SkillSpecializationValue<SMSkill> ref : tmp.getSpecializations()) {
				SMSkillSpecialization r2 = resolved.getSpecialization(ref.getKey());
				if (r2==null) logger.log(Level.ERROR, "Character {0} contains unknown mastership ''{1}''", model.getName(), ref.getKey());
				ref.setResolved(r2);
			}
		}
	}

	//-------------------------------------------------------------------
	private static void resolveLifeform(SMLifeform model) {
		resolveGenericLifeform(model);

		logger.log(Level.DEBUG, "resolve creature features");
		for (CreatureFeatureValue tmp : model.getFeatures()) {
			CreatureFeature resolved = SplitterMondCore.getItem(CreatureFeature.class, tmp.getKey());
			if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown creature feature '{1}'", model.getName(), tmp.getKey());
			tmp.setResolved(resolved);
		}

		logger.log(Level.DEBUG, "resolve spells");
		for (SpellValue tmp : model.getSpells()) {
			Spell resolved = SplitterMondCore.getItem(Spell.class, tmp.getKey());
			if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown spell '{1}'", model.getName(), tmp.getKey());
			tmp.setResolved(resolved);
		}

		if (model instanceof SummonableCreature) {
			logger.log(Level.DEBUG, "resolve services");
			for (ServiceValue tmp : ((SummonableCreature)model).getServices()) {
				Service resolved = SplitterMondCore.getItem(Service.class, tmp.getKey());
				if (resolved==null) logger.log(Level.ERROR, "Creature {0} contains unknown creature feature '{1}'", model.getName(), tmp.getKey());
				tmp.setResolved(resolved);
			}
		}
	}

	//-------------------------------------------------------------------
	public static List<ProcessingStep> getCharacterProcessingSteps(SpliMoCharacter model, Locale loc) {
		List<ProcessingStep> steps = new ArrayList<>();
		for (Class<? extends ProcessingStep> cls : RECALCULATE_STEPS) {
			Constructor<? extends ProcessingStep> cons =  null;
			try {
				cons = cls.getConstructor(SpliMoCharacter.class, Locale.class);
			} catch (Exception e) {
			}
			if (cons==null) {
				try { cons = cls.getConstructor(SpliMoCharacter.class, Locale.class);	} catch (Exception e) { }
			}
			if (cons==null) {
				try { cons = cls.getConstructor(SpliMoCharacter.class);	} catch (Exception e) { }
			}
			if (cons==null) {
				try { cons = cls.getConstructor(SpliMoCharacter.class);	} catch (Exception e) { }
			}

			if (cons!=null) {
				try {
					if (cons.getParameterCount()==2)
						steps.add( cons.newInstance(model, loc));
					else
						steps.add( cons.newInstance(model));
				} catch (Exception e) {
					logger.log(Level.ERROR, "Error instantiating "+cls);
					e.printStackTrace();
					System.exit(0);
				}
			} else {
				logger.log(Level.ERROR, "No constructor for "+cls);
				System.exit(1);
			}
		}

		return steps;
	}

	//-------------------------------------------------------------------
	public static void runProcessors(SpliMoCharacter model, Locale loc) {
		List<ProcessingStep> processChain = getCharacterProcessingSteps(model, loc);
		try {
			logger.log(Level.DEBUG, "\n\nSTART: runProcessors: "+processChain.size()+"-------------------------------------------------------");
			List<Modification> unprocessed = new ArrayList<>();
			for (ProcessingStep processor : processChain) {
				unprocessed = processor.process(unprocessed);
				logger.log(Level.WARNING, "------ after {0}     {1}|{2}",processor.getClass().getSimpleName(),model.getExpFree(), unprocessed);
			}
			logger.log(Level.DEBUG, "Remaining mods  = "+unprocessed);
			logger.log(Level.DEBUG, "STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed calculating character "+model.getName(),e);
		}
	}

	//-------------------------------------------------------------------
	public static boolean isRequirementMet(SpliMoCharacter model, ComplexDataItem requiredFor, Requirement req, Decision[] decisions) {
		if (req.getApply()!=null && req.getApply()!=ApplyTo.CHARACTER) return true;

		try {

		if (req instanceof ExistenceRequirement) {
			ExistenceRequirement tmp = (ExistenceRequirement)req;
			boolean negated = tmp.isNegate();
			SplittermondReference type = (SplittermondReference)tmp.getType();
			Object foo = null;
			if (type!=SplittermondReference.CARRIED)
				foo = SplittermondReference.resolve(type, req.getKey());
			if (!(foo instanceof DataItem)) {
				logger.log(Level.WARNING, "Character requirement check not implemented for {0} and foo={1}",type, foo);
				return true;
			}
			DataItem item = SplittermondReference.resolve(type, req.getKey());
			switch (type) {
			case POWER:
				if (negated) return !model.hasPower(req.getKey());
				return model.hasPower(req.getKey());
//			case MASTERSHIP:
//				if (negated) return model.getDrakeType()==null || item!=model.getDrakeType().getResolved();
//				return model.getDrakeType()!=null || item==model.getDrakeType().getResolved();
			case GEAR:
			case CARRIED:
				// Character needs to have a specific gear
				for (CarriedItem<ItemTemplate> gear : model.getCarriedItems()) {
					if (gear.getModifyable().getId().equals(req.getKey())) {
						return true;
					}
				}
				return false;
			default:
				System.err.println("Shadowrun6Tool: Todo: isRequirementMet for "+type+" = "+item);
				logger.log(Level.WARNING, "Todo: isRequirementMet for "+type+" = "+item);
			}
			return false;
		} else if (req instanceof AnyRequirement) {
			AnyRequirement any = (AnyRequirement)req;
			for (Requirement tmp : any.getOptionList()) {
				if (isRequirementMet(model, requiredFor, tmp, decisions))
					return true;
			}
			return false;
		} else if (req instanceof ValueRequirement) {
			ValueRequirement tmp = (ValueRequirement)req;
			SplittermondReference type = (SplittermondReference)tmp.getType();
//			switch (type) {
//			case ITEM_ATTRIBUTE:
//				SR6ItemAttribute itemAttr = SR6ItemAttribute.valueOf(tmp.getKey());
//				switch (itemAttr) {
//				case ITEMTYPE:
//					ItemType iType = ItemType.valueOf(tmp.getRawValue());
//				}
//			}
			int min = -1;
			int max = Integer.MAX_VALUE;
			if (tmp.getFormula().isResolved() && tmp.getFormula().isInteger()) {
				if (tmp.getRawValue()!=null) {
					min = tmp.getValue();
				} else {
					max = tmp.getMaxValue();
				}
			} else {
				if (requiredFor.getClass()==ItemTemplate.class) {
					CarryMode mode = ((ItemTemplate) requiredFor).getUsages().get(0).getMode();
					CarriedItem item = GearTool.buildItem((ItemTemplate) requiredFor, mode, model, false, decisions).get();
					VariableResolver resolver = new VariableResolver(item, model);
					if (((FormulaImpl)tmp.getFormula()).getAsString().startsWith("$")) {
						logger.log(Level.WARNING, "ToDo: Resolve "+tmp.getFormula());
						SMItemAttribute itemAttr = SMItemAttribute.valueOf( ((FormulaImpl)tmp.getFormula()).getAsString().substring(1));
						String raw = FormulaTool.resolve(SplittermondReference.ITEM_ATTRIBUTE, (FormulaImpl)tmp.getFormula(), resolver);
						min = Integer.valueOf(raw);
					}
				} else
					logger.log(Level.WARNING, "ToDo: check unresolved requirement "+req.getKey()+":"+tmp.getFormula()+" for "+requiredFor.getClass());
			}
			Object item = SplittermondReference.resolve(type, req.getKey());
			if (item==null && !("CHOICE".equals(req.getKey()))) {
				logger.log(Level.ERROR, "Cannot find item for key ''{0}''", tmp.getType()+":"+tmp.getKey());
				return false;
			}
			switch (type) {
			case SKILL:
				if ("CHOICE".equals(tmp.getKey())) {
					return true;
				}
				if (model.getSkillValue((SMSkill)item)==null) {
					return false;
				}
				int val = model.getSkillValue( (SMSkill)item).getModifiedValue();
				if (max!=Integer.MAX_VALUE && val>max) return false;
				if (min>0 && val<min) return false;
				//if (max>0 && val>min) return false;
				return true;
			case ATTRIBUTE:
				if ("CHOICE".equals(tmp.getKey())) {
					return true;
				}
				if (model.getAttribute((Attribute)item)==null) {
					return false;
				}
				val = model.getAttribute( (Attribute)item).getModifiedValue();
				if (max!=Integer.MAX_VALUE && val>max) return false;
				if (min>0 && val<min) return false;
				//if (max>0 && val>min) return false;
				return true;
			case ITEM_ATTRIBUTE:
				if ("CHOICE".equals(tmp.getKey())) {
					return true;
				}
				SMItemAttribute iAttr = tmp.getType().resolve(tmp.getKey());
//				if (requiredFor instanceof ItemTemplate) {
//					ItemAttributeDefinition def = ((ItemTemplate)requiredFor).getAttribute(iAttr);
//					if (def==null) return false;
//					switch (iAttr) {
//					case ITEMSUBTYPE:
//					case ITEMTYPE:
//						return tmp.getRawValue().equals(def.getRawValue());
//					default:
//						logger.log(Level.WARNING, "TODO: Compare "+tmp.getRawValue()+" with "+def.getRawValue());
//					}
//				} else {
					logger.log(Level.WARNING, "Don't know how to check item attribute "+tmp+" for "+requiredFor);
					return false;
//				}
//				return false;
//			case QUALITY:
//				if ("CHOICE".equals(tmp.getKey())) {
//					return true;
//				}
//				if (model.getQuality(tmp.getKey())==null) {
//					return false;
//				}
//				val = model.getQuality(tmp.getKey()).getModifiedValue();
//				if (max!=Integer.MAX_VALUE && val>max) return false;
//				if (min>0 && val<min) return false;
//				//if (max>0 && val>min) return false;
//				return true;
			default:
				logger.log(Level.WARNING, "Todo: isRequirementMet for "+type);
			}
		}
		System.err.println("Shadowrun6Tool: Requirement checking not supported for "+req.getClass()+" and "+req.getType());
		logger.log(Level.WARNING,"ToDo: Requirement checking not supported for "+req.getClass()+" and "+req.getType());
		return false;

		} catch (Exception e) {
			logger.log(Level.ERROR, "Error checking requirement "+req+" from "+requiredFor,e);
			e.printStackTrace();
		}
		return false;
	}

	//-------------------------------------------------------------------
	public static Possible areRequirementsMet(SpliMoCharacter model, ComplexDataItem data, Decision[] decisions) {
		List<Requirement> list = new ArrayList<>();
		for (Requirement req : data.getRequirements()) {
			if (req.getApply()!=null && req.getApply()!=ApplyTo.CHARACTER)
				continue;
			if (!isRequirementMet(model, data, req, decisions)) {
				list.add(req);
			}
		}

		if (list.isEmpty())
			return Possible.TRUE;
		return new Possible(list.toArray(new Requirement[list.size()]));
	}


	//-------------------------------------------------------------------
	/**
	 * Called from ProcessExtraStep when creating summonable creates
	 */
	public static DataItemModification instantiateModification(DataItemModification mod, Decision dec) {
		logger.log(Level.WARNING, "TODO: instantiate {0}",mod);
		if (mod instanceof ValueModification) {
			ValueModification newMod = ((ValueModification)mod).clone();
			if (newMod.getKey().equals("CHOICE")) {
				newMod.setKey(dec.getValue());
			}
			return newMod;
		}

		DataItemModification newMod = mod.clone();
		if (newMod.getKey().equals("CHOICE")) {
			newMod.setKey(dec.getValue());
		}

		logger.log(Level.DEBUG, "Converted {0} to {1}", mod, newMod);
		return newMod;
	}

	//-------------------------------------------------------------------
	public static Modification instantiateModification(Modification tmp, ComplexDataItemValue<?> value, int multiplier, SpliMoCharacter model) {
		logger.log(Level.TRACE, "instantiate {0} with multiplier {1}",tmp,multiplier);
		if (tmp instanceof ValueModification) {
			ValueModification clone = ((ValueModification)tmp).clone();
			int modVal = 0;
			if (clone.hasFormula()) {
				logger.log(Level.WARNING, "Found a formula :(  "+clone.getFormula());
				logger.log(Level.WARNING, "  model =  "+model);
				logger.log(Level.WARNING, "  data item =  "+value.getKey());
				logger.log(Level.WARNING, "  data item value =  "+value);
				String resolved = FormulaTool.resolve(clone.getReferenceType(), clone.getFormula(), new VariableResolver(value, model));
				logger.log(Level.DEBUG, "  {0} resolved as {1}", clone.getFormula(),resolved);
				modVal = Integer.parseInt(resolved);
				clone.setValue(modVal);
			} else {
				if (clone.getLookupTable()!=null) {
					modVal = clone.getValue();
					if (modVal>clone.getLookupTable().length) {
						logger.log(Level.ERROR, "Modification {0}, multiplier {1} is outside table", tmp, multiplier);
					}
					clone.setValue( clone.getLookupTable()[modVal-1] );
					clone.setLookupTable(null);
				} else if (multiplier>1) {
					if (clone.isDouble()) {
						double newVal = clone.getValueAsDouble()*multiplier;
						clone.setRawValue( String.valueOf( newVal ));
					} else {
						modVal = clone.getValue();
						clone.setRawValue( String.valueOf( modVal*multiplier ));
						clone.setValue( modVal*multiplier );
					}
				}
			}
			if ("CHOICE".equals( clone.getKey() )) {
				UUID uuid =  ((ValueModification) tmp).getConnectedChoice();
				Decision dec = value.getDecision(uuid);
				if (dec!=null) {
					clone.setKey( dec.getValue());
				} else {
					logger.log(Level.ERROR, "No decision for {0} found in {1}", uuid, value.getKey());
					System.err.println("Shadowrun6Tools.instantiate: No decision for "+uuid+" found in "+value);
					try {
						throw new RuntimeException("Trace");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Shadowrun6Tools.instantiate: No decision for "+uuid+" found in "+value.getKey());
					clone.setKey(null);
				}
			} else if ("ITEM".equals( clone.getKey() )) {
				// Get the connected item UUID
				String source = String.valueOf(value.getModifyable());
				Choice carriedChoice = value.getModifyable().getChoice(SplittermondReference.CARRIED);
				if (carriedChoice==null) {
					logger.log(Level.ERROR, "Trying to instantiate a ref=ITEM modification, but the {0} has no CARRIED choice",source);
				} else {
					Decision dec = value.getDecision(carriedChoice.getUUID());
					if (dec==null) {
						logger.log(Level.ERROR, "Trying to instantiate a ref=ITEM modification, but the decision for {0} is missing in {1}",carriedChoice.getUUID(), value);
					} else {
						UUID connectedItemUUID = UUID.fromString(dec.getValue());
						CarriedItem<ItemTemplate> item = model.getCarriedItem(connectedItemUUID);
						// No act depending on what is needed from referenced item
						switch ((SplittermondReference)tmp.getReferenceType()) {
						case SKILL:
							ItemAttributeObjectValue<SMItemAttribute> attrObj = item.getAsObject(SMItemAttribute.SKILL);
							if (attrObj==null) {
								logger.log(Level.ERROR, "No SKILL item attribute in carried item {0}", item);
							} else {
								SMSkill skill = attrObj.getModifiedValue();
								logger.log(Level.INFO, "Set skill {0} in instantiated {1}", skill.getId(), tmp);
								clone.setKey(skill.getId());
								logger.log(Level.INFO, "Add modification {0} to {1}", clone, item);
								item.addOutgoingModification(clone);
								return null;
							}
							break;
						default:
							logger.log(Level.ERROR, "ToDo: Implement fetching {0} from CarriedItem",tmp.getReferenceType());
							System.err.println("ToDo: Implement fetching "+tmp.getReferenceType()+" from CarriedItem");
						}
					}
				}
			}
			if ("$LEVEL".equals(clone.getRawValue())) {
				logger.log(Level.DEBUG, "Replace $LEVEL with {0}", multiplier);
				clone.setValue(multiplier);
			}

			return clone;
		}
		if (tmp instanceof DataItemModification) {
			DataItemModification clone = ((DataItemModification)tmp).clone();
			if ("CHOICE".equals( clone.getKey() )) {
				UUID uuid =  ((DataItemModification) tmp).getConnectedChoice();
				Decision dec = value.getDecision(uuid);
				logger.log(Level.DEBUG, "instantiate {2} with UUID {0} and decision {1}", uuid,dec,clone.getKey());
				if (dec!=null) {
					clone.setKey( dec.getValue());
				} else {
					logger.log(Level.ERROR, "No decision for {0} found in {1}", uuid, value);
				}
			}

			return clone;
		}
		if (tmp instanceof RelevanceModification) {
			return tmp;
		}

		throw new IllegalArgumentException("Cannot instantiate "+tmp.getClass());
	}

	//-------------------------------------------------------------------
	public static int getSpellValue(SpliMoCharacter model, SpellValue spell) {
		logger.log(Level.WARNING, "Implement SplitterTools.getSpellValue()");
		int value = model.getSkillValue( spell.getSkill() ).getModifiedValue();
		value+= model.getAttribute( spell.getSkill().getAttribute() ).getModifiedValue();
		value+= model.getAttribute( spell.getSkill().getAttribute2() ).getModifiedValue();
		return value;
	}

	//-------------------------------------------------------------------
	public static String getSpellCostString(SpliMoCharacter model, SpellValue spell) {
		SMSkillValue val = model.getSkillValue( spell.getSkill() );
		SpellCost cost = spell.getResolved().getCost();
		logger.log(Level.WARNING, "Implement SplitterTools.getSpellCost() for "+spell.getKey()+" and "+spell.getIncomingModifications());

		return spellCostConv.write(cost);
	}

	//-------------------------------------------------------------------
	public static List<SummoningCategory> getKnownSummonables(SpliMoCharacter model) {
		return List.of(
				new SummoningCategory("Wasserwesen rufen I", List.of()),
				new SummoningCategory("Wasserwesen rufen II", List.of()),
				new SummoningCategory("Merkmalskenntnis (0/0)", List.of())
				);
	}

}

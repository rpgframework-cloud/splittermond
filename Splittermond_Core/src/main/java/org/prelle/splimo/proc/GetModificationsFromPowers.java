package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SpliMoCharacter;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromPowers implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromPowers.class.getPackageName()+".power");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public GetModificationsFromPowers(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {

			for (PowerValue val : model.getPowers()) {
				val.updateOutgoingModificiations(model);
				List<Modification> modList = val.getOutgoingModifications();
				logger.log(Level.DEBUG, "Inject modifications from power {0} = {1}", val.getKey(), modList);
				for (Modification mod : modList) {
					if (mod==null) {
						logger.log(Level.ERROR, "Effective modifications from {0} contains NULL", val.getKey());
					} else
						unprocessed.add(mod);
				}
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

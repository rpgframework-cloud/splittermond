package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMasterships implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromMasterships.class.getPackageName()+".master");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public GetModificationsFromMasterships(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {

			for (SMSkillValue val : model.getSkillValues()) {
				for (MastershipReference ref : val.getMasterships()) {
					ref.updateOutgoingModificiations(model);
					List<Modification> modList = ref.getOutgoingModifications();
					if (modList.isEmpty()) continue;
					logger.log(Level.ERROR, "Inject modifications from skill {0} = {1}\n\n\n", val.getKey(), modList);
					for (Modification mod : modList) {
						if (mod==null) {
							logger.log(Level.ERROR, "Effective modifications from {0} contains NULL", val.getKey());
						} else {
							if (mod.getApplyTo()==ApplyTo.PARENT) {
								logger.log(Level.ERROR, "Add character mod to {0} ", val.getKey(), modList);
								val.addOutgoingModification(mod);
							} else {
								unprocessed.add(mod);
							}
						}
					}
				}

			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

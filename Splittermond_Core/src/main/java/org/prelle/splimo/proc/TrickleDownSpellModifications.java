package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.Pool;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 *
 */
public class TrickleDownSpellModifications implements ProcessingStep {

	private final static Logger logger = System.getLogger(TrickleDownSpellModifications.class.getPackageName()+".spells");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public TrickleDownSpellModifications(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {

			for (SMSkillValue val : model.getSkillValues()) {
				for (Modification mod : val.getOutgoingModifications()) {
					if (mod.getReferenceType()==SplittermondReference.SPELL_ATTRIBUTE) {
						model.getSpells().stream()
								.filter(s -> s.getSkill().getId()==val.getSkill().getId())
								.forEach(s -> {
									logger.log(Level.DEBUG, "Inject modification {0} to spell {1} in school {2}", mod, s, val.getKey());
									s.addIncomingModification(mod);
								});
					}
				}

			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

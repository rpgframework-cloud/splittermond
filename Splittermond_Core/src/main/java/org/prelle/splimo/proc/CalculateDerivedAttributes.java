package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.Pool;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 *
 */
public class CalculateDerivedAttributes implements ProcessingStep {

	private final static Logger logger = System.getLogger(CalculateDerivedAttributes.class.getPackageName()+".derived");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public CalculateDerivedAttributes(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		calculateSpeed();
		calculateLife();
		calculateFocus();
		calculateInitiative();
		calculateHit();
		calculateResistMind();
		calculateResistBody();

		calculateDefenseMind();
		calculateDefenseBody();
		calculateDefenseDamage();

		calculateLevel();
		return unprocessed;
	}

	//-------------------------------------------------------------------
	private void addNaturalModifier(AttributeValue<Attribute> val, int value, Object source) {
		ValueModification valMod = new ValueModification(SplittermondReference.ATTRIBUTE, val.getModifyable().name(), value, source);
		valMod.setSet(ValueType.NATURAL);
		val.addIncomingModification( valMod );
	}

	//-------------------------------------------------------------------
	private void calculateSpeed() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.SPEED);
		addNaturalModifier(val, Math.round(model.getAttribute(Attribute.AGILITY).getModifiedValue()), Attribute.AGILITY.getName());
		addNaturalModifier(val, Math.round(model.getAttribute(Attribute.SIZE).getModifiedValue()), Attribute.SIZE.getName());
		logger.log(Level.DEBUG, " Speed   = {0}    (AGILITY {1} + SIZE {2})",val.getModifiedValue(), model.getAttribute(Attribute.AGILITY).getModifiedValue(), model.getAttribute(Attribute.SIZE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateLife() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.LIFE);
		addNaturalModifier(val, Math.round(model.getAttribute(Attribute.SIZE).getModifiedValue()), Attribute.SIZE.getName());
		addNaturalModifier(val, Math.round(model.getAttribute(Attribute.CONSTITUTION).getModifiedValue()), Attribute.CONSTITUTION.getName());
		logger.log(Level.DEBUG, " Life    = {0}    (CONSTITION {1} + SIZE {2})",val.getModifiedValue(), model.getAttribute(Attribute.CONSTITUTION).getModifiedValue(), model.getAttribute(Attribute.SIZE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateFocus() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.FOCUS);
		addNaturalModifier(val, model.getAttribute(Attribute.MYSTIC).getModifiedValue()*2, Attribute.MYSTIC.getName()+"x2");
		addNaturalModifier(val, model.getAttribute(Attribute.WILLPOWER).getModifiedValue()*2, Attribute.WILLPOWER.getName()+"x2");
		logger.log(Level.DEBUG, " Focus   = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateInitiative() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.INITIATIVE);
		val.setDistributed(0);
		addNaturalModifier(val, 10, "10");
		addNaturalModifier(val, -model.getAttribute(Attribute.INTUITION).getModifiedValue()*2, "-"+Attribute.INTUITION.getName());
		logger.log(Level.DEBUG, " Initiative = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateHit() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.RESIST_DAMAGE);
		val.setDistributed(12);
		addNaturalModifier(val, model.getAttribute(Attribute.AGILITY ).getModifiedValue(), Attribute.AGILITY.getName());
		addNaturalModifier(val, model.getAttribute(Attribute.STRENGTH).getModifiedValue(), Attribute.STRENGTH.getName());
		logger.log(Level.DEBUG, " VTD = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateResistMind() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.RESIST_MIND);
		val.setDistributed(12);
		addNaturalModifier(val, model.getAttribute(Attribute.MIND ).getModifiedValue(), Attribute.MIND.getName());
		addNaturalModifier(val, model.getAttribute(Attribute.WILLPOWER).getModifiedValue(), Attribute.WILLPOWER.getName());
		logger.log(Level.DEBUG, " GW = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateResistBody() {
		AttributeValue<Attribute> val = model.getAttribute(Attribute.RESIST_BODY);
		val.setDistributed(12);
		addNaturalModifier(val, model.getAttribute(Attribute.CONSTITUTION).getModifiedValue(), Attribute.CONSTITUTION.getName());
		addNaturalModifier(val, model.getAttribute(Attribute.WILLPOWER).getModifiedValue(), Attribute.WILLPOWER.getName());
		logger.log(Level.DEBUG, " KW = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateDefenseMind() {
		SMSkill skill = SplitterMondCore.getSkill("determination");
		SMSkillValue sVal = model.getSkillValue(skill);
		int base = (sVal!=null)?sVal.getModifiedValue():0;
		AttributeValue<Attribute> val = model.getAttribute(Attribute.DEFENSE_MIND);
		addNaturalModifier(val, base, skill.getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute() ).getModifiedValue(), skill.getAttribute().getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute2()).getModifiedValue(), skill.getAttribute2().getName());
		logger.log(Level.DEBUG, " AA GW = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateDefenseBody() {
		SMSkill skill = SplitterMondCore.getSkill("endurance");
		SMSkillValue sVal = model.getSkillValue(skill);
		int base = (sVal!=null)?sVal.getModifiedValue():0;
		AttributeValue<Attribute> val = model.getAttribute(Attribute.DEFENSE_BODY);
		addNaturalModifier(val, base, skill.getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute() ).getModifiedValue(), skill.getAttribute().getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute2()).getModifiedValue(), skill.getAttribute2().getName());
		logger.log(Level.DEBUG, " AA KW = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateDefenseDamage() {
		SMSkill skill = SplitterMondCore.getSkill("acrobatics");
		SMSkillValue sVal = model.getSkillValue(skill);
		int base = (sVal!=null)?sVal.getModifiedValue():0;
		AttributeValue<Attribute> val = model.getAttribute(Attribute.DEFENSE_DAMAGE);
		addNaturalModifier(val, base, skill.getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute() ).getModifiedValue(), skill.getAttribute().getName());
		addNaturalModifier(val, model.getAttribute(skill.getAttribute2()).getModifiedValue(), skill.getAttribute2().getName());
		logger.log(Level.DEBUG, " AA VTD = {0}   ", val.getModifiedValue());
	}

	//-------------------------------------------------------------------
	private void calculateLevel() {
		int level = 1;
		if (model.getExpInvested()>=100) level=2;
		if (model.getExpInvested()>=300) level=3;
		if (model.getExpInvested()>=600) level=4;

		AttributeValue<Attribute> val = model.getAttribute(Attribute.LEVEL);
		val.setDistributed(level);
	}

}

package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Power;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.Race;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splimo.persist.MastershipConverter;
import org.prelle.splimo.persist.ReferenceException;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.AllowModification;
import de.rpgframework.genericrpg.modification.CheckModification;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 *
 */
public class ApplyModifications implements ProcessingStep {

	private final static Logger logger = System.getLogger(ApplyModifications.class.getPackageName());

	private SpliMoCharacter model;

	private static MastershipConverter mastershipConverter = new MastershipConverter();

	//-------------------------------------------------------------------
	/**
	 */
	public ApplyModifications(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @return TRUE when modification was processed
	 */
	public static boolean applyModification(SpliMoCharacter model, Modification tmp) {
		try {
			if (tmp instanceof DataItemModification) {
				DataItemModification mod = (DataItemModification)tmp;
				switch ((SplittermondReference) tmp.getReferenceType()) {
				case ATTRIBUTE  : return applyAttribute(model, (ValueModification) mod);
				case GEAR       : return applyGear(model, mod);
				case MASTERSHIP : return applyMastership(model, mod);
				case POWER      : return applyPower(model, mod);
				case RESOURCE   : return applyResource(model, mod);
				case SKILL		:
					if (mod instanceof ValueModification)
						return applySkill(model, (ValueModification) mod);
					else
						return applySkill(model, mod);
				case SKILL_SPECIAL: return applySkillSpecialization(model, mod);
				case ITEM_ATTRIBUTE:
//				case ACTION:
//					model.addItemModification(mod); return true;
				default:
					logger.log(Level.WARNING, "Don't know how to apply "+tmp.getReferenceType()+" of "+tmp);
					System.err.println("ApplyModifications: Don't know how to apply "+tmp.getReferenceType()+" of "+tmp);
				}
			} else
				logger.log(Level.WARNING, "Don't know how to apply {0}", tmp.getClass());
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error applying "+tmp+" from "+tmp.getSource(),e);
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "ENTER process");
		List<Modification> unprocessed = new ArrayList<>();

		try {
			// Walk modifications for creation points
			for (Modification tmp : previous) {
				logger.log(Level.DEBUG, "process "+tmp+" / "+tmp.getApplyTo());
				if (tmp instanceof AllowModification) {
					unprocessed.add(tmp);
//				} else if (tmp instanceof DataItemModification) {
//					DataItemModification mod = (DataItemModification)tmp;
				} else if (tmp.getApplyTo()==ApplyTo.CHARACTER || tmp.getApplyTo()==ApplyTo.UNARMED) {
					if (!applyAttribute(model, (ValueModification) tmp)) {
						unprocessed.add(tmp);
					}
				} else if (tmp instanceof DataItemModification) {
					switch ((SplittermondReference)tmp.getReferenceType()) {
					case ATTRIBUTE:
					case GEAR:
					case MASTERSHIP:
					case RESOURCE:
					case POWER:
					case SKILL:
					case SKILL_SPECIAL:
						if (!applyModification(model, tmp)) {
							unprocessed.add(tmp);
						}
						break;
					default:
						logger.log(Level.WARNING, "Don't know how to apply {0}",tmp);
						unprocessed.add(tmp);
					}
				} else {
					logger.log(Level.WARNING, "Don't know how to apply {0}",tmp);
					unprocessed.add(tmp);
				}
			}
			return unprocessed;
		} finally {
			if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "LEAVE process");
		}
	}
	// -------------------------------------------------------------------
	private static boolean applyAttribute(SpliMoCharacter model, ValueModification mod) {
		Attribute item = null;
		if ("CHOICE".equals(mod.getKey())) {
			UUID uuid = mod.getConnectedChoice();
			Decision dec = mod.getDecision(uuid);
			item = mod.getReferenceType().resolve(dec.getValue());
		} else {
			try {
				item = mod.getReferenceType().resolve(mod.getKey());
			} catch (ReferenceException e) {
				System.err.println("ApplyModifications: "+e.getContext()+": "+e);
			}
		}
		if (item == null) {
			logger.log(Level.ERROR, "Cannot apply modification " + mod + " - no such attribute {0}", mod.getKey());
		}
		AttributeValue<Attribute> value = model.getAttribute(item);
		if (value == null) {
			logger.log(Level.ERROR, "No value for attribute "+item);
			return false;
		}
		if (mod.getSet()==ValueType.MAX)
			return false;


		value.addIncomingModification(mod);
		if (!(mod instanceof CheckModification)) {
			logger.log(Level.INFO, "Added {0} to attribute {1} ({2}) from {3}", mod.getValue(), item, mod.getSet(), mod.getSource());
		}

		return true;
	}
	// -------------------------------------------------------------------
	private static boolean applyGear(SpliMoCharacter model, DataItemModification mod) {
		ItemTemplate item = SplitterMondCore.getItem(ItemTemplate.class, mod.getKey());
		// Check if item is a geardef
		if (item==null) {
			item = model.getGearDefinition(mod.getKey());
			if (item!=null) {
				logger.log(Level.DEBUG, "Resolved gear {0} from definitions in character", mod.getKey());
			}
		}
		logger.log(Level.DEBUG, "applyGear {0}",mod);
//		SR6PieceOfGearVariant variant = null;
//		if (mod.getVariant()!=null) {
//			variant = (SR6PieceOfGearVariant) item.getVariant(mod.getVariant());
//		}
//		CarryMode carry = CarryMode.CARRIED;
//		if (!item.getUsages().isEmpty()) {
//			carry = item.getUsages().get(0).getMode();
//		}
//		if (variant!=null && !variant.getUsages().isEmpty()) {
//			carry = variant.getUsages().get(0).getMode();
//		}
//		Decision[] dec = mod.getDecisions().toArray(new Decision[mod.getDecisions().size()]);
//		OperationResult<CarriedItem<ItemTemplate>> result = SR6GearTool.buildItem(item, carry, variant, model, false, dec);
//		if (result.hasError()) {
//			logger.log(Level.ERROR, "Failed creating {0}/{1}/{2}: {3}", mod.getKey(), mod.getVariant(), carry, result.getError());
//			return false;
//		}
//		if (mod.getId()!=null)
//			result.get().setUuid(mod.getId());
//		result.get().setInjectedBy(mod.getSource());
//		result.get().addModification(mod);
//
//		if (mod instanceof EmbedModification) {
//			EmbedModification embed = (EmbedModification)mod;
//			CarriedItem<ItemTemplate> target = model.getCarriedItem(embed.getIntoId());
//			if (target==null) {
//				logger.log(Level.WARNING, "<embed> specifies unknown item {0}", embed.getIntoId());
//				return false;
//			}
//			AvailableSlot slot = target.getSlot(embed.getHook());
//			if (slot==null) {
//				logger.log(Level.WARNING, "<embed> specifies unknown slot {0} in item {1}", embed.getHook(), target);
//				return false;
//			}
//			logger.log(Level.ERROR, "Put item {0} into slot {1} of {2}", result.get(), embed.getHook(), target);
//			target.addAccessory(result.get(), embed.getHook());
//			slot.addEmbeddedItem(target);
//		} else {
//			logger.log(Level.DEBUG, "Put item in inventory: {0}   (from {1})", result.get(), mod.getSource());
//			model.addVirtualCarriedItem(result.get());
//		}
		return true;
	}

	//-------------------------------------------------------------------
	private static boolean applyPower(SpliMoCharacter model, DataItemModification mod) {
		Power item = SplitterMondCore.getItem(Power.class, mod.getKey());
		if (item == null) {
			logger.log(Level.ERROR, "Cannot apply modification " + mod + " - no such power {0}", mod.getKey());
			return false;
		}

		logger.log(Level.DEBUG, "Add {0} with decisions {1}",mod,mod.getDecisions());
		PowerValue value = model.getPower(mod.getKey());
		if (value == null) {
			value = new PowerValue(item, 0);
			value.setInjectedBy(mod.getSource());
			value.addIncomingModification(mod);
			// Handle decisions
			for (Decision dec : mod.getDecisions()) {
				value.addDecision(dec);
				logger.log(Level.INFO, "Add decision {0} to quality {1}", dec, item);
			}

			model.addPower(value);
			logger.log(Level.INFO, "Add power {0} to character", item);
		} else {
			logger.log(Level.DEBUG, "Add {0} to existing power",mod);
			value.addIncomingModification(mod);
		}
		// Mark modifications from Race as non-removeable
		if (mod.getSource()!=null && mod.getSource() instanceof Race)
			value.setFixed(true);
		else
			value.setFixed(false);
		// Mark as auto-added
		value.addIncomingModification(mod);

		if (item.hasLevel()) {
			logger.log(Level.DEBUG, " Level is now distr={0}   mod={1} = " + value.getDistributed(), value.getModifier(),
					value.getModifier());
			logger.log(Level.DEBUG, "  result=" + value);
		}
		return true;
	}

	//-------------------------------------------------------------------
	private static boolean applyResource(SpliMoCharacter model, DataItemModification mod) {
		Resource item = SplitterMondCore.getItem(Resource.class, mod.getKey());
		if (item == null) {
			logger.log(Level.ERROR, "Cannot apply modification " + mod + " - no such resource {0}", mod.getKey());
			return false;
		}

		logger.log(Level.ERROR, "Add {0} with decisions {1}",mod,mod.getDecisions());
		ResourceValue value = model.getResource(mod.getKey());
		if (value == null) {
			value = new ResourceValue(item, 0);
			value.setInjectedBy(mod.getSource());
			value.addIncomingModification(mod);
			// Handle decisions
			for (Decision dec : mod.getDecisions()) {
				value.addDecision(dec);
				logger.log(Level.INFO, "Add decision {0} to resource {1}", dec, item);
			}

			model.addResource(value);
			logger.log(Level.INFO, "Add resource {0} to character", item);
		} else {
			logger.log(Level.DEBUG, "Add {0} to existing resource",mod);
			value.addIncomingModification(mod);
		}
		// Mark as auto-added
		value.addIncomingModification(mod);

		if (item.hasLevel()) {
			logger.log(Level.DEBUG, " Level is now distr={0}   mod={1} = " + value.getDistributed(), value.getModifier(),
					value.getModifier());
			logger.log(Level.DEBUG, "  result=" + value);
		}
		return true;
	}

	// -------------------------------------------------------------------
	private static boolean applySkill(SpliMoCharacter model, ValueModification mod) {
		SMSkill item = mod.getReferenceType().resolve(mod.getKey());
		if (item == null) {
			logger.log(Level.ERROR, "Cannot apply modification " + mod + " from {1} - no such skill {0}", mod.getKey(),mod.getSource());
		}
		SMSkillValue value = model.getSkillValue(item);
		if (value == null) {
			logger.log(Level.WARNING, "applySkill for skill unset: "+mod.getKey());
			return false;
		}

		value.addIncomingModification(mod);
		logger.log(Level.INFO, "Added {0} (now {4}) to skill {1} ({2}) from {3}", mod.getValue(), item, mod.getSet(), mod.getSource(), value.getModifiedValue());

		return true;
	}

	// -------------------------------------------------------------------
	private static boolean applySkill(SpliMoCharacter model, DataItemModification mod) {
		SMSkill item = mod.getReferenceType().resolve(mod.getKey());
		// Before adding a new skill, check if it already exists
		if (mod.getId()!=null) {
			SMSkillValue value = model.getSkillValue(mod.getId());
			if (value!=null)
				return true;
		}

		SMSkillValue value = new SMSkillValue(item, 1);
		if (mod.getId()!=null)
			value.setUuid(mod.getId());
		value.addIncomingModification(mod);
		logger.log(Level.ERROR, "Added skill {0} (from {1})",  item, mod.getSource());
		return true;
	}

	// -------------------------------------------------------------------
	private static boolean applyMastership(SpliMoCharacter model, DataItemModification mod) {
		Mastership master = mastershipConverter.read(mod.getKey());
		if (master==null) {
			logger.log(Level.ERROR, "Found unknown mastership in modification {0}", mod);
			return false;
		}
		SMSkill skill = master.getSkill();
		if (master.isCommon() && mod.getKey().indexOf('/')>=0) {
			String skillID = mod.getKey().substring(0, mod.getKey().indexOf('/'));
			skill = SplitterMondCore.getSkill(skillID);
		}

		SMSkillValue value = model.getSkillValue(skill);
		if (value == null) {
			value = new SMSkillValue(skill, 0);
			if (mod.getId()!=null)
				value.setUuid(mod.getId());
			model.addSkillValue(value);
		}
		value.addMastership(new MastershipReference(master, skill));
		logger.log(Level.INFO, "Added mastersip {0} to skill {1} (from {2})",  master.getId(), skill.getId(), mod.getSource());
		return true;
	}

	// -------------------------------------------------------------------
	private static boolean applySkillSpecialization(SpliMoCharacter model, DataItemModification mod) {
		SMSkillSpecialization item = mod.getReferenceType().resolve(mod.getKey());
		SMSkill skill = item.getSkill();
		SMSkillValue value = model.getSkillValue(skill);
		SkillSpecializationValue<SMSkill> val = value.getSpecialization(item);
		if (val==null) {
			value.setSpecializationLevel(item, 1);
			logger.log(Level.ERROR, "Added skill specialization {0} (from {1})",  item, mod.getSource());
			val = value.getSpecialization(item);
			if (!mod.getDecisions().isEmpty()) {
				logger.log(Level.WARNING, "ToDo: Add decicions {0} to skill specialization {1}",  mod.getDecisions(), val);
//			val.getDecisions().addAll(mod.getDecisions());
			}
			val.setInjectedBy(mod.getSource());
			return true;
		} else {
			logger.log(Level.ERROR, "Trying to set skill specialization, but it is already set to {0}", val);
//			System.exit(1);
			return false;
		}
	}

}

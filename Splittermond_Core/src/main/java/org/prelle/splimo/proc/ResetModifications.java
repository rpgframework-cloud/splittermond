package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.splittermond.log.Logging;

/**
 * @author prelle
 *
 */
public class ResetModifications implements ProcessingStep {

	private final static Logger logger = System.getLogger(ResetModifications.class.getPackageName()+".reset");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	/**
	 */
	public ResetModifications(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		logger.log(Level.DEBUG, "Reset modifications");

		model.clearAutoCultureLore();
		model.clearAutoLanguage();

		// Attributes
		for (AttributeValue<Attribute> val : model.getAttributes()) {
			val.clearIncomingModifications();
			if (val.getModifyable()==Attribute.SIZE)
				val.setDistributed(0);
		}
		// Skills
		for (SMSkillValue val : model.getSkillValues()) {
			val.clearIncomingModifications();
		}

		// Remove all auto-added items
		for (CarriedItem<ItemTemplate> item : model.getCarriedItems()) {
			if (item.isAutoAdded()) {
				model.removeCarriedItem(item);
			}
		}
		// Powers
		model.getPowers().forEach(ev -> {
			logger.log(Level.INFO, "Check {0}: fixed={1}  auto={2}", ev, ev.isFixed(), ev.isAutoAdded());
			if (ev.isFixed()) {
				// From race
				// May need to treat powers with level (Hoher geister GW von Gnomen)
				if (ev.getResolved().hasLevel()) {
					ev.clearIncomingModifications();
				} else {
					logger.log(Level.DEBUG, "Remove race-power ''{0}'' to re-add them later", ev.getKey());
					model.removePower(ev);
				}
			} else if (ev.isAutoAdded()) {
				if (ev.getResolved().hasLevel()) {
					ev.clearIncomingModifications();
				} else {
					logger.log(Level.DEBUG, "Remove auto-added ''{0}'' to re-add them later", ev.getKey());
					model.removePower(ev);
				}
			} else {
				logger.log(Level.DEBUG, "Keep power ''{0}''", ev.getKey());
			}
		});
		// Resources
		model.getResources().forEach(val -> {
			if (val.isAutoAdded()) {
//				if (!val.getModifications().isEmpty())
//					Logging.logger.log(Level.INFO, "Reset modifications for "+val);
				if (val.getDistributed()==0 && !val.getResolved().isBaseResource())
					model.removeResource(val);
			}
			val.clearIncomingModifications();
		});
		if (model.getResource("wealth")==null) model.addResource(new ResourceValue(SplitterMondCore.getItem(Resource.class, "wealth"),0));
		if (model.getResource("status")==null) model.addResource(new ResourceValue(SplitterMondCore.getItem(Resource.class, "status"),0));
		if (model.getResource("contacts")==null) model.addResource(new ResourceValue(SplitterMondCore.getItem(Resource.class, "contacts"),0));

		return unprocessed;
	}

}

package org.prelle.splimo.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Race;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromRace implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromRace.class.getPackageName()+".race");

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public GetModificationsFromRace(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.SpliMoCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {
			// Apply modifications by metatype
			if (model.getRace()!=null) {
				Race race = SplitterMondCore.getItem(Race.class, model.getRace());
				//updateChoices(race);
				logger.log(Level.DEBUG, "1. Apply modifications from race "+model.getRace()+" = "+race.getOutgoingModifications());
				for (Modification tmp : race.getOutgoingModifications()) {
					if (tmp.getSource()==null)
						tmp.setSource(race);


					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						if (tmp.getReferenceType()==SplittermondReference.ATTRIBUTE && model.isInCareerMode() && !dMod.getKey().equals("SIZE")) {
							// Ignore modifications from open decisions
							continue;
						}
						// Does this modification require a choice?
						if (dMod.getConnectedChoice()!=null) {
							Choice choice = race.getChoice(dMod.getConnectedChoice());
							if (choice==null) {
								logger.log(Level.ERROR, "Modification "+dMod+" of "+race.getTypeString()+" references unknown modification "+dMod.getConnectedChoice());
								continue;
							}
							Decision dec = model.getDecision(dMod.getConnectedChoice());
							if (dec==null) {
								// User has not yet decided
								logger.log(Level.ERROR, "No decision yet for choice "+dMod.getConnectedChoice());
							} else {
								// User already made a decision
								List<Modification> list = GenericRPGTools.decisionToModifications(dMod, choice, dec);
								logger.log(Level.ERROR, "Convert decision of "+choice.getUUID()+" into "+list.size()+" modifications");
								unprocessed.addAll(list);
							}
							continue;
						}
					}
					logger.log(Level.INFO, "Inject modification {0}",tmp);
					unprocessed.add(tmp);
				}
			} else {
				logger.log(Level.WARNING, "Metatype not selected yet");
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

/**
 * @author Stefan Prelle
 *
 */
open module splittermond.generators {

	requires transitive de.rpgframework.core;
	requires transitive splittermond.core;
	requires simple.persist;
	requires java.prefs;
	requires de.rpgframework.rules;
	requires de.rpgframework.generator;

	provides de.rpgframework.random.GeneratorInitializer with org.prelle.splittermond.generators.SplittermondGeneratorIntializer;

}
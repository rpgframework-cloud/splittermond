/**
 *
 */
package org.prelle.splittermond.generators;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;

import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
@Root(name="species")
public enum FilterSpecies implements Classification<FilterSpecies> {

	HUMAN,
	DWARF,
	VARG,
	ELF,
	GNOME,
	RATTLING
	;

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("filter.species");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "filter.species";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return SplittermondTaxonomy.SPECIES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public FilterSpecies getValue() {
		return this;
	}

}

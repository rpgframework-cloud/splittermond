package org.prelle.splittermond.generators;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.prelle.splimo.SpliMoNameTable;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.Gender;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.TagWildness;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.GeneratorVariable;
import de.rpgframework.random.RandomGenerator;

/**
 *
 */
public class NameGenerator implements RandomGenerator, GenericClassificationType, SplittermondTaxonomy {

	private final static Logger logger = System.getLogger(NameGenerator.class.getPackageName());

	//-------------------------------------------------------------------
	/**
	 */
	public NameGenerator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.NAME_PERSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		if (filter.getType()==CULTURE) {
			return SplitterMondCore.getItem(SpliMoNameTable.class, (String)filter.getValue())!=null;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(Classification<?> filter) {
		if (filter.getType()==GENDER) {
			return true;
		}
		if (filter==GenericClassificationType.WILDNESS) {
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate(java.util.Map, de.rpgframework.classification.Classification[])
	 */
	@Override
	public Object generate(Map<GeneratorVariable, Integer> variables, Classification<?>... classifier) {
		String cultID = null;
		Gender gender = Gender.MALE;
		SpliMoNameTable.OPTIONS options = null;
		for (Classification<?> cls : classifier) {
			if (cls.getType()==CULTURE) {
				cultID = (String)cls.getValue();
			} else if (cls.getType()==GENDER) {
				gender = ((Gender)cls.getValue());
			} else if (cls.getType()==WILDNESS) {
				TagWildness wild =  (TagWildness)cls.getValue();
				switch (wild) {
				case URBAN: options=SpliMoNameTable.OPTIONS.TOWN; break;
				default:
					options=SpliMoNameTable.OPTIONS.RURAL;
				}
			}
		}
		if (cultID==null) return null;
		SpliMoNameTable table = SplitterMondCore.getItem(SpliMoNameTable.class, cultID);
		if (table==null) return null;

		String name = table.generateName(gender, options);
		logger.log(Level.INFO, "Rolled name: "+name);
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of(GenericClassificationType.RULES,SplittermondTaxonomy.CULTURE);
	}

}

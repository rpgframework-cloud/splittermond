package org.prelle.splittermond.generators;

import java.util.List;

import de.rpgframework.random.GeneratorInitializer;
import de.rpgframework.random.RandomGenerator;

/**
 *
 */
public class SplittermondGeneratorIntializer implements GeneratorInitializer {

	private static List<RandomGenerator> generatorsToRegister;

	//-------------------------------------------------------------------
	public SplittermondGeneratorIntializer() {
		generatorsToRegister = List.of(
				new NameGenerator()
		);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorInitializer#getGeneratorsToRegister()
	 */
	@Override
	public List<RandomGenerator> getGeneratorsToRegister() {
		return generatorsToRegister;
	}

}

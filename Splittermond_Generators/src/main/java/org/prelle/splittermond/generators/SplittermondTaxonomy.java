package org.prelle.splittermond.generators;

import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
public interface SplittermondTaxonomy {

	final static MultiLanguageResourceBundle RES = SplitterMondCore.getI18nResources();

	public final static ClassificationType CULTURE = new ClassificationType("CULTURE", RES);
	public final static ClassificationType SPECIES = new ClassificationType("SPECIES", RES);

}

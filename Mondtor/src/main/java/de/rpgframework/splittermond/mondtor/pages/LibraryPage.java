package de.rpgframework.splittermond.mondtor.pages;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.Power;
import org.prelle.splimo.Race;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.jfx.creatures.LifeformPane;
import org.prelle.splittermond.chargen.jfx.pane.ItemTemplatePane;
import org.prelle.splittermond.chargen.jfx.pane.SpellDescriptionPane;

import de.rpgframework.ResourceI18N;
import de.rpgframework.jfx.FilteredListPage;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.splittermond.mondtor.FilterCreatures;
import de.rpgframework.splittermond.mondtor.FilterItems;
import de.rpgframework.splittermond.mondtor.FilterMasterships;
import de.rpgframework.splittermond.mondtor.FilterSpells;
import de.rpgframework.splittermond.mondtor.cells.CreatureListCell;
import de.rpgframework.splittermond.mondtor.cells.SpellListCell;
import de.rpgframework.splittermond.mondtor.panes.SpeciesPane;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

/**
 * @author prelle
 *
 */
public class LibraryPage extends Page {

	public final static ResourceBundle RES = ResourceBundle.getBundle(LibraryPage.class.getName());

	private final static Logger logger = System.getLogger(LibraryPage.class.getPackageName());

	private FlowPane content;
	private Button btnCreatFeat;
	private Button btnCreatures;
	private Button btnSpells;
	private Button btnMasterships;
	private Button btnPowers;
	private Button btnItems;
	private Button btnItemFeat;
	private Button btnSpecies;

	//-------------------------------------------------------------------
	public LibraryPage() {
		super(ResourceI18N.get(RES,"page.title"));

		btnCreatFeat = new Button(ResourceI18N.get(RES, "category.creatfeat"));
		btnCreatures = new Button(ResourceI18N.get(RES, "category.creatures"));
		btnSpells    = new Button(ResourceI18N.get(RES, "category.spells"));
		btnMasterships = new Button(ResourceI18N.get(RES, "category.masterships"));
		btnPowers    = new Button(ResourceI18N.get(RES, "category.powers"));
		btnItems     = new Button(ResourceI18N.get(RES, "category.items"));
		btnItemFeat  = new Button(ResourceI18N.get(RES, "category.itemfeat"));
		btnSpecies   = new Button(ResourceI18N.get(RES, "category.species"));

		btnCreatFeat.setId("creature-features");
		btnCreatures.setId("creatures");
		btnSpells.setId("spells");
		btnMasterships.setId("masterships");
		btnPowers.setId("powers");
		btnItems.setId("items");
		btnItemFeat.setId("item-features");
		btnSpecies.setId("species");

		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new FlowPane(btnCreatFeat, btnCreatures, btnSpells, btnMasterships, btnPowers, btnItems, btnItemFeat, btnSpecies);
		content.setVgap(10);
		content.setHgap(10);
		content.setId("categories");
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		ChangeListener<Node> scaleButtons = (ov,o,n) -> {
			if (n!=null && (n instanceof ImageView)) {
				((ImageView)n).setFitHeight(60);
				((ImageView)n).setPreserveRatio(true);
			}
		};

		content.getChildren().forEach(node -> {
			node.getStyleClass().add("category-button");
			((Button)node).graphicProperty().addListener( scaleButtons);
		});
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnCreatFeat.setOnAction(ev -> openCreatureFeatures(ev));
		btnCreatures.setOnAction(ev -> openCreatures(ev));
		btnSpells.setOnAction(ev -> openSpells(ev));
		btnMasterships.setOnAction(ev -> openMasterships(ev));
		btnPowers.setOnAction(ev -> openPowers(ev));
		btnItems.setOnAction(ev -> openItems(ev));
		btnItemFeat.setOnAction(ev -> openItemFeatures(ev));
		btnSpecies.setOnAction(ev -> openSpecies(ev));
	}

	//-------------------------------------------------------------------
	private void openCreatureFeatures(ActionEvent ev) {
		logger.log(Level.INFO, "Navigate CreatureFeature");
		try {
			FilteredListPage<CreatureFeature> page =new FilteredListPage<CreatureFeature>(
					ResourceI18N.get(LibraryPage.RES, "category.creatfeat"),
					() -> SplitterMondCore.getItemList(CreatureFeature.class),
					new GenericDescriptionVBox<CreatureFeature>(SplitterTools.requirementResolver(Locale.getDefault()), SplitterTools.modificationResolver(Locale.getDefault()))
					);
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening CreatureFeaturePage",e);
		}
	}

	//-------------------------------------------------------------------
	private void openCreatures(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Creatures");
		try {
			FilteredListPage<Creature> page =new FilteredListPage<Creature>(
					ResourceI18N.get(LibraryPage.RES, "category.creatures"),
					() -> SplitterMondCore.getItemList(Creature.class),
					new LifeformPane<Creature>(false)
					);
			page.setCellFactory(lv -> new CreatureListCell());
			page.setFilterInjector(new FilterCreatures());
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening PowersPage",e);
		}
	}

	//-------------------------------------------------------------------
	private void openSpells(ActionEvent ev) {
		logger.log(Level.INFO, "Navigate Spells");
		try {
			FilteredListPage<Spell> page =new FilteredListPage<Spell>(
					ResourceI18N.get(LibraryPage.RES, "category.spells"),
					() -> SplitterMondCore.getItemList(Spell.class),
					new SpellDescriptionPane()
					);
			page.setCellFactory(lv -> new SpellListCell());
			page.setFilterInjector(new FilterSpells());
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening SpellPage",e);
		}
	}

	//-------------------------------------------------------------------
	private void openMasterships(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Masterships");
		try {
			FilteredListPage<Mastership> page =new FilteredListPage<Mastership>(
					ResourceI18N.get(LibraryPage.RES, "category.masterships"),
					() -> SplitterMondCore.getItemList(Mastership.class),
					new GenericDescriptionVBox<Mastership>(SplitterTools.requirementResolver(Locale.getDefault()), SplitterTools.modificationResolver(Locale.getDefault()))
					);
//			page.setRequirementResolver();
			page.setFilterInjector(new FilterMasterships());
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening MastershipPage",e);
		}
	}

	//-------------------------------------------------------------------
	private void openPowers(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Powers");
		try {
			FilteredListPage<Power> page =new FilteredListPage<Power>(
					ResourceI18N.get(LibraryPage.RES, "category.powers"),
					() -> SplitterMondCore.getItemList(Power.class),
					new GenericDescriptionVBox<Power>(SplitterTools.requirementResolver(Locale.getDefault()), SplitterTools.modificationResolver(Locale.getDefault()))
					);
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening PowersPage",e);
		}
	}

	//-------------------------------------------------------------------
	private void openItems(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Items");
		try {
			FilteredListPage<ItemTemplate> page =new FilteredListPage<ItemTemplate>(
					ResourceI18N.get(LibraryPage.RES, "category.items"),
					() -> SplitterMondCore.getItemList(ItemTemplate.class),
					new ItemTemplatePane(SplitterTools.requirementResolver(Locale.getDefault()), null)
					);
			page.setFilterInjector(new FilterItems());
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening Items",e);
		}
	}

	//-------------------------------------------------------------------
	private void openItemFeatures(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Item Features");
		try {
			FilteredListPage<FeatureType> page =new FilteredListPage<FeatureType>(
					ResourceI18N.get(LibraryPage.RES, "category.itemfeat"),
					() -> SplitterMondCore.getItemList(FeatureType.class),
					new GenericDescriptionVBox<FeatureType>(SplitterTools.requirementResolver(Locale.getDefault()), SplitterTools.modificationResolver(Locale.getDefault()))
					);
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening Item Features",e);
		}
	}

	//-------------------------------------------------------------------
	private void openSpecies(ActionEvent ev) {
		logger.log(Level.DEBUG, "Navigate Species");
		try {
			FilteredListPage<Race> page =new FilteredListPage<Race>(
					ResourceI18N.get(RES, "category.species"),
					() -> SplitterMondCore.getItemList(Race.class),
					new SpeciesPane()
					);
			getAppLayout().getApplication().openScreen(new ApplicationScreen(page));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error opening SpeciesPage",e);
		}
	}
}

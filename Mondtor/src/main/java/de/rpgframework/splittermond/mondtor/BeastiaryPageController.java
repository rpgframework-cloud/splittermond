package de.rpgframework.splittermond.mondtor;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.creature.CreatureTypeValue;

import de.rpgframework.splittermond.mondtor.cells.CreatureListCell;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.util.StringConverter;

public class BeastiaryPageController {

	private Logger logger = System.getLogger(BeastiaryPageController.class.getPackageName());

	private transient Page page;

	@FXML
	private Label filterText;
	@FXML
	private TextField search;
	@FXML
	private FlowPane filterPane;
	@FXML
	private ListView<Creature> lvResult;

	private ChoiceBox<CreatureType> cbType;
	private ChoiceBox<CreatureFeature> cbFeature;

	//-------------------------------------------------------------------
	public BeastiaryPageController() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		/*
		 * Creature Types
		 */
		cbType = new ChoiceBox<CreatureType>();
		List<CreatureType> availTypes = new ArrayList<CreatureType>();
		for (Creature crea : SplitterMondCore.getCreatures()) {
			for (CreatureTypeValue type : crea.getCreatureTypes()) {
				if (!availTypes.contains(type.getType()))
					availTypes.add(type.getType());
			}
		}
		cbType.getItems().addAll(availTypes);
		Collections.sort(cbType.getItems(), new Comparator<CreatureType>() {
			public int compare(CreatureType o1, CreatureType o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.setConverter(new StringConverter<CreatureType>() {
			public String toString(CreatureType val) {
				if (val==null) return "jeder Art";
				return val.getName();
			}
			public CreatureType fromString(String string) { return null; }
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshList());
		filterPane.getChildren().add(cbType);

		/*
		 * Create Feature
		 */
		cbFeature = new ChoiceBox<CreatureFeature>();
		cbFeature.getItems().add(null);
		cbFeature.getItems().addAll(SplitterMondCore.getItemList(CreatureFeature.class));
		cbFeature.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshList());
		cbFeature.setConverter(new StringConverter<CreatureFeature>() {
			public String toString(CreatureFeature val) {
				if (val==null) return "kein besonderes Merkmal";
				return val.getName();
			}
			public CreatureFeature fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbFeature);


		// React to double clicks on creatures
		lvResult.setCellFactory(lv -> {
			CreatureListCell cell = new CreatureListCell();
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2) {
					showAction(cell.getItem());
				}
			});
			return cell;
			});
		// React to list selections
		lvResult.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				showAction(n);
		});

		refreshList();
	}

	//-------------------------------------------------------------------
	private void refreshList() {
		/*
		 * Update filter text
		 */
		List<String> filterNames = new ArrayList<>();
		if (cbFeature.getValue()!=null) {
			filterNames.add(cbFeature.getConverter().toString(cbFeature.getValue()));
		}
		if (cbType.getValue()!=null) {
			filterNames.add(cbType.getConverter().toString(cbType.getValue()));
		}
		filterText.setText(String.join(", ", filterNames));


		List<Creature> list = SplitterMondCore.getCreatures();
		// Match creature type
		if (cbType.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getCreatureTypes().stream()
							.map(ctv -> ctv.getType())
							.collect(Collectors.toList())
							.contains(cbType.getValue()))
					.collect(Collectors.toList());
		}
		// Match creature feature
		if (cbFeature.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getFeatures().stream()
							.map(ctv -> ctv.getResolved())
							.collect(Collectors.toList())
							.contains(cbFeature.getValue()))
					.collect(Collectors.toList());
		}

		Collections.sort(list, new Comparator<Creature>() {
			public int compare(Creature o1, Creature o2) {
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		lvResult.getItems().setAll(list);
	}


	//-------------------------------------------------------------------
	private void showAction(Creature value) {
		logger.log(Level.INFO,"Show creature "+value);

		StandardCreaturePage toOpen = new StandardCreaturePage(value);
		page.getAppLayout().getApplication().openScreen(new ApplicationScreen(toOpen));
	}

	//-------------------------------------------------------------------
	public void setComponent(Page page) {
		this.page = page;
	}

}

package de.rpgframework.splittermond.mondtor;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterMasterships extends AFilterInjector<Mastership> {

	private ChoiceBox<SMSkill> cbSkill;
	private ChoiceBox<Integer> cbLevel;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		cbSkill = new ChoiceBox<SMSkill>();
		List<SMSkill> availSchools = new ArrayList<SMSkill>();
		availSchools.add(null);
		availSchools.addAll(SplitterMondCore.getItemList(SMSkill.class));
		cbSkill.getItems().addAll(availSchools);
		Collections.sort(cbSkill.getItems(), new Comparator<SMSkill>() {
			public int compare(SMSkill o1, SMSkill o2) {
				if (o1==null) return -1;
				if (o2==null) return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbSkill.setConverter(new StringConverter<SMSkill>() {
			public String toString(SMSkill val) {
				if (val==null) return "alle Fertigkeiten";
				return val.getName();
			}
			public SMSkill fromString(String string) { return null; }
		});
		cbSkill.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		filterPane.getChildren().add(cbSkill);

		/*
		 * Mastership level
		 */
		cbLevel = new ChoiceBox<Integer>();
		cbLevel.getItems().add(null);
		cbLevel.getItems().addAll(1,2,3,4);
		cbLevel.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbLevel.setConverter(new StringConverter<Integer>() {
			public String toString(Integer val) {
				if (val==null) return "alle Grade";
				return String.valueOf(val);
			}
			public Integer fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbLevel);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(de.rpgframework.jfx.FilteredListPage, java.util.List)
	 */
	@Override
	public List<Mastership> applyFilter(List<Mastership> input) {
		System.getLogger("mondtor").log(Level.INFO,"ApplyFilter: "+cbSkill.getValue()+" for "+input.size()+" elements");
		if (cbSkill.getValue()!=null) {
			List<Mastership> list = new ArrayList<>();
			for (SMSkill skill : SplitterMondCore.getItemList(SMSkill.class)) {
				if ( (cbSkill.getValue()==null) || (cbSkill.getValue()==skill)) {
					for (Mastership m : skill.getMasterships()) {
						if (!list.contains(m))
							list.add(m);
					}

				}
			}
			System.getLogger("mondtor").log(Level.INFO,"ApplyFilter: returns "+list.size()+" elements");
			input.retainAll(list);
		}
		System.getLogger("mondtor").log(Level.INFO,"ApplyFilter: before grade: "+input.size()+" elements");
		// Match filter level
		if (cbLevel.getValue()!=null) {
			input = input.stream()
					.filter(data -> data.getLevel()==cbLevel.getValue())
					.collect(Collectors.toList());
		}
		System.getLogger("mondtor").log(Level.INFO,"ApplyFilter: after grade: "+input.size()+" elements");
		return input;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<Mastership> available) {
		// TODO Auto-generated method stub

	}

}

package de.rpgframework.splittermond.mondtor;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterSpells extends AFilterInjector<Spell> {

	private ChoiceBox<SMSkill> cbSchool;
	private ChoiceBox<SpellType> cbType;
	private ChoiceBox<Integer> cbLevel;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		cbSchool = new ChoiceBox<SMSkill>();
		List<SMSkill> availSchools = new ArrayList<SMSkill>();
		availSchools.add(null);
		availSchools.addAll(SplitterMondCore.getSkills(SkillType.MAGIC));
		cbSchool.getItems().addAll(availSchools);
		Collections.sort(cbSchool.getItems(), new Comparator<SMSkill>() {
			public int compare(SMSkill o1, SMSkill o2) {
				if (o1==null) return -1;
				if (o2==null) return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbSchool.setConverter(new StringConverter<SMSkill>() {
			public String toString(SMSkill val) {
				if (val==null) return "alle Zauberschulen";
				return val.getName();
			}
			public SMSkill fromString(String string) { return null; }
		});
		cbSchool.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		filterPane.getChildren().add(cbSchool);

		/*
		 * Spell Types
		 */
		cbType = new ChoiceBox<SpellType>();
		cbType.getItems().add(null);
		cbType.getItems().addAll(SpellType.values());
		Collections.sort(cbType.getItems(), new Comparator<SpellType>() {
			public int compare(SpellType o1, SpellType o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbType.setConverter(new StringConverter<SpellType>() {
			public String toString(SpellType val) {
				if (val==null) return "alle Typen";
				return val.getName();
			}
			public SpellType fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbType);

		/*
		 * Spell level
		 */
		cbLevel = new ChoiceBox<Integer>();
		cbLevel.getItems().add(null);
		cbLevel.getItems().addAll(1,2,3,4);
		cbLevel.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbLevel.setConverter(new StringConverter<Integer>() {
			public String toString(Integer val) {
				if (val==null) return "alle Grade";
				return String.valueOf(val);
			}
			public Integer fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbLevel);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(java.util.List)
	 */
	@Override
	public List<Spell> applyFilter(List<Spell> input) {
		// Exist in school
		if (cbSchool.getValue()!=null) {
			input = input.stream()
					.filter(crea -> crea.getLevelInSchool(cbSchool.getValue())>-1)
					.collect(Collectors.toList());
		}
		// Match spell type
		if (cbType.getValue()!=null) {
			input = input.stream()
					.filter(crea -> crea.getTypes().contains(cbType.getValue()))
					.collect(Collectors.toList());
		}
		// Match spell level
		if (cbLevel.getValue()!=null) {
			input = input.stream()
					.filter(data -> data.hasSchoolWithLevel(cbLevel.getValue()))
					.collect(Collectors.toList());
		}
		return input;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<Spell> available) {
		// TODO Auto-generated method stub
		
	}

}

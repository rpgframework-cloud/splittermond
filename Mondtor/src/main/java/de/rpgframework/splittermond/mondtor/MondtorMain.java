package de.rpgframework.splittermond.mondtor;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.LogManager;
import java.util.stream.Collectors;

import org.prelle.javafx.BitmapIcon;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.NavigationPane;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.jfx.SpliMoCharactersOverviewPage;
import org.prelle.splittermond.chargen.jfx.page.SummonableCreatorPage;
import org.prelle.splittermond.export.standard.StandardPDFPlugin;
import org.prelle.splittermond.export.vintage.VintagePDFPlugin;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.StartupStep;
import de.rpgframework.eden.client.jfx.AccountPage;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.EdenSettings;
import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.genericrpg.export.ExportPluginRegistry;
import de.rpgframework.jfx.attach.PDFViewerConfig;
import de.rpgframework.splittermond.mondtor.pages.LibraryPage;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Screen;
import javafx.stage.Stage;


public class MondtorMain extends EdenClientApplication {

	final static ResourceBundle RES = ResourceBundle.getBundle(MondtorMain.class.getName());

	static PrintWriter out;

	private MenuItem navigSummon;

    //-------------------------------------------------------------------
    public static void main(String[] args) {
 		LogManager.getLogManager().reset();
		EdenSettings.setupDirectories("Mondtor");
		Locale.setDefault(EdenSettings.getPreferredLangauge("Mondtor"));
    	LicenseManager.storeGlobalLicenses(List.of("SPLITTERMOND/CORE/de","SPLITTERMOND/MAGIE/de"));
    	System.out.println("MondtorMain.main");
    	checkInit();
    	System.out.println("MondtorMain: Default locale = "+Locale.getDefault());
//    	List<String> keys = new ArrayList<String>();
//    	System.getProperties().keySet().forEach(k -> keys.add( (String)k));
//    	Collections.sort(keys);
//		for (String key : keys) {
//			if (key.startsWith("com.sun") || key.startsWith("java."))
//				continue;
//			System.out.println("PROP "+key+" \t= "+System.getProperties().getProperty(key));
//		}
//		for (String key : args) {
//			System.out.println("argument "+key);
//		}

       launch(args);
    }

    //-------------------------------------------------------------------
	public MondtorMain() {
		super(RoleplayingSystem.SPLITTERMOND, "Mondtor");
	   	checkInit();

		ExportPluginRegistry.register(new VintagePDFPlugin());
		ExportPluginRegistry.register(new StandardPDFPlugin());
	}

    //-------------------------------------------------------------------
	private static void checkInit() {
		try {
			if (out != null) {
				System.out.println("Already initialized");
				return;
			}
			Path logDir = EdenSettings.logDir;
			System.setProperty("logdir", logDir.toAbsolutePath().toString());
			System.out.println("Log directory = " + logDir.toAbsolutePath().toString());
			if (!Files.exists(logDir)) {
				System.out.println("Log dir does not exist");
				Files.createDirectories(logDir);
			}
			// Delete all files
			Files.newDirectoryStream(logDir).forEach(file -> {
				if (Files.isWritable(file)) {
					try {
						Files.delete(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			Path logFile = logDir.resolve("logfile.txt");
			out = new PrintWriter(new FileWriter(logFile.toFile()));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
	}

	//-------------------------------------------------------------------
	@Override
	protected List<StartupStep> getPreGUISteps() {
		List<StartupStep> merged = new ArrayList<>(super.getPreGUISteps());
		merged.add(new LoadSpliMoDataStep());
		return merged;
	}

	//-------------------------------------------------------------------
	@Override
	public List<StartupStep> getPostGUISteps() {
		List<StartupStep> merged = new ArrayList<>(super.getPostGUISteps());
		merged.add(new LoadSpliMoCharactersStep(this));
		return merged;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.client.jfx.EdenClientApplication#init()
	 */
	@Override
	public void init() {
		super.init();
	    //   loadData();
		for (String key : super.getParameters().getRaw()) {
			System.getLogger(EdenClientApplication.class.getPackageName()).log(Level.INFO,"parameter "+key);
		}
	}

    //-------------------------------------------------------------------
    /**
     * @throws IOException
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
	@Override
    public void start(Stage stage) throws Exception {
		// Set icons
		List<String> sizes = List.of("128","192","256");
		List<Image> images = sizes.stream()
						.map(s -> ("icons/Mondtor_" + s + ".png"))
						.map(s -> getClass().getResource(s).toExternalForm())
						.map(Image::new)
						.collect(Collectors.toList());
		stage.getIcons().addAll(images);

    	int prefWidth = Math.min( (int)Screen.getPrimary().getVisualBounds().getWidth(), 1600);
    	int prefHeight = Math.min( (int)Screen.getPrimary().getVisualBounds().getHeight(), 900);
    	System.out.println("Start with "+prefWidth+"x"+prefHeight);
		stage.setWidth(prefWidth);
		stage.setHeight(prefHeight);
    	int minWidth = Math.min( (int)Screen.getPrimary().getVisualBounds().getWidth(), 360);
    	int minHeight = Math.min( (int)Screen.getPrimary().getVisualBounds().getHeight(), 650);
		stage.setMinWidth(minWidth);
		stage.setMinHeight(minHeight);
		try {
			super.start(stage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        setStyle(stage.getScene(), FlexibleApplication.LIGHT_STYLE);
        stage.getScene().getStylesheets().add(de.rpgframework.jfx.Constants.class.getResource("css/rpgframework.css").toExternalForm());
        stage.getScene().getStylesheets().add(getClass().getResource("styles.css").toExternalForm());

       PDFViewerConfig.setPDFPathResolver( (id,lang) -> getPDFPathFor(RoleplayingSystem.SPLITTERMOND,id,lang));
       PDFViewerConfig.setEnabled( super.isPDFEnabled());

       getAppLayout().visibleProperty().addListener( (ov,o,n) -> {
       		logger.log(Level.INFO, "Visibility changed to "+n);
           ResponsiveControlManager.initialize(getAppLayout());
       });
		logger.log(Level.INFO, "LEAVE start (thread {0})", Thread.currentThread());
   }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.client.jfx.EdenClientApplication#loadData()
	 */
    @Override
	protected void loadData() {
    	logger.log(Level.INFO, "Loading data-------------------------------------");
	}

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getErrorDialogResourceBundle()
     */
    @Override
	protected ResourceBundle getErrorDialogResourceBundle() {
		return RES;
	}

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getErrorDialogImage()
     */
    @Override
	protected Image getErrorDialogImage() {
		return new Image(MondtorMain.class.getResourceAsStream("ErrorDialog.png"));
	}

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getWarningDialogImage()
     */
    @Override
	protected Image getWarningDialogImage() {
		return new Image(MondtorMain.class.getResourceAsStream("WarningDialog.png"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getInfoDialogImage()
	 */
    @Override
	protected Image getInfoDialogImage() {
		return new Image(MondtorMain.class.getResourceAsStream("InfoDialog.png"));
	}

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getUpdateDialogImage()
     */
    @Override
	protected Image getUpdateDialogImage() {
		return new Image(MondtorMain.class.getResourceAsStream("UpdateDialog.png"));
	}

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.eden.client.jfx.EdenClientApplication#getSecurityDialogImage()
     */
    @Override
	public Image getSecurityDialogImage() {
		return new Image(MondtorMain.class.getResourceAsStream("SecurityDialog.png"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.FlexibleApplication#populateNavigationPane(org.prelle.javafx.NavigationPane)
	 */
	@Override
	public void populateNavigationPane(NavigationPane drawer) {
		// Header
		Label header = new Label("Mondtor");
		BitmapIcon icoMondtor = new BitmapIcon(MondtorMain.class.getResource("Logo_small.png").toString());
		icoMondtor.setStyle("-fx-pref-width: 3em");
		header.setGraphic(icoMondtor);
		drawer.setHeader(header);

		// Items
		SymbolIcon icoLookup = new SymbolIcon("library");
		//FontIcon icoAbout   = new FontIcon("\uD83D\uDEC8");
		SymbolIcon icoAbout = new SymbolIcon("setting");
		FontIcon icoAccount = new FontIcon("\uE2AF");
		navigChars  = new MenuItem(ResourceI18N.get(RES, "navig.chars"), new SymbolIcon("people"));
		navigLookup = new MenuItem(ResourceI18N.get(RES, "navig.lookup"), new SymbolIcon("library"));
		navigAccount= new MenuItem(ResourceI18N.get(RES, "navig.account"), icoAccount);
		navigAbout  = new MenuItem(ResourceI18N.get(RES, "navig.about"), icoAbout);
		navigSummon = new MenuItem(ResourceI18N.get(RES, "navig.summon"), new SymbolIcon("contact"));
		navigChars  .setId("navig-chars");
		navigLookup .setId("navig-lookup");
		navigAbout  .setId("navig-about");
		navigAccount.setId("navig-account");

		drawer.getItems().addAll(navigChars, navigLookup,  navigAccount, navigAbout);

		// Footer
		Image img = new Image(MondtorMain.class.getResourceAsStream("Splittermond-Logo.png"));
		if (img!=null) {
			ImageView ivShadowrun = new ImageView(img);
			ivShadowrun.setId("footer-logo");
			ivShadowrun.setPreserveRatio(true);
			ivShadowrun.fitWidthProperty().bind(drawer.prefWidthProperty());
			bxFooter.getChildren().add(ivShadowrun);
		}
		drawer.setFooter(bxFooter);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.FlexibleApplication#createPage(org.prelle.javafx.NavigationItem)
	 */
	@Override
	public Page createPage(MenuItem menuItem) {
		logger.log(Level.INFO, "createPage(" + menuItem + ")");
		try {
			if (menuItem==navigAbout) {
				return new AboutPage(super.getDirectories(), this, RoleplayingSystem.SPLITTERMOND);
			} else if (menuItem==navigLookup) {
				return new LibraryPage();
			} else if (menuItem==navigChars) {
				SpliMoCharactersOverviewPage pg = new SpliMoCharactersOverviewPage();
				CharacterProviderLoader.getCharacterProvider().setListener(pg);
				return pg;
			} else if (menuItem==navigSummon) {
				return new SummonableCreatorPage();
			} else if (menuItem==navigAccount) {
				return new AccountPage(this, RoleplayingSystem.SPLITTERMOND);
			} else {
				Label text = new Label("Diese Funktion steht in dieser Version noch nicht zur Verfügung.");
				text.setWrapText(true);
				Page page = new Page("Konto", text);
				logger.log(Level.WARNING, "No page for " + menuItem.getText());
				return page;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.WARNING, "No page for " + menuItem.getText());
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.client.jfx.EdenClientApplication#importXML(byte[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected SpliMoCharacter importXML(byte[] xml) {
		try {
			SpliMoCharacter model = SplitterMondCore.decode(xml);
			SplitterTools.resolveChar(model);
			SplitterTools.runProcessors(model, Locale.getDefault());
			return model;
		} catch (CharacterIOException e) {
			logger.log(Level.ERROR, "Failed decoding imported XML",e);
			return null;
		}
	}

}
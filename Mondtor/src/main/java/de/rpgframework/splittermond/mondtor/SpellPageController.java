package de.rpgframework.splittermond.mondtor;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.jfx.fxml.FilteredListPageController;
import de.rpgframework.splittermond.mondtor.cells.SpellListCell;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.util.StringConverter;

public class SpellPageController extends FilteredListPageController<Spell> {
	
	private Logger logger = System.getLogger(SpellPageController.class.getPackageName());

	@FXML
	private ChoiceBox<SMSkill> cbSchool;
	@FXML
	private ChoiceBox<SpellType> cbType;

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		super.initialize();

		/* 
		 * Creature Types
		 */
		cbSchool = new ChoiceBox<SMSkill>();
		List<SMSkill> availSchools = new ArrayList<SMSkill>();
		availSchools.add(null);
		availSchools.addAll(SplitterMondCore.getSkills(SMSkill.SkillType.MAGIC));
		cbSchool.getItems().addAll(availSchools);
		Collections.sort(cbSchool.getItems(), new Comparator<SMSkill>() {
			public int compare(SMSkill o1, SMSkill o2) {
				if (o1==null) return -1;
				if (o2==null) return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbSchool.setConverter(new StringConverter<SMSkill>() {
			public String toString(SMSkill val) {
				if (val==null) return "jede Schule";
				return val.getName();
			}
			public SMSkill fromString(String string) { return null; }
		});
		cbSchool.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshList());
		filterPane.getChildren().add(cbSchool);
		
		/*
		 * Spell Types
		 */
		cbType = new ChoiceBox<SpellType>();
		cbType.getItems().add(null);
		cbType.getItems().addAll(SpellType.values());
		Collections.sort(cbType.getItems(), new Comparator<SpellType>() {
			public int compare(SpellType o1, SpellType o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshList());
		cbType.setConverter(new StringConverter<SpellType>() {
			public String toString(SpellType val) {
				if (val==null) return "alle Typen";
				return val.getName();
			}
			public SpellType fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbType);

		
		// React to double clicks on creatures
		lvResult.setCellFactory(lv -> {
			SpellListCell cell = new SpellListCell();
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2) {
					showAction(cell.getItem());
				}
			});
			return cell;
			});
		
		refreshList();
	}

	//-------------------------------------------------------------------
	protected void refreshList() {
		List<Spell> list = SplitterMondCore.getItemList(Spell.class);
		// Match creature type
		if (cbSchool.getValue()!=null) {
			list = list.stream()
					.filter(data -> data.getLevelInSchool(cbSchool.getValue())>-1)
					.collect(Collectors.toList());
		}
		// Match creature feature
		if (cbType.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getTypes().contains(cbType.getValue()))
					.collect(Collectors.toList());
		}
		// Match search keyword
		if (search.getText()!=null && !search.getText().isBlank()) {
			list = list.stream()
					.filter(crea -> crea.getName().toLowerCase().indexOf(search.getText().toLowerCase())>-1)
					.collect(Collectors.toList());
		}
		
		Collections.sort(list, new Comparator<Spell>() {
			public int compare(Spell o1, Spell o2) {
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		lvResult.getItems().setAll(list);
	}
	
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.fxml.FilteredListPageController#showAction(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	protected void showAction(Spell value) {
		logger.log(Level.INFO, "Show spell "+value);
		
		description.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		description.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			try {
				Page toOpen = org.prelle.splittermond.chargen.jfx.fxml.ScreenLoader.loadSpellDescriptionPage(value);
				page.getAppLayout().getApplication().openScreen(new ApplicationScreen(toOpen));
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error opening SpellDescriptionPage",e);
			} 
		} else {
			descriptionController.setData(value, resolver);
		}
	}

}

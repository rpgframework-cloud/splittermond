package de.rpgframework.splittermond.mondtor.cells;

import org.prelle.splimo.creature.Creature;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class CreatureListCell extends ListCell<Creature> {

	private Label lbName;
	private Label lbLevel;
	private HBox layout;
	
	//-------------------------------------------------------------------
	public CreatureListCell() {
		lbName = new Label();
		lbLevel= new Label();
		layout = new HBox(20,lbName,lbLevel);
		HBox.setHgrow(lbName, Priority.ALWAYS);
		lbName.setMaxWidth(Double.MAX_VALUE);
		
	}


	//-------------------------------------------------------------------
	public void updateItem(Creature item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			lbLevel.setText(item.getLevelAlone()+"/"+item.getLevelGroup());
			setGraphic(layout);
		}
	}
}

package de.rpgframework.splittermond.mondtor;

import java.util.Locale;
import java.util.logging.LogManager;

import de.rpgframework.eden.client.jfx.EdenSettings;

public class MondtorStarter {

	public static void main(String[] args) {
 		LogManager.getLogManager().reset();
		EdenSettings.setupDirectories("Mondtor");
		Locale.setDefault(EdenSettings.getPreferredLangauge("Mondtor"));
		MondtorMain.main(args);
	}

}

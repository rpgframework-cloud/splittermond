package de.rpgframework.splittermond.mondtor.cells;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellSchoolEntry;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class SpellListCell extends ListCell<Spell> {

	private Label lbName;
	private Label lbLevel;
	private HBox layout;
	
	//-------------------------------------------------------------------
	public SpellListCell() {
		lbName = new Label();
		lbLevel= new Label();
		layout = new HBox(20,lbName,lbLevel);
		HBox.setHgrow(lbName, Priority.ALWAYS);
		lbName.setMaxWidth(Double.MAX_VALUE);
		
	}


	//-------------------------------------------------------------------
	public void updateItem(Spell item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			int minLevel = 10;
			int maxLevel = 0;
			for (SpellSchoolEntry entry : item.getSchools()) {
				if (entry.getLevel()<minLevel) minLevel=entry.getLevel();
				if (entry.getLevel()>maxLevel) maxLevel=entry.getLevel();
			}
			if (minLevel!=maxLevel)
				lbLevel.setText("Grad "+minLevel+".."+maxLevel);
			else
				lbLevel.setText("Grad "+minLevel);
			setGraphic(layout);
		}
	}
}

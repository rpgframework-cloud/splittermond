package de.rpgframework.splittermond.mondtor.panes;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;

import org.prelle.javafx.public_skins.ImageSpinnerSkin;
import org.prelle.splimo.Race;
import org.prelle.splimo.SplitterMondCore;

import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.splittermond.mondtor.MondtorMain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SpeciesPane extends ADescriptionPane<Race> {

	private final static Logger logger = System.getLogger(SpeciesPane.class.getPackageName());
	
	private Map<Race, Image> cachedImage;
	
	private Spinner<Race> spinner;
	
	private TextFlow description;
	
	//-------------------------------------------------------------------
	public SpeciesPane() {
		getStyleClass().add("description-pane");
		cachedImage = new HashMap<>();
		initComponents();
		setItems(SplitterMondCore.getItemList(Race.class));
		
		// Ensure image is always half of available height
		
		spinner.prefHeightProperty().bind(heightProperty().divide(2.0));
		spinner.maxHeightProperty().bind(heightProperty().divide(2.0));
		spinner.prefWidthProperty().bind(widthProperty().subtract(10));
		spinner.maxWidthProperty().bind(widthProperty().subtract(10));
		
		VBox perMetaDetails = new VBox(5, descTitle, descSources, description);
		perMetaDetails.setMaxHeight(Double.MAX_VALUE);
		ScrollPane scroll = new ScrollPane(perMetaDetails);
		scroll.setFitToWidth(true);
		scroll.setMaxHeight(Double.MAX_VALUE);
		
		VBox.setVgrow(scroll, Priority.ALWAYS);
		getChildren().addAll(spinner, scroll);
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		ObjectProperty<StringConverter<Race>> nameConv = new SimpleObjectProperty<>(new StringConverter<Race>() {
			public Race fromString(String arg0) {return null;}
			public String toString(Race value) {return value.getName();}
			});
		ObjectProperty<Function<Race,Image>> imgConv = new SimpleObjectProperty<>(new Function<Race, Image>() {
			public Image apply(Race key) {
				if (cachedImage.containsKey(key)) {
					logger.log(Level.INFO, "Image "+cachedImage.get(key).getWidth()+"x+"+cachedImage.get(key).getHeight());
					return cachedImage.get(key);
				}
				InputStream ins = MondtorMain.class.getResourceAsStream("images/species/"+key.getId()+".png");
				if (ins==null) {
					logger.log(Level.WARNING, "Missing resource "+"images/species/"+key.getId()+".png");
					return null;
				} else {
					Image img = new Image(ins);
					logger.log(Level.INFO, "Image "+img.getWidth()+"x+"+img.getHeight());
					cachedImage.put(key, img);
					return img;
				}
			}
		});
		spinner = new Spinner<Race>();
		spinner.setSkin(new ImageSpinnerSkin<Race>(spinner, imgConv, nameConv,null,null));

		description = new TextFlow();
		
	}
	
	//-------------------------------------------------------------------
	public void setItems(List<Race> list) {
		spinner.setValueFactory(new SpinnerValueFactory.ListSpinnerValueFactory<Race>(FXCollections.observableArrayList(list)));
		spinner.getValueFactory().setWrapAround(true);	
		spinner.getValueFactory().valueProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				setData(n);
			} else {
				descTitle.setText(null);
				descSources.setText(null);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(Race value) {
		logger.log(Level.DEBUG, "setData: "+value);
		if (spinner.getValue()!=value) {
			spinner.getValueFactory().setValue(value);
		}
		descTitle.setText(value.getName(Locale.getDefault()));
		descSources.setText(RPGFrameworkJavaFX.createSourceText(value));
		
		// Set descriptive text
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, value.getDescription(Locale.getDefault()));
	}

}

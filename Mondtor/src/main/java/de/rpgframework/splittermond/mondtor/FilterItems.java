package de.rpgframework.splittermond.mondtor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemSubType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.SMItemAttribute;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterItems extends AFilterInjector<ItemTemplate> {

	private ChoiceBox<ItemType> cbType;
	private ChoiceBox<ItemSubType> cbSub;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		/*
		 * Creature Types
		 */
		cbType = new ChoiceBox<ItemType>();
		List<ItemType> availTypes = List.of(ItemType.values());
		cbType.getItems().add(null);
		cbType.getItems().addAll(availTypes);
		Collections.sort(cbType.getItems(), new Comparator<ItemType>() {
			public int compare(ItemType o1, ItemType o2) {
				if (o1==null) return -1;
				if (o2==null) return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType val) {
				if (val==null) return "jeder Art";
				return val.getName();
			}
			public ItemType fromString(String string) { return null; }
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		filterPane.getChildren().add(cbType);

		/*
		 * Create Feature
		 */
		cbSub = new ChoiceBox<ItemSubType>();
		cbSub.getItems().add(null);
		cbSub.getItems().addAll(List.of(ItemSubType.values()));
		cbSub.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbSub.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType val) {
				if (val==null) return "jede Unterart";
				return val.getName();
			}
			public ItemSubType fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbSub);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(de.rpgframework.jfx.FilteredListPage, java.util.List)
	 */
	@Override
	public List<ItemTemplate> applyFilter(List<ItemTemplate> list) {
		// Match creature type
		if (cbType.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getAttribute(SMItemAttribute.TYPE).getValue()==cbType.getValue())
					.collect(Collectors.toList());
		}

		// Match creature feature
		if (cbSub.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getAttribute(SMItemAttribute.SUBTYPE).getValue()==cbSub.getValue())
					.collect(Collectors.toList());
		}
		return list;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<ItemTemplate> available) {
		ItemSubType oldSub = cbSub.getValue();
		// Determine available subtypes
		List<ItemSubType> availSubTypes = new ArrayList<ItemSubType>();
		availSubTypes.add(null);
		for (ItemTemplate item : SplitterMondCore.getItemList(ItemTemplate.class)) {
			ItemType type = item.getAttribute(SMItemAttribute.TYPE).getValue();
			if (cbType.getValue()!=null && type!=cbType.getValue()) continue;
			ItemSubType sub = item.getSubType();
			if (!availSubTypes.contains(sub))
				availSubTypes.add(sub);
		}
		cbSub.getItems().setAll(availSubTypes);
		if (availSubTypes.contains(oldSub))
			cbSub.getSelectionModel().select(oldSub);
	}

}

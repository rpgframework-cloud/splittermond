package de.rpgframework.splittermond.mondtor.cells;

import org.prelle.splimo.creature.CreatureFeature;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class CreatureFeatureListCelll extends ListCell<CreatureFeature> {

	private Label lbName;
	private Label lbLevel;
	private HBox layout;

	//-------------------------------------------------------------------
	public CreatureFeatureListCelll() {
		lbName = new Label();
		lbLevel= new Label();
		layout = new HBox(20,lbName,lbLevel);
		HBox.setHgrow(lbName, Priority.ALWAYS);
		lbName.setMaxWidth(Double.MAX_VALUE);

	}


	//-------------------------------------------------------------------
	public void updateItem(CreatureFeature item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			if (item.hasLevel())
				lbLevel.setText("(Stufen)");
			else
				lbLevel.setText(null);
			setGraphic(layout);
		}
	}
}

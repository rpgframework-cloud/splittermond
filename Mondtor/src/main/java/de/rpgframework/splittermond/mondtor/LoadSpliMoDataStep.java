package de.rpgframework.splittermond.mondtor;

import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;

import de.rpgframework.core.StartupStep;

/**
 * @author prelle
 *
 */
public class LoadSpliMoDataStep implements StartupStep {

	//-------------------------------------------------------------------
	public LoadSpliMoDataStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init( );
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return true;
	}

}

package de.rpgframework.splittermond.mondtor;

import org.prelle.javafx.Page;
import org.prelle.splimo.creature.Creature;
import org.prelle.splittermond.chargen.jfx.creatures.AttributeViewPane;
import org.prelle.splittermond.chargen.jfx.creatures.LifeformPane;

import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author stefa
 *
 */
public class StandardCreaturePage extends Page {
	
	private LifeformPane bxStats;
	
	//-------------------------------------------------------------------
	public StandardCreaturePage(Creature data) {
		initComponents();
		initLayout();
		
		bxStats.setData(data);
//		super.setTitle(data.getName());
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		bxStats = new LifeformPane(true);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		TilePane layout = new TilePane(bxStats);
		
		setContent(layout);
	}

}

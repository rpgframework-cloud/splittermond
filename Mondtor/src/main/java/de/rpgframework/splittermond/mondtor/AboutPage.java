package de.rpgframework.splittermond.mondtor;

import java.io.File;
import java.util.Locale;

import org.prelle.javafx.DebugPage;
import org.prelle.javafx.PagePile;

import com.gluonhq.attach.util.Platform;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.client.jfx.EdenClientApplication;
import de.rpgframework.eden.client.jfx.EdenSettingsPage;
import de.rpgframework.eden.client.jfx.PDFPage;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;

/**
 * @author prelle
 *
 */
public class AboutPage extends PagePile {

	private EdenSettingsPage pgSettings;
	private PDFPage pgPDF;
	private InfoPage pgInfo;
	private CopyrightPage pgCopy;
	private DebugPage pgDebug;

	//-------------------------------------------------------------------
	public AboutPage(File[] directories, EdenClientApplication app, RoleplayingSystem rules) {
		pgSettings = new EdenSettingsPage(Locale.GERMAN);
//		pgSettings.addSettingsOneLine(ComLinkMain.RES ,"page.about.settings.style", cbStyle);

		pgPDF = new PDFPage(app, rules);
		pgInfo = new InfoPage();
		pgCopy  = new CopyrightPage();
		pgDebug = new DebugPage(new File[3]);

		getPages().addAll(pgSettings, pgPDF, pgInfo, pgCopy, pgDebug);
		if (!Platform.isDesktop() || !PDFViewerServiceFactory.create().isPresent()) {
			getPages().remove(pgPDF);
		}
//		setStartPage(pgInfo);
		pgDebug.refresh();

		visibleProperty().addListener( (ov,o,n) -> pgDebug.refresh());
	}

}

package de.rpgframework.splittermond.mondtor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureType;
import org.prelle.splimo.creature.CreatureTypeValue;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterCreatures extends AFilterInjector<Creature> {

	private ChoiceBox<CreatureType> cbType;
	private ChoiceBox<CreatureFeature> cbFeature;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		/*
		 * Creature Types
		 */
		cbType = new ChoiceBox<CreatureType>();
		List<CreatureType> availTypes = new ArrayList<CreatureType>();
		for (Creature crea : SplitterMondCore.getCreatures()) {
			for (CreatureTypeValue type : crea.getCreatureTypes()) {
				if (!availTypes.contains(type.getType()))
					availTypes.add(type.getType());
			}
		}
		cbType.getItems().addAll(availTypes);
		Collections.sort(cbType.getItems(), new Comparator<CreatureType>() {
			public int compare(CreatureType o1, CreatureType o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.setConverter(new StringConverter<CreatureType>() {
			public String toString(CreatureType val) {
				if (val==null) return "jeder Art";
				return val.getName();
			}
			public CreatureType fromString(String string) { return null; }
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		filterPane.getChildren().add(cbType);

		/*
		 * Create Feature
		 */
		cbFeature = new ChoiceBox<CreatureFeature>();
		cbFeature.getItems().add(null);
		cbFeature.getItems().addAll(SplitterMondCore.getItemList(CreatureFeature.class));
		cbFeature.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbFeature.setConverter(new StringConverter<CreatureFeature>() {
			public String toString(CreatureFeature val) {
				if (val==null) return "kein besonderes Merkmal";
				return val.getName();
			}
			public CreatureFeature fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbFeature);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(de.rpgframework.jfx.FilteredListPage, java.util.List)
	 */
	@Override
	public List<Creature> applyFilter(List<Creature> list) {
		// Match creature type
		if (cbType.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getCreatureTypes().stream()
							.map(ctv -> ctv.getType())
							.collect(Collectors.toList())
							.contains(cbType.getValue()))
					.collect(Collectors.toList());
		}
		// Match creature feature
		if (cbFeature.getValue()!=null) {
			list = list.stream()
					.filter(crea -> crea.getFeatures().stream()
							.map(ctv -> ctv.getResolved())
							.collect(Collectors.toList())
							.contains(cbFeature.getValue()))
					.collect(Collectors.toList());
		}
		return list;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<Creature> available) {
		// TODO Auto-generated method stub

	}

}

package de.rpgframework.splittermond.mondtor;

import java.lang.System.Logger;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.javafx.Page;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.CreatureFeature;

import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextFlow;

public class CreatureFeaturePageController {
	
	private Logger logger = System.getLogger(CreatureFeaturePageController.class.getPackageName());

	private transient Page page;

	@FXML
    private ResourceBundle resources;
	@FXML
	private FlowPane filterPane;
	@FXML
	private Accordion lvResult;
	@FXML
	private ScrollPane scroll;

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		scroll.setStyle("-fx-pref-width: 30em; -fx-max-width: 35em");
		scroll.setPannable(true);
//		lvResult.setCellFactory(lv -> {
//			CreatureFeatureListCelll cell = new CreatureFeatureListCelll();
//			cell.setOnMouseClicked(ev -> {
//				if (ev.getClickCount()==2) {
//					showAction(cell.getItem());
//				}
//			});
//			return cell;
//			});
//		// React to list selections
//		lvResult.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			if (n!=null)
//				showAction(n);
//		});
		
		refreshList();
	}

	//-------------------------------------------------------------------
	private void refreshList() {
		List<CreatureFeature> list = new ArrayList<CreatureFeature>(SplitterMondCore.getItemList(CreatureFeature.class));
		
		Collections.sort(list, new Comparator<CreatureFeature>() {
			public int compare(CreatureFeature o1, CreatureFeature o2) {
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		
		lvResult.getPanes().clear();
		for (CreatureFeature feat : list) {
			TextFlow flow = new TextFlow();
			RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(flow, feat.getDescription());
			TitledPane pane = new TitledPane(feat.getName(), flow);
			lvResult.getPanes().add(pane);
		}
	}

	//-------------------------------------------------------------------
	public void setComponent(Page page) {
		this.page = page;
//		page.setTitle(ResourceI18N.get(resources, "page.title"));
	}

}

module de.rpgframework.mondtor {
	exports de.rpgframework.splittermond.mondtor.panes;
	exports de.rpgframework.splittermond.mondtor;
	exports de.rpgframework.splittermond.mondtor.cells;
	exports de.rpgframework.splittermond.mondtor.pages;

	provides java.lang.System.LoggerFinder with de.rpgframework.splittermond.mondtor.CustomLoggerFinder;

	requires com.gluonhq.attach.util;
	requires de.rpgframework.core;
	requires de.rpgframework.eden.client;
	requires de.rpgframework.eden.client.jfx;
	requires de.rpgframework.javafx;
	requires de.rpgframework.rules;
	requires de.rpgframework.splittermond.data;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.fxml;
	requires javafx.graphics;
	requires rpgframework.pdfviewer;
	requires simple.persist;
	requires splittermond.core;
	requires splittermond.chargen.jfx;
	requires splittermond.export.pdf;
	requires com.gluonhq.attach.browser;
}
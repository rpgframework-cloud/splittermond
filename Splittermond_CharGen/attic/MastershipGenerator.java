package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.IRejectReasons;
import org.prelle.splittermond.chargen.charctrl.MastershipController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MastershipGenerator extends ControllerImpl<Mastership> implements MastershipController {
	
	protected static Logger logger = System.getLogger("splittermond.gen.master");
	
	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(MastershipGenerator.class, Locale.GERMAN);
	/** List of tokens usable for free selections */
	private List<FreeMastershipSelection> freeSelections;
	private Map<DataItemModification, FreeMastershipSelection> systemAdded = new HashMap<>();

	//-------------------------------------------------------------------
	protected MastershipGenerator(SpliMoCharacterController parent) {
		super(parent);
		freeSelections = new ArrayList<MastershipController.FreeMastershipSelection>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.MastershipController#getFreeMasterships()
	 */
	@Override
	public int getFreeMasterships() {
		int count = 0;
		for (FreeMastershipSelection tmp : freeSelections)
			if (tmp.getUsedFor()==null)
				count++;
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.MastershipController#getFreeMasterships(org.prelle.splimo.SMSkill)
	 */
	@Override
	public int getFreeMasterships(SMSkill skill) {
		int count = 0;
		for (FreeMastershipSelection tmp : freeSelections)
			if (tmp.getSkill()==skill && tmp.getUsedFor()==null)
				count++;
		return count;
	}

	//-------------------------------------------------------------------
	private void link(FreeMastershipSelection free, MastershipReference ref) {
		free.setUsedFor(ref);
		ref.setFree(free.getLevel());
	}

	//-------------------------------------------------------------------
	private void unlink(FreeMastershipSelection free, MastershipReference ref) {
		free.setUsedFor(null);
		ref.setFree(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		choices.clear();
		todos.clear();
		
		// Undo all system added linking
		for (FreeMastershipSelection sel : systemAdded.values()) {
			unlink(sel, sel.getUsedFor());
		}
		systemAdded.clear();
		

		try {
			updateFreeSelections();
			for (Modification tmp : previous) {
				if (tmp.getReferenceType() != SplittermondReference.MASTERSHIP) {
					// Does not relate to powers
					unprocessed.add(tmp);
					continue;
				}
				logger.log(Level.WARNING, "process "+tmp);
				if (tmp instanceof DataItemModification) {
					DataItemModification mod = (DataItemModification)tmp;
					Mastership master = mod.getResolvedKey();
					SMSkill skill = master.getSkill();
					int pos = mod.getKey().indexOf("/");
					if (pos>0) {
						String key = mod.getKey().substring(0, pos);
						skill = SplitterMondCore.getSkill(key);
					}
					logger.log(Level.WARNING, "    "+master+" / "+master.getAssignedSkill()+" / "+master.getSkill()+" = "+skill);
					
					SMSkillValue sVal = model.getSkillValue(skill);
					MastershipReference ref = new MastershipReference(master);

					// Ensure there is a free mastership
					boolean found = false;
					for (FreeMastershipSelection free : freeSelections) {
						if (free.getUsedFor()==null && free.getSkill()==null && master.getLevel()==1) {
							link(free, ref);
							systemAdded.put(mod, free);
							logger.log(Level.DEBUG,"Use "+free+" for "+mod);
							found = true;
							break;
						}
					}
					if (!found) {
						logger.log(Level.ERROR,"Cannot add mastership modification - no free selection");
						continue;
					}

					sVal.addMastership(ref);
				}
			}
			/*
			 * ToDos
			 */
			int unused = getFreeMasterships();
			if (unused > 0) {
				todos.add(new ToDoElement(Severity.WARNING, RES, "todos.pointsLeft", unused));
				logger.log(Level.WARNING, "There are {0} unused masterships", unused);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			logger.log(Level.TRACE, "STOP : process");
		}
		return unprocessed;
	}

	@Override
	public RecommendationState getRecommendationState(Mastership item) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	private FreeMastershipSelection getFreeSelectionFor(SMSkill skill, int minLevel) {
		for (FreeMastershipSelection free : freeSelections) {
			if (free.getUsedFor()==null && free.getSkill()==skill && free.getLevel()>=minLevel)
				return free;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void updateFreeSelections() {
		SpliMoCharacter model = getModel();
		/*
		 * At first calculate how many unlimited free selections need to
		 * be created. This is only relevant when this class is initiated
		 */
		int missingUnlimited = 3;
		for (FreeMastershipSelection free : freeSelections) {
			if (free.getSkill()==null)
				missingUnlimited--;
		}
		for (int i=0; i<missingUnlimited; i++) {
			freeSelections.add(new FreeMastershipSelection(null, 1));
			logger.log(Level.DEBUG,"Created free level 1 mastership selection");
		}

		/*
		 * Now check skill levels and ensure that there are the necessary
		 * free selections
		 */
		for (SMSkill skill : SplitterMondCore.getItemList(SMSkill.class)) {
			SMSkillValue sVal = model.getSkillValue(skill);
			// Level 1
			if (sVal.getModifiedValue()>=6) {
				FreeMastershipSelection free = getFreeSelectionFor(skill, 1);
				if (free==null) {
					freeSelections.add(new FreeMastershipSelection(skill, 1));
					logger.log(Level.DEBUG,"Created free level 1 mastership selection for "+skill);
				}
			}
			// Level 2
			if (sVal.getModifiedValue()>=9) {
				FreeMastershipSelection free = getFreeSelectionFor(skill, 2);
				if (free==null) {
					freeSelections.add(new FreeMastershipSelection(skill, 2));
					logger.log(Level.DEBUG,"Created free level 2 mastership selection for "+skill);
				}
			}
			// Level 3
			if (sVal.getModifiedValue()>=12) {
				FreeMastershipSelection free = getFreeSelectionFor(skill, 3);
				if (free==null) {
					freeSelections.add(new FreeMastershipSelection(skill, 3));
					logger.log(Level.DEBUG,"Created free level 3 mastership selection for "+skill);
				}
			}
			// Level 4
			if (sVal.getModifiedValue()>=15) {
				FreeMastershipSelection free = getFreeSelectionFor(skill, 4);
				if (free==null) {
					freeSelections.add(new FreeMastershipSelection(skill, 4));
					logger.log(Level.DEBUG,"Created free level 4 mastership selection for "+skill);
				}
			}
		}

		/*
		 * In case that a skill has been decreased, remove invalid free masterships
		 */
		for (FreeMastershipSelection free : new ArrayList<>(freeSelections)) {
			if (free.getSkill()==null)
				continue;
			SMSkillValue sVal = model.getSkillValue(free.getSkill());
			if (free.getLevel()==4 && sVal.getModifiedValue()<15) {
				freeSelections.remove(free);
				logger.log(Level.DEBUG,"Removed free level 4 mastership selection for "+free.getSkill());
			}
			if (free.getLevel()==3 && sVal.getModifiedValue()<12) {
				freeSelections.remove(free);
				logger.log(Level.DEBUG,"Removed free level 3 mastership selection for "+free.getSkill());
			}
			if (free.getLevel()==2 && sVal.getModifiedValue()<9) {
				freeSelections.remove(free);
				logger.log(Level.DEBUG,"Removed free level 2 mastership selection for "+free.getSkill());
			}
			if (free.getLevel()==1 && sVal.getModifiedValue()<6) {
				freeSelections.remove(free);
				logger.log(Level.DEBUG,"Removed free level 1 mastership selection for "+free.getSkill());
			}
		}

		logger.log(Level.DEBUG, "updated free selection: "+freeSelections);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.MastershipController#canBeSelected(org.prelle.splimo.SMSkill, org.prelle.splimo.Mastership, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(SMSkill skill, Mastership value, Decision... decisions) {
		// If there is a free mastership, anything goes
		FreeMastershipSelection free = getFreeSelectionFor(skill, value.getLevel());
		if (free!=null) {
			return Possible.TRUE;
		}
		
		// Minimum skill level = 1
		int minLvl = 3 + value.getLevel();
		if (getModel().getSkillValue(skill).getModifiedValue()<minLvl) {
			return new Possible(false, IRejectReasons.IMPOSS_SKILL_TOO_LOW);
		}
		
		// Is there enough exp
		int expCost = value.getLevel()*5;
		if (getModel().getExpFree()<expCost) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_ENOUGH_EXP);
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.MastershipController#select(org.prelle.splimo.SMSkill, org.prelle.splimo.Mastership, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<MastershipReference> select(SMSkill skill, Mastership value, Decision... decisions) {
		try {
			Possible poss = canBeSelected(skill, value, decisions);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Tried to select mastership ''{0}'', but {1}", value, poss.getMostSevere());
				return new OperationResult<>(poss);
			}
			
			MastershipReference ref = new MastershipReference(value);
			SMSkillValue sVal = getModel().getSkillValue(skill);
			sVal.addMastership(ref);
			logger.log(Level.INFO, "Added mastership {0}", ref);
			
			parent.runProcessors();
			return new OperationResult<MastershipReference>(ref);
		} finally {
			
		}
	}

	@Override
	public Possible canBeDeselected(MastershipReference value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deselect(MastershipReference value) {
		// TODO Auto-generated method stub
		return false;
	}

}

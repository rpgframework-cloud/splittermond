package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.Decision;

/**
 * @author prelle
 *
 */
public interface MastershipController extends PartialController<Mastership>, RecommendingController<Mastership> {

	public class FreeMastershipSelection implements Comparable<FreeMastershipSelection> {
		private SMSkill skill;
		private int   level;
		private MastershipReference usedFor;
		public FreeMastershipSelection(SMSkill school, int lvl) {
			this.skill = school;
			this.level  = lvl;
		}
		//--------------------------------------------------------------------
		public SMSkill getSkill() {return skill;}
		public void setSkill(SMSkill skill) {this.skill = skill;}
		//--------------------------------------------------------------------
		public int getLevel() {	return level;}
		//--------------------------------------------------------------------
		public MastershipReference getUsedFor() {return usedFor;}
		public void setUsedFor(MastershipReference used) { usedFor = used; }
		//--------------------------------------------------------------------
		public String toString() {
			return "Level "+level+" mastership in "+skill;
		}
		//--------------------------------------------------------------------
		@Override
		public int compareTo(FreeMastershipSelection other) {
			int cmp = skill.compareTo(other.getSkill());
			if (cmp!=0) return cmp;
			return ((Integer)level).compareTo(other.getLevel());
		}
	}
	
	//-------------------------------------------------------------------
	public int getFreeMasterships();

	//-------------------------------------------------------------------
	public int getFreeMasterships(SMSkill skill);
	
	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to select the item
	 * @param value  Item to select
	 * @param decisions Decisions made
	 * @return Selection allowed or not
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public Possible canBeSelected(SMSkill skill, Mastership value, Decision... decisions);
	
	//-------------------------------------------------------------------
	/**
	 * Add/Select the item using the given decisions
	 * @param value  Item to select
	 * @param decisions Decisions made
	 * @return value instance of selected item
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public OperationResult<MastershipReference> select(SMSkill skill, Mastership value, Decision... decisions);
	
	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to deselect the item
	 * @param value  ItemValue to deselect
	 * @return Deselection allowed or not
	 */
	public Possible canBeDeselected(MastershipReference value);
	
	//-------------------------------------------------------------------
	/**
	 * Remove/Deselect the item
	 * @param value  Item to select
	 * @return TRUE if item has been deselected
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public boolean deselect(MastershipReference value);

}

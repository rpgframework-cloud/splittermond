package org.prelle.splittermond.chargen.companion.entourage;

import java.lang.System.Logger;
import java.util.Collection;
import java.util.List;

import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Race;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splittermond.chargen.companion.ALifeformController;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class EntourageController extends ALifeformController implements NumericalValueController<SMSkill, SMSkillValue> {

	private final static Logger logger = System.getLogger(EntourageController.class.getPackageName()+".entourage");

	private Collection<ControllerListener> listener;

	protected List<ProcessingStep> processChain;
	protected boolean dontProcess;
	private List<Modification> unitTestModifications;
	protected boolean allowRunProcessor = true;

	//-------------------------------------------------------------------
	public EntourageController(SpliMoCharacter charModel, ResourceValue rsrc) {
		super(charModel, new Companion(LifeformType.ENTOURAGE));
	}

	//-------------------------------------------------------------------
	public EntourageController(SpliMoCharacter charModel, Companion model, ResourceValue rsrc) {
		super(charModel, model);
	}

	//-------------------------------------------------------------------
	public void selectRace(Race value, Decision...decisions) {
		ComplexDataItemValue<Race> selection = new ComplexDataItemValue<Race>(value);
		for (Decision dec : decisions) selection.addDecision(dec);
		model.setRace(selection);
		
	}

	//-------------------------------------------------------------------
	public void selectCulture(Culture value, Decision...decisions) {
		ComplexDataItemValue<Culture> selection = new ComplexDataItemValue<Culture>(value);
		for (Decision dec : decisions) selection.addDecision(dec);
		model.setCulture(selection);		
		
	}

	//-------------------------------------------------------------------
	public void selectBackground(Background value, Decision...decisions) {
		ComplexDataItemValue<Background> selection = new ComplexDataItemValue<Background>(value);
		for (Decision dec : decisions) selection.addDecision(dec);
		model.setBackground(selection);		
		
	}

	@Override
	public RecommendationState getRecommendationState(SMSkill item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getValue(SMSkillValue value) {
		return value.getModifiedValue();
	}

	@Override
	public Possible canBeIncreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canBeDecreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SMSkillValue> increase(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SMSkillValue> decrease(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}

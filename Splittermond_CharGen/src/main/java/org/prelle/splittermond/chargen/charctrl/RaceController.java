package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Race;

import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface RaceController extends PartialController<Race>, RecommendingController<Race> {

	//-------------------------------------------------------------------
	public List<Race> getAvailable();
	
	//-------------------------------------------------------------------
	public void select(Race value);

	//-------------------------------------------------------------------
	public Gender rollGender();

	//-------------------------------------------------------------------
	public int[] rollSizeAndWeight();

	//-------------------------------------------------------------------
	public String rollHair();

	//-------------------------------------------------------------------
	public String rollEyes();

}

package org.prelle.splittermond.chargen.companion.summon;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.Service;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.creature.ServiceValue;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.companion.ALifeformController;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.SelectedValueController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ServicesController extends ALifeformController
		implements SelectedValueController<ServiceValue> {

	private List<ServiceValue> available;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ServicesController(SpliMoCharacter charModel, SummonableController control) {
		super(charModel, control.getModel());
		available = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#getAvailable()
	 */
	@Override
	public List<ServiceValue> getAvailable() {
		return available.stream().filter(s -> !getSelected().contains(s)).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#getSelected()
	 */
	@Override
	public List<ServiceValue> getSelected() {
		return model.getSummonable().getServices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(ServiceValue value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#canBeSelected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeSelected(ServiceValue value) {
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#select(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public OperationResult<ServiceValue> select(ServiceValue value) {
		Possible poss = canBeSelected(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to select a service that may not be selected: "+poss.get());
			return new OperationResult<>(poss);
		}

		model.getSummonable().addService(value);

		runProcessors();
		return new OperationResult<ServiceValue>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(ServiceValue value) {
		return new Possible(getSelected().contains(value));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.SelectedValueController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(ServiceValue value) {
		Possible poss = canBeDeselected(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to deselect a service that may not be deselected: "+poss.get());
			return false;
		}

		model.getSummonable().removeService(value);

		runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	private boolean canAddService(ServiceValue toAdd) {
		for (ServiceValue t : getAvailable()) {
			if (t.getResolved()==toAdd.getResolved()) {
				// Identical services - inspect decisions
				Service service = toAdd.getModifyable();
				boolean foundDifference = false;
				for (Choice ch : service.getChoices()) {
					Decision d1 = t.getDecision(ch.getUUID());
					Decision d2 = toAdd.getDecision(ch.getUUID());
					if (d1==null || d2==null) {
						foundDifference=true;
						break;
					}
					if (!d1.getValue().equals(d2.getValue())) {
						foundDifference=true;
						break;
					}
				}
				if (!foundDifference)
					return false;
			}
		}
		return true;
	}

	//-------------------------------------------------------------------
	public boolean markAsBaseService(ServiceValue value) {
		if (getSelected().contains(value)) {
			// Remove base service flag from all services ...
			for (ServiceValue tmp : getSelected()) {
				tmp.setBaseService(false);
			}
			// ... and add it to the selected
			value.setBaseService(true);
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		available.clear();
		for (Modification mod : previous) {
			if (mod.getReferenceType()==SplittermondReference.SERVICE) {
				DataItemModification sMod = (DataItemModification)mod;
				ServiceValue service = new ServiceValue(sMod.getResolvedKey());
				sMod.getDecisions().forEach(d -> service.addDecision(d));

				if (canAddService(service)) {
					logger.log(Level.TRACE, "Add service {0}", service.getKey());
					available.add(service);
				} else {
					logger.log(Level.TRACE, "Avoid duplicate {0}", service.getKey());
				}
			} else
				unprocessed.add(mod);
		}
		return unprocessed;
	}

}

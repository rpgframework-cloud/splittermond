package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.splimo.Power;
import org.prelle.splimo.Power.SelectionType;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.ExistenceRequirement;

public class PowerGenerator extends ControllerImpl<Power> implements PowerController {

	protected static Logger logger = System.getLogger("splittermond.gen.power");

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(PowerGenerator.class, Locale.GERMAN);

	private int powerPoints, pointsMax;

	//-------------------------------------------------------------------
	public PowerGenerator(SpliMoCharacterController parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return powerPoints;
	}

	//-------------------------------------------------------------------
	@Override
	public RecommendationState getRecommendationState(Power item) {
			return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getAvailable()
	 */
	@Override
	public List<Power> getAvailable() {
		return SplitterMondCore.getItemList(Power.class).stream()
				.filter(p -> getCharacterController().showDataItem(p))
				.filter(p -> p.canBeUsedMultipleTimes() || getModel().getPower(p)==null)
				.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<PowerValue> getSelected() {
		return getModel().getPowers();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Power data, Decision...dec) {
		if (powerPoints<data.getCost() && getModel().getExpFree()<(data.getCost()*7))
			return new Possible(Severity.STOPPER, RES, SpliMoRejectReasons.IMPOSS_NOT_ENOUGH_POINTS, data.getCost(), data.getCost()*7);
		if (getModel().hasPower(data) && !data.canBeUsedMultipleTimes())
			return new Possible(Severity.STOPPER, RES, SpliMoRejectReasons.IMPOSS_ALREADY_EXISTS);

		return new Possible(true);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(PowerValue value) {
		// Cannot deselect modifications from race
		if (value.isFixed())
			return new Possible(new ExistenceRequirement(SplittermondReference.RACE, null));

		if (!getModel().getPowers().contains(value)) {
			return new Possible(Severity.STOPPER, RES, SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);
		}

		return  new Possible(true);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<PowerValue> select(Power data, Decision...dec) {
		logger.log(Level.DEBUG, "select ''{0}'' with {1}", data.getId(), Arrays.toString(dec));
		Possible poss = canBeSelected(data, dec);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to select ''{0}'' although not possible: {1}", data.getId(), poss.getMostSevere());
			return new OperationResult<>(poss);
		}

		PowerValue ref = getModel().getPower(data);
		if (ref==null) {
			ref = new PowerValue(data, data.hasLevel()?1:0);
			getModel().addPower(ref);
			logger.log(Level.INFO, "Selected new power ''{0}''", ref.getKey());
		} else {
			ref.setDistributed( ref.getDistributed() +1);
			logger.log(Level.INFO, "Increased power ''{0}'' to user distributed {1}", ref.getKey(), ref.getDistributed());
			logger.log(Level.DEBUG, "Now {0}", ref.toString());
		}

		parent.runProcessors();

		return new OperationResult<>(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(PowerValue value) {
		logger.log(Level.WARNING, "TODO: deselect");
		Possible poss = canBeDeselected(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to deselect ''{0}'' although not possible: {1}", value.getKey(), poss.getMostSevere());
			return false;
		}

		getModel().removePower(value);
		logger.log(Level.INFO, "Deselected power ''{0}''", value.getKey());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(PowerValue value) {
		if (!value.getResolved().hasLevel())
			return Possible.FALSE;

		Power power = value.getResolved();
		if (value.getDistributed()>0 && power.getSelectable()==SelectionType.LEVEL)
			return Possible.FALSE;

		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(PowerValue value) {
		return new Possible( value.getDistributed()>0 );
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<PowerValue> increase(PowerValue value) {
		logger.log(Level.DEBUG, "increase ''{0}'' to {1}", value.getKey(), value.getModifiedValue()+1);
		Possible poss = canBeIncreased(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to increase ''{0}'' although not possible: {1}", value.getKey(), poss.getMostSevere());
			return new OperationResult<>(poss);
		}

		value.setDistributed( value.getDistributed() +1);
		logger.log(Level.INFO, "Increased power ''{0}''", value.getKey());

		parent.runProcessors();
		return new OperationResult<PowerValue>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<PowerValue>  decrease(PowerValue value) {
		logger.log(Level.DEBUG, "decrease ''{0}'' to {1}", value.getKey(), value.getModifiedValue()+1);
		Possible poss = canBeDecreased(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to decrease ''{0}'' although not possible: {1}", value.getKey(), poss.getMostSevere());
			return new OperationResult<>(poss);
		}

		value.setDistributed( value.getDistributed() -1);
		logger.log(Level.INFO, "Decreased power ''{0}''", value.getKey());

		parent.runProcessors();
		return new OperationResult<PowerValue>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		choices.clear();
		todos.clear();
		powerPoints = 6;
		pointsMax = 6;

		try {

			// Iterate all powers
			for (PowerValue ref : model.getPowers()) {
//				Power power = ref.getModifyable();
				int cost = ref.getCostPoints();
				logger.log(Level.INFO, "Pay {0} for {1}  (auto={2}){3}", cost, ref.getKey()+"/"+ref.getDistributed()+"/"+ref.getModifiedValue(), ref.isAutoAdded(), ref.getIncomingModifications());
				powerPoints -= cost; //(level*power.getCost());
			}

			/*
			 * ToDos
			 */
			if (powerPoints>0) {
				todos.add(new ToDoElement(Severity.STOPPER, RES, "todos.pointsLeft", powerPoints));
			}
			if (powerPoints<0) {
				todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.TODOS_OVERSPENT_POWER, (int)-powerPoints));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			logger.log(Level.TRACE, "STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(PowerValue value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Power value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Power data, Decision... decisions) {
		return data.getCost();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCostString(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public String getSelectionCostString(Power data) {
		return String.valueOf(getSelectionCost(data));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(PowerValue value) {
		return value.getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.PowerController#getPointsMax()
	 */
	@Override
	public int getPointsMax() {
		return pointsMax;
	}

}

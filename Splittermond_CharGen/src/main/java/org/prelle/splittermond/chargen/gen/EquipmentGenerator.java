package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.items.SMItemFlag;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;
import org.prelle.splittermond.chargen.common.ACommonEquipmentController;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class EquipmentGenerator extends ACommonEquipmentController {

	private static record BasicEquip(String id, int count) {}
	private BasicEquip[] BASICS = new BasicEquip[] {
			new BasicEquip("decke", 1),
			new BasicEquip("wildniskleidung", 1),
			new BasicEquip("wasserschlauch", 1),
			new BasicEquip("fackel", 5),
			new BasicEquip("essgeschirr",1),
			new BasicEquip("feuersteinstahlundzunder",1),
			new BasicEquip("marschrationprotag",3)
	};

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public EquipmentGenerator(SpliMoCharacterController parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(ItemTemplate item, Decision... decisions) {
		if (item==null) {
			return Possible.FALSE;
		}
		ItemAttributeDefinition def = item.getAttribute(SMItemAttribute.PRICE);
		if (def==null) {
			logger.log(Level.WARNING, "No PRICE definition for {0}", item.getId());
			return Possible.TRUE;
		}
		int value = def.getDistributed();
		if (value>getModel().getMoney()) {
			return new Possible(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_NOT_ENOUGH_MONEY, value, getModel().getMoney());
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(CarriedItem<ItemTemplate> value) {
		if (value.getInjectedBy()==BASICS)
			return Possible.FALSE;
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		logger.log(Level.TRACE, "ENTER process");
		todos.clear();
		SpliMoCharacter model = getModel();

		try {
			// Derive the available money by the wealth resource
			int wealth = model.getResource("wealth").getModifiedValue();
			switch (wealth) {
			case -2: model.setMoney(0); break;
			case -1: model.setMoney(100); break;
			case  0: model.setMoney(300); break;
			case  1: model.setMoney(500); break;
			case  2: model.setMoney(2000); break;
			case  3: model.setMoney(4000); break;
			case  4: model.setMoney(6500); break;
			case  5: model.setMoney(10000); break;
			case  6: model.setMoney(15000); break;
			}
			logger.log(Level.INFO, "Startvermögen {0} Telare", model.getMoney());

			// If necessary inject start equipment
			for (BasicEquip basic : BASICS) {
				CarriedItem<ItemTemplate> item = model.getCarriedItem(basic.id());
				if (item==null) {
					// Item not present yet -> add it
					item = new CarriedItem<ItemTemplate>(
							SplitterMondCore.getItem(ItemTemplate.class, basic.id),
							null, CarryMode.CARRIED);
					item.setCount(basic.count());
					item.setInjectedBy(BASICS);
					SMGearTool.recalculate("", model, item);
					logger.log(Level.INFO, "Added basic gear {0}", item.getNameWithoutRating());
					model.addCarriedItem(item);
				} else {
					// Item already added
					if (item.getInjectedBy()!=BASICS) {
						item.setCount(basic.count);
					}
				}
			}


			// Pay all items that are not tools of the trade, free weapon or relic
			for (CarriedItem<ItemTemplate> item : getModel().getCarriedItems()) {
				if (item.hasFlag(SMItemFlag.FREE_WEAPON)) continue;
				if (item.hasFlag(SMItemFlag.TOOL_OF_THE_TRADE)) continue;
				if (item.getInjectedBy()==BASICS) continue;
				int price = item.getAsValue(SMItemAttribute.PRICE).getModifiedValue();
				int telare= item.getCount() * price;
				logger.log(Level.INFO, "Pay {0} Lunare for {1}", ((double)telare)/100.0, item.getKey());
				model.setMoney( model.getMoney() - telare );
			}

			// Check money spent
			double money = model.getMoney();
			if (money<0) {
				double overspent = ((double)money)/100;
				logger.log(Level.WARNING, "Overspent start money: {0}",money);
				todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.TODOS_OVERSPENT_START, (int)overspent));
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE process");
		}
		return unprocessed;
	}

}

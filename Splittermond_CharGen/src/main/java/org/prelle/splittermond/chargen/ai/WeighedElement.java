/**
 * 
 */
package org.prelle.splittermond.chargen.ai;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * @author prelle
 *
 */
public class WeighedElement {
	
	private final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(WeighedElement.class.getName());

	@Attribute
	private String id;
	@Attribute
	private int weight;
	@Element
	private ModificationList modifications;
}

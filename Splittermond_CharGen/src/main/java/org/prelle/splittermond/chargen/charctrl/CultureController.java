package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Culture;
import org.prelle.splimo.Deity;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface CultureController extends PartialController<Culture>, RecommendingController<Culture> {
	
	//-------------------------------------------------------------------
	public void setAllowUnusualCultures(boolean allow);
	public boolean isAllowUnusualCultures();
	
	//-------------------------------------------------------------------
	public void setAllowUnusualDeities(boolean allow);
	public boolean isAllowUnusualDeities();

	//-------------------------------------------------------------------
	public List<Culture> getAvailable();
	
	//-------------------------------------------------------------------
	public void select(Culture value);

	//-------------------------------------------------------------------
	public List<Deity> getAvailableDeities();
	
	//-------------------------------------------------------------------
	public void selectDeity(Deity value);

	//-------------------------------------------------------------------
	public String rollName();

	//-------------------------------------------------------------------
	public Deity rollDeity();
	
	//-------------------------------------------------------------------
	public RecommendingController<Deity> getDeityRecommender();

}

package org.prelle.splittermond.chargen.ai;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.ai.Weight;
import de.rpgframework.genericrpg.modification.RecommendationModification;

/**
 * This class stores the result of all recommendations
 * @param <T>  Any DataItem or Attribute
 */
public class Recommendation<T> implements Comparable<Recommendation<T>> {
	T data;
	List<RecommendationModification> sources;
	int pointsOverride;
	Weight weightOverride;
	/** To be used for modules */
	RecommendationState recommendationState;
	
	private transient Weight highestWeight;
	private transient int points;
	
	//-------------------------------------------------------------------
	public Recommendation(T data) {
		this.data = data;
		sources = new ArrayList<RecommendationModification>();
	}
	
	public void clear() { sources.clear(); points=0; highestWeight=Weight.NEUTRAL; pointsOverride=0;}
	public void add(RecommendationModification mod) {
		sources.add(mod);
		calculateHighestWeight();
		calculatePointsValue();
	}
	
	//-------------------------------------------------------------------
	private void calculateHighestWeight() {
		highestWeight = null;
		for (RecommendationModification rec : sources) {
			if (highestWeight==null || rec.getWeight().ordinal()>highestWeight.ordinal())
				highestWeight = rec.getWeight();
		}
		if (highestWeight==null) highestWeight=Weight.NEUTRAL;
	}
	
	//-------------------------------------------------------------------
	private void calculatePointsValue() {
		points=0;
		for (RecommendationModification rec : sources) {
			points += rec.getWeight().ordinal();
		}
	}
	
	public Weight getWeight() { return weightOverride!=null?weightOverride:highestWeight; }
	public int getPoints() { return (pointsOverride!=0)?pointsOverride:points; }
	
//	public void merge(Recommendation<T> other) {
//		if (data!=other.data)
//			throw new IllegalArgumentException("does not match");
//		if (other.level.ordinal()>level.ordinal()) {
//			level = other.level;
//			count = 1;
//		} else if (other.level==level)
//			count++;
//	}
	
	public  String toString() { return data+"("+getWeight()+","+getPoints()+","+recommendationState+")"; }
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Recommendation<T> other) {
		return - Integer.compare(getPoints(), other.getPoints());
	}

	//-------------------------------------------------------------------
	/**
	 * @param weightOverride the weightOverride to set
	 */
	public void setWeightOverride(Weight weightOverride) {
		this.weightOverride = weightOverride;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the recommendationState
	 */
	public RecommendationState getRecommendationState() {
		return recommendationState;
	}

	//-------------------------------------------------------------------
	/**
	 * @param recommendationState the recommendationState to set
	 */
	public void setRecommendationState(RecommendationState recommendationState) {
		this.recommendationState = recommendationState;
	}
	
}
package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public abstract class CommonPerSkillSpellController extends ControllerImpl<Spell> implements IPerSkillSpellController {

	protected final static Logger logger = System.getLogger(CommonPerSkillSpellController.class.getPackageName()+".spell");

	protected static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SpliMoCharacterGenerator.class.getName());

	protected static Random RANDOM = new Random();

	protected SMSkill school;

	protected List<Integer> freeSpellLevel;

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public CommonPerSkillSpellController(SpliMoCharacterController parent, SMSkill school) {
		super(parent);
		this.school = school;
		freeSpellLevel = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		todos.clear();

		// Determine free spell levels
		SMSkillValue val = getModel().getSkillValue(school);
		freeSpellLevel.clear();
		int points=val.getModifiedValue();
		if (points>=1) freeSpellLevel.add(0);
		if (points>=3) freeSpellLevel.add(1);
		if (points>=6) freeSpellLevel.add(2);
		if (points>=9) freeSpellLevel.add(3);
		if (points>=12) freeSpellLevel.add(4);
		if (points>=15) freeSpellLevel.add(5);

		// Remove already used free spells
		for (SpellValue spell : getSelected()) {
			Integer freeUsed = spell.getFreeLevel();
			if (freeUsed!=null) {
				// Spell claims to have used a free slot
				// Test is that slot wasn't used before
				if (freeSpellLevel.contains(freeUsed)) {
					// Okay, slot wasn't in use yet
					freeSpellLevel.remove(freeUsed);
					logger.log(Level.INFO, "Use level {0} slot in {1} to pay spell {2}",freeUsed,school.getId(),spell.getKey());
				} else {
					// Free spell slot was already taken
					logger.log(Level.WARNING, "Spell {0} claimed to use free level {1} in {2}, but that was already taken - fix that", spell.getKey(), freeUsed, school.getId());
					// Mark spell as NOT using free slot
					spell.setFreeLevel(null);
				}
			}
		}

		for (Integer free : freeSpellLevel) {
			todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_FREE_SPELL_LEFT, free, school.getName()));
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Spell> getAvailable() {
		// Return all spells in the school that are not yet selected
		return SplitterMondCore.getItemList(Spell.class).stream()
			.filter(item -> parent.showDataItem(item))
			.filter(item -> item.hasSchool(school))
			.filter(item -> !getModel().hasSpell(item, school))
			.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<SpellValue> getSelected() {
		return getModel().getSpells().stream()
				.filter(spell -> spell.getSkill()==school)
				.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(Spell item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(SpellValue item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item.getResolved());
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Spell value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Spell data, Decision... decisions) {
		if (data==null) return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);
		int lvl = data.getLevelInSchool(school);
		if (lvl==-1) return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);

		int minLvl = (lvl==0)?1:(lvl*3);
		SMSkillValue val = getModel().getSkillValue(school);
		if (val==null)
			return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_SKILLVALUE_NOT_MET);
		if (val.getModifiedValue()<minLvl)
			return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_VALUE_TOO_LOW);
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<SpellValue> select(Spell value, Decision... decisions) {
		logger.log(Level.TRACE, "ENTER select({0})", value);
		try {
			Possible poss = canBeSelected(value, decisions);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to select spell ''{0}'', but {1}",value.getId(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			// Decide how to pay
			int spellLevel = value.getLevelInSchool(school);
			int usedFree = -1;
			for (int free : freeSpellLevel) {
				if (free>=spellLevel) {
					usedFree = free;
					break;
				}
			}

			SpellValue val = new SpellValue(value, school);
			getModel().addSpell(val);
			if (usedFree==-1) {
				int cost = (int)getSelectionCost(value);
				logger.log(Level.INFO, "Select spell {0} in school {1} for {2} XP", value.getId(), school.getId(), cost);
			} else {
				logger.log(Level.INFO, "Select spell {0} in school {1} using a level {2} free spell", value.getId(), school.getId(), usedFree);
				val.setFreeLevel(usedFree);
			}

			return new OperationResult<>(val);
		} finally {
			logger.log(Level.TRACE, "LEAVE select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(SpellValue value) {
		if (getModel().getSpells().contains(value))
			return Possible.TRUE;
		return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(SpellValue value) {
		logger.log(Level.TRACE, "ENTER deselect({0})", value);
		try {
			Possible poss = canBeDeselected(value);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to deselect spell ''{0}'', but {1}",value.getKey(), poss.getMostSevere());
				return false;
			}

			getModel().removeSpell(value);
			logger.log(Level.INFO, "Removed spell {0} in school {1}", value.getKey(), school.getId());

			return true;
		} finally {
			logger.log(Level.TRACE, "LEAVE deselect({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Spell data, Decision... decisions) {
		int lvl = data.getLevelInSchool(school);
		switch (lvl) {
		case 0: return 1;
		case 1: return 3;
		case 2: return 6;
		case 3: return 9;
		case 4: return 12;
		case 5: return 15;
		}
		return 0;
	}

}

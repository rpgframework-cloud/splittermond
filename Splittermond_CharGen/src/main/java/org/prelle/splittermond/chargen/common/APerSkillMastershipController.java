package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.PerSkillMastershipController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 *
 */
public abstract class APerSkillMastershipController extends ControllerImpl<Mastership>
		implements PerSkillMastershipController {

	protected SMSkillValue sVal;
	protected List<Integer> freeLevel;

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public APerSkillMastershipController(SpliMoCharacterController parent, SMSkillValue value) {
		super(parent);
		this.sVal = value;
		freeLevel = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Mastership> getAvailable() {
		List<Mastership> ret = new ArrayList<>();
		ret.addAll(sVal.getSkill().getMasterships().stream()
			// Not present yet
		    .filter(spec -> !sVal.hasMastership(spec))
			.collect(Collectors.toList())
			);

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<MastershipReference> getSelected() {
		return sVal.getMasterships();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(Mastership value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(MastershipReference value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Mastership value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Mastership value, Decision... decisions) {
		SpliMoCharacter model = getCharacterController().getModel();
		// Check points needed in skill
		int valueRequired = 3+ 3*value.getLevel();
		// For generation, 1 skill point is enough for level 1, if there are still "free" masterships
		if (value.getLevel()==1 && !model.isInCareerMode() && !freeLevel.isEmpty()) {
			valueRequired = 1;
		}
		if (sVal.getModifiedValue()<valueRequired) {
			return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,
					SpliMoRejectReasons.IMPOSS_SKILLVALUE_NOT_MET, valueRequired, sVal.getSkill().getName(getCharacterController().getLocale()));
		}
		// Check EXP
		int expNeeded = (int)getSelectionCost(value);
		if (model.getLevel()<expNeeded) {
			return new Possible(Severity.WARNING, SpliMoRejectReasons.RES,
					SpliMoRejectReasons.IMPOSS_NOT_ENOUGH_EXP, expNeeded);
		}
		// Check requirements
		Possible poss = SplitterTools.areRequirementsMet(model, value, decisions);
		if (!poss.get()) {
			return poss;
		}
		return GenericRPGTools.areAllDecisionsPresent(value, decisions);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 * Child classes should override this
	 */
	@Override
	public OperationResult<MastershipReference> select(Mastership value, Decision... decisions) {
		logger.log(Level.TRACE, "select({0}, {1})", value, List.of(decisions));
		try {
			Possible poss = canBeSelected(value, decisions);
			if (!poss.get()) {
				logger.log(Level.ERROR, "Trying to select {0} but {1}", value.getId(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			// Add
			MastershipReference ref = new MastershipReference(value, sVal.getResolved());
			sVal.addMastership(ref);
			logger.log(Level.INFO, "Added mastery {0} to skill {1}", value.getId(), sVal.getKey());

			return new OperationResult<MastershipReference>(ref);
		} finally {
			logger.log(Level.TRACE, "select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(MastershipReference value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(MastershipReference value) {
		logger.log(Level.TRACE, "deselect({0})", value.getKey());
		Possible poss = canBeDeselected(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Trying to deselect {0} but {1}", value.getKey(), poss.getMostSevere());
			return false;
		}

		logger.log(Level.INFO, "remove mastery {0} from {1}", value.getKey(), sVal.getKey());
		sVal.getMasterships().remove(value);

		SpliMoCharacter model = getCharacterController().getModel();
		if (value.getFree()==MastershipReference.PAYED_WITH_EXP) {
			// Mastership was paid with XP - return it
			int xp = value.getResolved().getLevel();
			model.setExpFree( model.getExpFree()+xp);
			model.setExpInvested(model.getExpInvested()-xp);
			logger.log(Level.INFO, "Release {0} XP", xp);
		}

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Mastership data, Decision... decisions) {
		return data.getLevel()*5;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCostString(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public String getSelectionCostString(Mastership data) {
		return String.valueOf( (int)getSelectionCost(data));
	}

	//-------------------------------------------------------------------
	private void calculateFree() {
		freeLevel.clear();
		int val = sVal.getModifiedValue();
		if (val>=6) freeLevel.add(1);
		if (val>=9) freeLevel.add(2);
		if (val>=12) freeLevel.add(3);
		if (val>=15) freeLevel.add(4);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		return unprocessed;
	}

}

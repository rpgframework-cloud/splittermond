/**
 * 
 */
package org.prelle.splittermond.chargen.ai;

/**
 * @author prelle
 *
 */
public class CostPerLevel {
	
	int lvl1, lvl2, lvl3, lvl4;
	
	public CostPerLevel(int val) {
		this.lvl1 = val;
		this.lvl2 = val;
		this.lvl3 = val;
		this.lvl4 = val;
	}
	
	public CostPerLevel(int l1, int l2, int l3, int l4) {
		this.lvl1 = l1;
		this.lvl2 = l2;
		this.lvl3 = l3;
		this.lvl4 = l4;
	}

	public CostPerLevel add(CostPerLevel toAdd)  {
		lvl1 += toAdd.lvl1;
		lvl2 += toAdd.lvl2;
		lvl3 += toAdd.lvl3;
		lvl4 += toAdd.lvl4;
		return this;
	}
	
	public String toString() {
		return String.format("%d/%d/%d/%d", lvl1,lvl2,lvl3,lvl4);
	}
}

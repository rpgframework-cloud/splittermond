package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.items.ItemTemplate;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.items.CarriedItem;

/**
 *
 */
public interface IEquipmentController extends
	ComplexDataItemController<ItemTemplate, CarriedItem<ItemTemplate>>,
	RecommendingController<ItemTemplate>,
	PartialController<ItemTemplate>,
	NumericalValueController<ItemTemplate, CarriedItem<ItemTemplate>>{

	//-------------------------------------------------------------------
	public EnhancementController getEnhancementController(CarriedItem<ItemTemplate> value);

	//-------------------------------------------------------------------
	public ICarriedItemController getCarriedItemController(CarriedItem<ItemTemplate> value);

	//-------------------------------------------------------------------
	public int getMoneyRemaining();

}

package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.common.APerSkillMastershipController;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class PerSkillMastershipGenerator extends APerSkillMastershipController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 * @param value
	 */
	public PerSkillMastershipGenerator(SpliMoCharacterController parent, SMSkillValue value) {
		super(parent, value);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Mastership> getAvailable() {
		return super.getAvailable().stream()
				.filter(m -> m.getLevel()==1)
				.collect(Collectors.toList())
			;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 * Child classes should override this
	 */
	@Override
	public OperationResult<MastershipReference> select(Mastership value, Decision... decisions) {
		logger.log(Level.TRACE, "select({0}, {1})", value, List.of(decisions));
		try {
			OperationResult<MastershipReference> result = super.select(value, decisions);
			if (result.wasSuccessful()) {
				parent.runProcessors();
			}
			return result;
		} finally {
			logger.log(Level.TRACE, "select({0})", value);
		}
	}

}

package org.prelle.splittermond.chargen.charctrl;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.requirements.Requirement;

public interface IRejectReasons {

	public final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(IRejectReasons.class, Locale.ENGLISH, Locale.GERMAN);;
	
	public final static String IMPOSS_NOT_ENOUGH_EXP     = "impossible.notEnoughExp";
	public final static String IMPOSS_NOT_ENOUGH_MONEY   = "impossible.notEnoughMoney";
	public final static String IMPOSS_MISSING_DECISIONS  = "impossible.missingDecisions";
	public final static String IMPOSS_NOT_PRESENT        = "impossible.notPresent";
	public final static String IMPOSS_ALREADY_PRESENT    = "impossible.alreadyPresent";
	public final static String IMPOSS_AUTO_ADDED         = "impossible.autoAdded";
	public final static String IMPOSS_ITEM_HAS_NO_LEVELS = "impossible.noLevels";
	public final static String IMPOSS_MAX_LEVEL_REACHED  = "impossible.maxLevelReached";
	public final static String IMPOSS_MIN_LEVEL_REACHED  = "impossible.minLevelReached";
	public final static String IMPOSS_MUST_CHOOSE_VARIANT= "impossible.mustChooseVariant";
	public final static String IMPOSS_NOT_ENOUGH_POINTS  = "impossible.notEnoughPoints";
	public final static String IMPOSS_NOT_EMBEDDABLE     = "impossible.notEmbeddable";
	public final static String IMPOSS_BASE_RESOURCE      = "impossible.deleteBaseResource";
	public final static String IMPOSS_SKILL_TOO_LOW      = "impossible.skillTooLow";


}

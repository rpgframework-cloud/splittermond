package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.splimo.Culture;
import org.prelle.splimo.CultureLore;
import org.prelle.splimo.Deity;
import org.prelle.splimo.Language;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SpliMoNameTable;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public class CultureGenerator extends ControllerImpl<Culture> implements CultureController {

	protected static Logger logger = System.getLogger(CultureGenerator.class.getPackageName()+".cult");

	private static Random RANDOM = new Random();

	private boolean allowUnusualCultures = false;
	private boolean allowUnusualDeities = false;

	private List<Culture> available;
	private List<Deity> availableDeities;

	//-------------------------------------------------------------------
	protected CultureGenerator(SpliMoCharacterController parent) {
		super(parent);
		available = new ArrayList<>();
		availableDeities = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(Culture decideFor, UUID choice, Decision decision) {
		logger.log(Level.WARNING, "decide {0}={1} for {2}", choice, decision.getValue(), decideFor.getClass().getSimpleName());
		parent.getModel().getCulture().addDecision(decision);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	public void setAllowUnusualCultures(boolean allow) {
		allowUnusualCultures = allow;
		updateAvailable();
	}
	public boolean isAllowUnusualCultures() { return allowUnusualCultures;}

	//-------------------------------------------------------------------
	public void setAllowUnusualDeities(boolean allow) {
		allowUnusualDeities = allow;
	}
	public boolean isAllowUnusualDeities() { return allowUnusualDeities; }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Culture item) {
		if (item==null) return RecommendationState.NEUTRAL;
		// No recommender set, means no recommendation
		if (parent.getRecommender().isEmpty()) {
			return RecommendationState.NEUTRAL;
		}

		return parent.getRecommender().get().getRecommendationState(item);
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		available.clear();

		nextCulture:
		for (Culture cult : SplitterMondCore.getItemList(Culture.class)) {
			if (!allowUnusualCultures) {
				for (Requirement req : cult.getRequirements()) {
					boolean met = SplitterTools.isRequirementMet(parent.getModel(), req);
					if (!met) {
						logger.log(Level.TRACE,"CultureGenerator: {0} does not meet {1}",cult,req, SplitterTools.getRequirementString(req, Locale.getDefault()));
						continue nextCulture;
					}
				}
			}
			available.add(cult);
		}
		logger.log(Level.INFO, "Available: {0}",available);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#getAvailable()
	 */
	@Override
	public List<Culture> getAvailable() {
		return new ArrayList<Culture>(available);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#select(org.prelle.splimo.Culture)
	 */
	@Override
	public void select(Culture value) {
		logger.log(Level.ERROR, "Select culture ''{0}''",value);
		SpliMoCharacter model = getModel();
		if (value!=null && value.getId().equals(model.getCulture().getKey()))
			return;
		logger.log(Level.ERROR, "Change culture from ''{0}'' to {1}",parent.getModel().getCulture().getResolved(), value);
		for (PowerValue ref : model.getPowers()) {
			logger.log(Level.INFO, "Before change "+ref+" auto="+ref.isAutoAdded());
		}
		parent.getModel().setCulture(value);
		for (PowerValue ref : model.getPowers()) {
			logger.log(Level.INFO, "After change "+ref+" auto="+ref.isAutoAdded());
		}
		if (value!=null)
			updateChoices(value);

		// Recalculate
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	private void updateAvailableDeities() {
		availableDeities.clear();

		nextCulture:
			for (Deity deity : SplitterMondCore.getItemList(Deity.class)) {
				if (!allowUnusualDeities) {
					boolean foundCulture = false;
					for (Culture cult : deity.getFavoredCultures()) {
						if (cult.getId().equals(parent.getModel().getCulture())) {
							foundCulture = true;
							break;
						}
					}
					if (!foundCulture) {
						continue nextCulture;
					}
				}
				availableDeities.add(deity);
		}

		Collections.sort(availableDeities, new Comparator<Deity>() {
			public int compare(Deity o1, Deity o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		logger.log(Level.INFO, "Available deities: "+availableDeities);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#getAvailableDeities()
	 */
	@Override
	public List<Deity> getAvailableDeities() {
		return availableDeities;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#selectDeity(org.prelle.splimo.Deity)
	 */
	@Override
	public void selectDeity(Deity value) {
		// Do nothing, if nothing changes
		if (value==null && parent.getModel().getDeity()==null || (value.getId().equals(parent.getModel().getDeity()))) {
			return;
		}

		logger.log(Level.INFO, "Select deity '"+value.getId()+"'");
		parent.getModel().setDeity(value.getId());

		// Recalculate
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ControllerImpl#roll()
	 */
	@Override
	public void roll() {
		List<Culture> options = getAvailable().stream()
				.filter(c -> SplitterTools.areRequirementsMet(getModel(), c, new Decision[0]).get())
				.collect(Collectors.toList());
		if (parent.getRecommender().isPresent()) {
			options = (List<Culture>) parent.getRecommender().get().getMostRecommended(options.get(0).getClass(), options);
		}
		int idx = random.nextInt(options.size());
		Culture item = options.get(idx);
		logger.log(Level.ERROR, "Rolled {0} from {1} possible options", item.getId(), options.size());
		select(item);

		List<Decision> decisions = new ArrayList<>();
		for (Choice choice : item.getChoices()) {
			logger.log(Level.ERROR, "Choice "+choice);
			parent.getRecommender().ifPresent(r -> decide(item, choice.getUUID(),r.decide(choice)));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#rollName()
	 */
	@Override
	public String rollName() {
		String cultID = parent.getModel().getCulture().getKey();
		logger.log(Level.DEBUG, "Roll name for culture "+cultID);
		if (cultID==null)
			return null;
		SpliMoNameTable table = SplitterMondCore.getItem(SpliMoNameTable.class, cultID);
		if (table==null) {
			logger.log(Level.WARNING, "Cannot generate random name - no nametable for culture '"+cultID+"'");
			return null;
		}
		String name = table.generateName(parent.getModel().getGender(), SpliMoNameTable.OPTIONS.TOWN);
		logger.log(Level.INFO, "Rolled name: "+name);
		parent.getModel().setName(name);
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#rollDeity()
	 */
	@Override
	public Deity rollDeity() {
		logger.log(Level.INFO, "Roll deity: "+availableDeities);
		logger.log(Level.DEBUG, "Roll deity2: "+availableDeities);
		if (availableDeities.isEmpty())
			return null;
		int idx = RANDOM.nextInt(availableDeities.size());
		Deity deity = availableDeities.get(idx);
		selectDeity(deity);
		return deity;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		todos.clear();
		int undecided = 0;

		Culture selected = null;
		try {
			updateAvailable();
			updateAvailableDeities();

			// Apply modifications by culture
			// Create DecisionToMake for each ModificationChoice
			String key = model.getCulture().getKey();
			if (key==null) {
				todos.add(new ToDoElement(Severity.INFO, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_CULTURE_NOT_SELECTED));
			} else {
				selected = SplitterMondCore.getItem(Culture.class, model.getCulture().getKey());
				if (selected==null) {
					logger.log(Level.ERROR, "Unknown culture '"+model.getCulture()+"' selected");
					todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_CULTURE_UNKNOWN, model.getCulture().getKey()));
					return previous;
				}
				// selected != null  from here
				super.updateChoices(selected);
				logger.log(Level.DEBUG, "1. Apply modifications from culture "+selected+" = "+selected.getOutgoingModifications());
				for (Modification tmp : selected.getOutgoingModifications()) {

					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						// Apply modifications from choices
						if ("CHOICE".equals(dMod.getKey())) {
							Choice choice = selected.getChoice(dMod.getConnectedChoice());
							Decision dec = parent.getModel().getCulture().getDecision(dMod.getConnectedChoice());
//							if (dec==null) {
//								dec = parent.getModel().getDecision(dMod.getConnectedChoice());
//							}
							if (dec!=null) {
								logger.log(Level.DEBUG, "  add modifications from choice "+dMod.getConnectedChoice());
								logger.log(Level.DEBUG, "  ==> "+GenericRPGTools.decisionToModifications(dMod, choice, dec));
								unprocessed.addAll(GenericRPGTools.decisionToModifications(dMod, choice, dec));
							} else {
								logger.log(Level.DEBUG, "  ignore modifications from open decision "+dMod.getConnectedChoice());
								undecided++;
							}
						} else if (SplittermondReference.LANGUAGE==dMod.getReferenceType()) {
							Language lang = dMod.getResolvedKey();
							if (lang!=null) {
								logger.log(Level.DEBUG, "  add language "+dMod.getResolvedKey());
								model.addAutoLanguage(lang);
							}
						} else if (SplittermondReference.CULTURE_LORE==dMod.getReferenceType()) {
							CultureLore lore = dMod.getResolvedKey();
							if (lore!=null) {
								logger.log(Level.DEBUG, "  add culture lore "+dMod.getResolvedKey());
								model.addAutoCultureLore(lore);
							}
						} else {
							if (dMod.getReferenceType()==SplittermondReference.SKILL) {
								logger.log(Level.DEBUG, "Inject modification {0}",tmp);
							}
							unprocessed.add(tmp);
						}
					} else if (tmp instanceof ModificationChoice) {
						ModificationChoice cMod = (ModificationChoice)tmp;
						UUID choiceUUID = cMod.getUUID();
						Decision dec = parent.getModel().getCulture().getDecision(choiceUUID);
						if (dec==null) {
							logger.log(Level.DEBUG, "  ignore modifications from open decision {0}",choiceUUID);
							undecided++;
						} else {
							Modification selectedMod = cMod.getModification(dec.getValueAsUUID());
							logger.log(Level.WARNING,"{0} = {1}", choiceUUID, selectedMod);
							if (selectedMod!=null) {
								unprocessed.add(selectedMod);
							} else {
								logger.log(Level.WARNING, "Decision {0} of culture {1} is unknown", dec.getValueAsUUID(), key);
							}
						}
					} else {
						logger.log(Level.WARNING, "Ignore "+tmp);
						System.exit(1);
					}
				}
			}

			if (undecided>0) {
				todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_CULTURE_DECISIONS, undecided));
			}
		} finally {
			logger.log(Level.TRACE, "STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.CultureController#getDeityRecommender()
	 */
	@Override
	public RecommendingController<Deity> getDeityRecommender() {
		return new RecommendingController<Deity>() {

			@Override
			public RecommendationState getRecommendationState(Deity item) {
				// TODO Auto-generated method stub
				return RecommendationState.RECOMMENDED;
			}
		};
	}

}

package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Education;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.PerSkillMastershipController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;
import org.prelle.splittermond.chargen.common.ASkillController;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class SkillGenerator extends ASkillController {

	//-------------------------------------------------------------------
	public SkillGenerator(SpliMoCharacterController parent) {
		super(parent);
		for (SMSkill school : SplitterMondCore.getSkills(SkillType.MAGIC)) {
			spellController.put(school, new PerSkillSpellGenerator(parent, school));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		List<SMSkillValue> ordered = new ArrayList<>(parent.getModel().getSkillValues());
		Collections.sort(ordered, new Comparator<SMSkillValue>() {
			@Override
			public int compare(SMSkillValue o1, SMSkillValue o2) {
				return Integer.compare(o1.getModifiedValue(), o2.getModifiedValue());
			}
		});

		for (SMSkillValue val : ordered) {
			if (points>0 && val.getModifiedValue()<6) {
				val.setDistributed( val.getDistributed()+1);
				points--;
			}
		}

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(SMSkillValue value) {
		Possible poss = super.canBeIncreased(value);
		if (!poss.get())
			return poss;

		// Can it be paid?
		if (points>0)
			return Possible.TRUE;
		int exp = 3;
		if (parent.getModel().getExpFree()>=exp)
			return Possible.TRUE;

		return Possible.FALSE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<SMSkillValue> increase(SMSkillValue value) {
		OperationResult<SMSkillValue> result = super.increase(value);
		if (result.hasError())
			return result;

		parent.runProcessors();
		return new OperationResult<>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(SMSkillValue value) {
		return new Possible(value.getModifiedValue()>0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return points;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getToDos(org.prelle.splimo.SMSkill.SkillType)
	 */
	@Override
	public List<String> getToDos(SkillType type) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getToDos(org.prelle.splimo.SMSkill)
	 */
	@Override
	public List<String> getToDos(SMSkill skill) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	private List<Modification> processModifications(List<Modification> previous, int depth) {
		List<Modification> unprocessed = new ArrayList<>();
		// Consume skill modifications
		for (Modification mod : previous) {
			if ((mod instanceof ValueModification) && ((ValueModification)mod).getReferenceType()==SplittermondReference.SKILL) {
				ValueModification vMod = (ValueModification)mod;
//				if ("CHOICE".equals(vMod.getKey())) {
//					logger.log(Level.WARNING, depth+": ToDo: choice "+vMod.getConnectedChoice()+" from "+vMod.getSource());
//					if (parent.getModel().hasDecisionBeenMade(vMod.getConnectedChoice())) {
//						Choice choice = ((ComplexDataItem)vMod.getSource()).getChoice(vMod.getConnectedChoice());
//						Decision decis = parent.getModel().getDecision(vMod.getConnectedChoice());
//						// Convert decision into a list of modifications to add
//						List<Modification> newMods = GenericRPGTools.decisionToModifications(vMod, choice, decis);
//						if (!newMods.isEmpty()) {
//							logger.log(Level.WARNING, depth+": Do  "+newMods);
//							unprocessed.addAll(processModifications(newMods, depth+1));
//							logger.log(Level.WARNING, depth+": Done  "+unprocessed);
////							System.exit(1);
//						}
//					} else {
//						// No decision has been made
//						Choice choice = ((ComplexDataItem)vMod.getSource()).getChoice(vMod.getConnectedChoice());
//						String text = SplitterTools.getChoiceString(((ComplexDataItem)vMod.getSource()), choice);
//						logger.log(Level.WARNING, depth+": No decision yet for: "+text);
//						todos.add(new ToDoElement(Severity.INFO, ResourceI18N.format(RES, "todos.undecided", text)));
//					}
//				} else
					if (vMod.getKey().indexOf(":")>0) {
					// Choice with a value
					String sKey = vMod.getKey().substring(0, vMod.getKey().indexOf(":"));
					int val = Integer.valueOf(vMod.getKey().substring(vMod.getKey().indexOf(":")+1));
					SMSkill skill = SplitterMondCore.getSkill(sKey);
					if (skill==null) {
						logger.log(Level.ERROR, "Unknown skill found : "+vMod.getKey()+" or "+sKey);
						System.err.println("Unknown skill found : "+vMod.getKey());
					}
					SMSkillValue value = parent.getModel().getSkillValue(skill);
					if (value==null) {
						logger.log(Level.INFO, depth+": No skillvalue for "+skill);
					}
					ValueModification newMod = new ValueModification(SplittermondReference.SKILL, sKey, val);
					newMod.setSource(vMod.getSource());
					value.addIncomingModification(newMod);
					logger.log(Level.INFO, depth+": Consume skill modification "+vMod+"  with "+vMod.getConnectedChoice()+" from "+vMod.getSource()+" ... "+value.getModifiedValue());
				} else {
					SMSkill skill = SplitterMondCore.getSkill(vMod.getKey());
					if (skill==null) {
						logger.log(Level.ERROR, "Unknown skill found : "+vMod.getKey());
						System.err.println("Unknown skill found : "+vMod.getKey());
					} else {
					SMSkillValue value = parent.getModel().getSkillValue(skill);
					if (value==null) {
						logger.log(Level.INFO, depth+": No skillvalue for "+skill);
					}
					value.addIncomingModification(vMod);
					//logger.log(Level.DEBUG, "Consume skill modification "+vMod+"  with "+vMod.getConnectedChoice()+" from "+vMod.getSource()+" ... "+value.getModifiedValue());
					}
				}
			} else if (mod instanceof DataItemModification && ((DataItemModification)mod).getReferenceType()==SplittermondReference.SKILL_SPECIAL) {
				DataItemModification dMod = (DataItemModification)mod;
				try {
					SMSkillSpecialization spec = skillSpecConv.read(dMod.getKey());
					SMSkill skill = spec.getSkill();
					SMSkillValue  value = parent.getModel().getSkillValue(skill);
					SkillSpecializationValue<SMSkill> result = value.setSpecializationLevel(spec, 1);
					if (result!=null) {
						result.setInjectedBy(dMod.getSource());
						result.addIncomingModification(dMod);
					}
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed adding skill specialization "+mod,e);
					unprocessed.add(dMod);
				}
			} else {
				// Don't consume modification
				unprocessed.add(mod);
			}
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);
		SpliMoCharacter model = getModel();

		logger.log(Level.DEBUG, "START: process");
		points = 55;
		masterPoints = 3;
		choices.clear();
		todos.clear();

		try {
			// Count all skill points present
			int pointsBG = 5;
			int pointsCU = 15;
			int pointsED = 30;
			for (SMSkillValue sVal : parent.getModel().getSkillValues()) {
				int toPay = sVal.getModifiedValue();
				if (toPay>0) {
					for (Modification tmp : sVal.getIncomingModifications()) {
						if (!(tmp instanceof ValueModification)) continue;
						ValueModification vMod = (ValueModification)tmp;
						if (vMod.getSource() instanceof Education) pointsED -= vMod.getValue();
						if (vMod.getSource() instanceof Background) pointsBG -= vMod.getValue();
						if (vMod.getSource() instanceof Culture) pointsCU -= vMod.getValue();
					}
					String foo = sVal.getIncomingModifications().stream().map(m -> ((ValueModification)m).getValue() +" "+ m.getSource()).toList().toString();
					logger.log(Level.INFO, points+" Pay {0} for {1} \t{3} {4} {5} \t{2}", toPay, sVal.getKey(), foo, pointsCU, pointsBG, pointsED);
					int canPay = Math.min(toPay, points);
					if (canPay>0) {
						points -= canPay;
						logger.log(Level.TRACE, "invest "+sVal.getModifiedValue()+" points in "+sVal.getModifyable().getId());
						toPay -= canPay;
					}
					if (toPay>0) {
						toPay = toPay*3;
						logger.log(Level.INFO, "invest {0} exp in {1}", toPay, sVal.getModifyable().getId());
						model.setExpFree( model.getExpFree() - toPay);
					}
				}

				// Pay for masteries
				boolean oneFree = sVal.getModifiedValue()>=6;
				for (MastershipReference master : sVal.getMasterships()) {
					if (oneFree) {
						logger.log(Level.INFO, "   Pay {0} with free mastership in {1}", master.getKey(), sVal.getModifyable().getId());
						oneFree=false;
						master.setFree(1);
						continue;
					}
					master.setFree(0);
					if (masterPoints>0) {
						masterPoints -= 1;
						logger.log(Level.INFO, "   Pay {0} from the 3 free masteries", master.getKey());
					} else {
						getModel().setExpFree(getModel().getExpFree()-5);
						logger.log(Level.INFO, "   Pay {0} with XP", master.getKey());
					}
				}
				for (SkillSpecializationValue<SMSkill> master : sVal.getSpecializations()) {
					if (oneFree) {
						logger.log(Level.INFO, "   Pay specialization {0} with free mastership in {1}", master.getKey(), sVal.getModifyable().getId());
						oneFree=false;
						continue;
					}
					if (masterPoints>0) {
						masterPoints -= 1;
						logger.log(Level.INFO, "   Pay {0} from the 3 free masteries", master.getKey());
					} else {
						getModel().setExpFree(getModel().getExpFree()-5);
						logger.log(Level.INFO, "   Pay {0} with XP", master.getKey());
					}
				}
				if (oneFree) {
					todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_FREE_MASTERSHIPS_IN_LEFT, sVal.getNameWithoutRating(getCharacterController().getLocale())));
				}

				// There cannot be specializations or masteries if the final value is 0
				if ( (!sVal.getMasterships().isEmpty() || !sVal.getSpecializations().isEmpty()) && sVal.getModifiedValue()==0 ) {
					todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_FREE_MASTERSHIPS_IN_LEFT, sVal.getNameWithoutRating(getCharacterController().getLocale())));
				}
			}
			logger.log(Level.INFO, "Skill points left: "+points);

			if (points>0) {
				todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_SKILLPOINTS_LEFT, points));
			}
			if (masterPoints>0) {
				todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_FREE_MASTERSHIPS_LEFT, masterPoints));
			}

			/* Now walk through all spell controllers */
			for (IPerSkillSpellController spell : spellController.values()) {
				unprocessed = spell.process(unprocessed);
			}
//			for (SMSkill skill : SplitterMondCore.getItemList(SMSkill.class)) {
//				PerSkillMastershipController master = getMastershipController(model.getSkillValue(skill));
//				unprocessed = master.process(unprocessed);
//			}
		} finally {
			logger.log(Level.DEBUG, "STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getSelected()
	 */
	@Override
	public List<SMSkillValue> getSelected() {
		return parent.getModel().getSkillValues();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#canSelectSpecialization(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecialization)
	 */
	@Override
	public Possible canSelectSpecialization(SMSkillValue data, SkillSpecialization<SMSkill> spec) {
		logger.log(Level.TRACE, "ENTER canSelectSpecialization( {0}, {1} )", data, spec);
		try {
			// Skill does not exist in character
			if (!parent.getModel().getSkillValues().contains(data)) {
				return Possible.FALSE;
			}
			// Specialization already exist in character
			if (data.getSpecialization(spec)!=null) {
				return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_ALREADY_EXISTS, spec.getName());
			}
			// If there are free points, fine
			if (points>0) {
				return Possible.TRUE;
			}
			// No free points - need 7 XP and a value of at least 1
			if (data.getModifiedValue()<1) {
				return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_VALUE_TOO_LOW, spec.getName());
			}
			if (getModel().getExpFree()<7) {
				return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_NOT_ENOUGH_EXP, spec.getName());
			}
			return Possible.TRUE;
		} finally {
			logger.log(Level.TRACE, "LEAVE canSelectSpecialization( {0}, {1} )", data, spec);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#canDeselectSpecialization(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecializationValue)
	 */
	@Override
	public Possible canDeselectSpecialization(SMSkillValue data, SkillSpecializationValue<SMSkill> val) {
		logger.log(Level.TRACE, "ENTER canSelectSpecialization( {0}, {1} )", data, val);
		try {
			// Skill does not exist in character
			if (!parent.getModel().getSkillValues().contains(data)) {
				return Possible.FALSE;
			}
			// Specialization does not exist in character
			if (!data.getSpecializations().contains(val)) {
				return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST, val.getNameWithoutRating());
			}
			return Possible.TRUE;
		} finally {
			logger.log(Level.TRACE, "LEAVE canDeelectSpecialization( {0}, {1} )", data, val);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(SMSkillValue value) {
		return value.getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#selectSpecialization(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecialization)
	 */
	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> selectSpecialization(SMSkillValue data,
			SkillSpecialization<SMSkill> spec) {
		logger.log(Level.TRACE, "ENTER selectSpecialization");
		try {
			Possible poss = canSelectSpecialization(data, spec);
			if (!poss.get()) {
				return new OperationResult<SkillSpecializationValue<SMSkill>>(poss);
			}
			// Remove specialization
			logger.log(Level.INFO, "Add specialization {1} to {0}", spec, poss);
			SkillSpecializationValue<SMSkill> sval = new SkillSpecializationValue<>(spec);
			sval.setDistributed(1);
			data.getSpecializations().add(sval);

			parent.runProcessors();
			return new OperationResult<SkillSpecializationValue<SMSkill>>(sval);
		} finally {
			logger.log(Level.TRACE, "LEAVE selectSpecialization");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#deselectSpecialization(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecialization)
	 */
	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> deselectSpecialization(SMSkillValue data, SkillSpecializationValue<SMSkill> sval) {
		logger.log(Level.TRACE, "ENTER deselectSpecialization");
		try {
			Possible poss = canDeselectSpecialization(data, sval);
			if (!poss.get()) {
				return new OperationResult<SkillSpecializationValue<SMSkill>>(poss);
			}
			// Remove specialization
			logger.log(Level.INFO, "remove {0} from {1}", sval, poss);
			data.getSpecializations().remove(sval);

			parent.runProcessors();
			return null;
		} finally {
			logger.log(Level.TRACE, "LEAVE deselectSpecialization");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#canBeIncreased(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecializationValue)
	 */
	@Override
	public Possible canBeIncreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value) {
		// Cannot increase above 1 at chargen
		return Possible.FALSE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#canBeDecreased(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecializationValue)
	 */
	@Override
	public Possible canBeDecreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value) {
		if (!data.getSpecializations().contains(value))
			return Possible.FALSE;
		if (value.getDistributed()<=0)
			return Possible.FALSE;
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#increase(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecializationValue)
	 */
	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> increase(SMSkillValue data,
			SkillSpecializationValue<SMSkill> value) {
		return new OperationResult<>(Possible.FALSE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#decrease(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecializationValue)
	 */
	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> decrease(SMSkillValue data,
			SkillSpecializationValue<SMSkill> value) {
		Possible poss = canBeDecreased(data, value);
		if (!poss.get())
			return new OperationResult<>(poss);
		return deselectSpecialization(data, value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getMastershipController(org.prelle.splimo.SMSkillValue)
	 */
	@Override
	public PerSkillMastershipController getMastershipController(SMSkillValue data) {
		CharacterController<Attribute,SpliMoCharacter> ctrl = getCharacterController();
		return new PerSkillMastershipGenerator((SpliMoCharacterController)ctrl, data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getFreeMastershipPoints()
	 */
	@Override
	public int getFreeMastershipPoints() {
		return masterPoints;
	}

}

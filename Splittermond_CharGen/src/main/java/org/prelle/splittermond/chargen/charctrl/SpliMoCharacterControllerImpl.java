package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.ai.ProfileControllerImpl;
import org.prelle.splittermond.chargen.common.CommonCombinedSpellController;
import org.prelle.splittermond.chargen.gen.PerSkillSpellGenerator;

import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.CharacterControllerImpl;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * A base class for leveller and generator
 * @author Stefan
 *
 */
public abstract class SpliMoCharacterControllerImpl extends CharacterControllerImpl<Attribute,SpliMoCharacter>
		implements SpliMoCharacterController {

	protected PowerController powers;
	protected ResourceController resources;
	protected AttributeController attrib;
	protected SkillController skills;
	protected ICombinedSpellController spells;
	protected IEquipmentController equip;


	//-------------------------------------------------------------------
	public SpliMoCharacterControllerImpl() {
		super.profileCtrl = new ProfileControllerImpl(this);
		this.spells = new CommonCombinedSpellController(this);
	}

	//-------------------------------------------------------------------
	@Override
	public void fireEvent(ControllerEvent type, Object...param) {
		super.fireEvent(type, param);
		if (type==BasicControllerEvents.CHARACTER_PROFILES_CHANGED) {
			recommender.ifPresent(r -> r.update());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called by constructor to setup partial controllers
	 */
	abstract protected void createPartialController();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getRecommendingControllerFor(java.lang.Object)
	 */
	@Override
	public <T> RecommendingController<T> getRecommendingControllerFor(T item) {
		if (item instanceof Attribute && attrib instanceof RecommendingController)
			return (RecommendingController<T>) getAttributeController();
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getPowerController()
	 */
	@Override
	public PowerController getPowerController() {
		return powers;
	}

	//-------------------------------------------------------------------
	public ResourceController getResourceController() {
		return resources;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attrib;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController()
	 */
	@Override
	public ICombinedSpellController getSpellController() {
		return spells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController()
	 */
	@Override
	public IPerSkillSpellController getSpellController(SMSkill school) {
		return new PerSkillSpellGenerator(this,school);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getEquipmentController()
	 */
	@Override
	public IEquipmentController getEquipmentController() {
		return equip;
	}

}

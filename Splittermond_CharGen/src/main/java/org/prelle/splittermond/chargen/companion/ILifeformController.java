package org.prelle.splittermond.chargen.companion;

import java.util.Collection;
import java.util.List;

import org.prelle.splimo.creature.Companion;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.data.DataItem;

public interface ILifeformController {

	//-------------------------------------------------------------------
	public Companion getModel();
	public void setModel(Companion data);

	//-------------------------------------------------------------------
	/**
	 * Has the user the content pack and correct language for the item?
	 */
	public default boolean showDataItem(DataItem item) { return true; }

	//-------------------------------------------------------------------
	public void addListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public void removeListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public boolean hasListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public Collection<ControllerListener> getListener();

	//-------------------------------------------------------------------
	public void fireEvent(ControllerEvent type, Object...param);

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos();

	//-------------------------------------------------------------------
	public void setAllowRunProcessor(boolean value);

	//-------------------------------------------------------------------
	public void runProcessors();

}

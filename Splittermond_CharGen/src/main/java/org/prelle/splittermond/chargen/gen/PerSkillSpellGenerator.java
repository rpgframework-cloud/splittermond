package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpellValue;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.common.CommonPerSkillSpellController;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class PerSkillSpellGenerator extends CommonPerSkillSpellController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 * @param school
	 */
	public PerSkillSpellGenerator(SpliMoCharacterController parent, SMSkill school) {
		super(parent, school);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		super.process(unprocessed);
		
		// Pay all non-free spells with XP cost
		for (SpellValue spell : getSelected()) {
			if (spell.getFreeLevel()!=null)
				continue;
			int exp = (int)getSelectionCost(spell.getResolved());
			getModel().setExpInvested( getModel().getExpInvested() +exp);
			getModel().setExpFree    ( getModel().getExpFree() -exp);
			logger.log(Level.INFO, "Pay {0} XP for ''{1}'' in {2}", exp, spell.getKey(), school.getId());
		}
		
		return unprocessed;
	}

}

package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface IPerSkillSpellController extends PartialController<Spell>, RecommendingController<Spell>, ComplexDataItemController<Spell, SpellValue> {

}

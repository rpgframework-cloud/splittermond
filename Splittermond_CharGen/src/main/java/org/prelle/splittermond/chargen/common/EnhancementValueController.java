package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;

public class EnhancementValueController implements NumericalValueController<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> {

	private final static Logger logger = System.getLogger(EnhancementValueController.class.getPackageName());

	private CarriedItem<ItemTemplate> item;
	private Enhancement what;
	private EnhancementController enhanceCtrl;
	private boolean reverseDirection;

	//-------------------------------------------------------------------
	public EnhancementValueController(CarriedItem<ItemTemplate> item, Enhancement what, EnhancementController enhanceCtrl, boolean reverseDirection) {
		this.item = item;
		this.what = what;
		this.enhanceCtrl = enhanceCtrl;
		this.reverseDirection = reverseDirection;
		if (what==null)
			throw new NullPointerException("Enhancement to manage is NULL");
	}

	//-------------------------------------------------------------------
	@Override
	public RecommendationState getRecommendationState(SMItemAttribute item) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	@Override
	public int getValue(ItemAttributeNumericalValue<SMItemAttribute> value) {
		return value.getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(ItemAttributeNumericalValue<SMItemAttribute> data) {
		logger.log(Level.INFO, "can LOAD be canBeIncreased in {0} (meaning decrease LOAD enhancement)", item);
		ItemEnhancementValue<Enhancement> val = item.getEnhancement(what);
		if (val==null) {
			// No such enhancement yet
			logger.log(Level.WARNING, "TODO: allow negative quality");
			return Possible.FALSE;
		}

		if (item.hasEnhancement(what)) {
			return reverseDirection? enhanceCtrl.canBeDecreased(item.getEnhancement(what)) : enhanceCtrl.canBeIncreased(item.getEnhancement(what));
		} else {
			return reverseDirection? enhanceCtrl.canBeDeselected(val) : enhanceCtrl.canBeSelected(what, new Decision("c2d17c87-1cfe-4355-9877-a20fe09c170d","1"));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(ItemAttributeNumericalValue<SMItemAttribute> data) {
		ItemEnhancementValue<Enhancement> val = item.getEnhancement(what);
		logger.log(Level.INFO, "can LOAD be decreased in {0} (meaning increase LOAD enhancement)", item);

		if (val!=null) {
			return reverseDirection? enhanceCtrl.canBeIncreased(val) : enhanceCtrl.canBeDecreased(item.getEnhancement(what));
		} else {
			return reverseDirection? enhanceCtrl.canBeSelected(what, new Decision("c2d17c87-1cfe-4355-9877-a20fe09c170d","1")) : enhanceCtrl.canBeDeselected(val);
		}
	}

	//-------------------------------------------------------------------
	@Override
	public OperationResult<ItemAttributeNumericalValue<SMItemAttribute>> increase(ItemAttributeNumericalValue<SMItemAttribute> data) {
		ItemEnhancementValue<Enhancement> value = item.getEnhancement(what);
		Possible poss = canBeIncreased(data);
		if (!poss.get()) {
			logger.log(Level.WARNING,"Tried to increase LOAD which is not allowed: {0}",poss);
			return new OperationResult<ItemAttributeNumericalValue<SMItemAttribute>>(poss);
		}
		enhanceCtrl.increase(value);
		return new OperationResult<ItemAttributeNumericalValue<SMItemAttribute>>(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<ItemAttributeNumericalValue<SMItemAttribute>> decrease(ItemAttributeNumericalValue<SMItemAttribute> data) {
		ItemEnhancementValue<Enhancement> val = item.getEnhancement(what);
		Possible poss = canBeDecreased(data);
		if (!poss.get()) {
			logger.log(Level.WARNING,"Tried to decrease LOAD which is not allowed: {0}",poss);
			return new OperationResult<ItemAttributeNumericalValue<SMItemAttribute>>(poss);
		}

		if (val!=null) {
			enhanceCtrl.increase(val);
			logger.log(Level.INFO, "Increased {0} for {1}", what, item);
		} else {
			enhanceCtrl.select(what, new Decision("c2d17c87-1cfe-4355-9877-a20fe09c170d","1"));
			logger.log(Level.INFO, "Selected {0} for {1}", what, item);
		}

		return new OperationResult<ItemAttributeNumericalValue<SMItemAttribute>>(data);
	}

}

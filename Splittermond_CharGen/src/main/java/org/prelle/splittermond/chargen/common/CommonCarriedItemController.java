package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.Consumer;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;

/**
 *
 */
public class CommonCarriedItemController implements ICarriedItemController {

	private final static Logger logger = System.getLogger(CommonCarriedItemController.class.getPackageName());

	private CarriedItem<ItemTemplate> item;
	private EnhancementController enhanceCtrl;

	private Enhancement enhLoad;
	private Enhancement enhRigid;
	private Enhancement enhDamage;
	private Enhancement enhSpeed;

	//-------------------------------------------------------------------
	public CommonCarriedItemController(CarriedItem<ItemTemplate> item, EnhancementController enhanceCtrl) {
		this.item = item;
		this.enhanceCtrl = enhanceCtrl;

		enhLoad = SplitterMondCore.getItem(Enhancement.class, "load");
		enhRigid = SplitterMondCore.getItem(Enhancement.class, "load");
		enhDamage = SplitterMondCore.getItem(Enhancement.class, "damage");
		enhSpeed = SplitterMondCore.getItem(Enhancement.class, "speed");
	}

	//-------------------------------------------------------------------
	public EnhancementController getEnhancementController() {
		return enhanceCtrl;
	}

	//-------------------------------------------------------------------
	public void addListener(Consumer<CarriedItem<ItemTemplate>> listener) {
		enhanceCtrl.addListener(listener);
	}
	//-------------------------------------------------------------------
	public void removeListener(Consumer<CarriedItem<ItemTemplate>> listener) {
		enhanceCtrl.removeListener(listener);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ICarriedItemController#getValueController(org.prelle.splimo.items.SMItemAttribute)
	 */
	@Override
	public NumericalValueController<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> getValueController(
			SMItemAttribute attrib) {
		switch (attrib) {
		case DAMAGE:
			return new EnhancementValueController(item, enhDamage, enhanceCtrl, false);
		case LOAD:
			return new EnhancementValueController(item, enhLoad, enhanceCtrl, true);
		case RIGIDITY:
			return new EnhancementValueController(item, enhRigid, enhanceCtrl, false);
		case SPEED:
			return new EnhancementValueController(item, enhSpeed, enhanceCtrl, true);
		}
		logger.log(Level.ERROR, "Not implemented: Value Controller for {0}", attrib);
		return null;
	}

}

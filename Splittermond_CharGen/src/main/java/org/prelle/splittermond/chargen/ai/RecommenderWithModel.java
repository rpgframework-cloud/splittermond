/**
 *
 */
package org.prelle.splittermond.chargen.ai;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Education;
import org.prelle.splimo.Race;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.RecommendationModification;
import de.rpgframework.genericrpg.requirements.ExistenceRequirement;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public class RecommenderWithModel extends RecommenderWithoutModel {

	private SpliMoCharacter model;

	//-------------------------------------------------------------------
	public RecommenderWithModel(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	private boolean areRequirementsMet(Collection<Requirement> requires)  {
		List<Race> requiredRaces = new ArrayList<Race>();
		List<Culture> requiredCultures = new ArrayList<Culture>();
		boolean raceRequired =  false;
		boolean supportsRace = false;
		boolean cultureRequired = false;
		boolean supportsCulture = false;
		for (Requirement req : requires) {
			List<String> keys = Arrays.asList(req.getAsKeys());
			if (req instanceof ExistenceRequirement) {
				ExistenceRequirement eReq = (ExistenceRequirement)req;
				SplittermondReference type = (SplittermondReference) eReq.getType();
				if (type==null) {
					logger.log(Level.WARNING, "Unknown reference type in "+req);
					continue;
				}
				switch (type) {
				case CULTURE:
					cultureRequired = true;
					if (keys.contains(model.getCulture().getKey())) {
						supportsCulture = true;
						continue;
					}
					break;
				case RACE:
					raceRequired = true;
					if (keys.contains(model.getRace())) {
						supportsRace = true;
						continue;
					}
					break;
				default:
					logger.log(Level.WARNING, "TODO: "+type);
				}
			}
		}
//		for (Modification mod : modifications) {
//			if (mod instanceof RaceModification) {
//				requiredRaces.add(((RaceModification)mod).getRace());
//				raceRequired = true;
//				if (model.getRace()!=null && ((RaceModification)mod).getRace()==model.getRace()) {
//					supportsRace = true;
//					continue;
//				}
//			} else if (mod instanceof RequirementModification) {
//				RequirementModification rMod = (RequirementModification)mod;
//				switch ( rMod.getType()) {
//				case CULTURE:
//					List<String> splitted = new ArrayList<String>();
//					for (StringTokenizer tok = new StringTokenizer(rMod.getReference(), ", ;"); tok.hasMoreTokens(); )
//						splitted.add(tok.nextToken());
//					cultureRequired = splitted.size()>0;
//					for (String ref : splitted) {
//						Culture reqCulture = SplitterMondCore.getCulture(ref);
//						requiredCultures.add(reqCulture);
//						if (model.getCulture()!=null && model.getCulture().getId().equals(ref)) {
//							supportsCulture = true;
////							logger.log(Level.DEBUG, "Required culture "+ref+" matches "+model.getCulture());
//						}
//					}
//					break;
//				case RACE:
//					splitted = new ArrayList<String>();
//					for (StringTokenizer tok = new StringTokenizer(rMod.getReference(), ", ;"); tok.hasMoreTokens(); )
//						splitted.add(tok.nextToken());
//					raceRequired = splitted.size()>0;
//					for (String ref : splitted) {
//						Race reqRace = SplitterMondCore.getRace(ref);
//						requiredRaces.add(reqRace);
//						if (model.getRace()!=null && model.getRace().getId().equals(ref)) {
//							supportsRace = true;
//							logger.log(Level.DEBUG, "Required races "+ref+" matches "+model.getRace());
//						}
//					}
//					break;
//				default:
//					logger.log(Level.WARNING, "Check "+mod);
//				}
//			}
//		}
		if (raceRequired && !supportsRace && model.getRace()!=null) {
			return false;
		}
		if (cultureRequired && !supportsCulture && model.getCulture()!=null) {
			return false;
		}
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.ai.RecommenderWithoutModel#updateCultureRecommendations()
	 */
//	@Override
//	public List<Culture> updateCultureRecommendations() {
//		List<Culture> list = super.updateCultureRecommendations();
//		// Drop those where requirements are not met
//		if (model!=null) {
//			logger.log(Level.INFO, "Filter for race "+model.getRace());
//			for (Culture cult : new ArrayList<Culture>(list)) {
//				if (!areRequirementsMet(cult.getRequirements())) {
//					logger.log(Level.DEBUG, " drop culture '"+cult.getId()+"' since required race "+model.getRace()+" not likely");
//					list.remove(cult);
//				}
//			}
//		}
//
//		int count=5;
//		for (Culture data : list) {
//			logger.log(Level.DEBUG, data+" \t: "+getRecommendation(data)+"    "+data.getPageReferences());
//			count--;
//			if (count==0)
//				break;
//		}
//		logger.log(Level.DEBUG, "--");
//		return list.subList(0, 5);
//	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splittermond.chargen.ai.RecommenderWithoutModel#recommendBackground()
//	 */
//	@Override
//	public List<Background> recommendBackground() {
//		List<Background> list = super.recommendBackground();
//
//		int count=5;
//		for (Background data : list) {
//			System.out.println(data+" \t: "+getRecommendation(data)+"    "+data.getPageReferences());
//			count--;
//			if (count==0)
//				break;
//		}
//		System.out.println("--");
//		return list.subList(0, 5);
//	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splittermond.chargen.ai.RecommenderWithoutModel#recommendEducations()
//	 */
//	@Override
//	public List<Education> recommendEducations() {
//		List<Education> list = super.recommendEducations();
//		// Drop those where requirements are not met
//		if (model!=null) {
//			logger.log(Level.INFO, "Filter for race "+model.getRace()+" and culture "+model.getCulture()+"  in "+list.size()+" educations");
//			for (Education edu : new ArrayList<Education>(list)) {
//				if (!areRequirementsMet(edu.getRequirements())) {
//					logger.log(Level.DEBUG, " drop education '"+edu.getId()+"' since requirements not met");
//					list.remove(edu);
//				}
//			}
//		}
//
//		int count=5;
//		for (Education data : list) {
//			logger.log(Level.INFO, data+" \t: "+getRecommendation(data)+"    "+data.getPageReferences());
//			count--;
//			if (count==0)
//				break;
//		}
//		logger.log(Level.INFO, "--");
//		return list.subList(0, 5);
//	}

	//-------------------------------------------------------------------
	@Override
	public void update() {
		logger.log(Level.INFO, "update");
		clearWeighedSkills();
		List<RecommendationModification> allMods = new ArrayList<>();
		for (LevellingProfileValue prof : model.getProfiles()) {
			for (Modification mod : prof.getResolved().getOutgoingModifications()) {
				if (mod instanceof RecommendationModification) {
					allMods.add((RecommendationModification) mod);
				}
			}
		}
		RecommendationModification[] array = new RecommendationModification[allMods.size()];
		array = allMods.toArray(array);
		super.addWeighedSkills(array);
		logger.log(Level.INFO, "Recommender configured");

		super.update();
//		recommendRace();
//		recommendCulture();
//		recommendBackground();
//		recommendEducations();
//
//		// Final
////		recommendWeapon();
	}

}

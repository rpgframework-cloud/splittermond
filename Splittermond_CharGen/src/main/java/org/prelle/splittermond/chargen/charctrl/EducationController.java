package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Education;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface EducationController extends PartialController<Education>, RecommendingController<Education> {
	
	//-------------------------------------------------------------------
	public void setAllowUnusualEducations(boolean allow);
	public boolean isAllowUnusualEducations();

	//-------------------------------------------------------------------
	public List<Education> getAvailable();
	
	//-------------------------------------------------------------------
	public void select(Education value);

}

package org.prelle.splittermond.chargen.companion.summon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModule.Category;
import org.prelle.splimo.creature.CreatureModule.Type;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SummonableCreature;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ExtraSummonableController implements IExtraCreatureModulesController {

	protected static Logger logger = System.getLogger(ExtraSummonableController.class.getPackageName());

	protected List<ToDoElement> todos;
	protected SummonableController control;;

	public ExtraSummonableController(SummonableController control) {
		this.control = control;
	}

	@Override
	public List<CreatureModule> getAvailable() {
		List<CreatureModule> ret = SplitterMondCore.getItemList(CreatureModule.class).stream()
				.filter(m -> m.getCategory()==Category.SUMMONABLE)
				.filter(m -> m.getType()==Type.EXTRA || m.getType()==Type.ELEMENT || m.getType()==Type.ROLE)
				.filter(m -> getSelected().stream().noneMatch(cv -> cv.getResolved()==m) || m.getType()==Type.ROLE)
				.collect(Collectors.toList());
		return ret;
	}

	@Override
	public List<CreatureModuleValue> getSelected() {
		return control.getModel().getSummonable().getModules();
	}

	@Override
	public RecommendationState getRecommendationState(CreatureModule value) {
		return RecommendationState.NEUTRAL;
	}

	@Override
	public RecommendationState getRecommendationState(CreatureModuleValue value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(CreatureModule value) {
		return value.getChoices();
	}

	@Override
	public Possible canBeSelected(CreatureModule value, Decision... decisions) {
		if (!getAvailable().contains(value))
			return Possible.FALSE;

		// TODO: Check max level
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<CreatureModuleValue> select(CreatureModule value, Decision... decisions) {
		Possible poss = canBeSelected(value, decisions);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Try to select module {0} but that is not allowed: {1}", value.getId(), poss.getMostSevere());
			return new OperationResult<>(poss);
		}

		CreatureModuleValue val = new CreatureModuleValue(value);
		for (Decision dec : decisions)
			val.addDecision(dec);
		control.getModel().getSummonable().addModule(val);
		logger.log(Level.INFO, "Added module {0}", value.getId());
		logger.log(Level.INFO, "Selected now {0}", value.getOutgoingModifications());

		control.runProcessors();
		return new OperationResult<CreatureModuleValue>(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(CreatureModuleValue value) {
		return new Possible(getSelected().contains(value));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(CreatureModuleValue value) {
		Possible poss = canBeDeselected(value);
		if (!poss.get()) {
			logger.log(Level.ERROR, "Try to deselect module {0} but that is not allowed: {1}", value.getKey(), poss.getMostSevere());
			return false;
		}

		control.getModel().getSummonable().removeModule(value);
		logger.log(Level.INFO, "Removed module {0}", value.getKey());

		control.runProcessors();
		return true;
	}

	@Override
	public float getSelectionCost(CreatureModule data, Decision... decisions) {
		return 0;
	}

	@Override
	public String getSelectionCostString(CreatureModule data) {
		return null;
	}

	@Override
	public <A extends IAttribute, M extends RuleSpecificCharacterObject<A, ?, ?, ?>> CharacterController<A, M> getCharacterController() {
		return null;
	}

	@Override
	public void roll() {
	}

	@Override
	public List<UUID> getChoiceUUIDs() {
		return List.of();
	}

	@Override
	public void decide(CreatureModule decideFor, UUID choice, Decision decision) {
	}

	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		todos.clear();
		return unprocessed;
	}

	@Override
	public <C extends RuleSpecificCharacterObject<? extends IAttribute, ?, ?, ?>> C getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

}

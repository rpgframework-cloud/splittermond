package org.prelle.splittermond.chargen.charctrl;

import java.util.function.Consumer;

import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;

/**
 *
 */
public interface ICarriedItemController {

	public NumericalValueController<SMItemAttribute, ItemAttributeNumericalValue<SMItemAttribute>> getValueController(SMItemAttribute attrib);

	public void addListener(Consumer<CarriedItem<ItemTemplate>> listener);
	public void removeListener(Consumer<CarriedItem<ItemTemplate>> listener);

}

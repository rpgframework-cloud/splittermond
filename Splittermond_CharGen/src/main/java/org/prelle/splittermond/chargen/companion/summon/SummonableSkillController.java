package org.prelle.splittermond.chargen.companion.summon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.splimo.CreatePoints;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.PerSkillMastershipController;
import org.prelle.splittermond.chargen.charctrl.SkillController;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class SummonableSkillController implements ProcessingStep, SkillController {

	protected static Logger logger = System.getLogger(SummonableSkillController.class.getPackageName());

	protected List<ToDoElement> todos;
	protected SummonableController control;

	private int pointsExist;


	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public SummonableSkillController(SummonableController control) {
		this.control = control;
		todos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getCharacterController()
	 */
	@Override
	public <A extends IAttribute, M extends RuleSpecificCharacterObject<A, ?, ?, ?>> CharacterController<A, M> getCharacterController() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
	 */
	@Override
	public <C extends RuleSpecificCharacterObject<? extends IAttribute, ?, ?, ?>> C getModel() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getChoiceUUIDs()
	 */
	@Override
	public List<UUID> getChoiceUUIDs() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(SMSkill decideFor, UUID choice, Decision decision) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		todos.clear();

		for (Modification mod : previous) {
			if (mod instanceof ValueModification) {
				ValueModification valMod = (ValueModification)mod;
				if (valMod.getReferenceType()==SplittermondReference.CREATION_POINTS) {
					CreatePoints type = valMod.getReferenceType().resolve(valMod.getKey());
					logger.log(Level.WARNING, "TODO: {0} = {1}",type, valMod.getRawValue());
					switch (type) {
					case SKILL:
					case SKILL_NEW:
					}
				}
			}
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(SMSkillValue value) {
		return control.getModel().getSkillValue(value.getResolved()).getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<SMSkillValue> increase(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<SMSkillValue> decrease(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(SMSkill item) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getSelected()
	 */
	@Override
	public List<SMSkillValue> getSelected() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getToDos(org.prelle.splimo.SMSkill.SkillType)
	 */
	@Override
	public List<String> getToDos(SkillType type) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SkillController#getToDos(org.prelle.splimo.SMSkill)
	 */
	@Override
	public List<String> getToDos(SMSkill skill) {
		// TODO Auto-generated method stub
		return List.of();
	}

	@Override
	public Possible canSelectSpecialization(SMSkillValue data, SkillSpecialization<SMSkill> spec) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canDeselectSpecialization(SMSkillValue data, SkillSpecializationValue<SMSkill> val) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> selectSpecialization(SMSkillValue data,
			SkillSpecialization<SMSkill> spec) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> deselectSpecialization(SMSkillValue data,
			SkillSpecializationValue<SMSkill> spec) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canBeIncreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canBeDecreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> increase(SMSkillValue data,
			SkillSpecializationValue<SMSkill> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> decrease(SMSkillValue data,
			SkillSpecializationValue<SMSkill> value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PerSkillMastershipController getMastershipController(SMSkillValue data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFreeMastershipPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public IPerSkillSpellController getSpellController(SMSkill school) {
		// TODO Auto-generated method stub
		return null;
	}

}

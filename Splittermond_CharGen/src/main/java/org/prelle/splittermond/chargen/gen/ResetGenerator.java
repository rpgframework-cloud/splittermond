package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.splimo.Language;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.modification.Modification;

public class ResetGenerator implements ProcessingStep {

	protected final static Logger logger = System.getLogger(ResetGenerator.class.getPackageName()+".reset");

	protected SpliMoCharacterGenerator charGen;

	//-------------------------------------------------------------------
	public ResetGenerator(SpliMoCharacterGenerator charGen) {
		this.charGen = charGen;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		// Reset all attributes
		SpliMoCharacter model = charGen.getModel();

		// Remove all items that are auto-injected
		for (CarriedItem<ItemTemplate> tmp : model.getCarriedItems()) {
			if (tmp.isAutoAdded()) {
				logger.log(Level.DEBUG, "Remove item {0} injected from {1}", tmp.getKey(), tmp.getInjectedBy());
				model.removeCarriedItem(tmp);
			}
		}

		// Remove all race-modifications from attributes
//		for (Attribute attr : Attribute.values()) {
//			AttributeValue<Attribute> val = (AttributeValue<Attribute>) model.getAttribute(attr);
//			val.clearModifications();
//		}

		for (SMSkillValue val : model.getSkillValues()) {
			for (SkillSpecializationValue<SMSkill> spec : new ArrayList<>(val.getSpecializations())) {
				if (spec.isAutoAdded()) {
					val.removeSpecialization((SMSkillSpecialization)spec.getResolved());
				}
			}
		}

		for (ResourceValue tmp : model.getResources()) {
			tmp.clearIncomingModifications();
		}

		for (PowerValue tmp : model.getPowers()) {
			// Keep user-added powers and those introduced from race
			// (which have been filtered by ResetModifications and re-injected before
			if (tmp.isAutoAdded() && !tmp.isFixed()) {
				tmp.clearIncomingModifications();
				if (tmp.getDistributed()==0) {
					logger.log(Level.ERROR, "Remove item {0} injected from {1}", tmp.getKey(), tmp.getInjectedBy());
					model.removePower(tmp);
				}
			}
		}

		model.setExpInvested(0);
		model.setExpFree(15);
		model.setCharGenUsed(charGen.getId());
		model.addAutoLanguage(SplitterMondCore.getItem(Language.class, "basargnomisch"));

		SpliMoCharacterGenerator real = charGen;
		if (charGen instanceof GeneratorWrapper)
			real = ((GeneratorWrapper)charGen).getWrapped();

		logger.log(Level.INFO, "Start with {0} EXP", model.getExpFree());

		return unprocessed;
	}

}

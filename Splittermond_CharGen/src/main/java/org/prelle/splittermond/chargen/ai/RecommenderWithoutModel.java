package org.prelle.splittermond.chargen.ai;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Education;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.IRecommender;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.ai.Weight;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.RecommendationModification;

/**
 * @author prelle
 *
 */
public class RecommenderWithoutModel implements IRecommender<Attribute> {
	
	protected final static Logger logger = System.getLogger("splittermond.ai");
	
	protected List<RecommendationModification> weighedSkills;
	
	protected Map<Attribute, Recommendation<Attribute>> recommendedAttributes;
	protected Map<Class<? extends DataItem>, Map<String,Recommendation<? extends DataItem>>> recMaps;
	
	//-------------------------------------------------------------------
	public RecommenderWithoutModel() {
//		try {
//			throw new RuntimeException("Trace");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		weighedSkills  = new ArrayList<RecommendationModification>();
		
		recMaps  = new HashMap<Class<? extends DataItem>, Map<String,Recommendation<? extends DataItem>>>();
		recommendedAttributes = new HashMap<Attribute, Recommendation<Attribute>>();
		System.err.println("RecommenderWithoutModel.<init>: "+this);
	}
	
	//-------------------------------------------------------------------
	public void addWeighedSkill(RecommendationModification profile) {
		logger.log(Level.DEBUG, "ENTER addWeighedSkill({0})", profile);
		weighedSkills.add(profile);
		updateInternal();
		logger.log(Level.DEBUG, "LEAVE addWeighedSkill({0})", profile);
	}
	
	//-------------------------------------------------------------------
	public void addWeighedSkills(RecommendationModification... profiles) {
		logger.log(Level.DEBUG, "ENTER addWeighedSkills()");
		for (RecommendationModification profile : profiles)
			weighedSkills.add(profile);
		updateInternal();
		logger.log(Level.DEBUG, "LEAVE addWeighedSkills()");
	}
	
	//-------------------------------------------------------------------
	public void clearWeighedSkills() {
		weighedSkills.clear();
	}
	
	//-------------------------------------------------------------------
	private int getSkillWeight(SMSkill skill) {
		for (RecommendationModification tmp : weighedSkills) {
			if (tmp.getResolvedKey()==skill)
				return tmp.getWeight().ordinal()+1;
		}
		return 0;
	}
	
//	//-------------------------------------------------------------------
//	private List<Recommendation<Attribute>> getUseful(Map<Attribute,Integer> att1, Weight level) {
//		List<Attribute> tmp1 = new ArrayList<Attribute>(att1.keySet());
//		Collections.sort(tmp1, new Comparator<Attribute>() {
//			public int compare(Attribute a1, Attribute a2) {
//				return -att1.get(a1).compareTo(att1.get(a2));
//			}
//		});
//		List<Recommendation<Attribute>> best1 = new ArrayList<Recommendation<Attribute>>();
//		if (att1.isEmpty())
//			return best1;
//		int last= att1.get(tmp1.get(0));
//		Weight currentLevel = level;
//		for (Attribute key : tmp1) {
//			if (att1.get(key)>=(last/3)) {
//				best1.add(new Recommendation<Attribute>(key, currentLevel));
//				last = att1.get(key);
//				if (currentLevel==Weight.HOBBY)
//					break;
//				// Decrease level
//				currentLevel = Weight.values()[currentLevel.ordinal()-1];
//			}
//		}
//		
//		return best1;
//	}
	
//	//-------------------------------------------------------------------
//	private static <T> List<Recommendation<T>> merge(List<Recommendation<T>> list1, List<Recommendation<T>> list2) {
//		Map<T, Recommendation<T>> ret = new HashMap<>();
//		// List 1
//		for (Recommendation<T> tmp : list1) {
//			ret.put(tmp.data, tmp);
//		}
//		// List 2		
//		for (Recommendation<T> tmp : list2) {
//			if (ret.containsKey(tmp.data)) {
//				ret.get(tmp.data).merge(tmp);
//			} else
//				ret.put(tmp.data, tmp);
//		}
//		// Convert back to list
//		return new ArrayList<Recommendation<T>>(ret.values());
//	}
	
//	//-------------------------------------------------------------------
//	private List<Recommendation<Attribute>> getRecommendedAttribsForCombatSkill(SMSkill skill, Weight level) {
//		List<Recommendation<Attribute>> ret = new ArrayList<Recommendation<Attribute>>();
//		logger.log(Level.ERROR, "ToDo: getting recommended attributes for combat skill");
//		List<ItemTemplate> weapons = new ArrayList<ItemTemplate>();
////		weapons.addAll(SplitterMondCore.getItems(Weapon.class));
//		
//		Map<Attribute,Integer> att1 = new HashMap<Attribute, Integer>();
//		Map<Attribute,Integer> att2 = new HashMap<Attribute, Integer>();
//		for (ItemTemplate item : weapons) {
////			for (ItemTypeData data : item.getTypeData()) {
////				if (data instanceof Weapon) {
////					// Attribute 1
////					Integer x = att1.get(((Weapon)data).getAttribute1());
////					if (x==null)
////						x=0;
////					att1.put(((Weapon)data).getAttribute1(), x+1);
////					// Attribute 2
////					x = att2.get(((Weapon)data).getAttribute2());
////					if (x==null)
////						x=0;
////					att2.put(((Weapon)data).getAttribute2(), x+1);
////				}
////			}
//		}
//		
//		List<Recommendation<Attribute>> rec1 = getUseful(att1, level);
//		List<Recommendation<Attribute>> rec2 = getUseful(att2, level);
//		
//		
////		System.out.println("att1 = "+att1);
////		System.out.println("att1 = "+rec1);
////		System.out.println("att2 = "+att2);
////		System.out.println("att2 = "+rec2);
//		ret = merge(rec1, rec2);
////		System.out.println("ret = "+ret);
//		return ret;
//	}
	
	//-------------------------------------------------------------------
	private void recommendAttribute(Attribute attr, Weight weight)  {
		Recommendation<Attribute> aRec = getRecommendation(attr);
		if (aRec==null) {
			aRec = new Recommendation<Attribute>(attr);
			recommendedAttributes.put(attr, aRec);
		} 
		aRec.add(new RecommendationModification(SplittermondReference.ATTRIBUTE, attr.name(), weight));
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected <T extends DataItem> Recommendation<T> getRecommendation(T data) {
		if (data==null) return null;
		Map<String, Recommendation<? extends DataItem>> recMap = recMaps.get(data.getClass());
		if (recMap!=null && recMap.containsKey(data.getId()))
			return (Recommendation<T>) recMap.get(data.getId());
//		logger.log(Level.DEBUG, "No recommendation for "+data.getClass());
		return null;
	}
	
	//-------------------------------------------------------------------
	protected <T extends DataItem> int compareImpl(T data1, T data2) {
		Recommendation<T> r1 = getRecommendation(data1);
		Recommendation<T> r2 = getRecommendation(data2);
		if (r1==null && r2==null) return 0;
		if (r1!=null && r2==null) return -2;
		if (r1==null && r2!=null) return -2;
		return r1.compareTo(r2);
	}
	
	//-------------------------------------------------------------------
	public void updateCultureRecommendations() {
		logger.log(Level.DEBUG, "ENTER updateCultureRecommendations");
		// Ensure an empty list of culture recommendations exist
		Map<String, Recommendation<? extends DataItem>> recMap = recMaps.get(Culture.class);
		if (recMap==null) {
			recMap = new HashMap<String, Recommendation<? extends DataItem>>();
			recMaps.put(Culture.class, recMap);
		}
		recMap.clear();
		
		// For each ...
		List<Culture> list = SplitterMondCore.getItemList(Culture.class);
		for (Culture item : list) {
			Recommendation<? extends DataItem> rec = SpliMoAITool.convertModuleToRecommendation(item, this);
			recMap.put(item.getId(), rec);
		}
		
		SpliMoAITool.determineRecommenationStates(recMap.values(), 10, 10, 5);
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "Recommended cultures");
			logger.log(Level.DEBUG, "======================");
			List<Recommendation> recList = new ArrayList<Recommendation>(recMap.values());
			Collections.sort(recList);
			for (Recommendation<? extends DataItem> rec : recList) {
				if (rec.getRecommendationState()==RecommendationState.STRONGLY_RECOMMENDED || rec.getRecommendationState()==RecommendationState.RECOMMENDED)
					logger.log(Level.DEBUG, "Education {0} \t: {1} ({2})",  rec.data.getId(), rec.getRecommendationState(), rec.getPoints());
			}
		}
	}
	
	//-------------------------------------------------------------------
	public void updateBackgroundRecommendations() {
		logger.log(Level.DEBUG, "ENTER updateBackgroundRecommendations");
		// Ensure an empty list of culture recommendations exist
		Map<String, Recommendation<? extends DataItem>> recMap = recMaps.get(Background.class);
		if (recMap==null) {
			recMap = new HashMap<String, Recommendation<? extends DataItem>>();
			recMaps.put(Background.class, recMap);
		}
		recMap.clear();
		
		// For each ...
		List<Background> list = SplitterMondCore.getItemList(Background.class);
		for (Background item : list) {
			Recommendation<? extends DataItem> rec = SpliMoAITool.convertModuleToRecommendation(item, this);
			recMap.put(item.getId(), rec);
		}

		SpliMoAITool.determineRecommenationStates(recMap.values(), 10, 20, 5);
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "Recommended backgrounds");
			logger.log(Level.DEBUG, "======================");
			List<Recommendation> recList = new ArrayList<>(recMap.values());
			Collections.sort(recList);
			for (Recommendation<? extends DataItem> rec : recList) {
				if (rec.getRecommendationState()==RecommendationState.STRONGLY_RECOMMENDED || rec.getRecommendationState()==RecommendationState.RECOMMENDED)
					logger.log(Level.DEBUG, "Education {0} \t: {1} ({2})",  rec.data.getId(), rec.getRecommendationState(), rec.getPoints());
			}
		}
	}
	
	//-------------------------------------------------------------------
	public void updateEducationRecommendations() {
		logger.log(Level.DEBUG, "ENTER updateEducationRecommendations");
		logger.log(Level.INFO, "RecMaps for "+recMaps.keySet());
		// Ensure an empty list of culture recommendations exist
		Map<String, Recommendation<? extends DataItem>> recMap = recMaps.get(Education.class);
		if (recMap==null) {
			recMap = new HashMap<String, Recommendation<? extends DataItem>>();
			recMaps.put(Education.class, recMap);
		}
		recMap.clear();
		
		// For each ...
		List<Education> list = SplitterMondCore.getItemList(Education.class);
		for (Education item : list) {
			Recommendation<? extends DataItem> rec = SpliMoAITool.convertModuleToRecommendation(item, this);
			recMap.put(item.getId(), rec);
		}
		
		SpliMoAITool.determineRecommenationStates(recMap.values(), 3, 10, 5);
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "Recommended educations");
			logger.log(Level.DEBUG, "======================");
			List<Recommendation> recList = new ArrayList<>(recMap.values());
			Collections.sort(recList);
			for (Recommendation<? extends DataItem> rec : recList) {
				if (rec.getRecommendationState()==RecommendationState.STRONGLY_RECOMMENDED || rec.getRecommendationState()==RecommendationState.RECOMMENDED)
					logger.log(Level.DEBUG, "Education {0} \t: {1} ({2})",  rec.data.getId(), rec.getRecommendationState(), rec.getPoints());
			}
		}
	}
	
	//-------------------------------------------------------------------
	private void updateInternal() {
		recMaps.clear();
		recommendedAttributes.clear();
		
		for (RecommendationModification prof : weighedSkills) {
			
			// Mark skill itself as recommended
			DataItem item = prof.getResolvedKey();
			Recommendation<? extends DataItem> rec = getRecommendation(item);
			if (rec==null) {
				rec = new Recommendation<>(item);
				Map<String, Recommendation<? extends DataItem>> recMap = recMaps.get(item.getClass());
				if (recMap==null) {
					recMap = new HashMap<String, Recommendation<? extends DataItem>>();
					recMaps.put(item.getClass(), recMap);
				}
				recMap.put(item.getId(), rec);
			}
			rec.add(prof);
			
			// For non-combat skills, recommend the attributes 
			if (prof.getReferenceType()==SplittermondReference.SKILL) {
				SMSkill skill = prof.getResolvedKey();
				if (skill.getType()!=SkillType.COMBAT) {
					// Eventually existing rec for attribute 1
					recommendAttribute(skill.getAttribute(), prof.getWeight());
					recommendAttribute(skill.getAttribute2(), prof.getWeight());
				} else {
					logger.log(Level.INFO, "TODO: Recommend attributes for combat skill");
//					for (Recommendation<Attribute> tmp : getRecommendedAttribsForCombatSkill(skill, prof.getWeight())) {
//						addIfBetter(recommendedAttributes, tmp);					
//					}
				}
			}
		}
		
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "Recommended attributes");
			logger.log(Level.DEBUG, "======================");
			for (Attribute key : Attribute.primaryValues()) {
				Recommendation<Attribute> rec = recommendedAttributes.get(key);
				if (rec!=null) {
					logger.log(Level.DEBUG, "  {0}\t: {1}   {2}", key, rec.getWeight(), rec.getPoints());
				}
			}
			logger.log(Level.DEBUG, "Recommended skills");
			logger.log(Level.DEBUG, "======================");
			for (SMSkill key : SplitterMondCore.getItemList(SMSkill.class)) {
				Recommendation<SMSkill> rec = getRecommendation(key);
				if (rec!=null) {
					logger.log(Level.DEBUG, "  {0}\t: {1}   {2}", key.getId(), rec.getWeight(), rec.getPoints());
				}
			}
		}
	}
	
	//-------------------------------------------------------------------
	public void update() {
		logger.log(Level.ERROR, "update");
		updateInternal();
		updateCultureRecommendations();
		updateBackgroundRecommendations();
		updateEducationRecommendations();
		
		// Final
//		recommendWeapon();
	}
	
	//-------------------------------------------------------------------
	public boolean isRecommended(DataItem key) {
		return getRecommendation(key)!=null;
	}
	
	//-------------------------------------------------------------------
	public boolean isRecommended(Attribute key) {
		return recommendedAttributes.containsKey(key);
	}
	
	//-------------------------------------------------------------------
	public Recommendation<Attribute> getRecommendation(Attribute key) {
		return recommendedAttributes.get(key);
	}
	
	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nAttrib: "+recommendedAttributes.values());
//		buf.append("\nSkills: "+recommendedSkills.values());
		
		return buf.toString();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.chargen.IRecommender#sort(java.lang.Class, java.util.List)
//	 */
//	@Override
//	public <D extends DataItem> List<D> sort(Class<D> cls, List<D> input) {
//		// TODO Auto-generated method stub
//		return (List<D>)input;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IRecommender#getMostRecommended(java.lang.Class, java.util.List)
	 */
	@Override
	public <D extends DataItem> List<D> getMostRecommended(Class<D> cls, List<? extends DataItem> input) {
		// TODO Auto-generated method stub
		return (List<D>)input;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IRecommender#getMostRecommended(java.util.List)
	 */
	@Override
	public List<Attribute> getMostRecommended(List<Attribute> input) {
		List<Attribute> ret = input.stream()
			.filter(attr -> recommendedAttributes.containsKey(attr) && recommendedAttributes.get(attr).getWeight()!=Weight.NOT_RECOMMENDED)
			.collect(Collectors.toList());
		Collections.sort(ret, new Comparator<Attribute>() {
			public int compare(Attribute o1, Attribute o2) {
				return getRecommendation(o1).compareTo(getRecommendation(o2));
			}});
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IRecommender#getRecommendationState(de.rpgframework.genericrpg.data.IAttribute)
	 */
	@Override
	public RecommendationState getRecommendationState(Attribute key) {
		Recommendation<Attribute> rec = recommendedAttributes.get(key);
		if (rec==null) return RecommendationState.NEUTRAL;
		if (rec.getRecommendationState()!=null)	return rec.getRecommendationState();
		if (rec.getWeight()==null) return RecommendationState.NEUTRAL;
		switch (rec.getWeight()) {
		case INSANE:
		case MASTER:
			return RecommendationState.STRONGLY_RECOMMENDED;
		case VERY_GOOD:
		case GOOD:
			return RecommendationState.RECOMMENDED;
		case NOT_RECOMMENDED:
			return RecommendationState.UNRECOMMENDED;
		default:
			return RecommendationState.NEUTRAL;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IRecommender#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(DataItem item) {
		Recommendation<?> rec = getRecommendation(item);
		if (rec==null) return RecommendationState.NEUTRAL;
		if (rec.getRecommendationState()!=null) return rec.getRecommendationState();
		if (rec.getWeight()==null) return RecommendationState.NEUTRAL;
		switch (rec.getWeight()) {
		case INSANE:
		case MASTER:
			return RecommendationState.STRONGLY_RECOMMENDED;
		case VERY_GOOD:
		case GOOD:
			return RecommendationState.RECOMMENDED;
		case NOT_RECOMMENDED:
			return RecommendationState.UNRECOMMENDED;
		default:
			return RecommendationState.NEUTRAL;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IRecommender#decide(de.rpgframework.genericrpg.data.Choice)
	 */
	@Override
	public Decision decide(Choice choice) {
		logger.log(Level.WARNING, "DO IMPLEMENT deciding "+choice);
		if (choice.getChoiceOptions()!=null) {
			int pos = (new Random()).nextInt(choice.getChoiceOptions().length);
			return new Decision(choice, choice.getChoiceOptions()[pos]);
		}
		return null;
	}
	
}

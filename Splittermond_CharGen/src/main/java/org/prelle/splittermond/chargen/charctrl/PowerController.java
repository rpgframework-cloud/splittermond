package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Power;
import org.prelle.splimo.PowerValue;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface PowerController extends
	PartialController<Power>,
	RecommendingController<Power>,
	ComplexDataItemController<Power, PowerValue>,
	NumericalValueController<Power, PowerValue> {

	public int getPointsLeft();
	public int getPointsMax();

}

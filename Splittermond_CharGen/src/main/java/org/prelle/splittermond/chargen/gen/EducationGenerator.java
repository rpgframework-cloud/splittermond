package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.splimo.Education;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public class EducationGenerator extends ControllerImpl<Education> implements EducationController {

	protected static Logger logger = System.getLogger(EducationGenerator.class.getPackageName()+".edu");

	private static Random RANDOM = new Random();

	private boolean allowUnusualEducations = false;

	private List<Education> available;

	//-------------------------------------------------------------------
	protected EducationGenerator(SpliMoCharacterController parent) {
		super(parent);
		available = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.EducationController#setAllowUnusualEducations(boolean)
	 */
	@Override
	public void setAllowUnusualEducations(boolean allow) {
		allowUnusualEducations = allow;
		updateAvailable();
	}
	@Override
	public boolean isAllowUnusualEducations() { return allowUnusualEducations;}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Education item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		available.clear();

		List<Education> mainList = new ArrayList<>();
		Map<Education, List<Education>> variants = new LinkedHashMap<>();


		nextEducation:
		for (Education cult : SplitterMondCore.getItemList(Education.class)) {
			if (!allowUnusualEducations) {
				for (Requirement req : cult.getRequirements()) {
					boolean met = SplitterTools.isRequirementMet(parent.getModel(), req);
					if (!met) {
						continue nextEducation;
					}
				}
			}

			if (cult.getVariantOf()!=null) {
				Education main = SplitterMondCore.getItem(Education.class, cult.getVariantOf());
				if (!mainList.contains(main)) {
					mainList.add(main);
					variants.put(main, new ArrayList<Education>());
				}
				variants.get(main).add(cult);
			} else {
				if (!mainList.contains(cult)) {
					mainList.add(cult);
					variants.put(cult, new ArrayList<Education>());
				}
			}
		}
		Collections.sort(mainList, new Comparator<Education>() {
			public int compare(Education o1, Education o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		// Fill available
		available.clear();
		for (Education edu : mainList) {
			available.add(edu);
			List<Education> variantList = variants.get(edu);
			Collections.sort(variantList, new Comparator<Education>() {
				public int compare(Education o1, Education o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			available.addAll(variants.get(edu));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.EducationController#getAvailable()
	 */
	@Override
	public List<Education> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(Education decideFor, UUID choice, Decision decision) {
		logger.log(Level.WARNING, "decide {0}={1} for {2}", choice, decision.getValue(), decideFor.getClass().getSimpleName());
		parent.getModel().getEducation().addDecision(decision);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.EducationController#select(org.prelle.splimo.Education)
	 */
	@Override
	public void select(Education value) {
		logger.log(Level.INFO, "Select culture '"+value+"'");
		parent.getModel().setEducation(value);
		updateChoices(value);

		// Recalculate
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ControllerImpl#roll()
	 */
	@Override
	public void roll() {
		List<Education> options = getAvailable().stream()
				.filter(c -> SplitterTools.areRequirementsMet(getModel(), c, new Decision[0]).get())
				.collect(Collectors.toList());
		if (parent.getRecommender().isPresent()) {
			options = (List<Education>) parent.getRecommender().get().getMostRecommended(options.get(0).getClass(), options);
		}
		int idx = random.nextInt(options.size());
		Education item = options.get(idx);
		logger.log(Level.ERROR, "Rolled {0} from {1} possible options", item.getId(), options.size());
		select(item);

		List<Decision> decisions = new ArrayList<>();
		for (Choice choice : item.getChoices()) {
			logger.log(Level.ERROR, "Choice "+choice);
			parent.getRecommender().ifPresent(r -> decide(item, choice.getUUID(),r.decide(choice)));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		todos.clear();
		int undecided = 0;

		Education selected = null;
		try {
			updateAvailable();

			String key = model.getEducation().getKey();
			if (key==null) {
				todos.add(new ToDoElement(Severity.INFO, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_EDUCATION_NOT_SELECTED));
			} else {
				selected = SplitterMondCore.getItem(Education.class, model.getEducation().getKey());
				if (selected==null) {
					logger.log(Level.ERROR, "Unknown Education '"+model.getEducation().getKey()+"' selected");
					todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_EDUCATION_UNKNOWN, key));
					return previous;
				}
				// selected != null  from here
				updateChoices(selected);
				logger.log(Level.DEBUG, "1. Apply modifications from Education "+selected+" = "+selected.getOutgoingModifications());
				for (Modification tmp : selected.getOutgoingModifications()) {
					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						if ("CHOICE".equals(dMod.getKey())) {
							Choice choice = selected.getChoice(dMod.getConnectedChoice());
							Decision dec = parent.getModel().getEducation().getDecision(dMod.getConnectedChoice());
//							Decision dec = parent.getModel().getDecision(dMod.getConnectedChoice());
							if (dec!=null) {
								List<Modification> mods = GenericRPGTools.decisionToModifications(dMod, choice, dec);
								logger.log(Level.DEBUG, "  Inject modifications from choice {0}",dMod.getConnectedChoice());
								for (Modification mod : mods) {
									logger.log(Level.DEBUG, "    Inject modification {0}",mod);
								}
								unprocessed.addAll(mods);
							} else {
								logger.log(Level.DEBUG, "  ignore modifications from open decision "+dMod.getConnectedChoice());
								undecided++;
							}
						} else {
							unprocessed.add(tmp);
						}
					}
				}
			}

			/*
			 * Apply modifications from already made decisions
			 */
//			for (Choice choice : choices) {
//				if (model.hasDecisionBeenMade(choice.getUUID())) {
//					Decision dec = (Decision) model.getDecision(choice.getUUID());
//					logger.log(Level.WARNING, "TODO: apply modifications from "+dec);
//					System.err.println("TODO: apply modifications from "+dec);
//					unprocessed.addAll(GenericRPGTools.decisionToModifications(origMod, choice, dec));
////				if (decision.getDecision()!=null) {
////					logger.log(Level.DEBUG, "2. From "+decision.getChoice()+" add "+decision.getDecision());
////					unprocessed.addAll(decision.getDecision());
//				} else {
//					hasUndecided = true;
//					logger.log(Level.WARNING, "Undecided "+choice);
//				}
//			}
			if (undecided>0) {
				todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_EDUCATION_DECISIONS, undecided));
			}
		} finally {
			logger.log(Level.TRACE, "STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

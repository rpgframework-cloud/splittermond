package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.CultureLore;
import org.prelle.splimo.Language;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.ai.ProfileControllerImpl;
import org.prelle.splittermond.chargen.ai.RecommenderWithModel;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterControllerImpl;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.genericrpg.chargen.RuleInterpretation;
import de.rpgframework.genericrpg.data.RuleController;

/**
 * @author Stefan
 *
 */
public abstract class SpliMoCharacterGeneratorImpl extends SpliMoCharacterControllerImpl implements SpliMoCharacterGenerator, CharacterGenerator<Attribute,SpliMoCharacter> {

	public enum PageType {
		DATASETS,
		RECOMMENDER,
		RACE,
		CULTURE,
		BACKGROUND,
		EDUCATION,
		ATTRIBUTES,
		SKILLS,
		POWERS,
		SPELLS,
		RESOURCES,
		MOONSIGN,
		TOOLS_TRADE,
		START_GEAR
	}

	protected final static Logger logger = System.getLogger("splittermond.gen");

	protected RaceController races;
	protected CultureController cultures;
	protected BackgroundController backgrounds;
	protected EducationController educations;
//	protected MastershipController masteries;
	protected ToolsOfTheTradeGenerator toolsOfTrade;

	//-------------------------------------------------------------------
	public SpliMoCharacterGeneratorImpl(SpliMoCharacter model, CharacterHandle handle) {
		this.model = model;
		this.handle= handle;
		recommender = Optional.of(new RecommenderWithModel(model));
		ruleCtrl = new RuleController(model, SplitterMondCore.getItemList(RuleInterpretation.class), SplittermondRules.values());
		profileCtrl  = new ProfileControllerImpl(this);
		toolsOfTrade = new ToolsOfTheTradeGenerator(this);
		createPartialController();
		setupProcessChain();
		runProcessors();
	}

//	//-------------------------------------------------------------------
//	public SpliMoCharacterGeneratorImpl() {
//		this(new SpliMoCharacter(), null);
//	}

	// --------------------------------------------------------------------
	protected abstract void setupProcessChain();

	protected abstract void initializeModel();

	//-------------------------------------------------------------------
	public static String getID() { return "none"; }

	//-------------------------------------------------------------------
	public abstract List<PageType> getWizardPages() ;

//	//-------------------------------------------------------------------
//	/**
//	 */
//	@Override
//	protected void createPartialController() {
//		profile= new ProfileController(recommender);
//		races  = new RaceGenerator(this);
//		cultures = new CultureGenerator(this);
//		backgrounds = new BackgroundGenerator(this);
//		attrib = new AttributeGenerator(this);
//		skills = new SkillGenerator(this);
//
//		processChain.add(races);
//		processChain.add(cultures);
//		processChain.add(backgrounds);
//		processChain.add(attrib);
//		processChain.add(skills);
//	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		logger.log(Level.INFO, "canBeFinished: "+attrib.getPointsLeft());
		if (attrib.getPointsLeft()>0) return false;
		if (model.getRace()==null) return false;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#finish()
	 */
	@Override
	public void finish() {
		logger.log(Level.WARNING, "TODO: finish character");
		model.setExpInvested(0);

		// Sort skills
		List<SMSkillValue> list = getModel().getSkillValues();
		Collections.sort(list, new Comparator<SMSkillValue>() {
			public int compare(SMSkillValue o1, SMSkillValue o2) {
				SMSkill s1 = o1.getModifyable();
				SMSkill s2 = o2.getModifyable();
				int cmp1 = s1.getType().compareTo(s2.getType());
				if (cmp1!=0)
					return cmp1;

				return s1.getName(Locale.GERMAN).compareTo(s2.getName(Locale.GERMAN));
			}
		});

		// Copy over auto-added values
		for (Language lang : getModel().getAutoLanguages()) {
			getModel().addLanguage(lang);
		}
		getModel().clearAutoLanguage();

		for (CultureLore lang : getModel().getCultureLores()) {
			getModel().addCultureLore(lang);
		}
		getModel().clearAutoCultureLore();

		model.setInCareerMode(true);
	}

	//-------------------------------------------------------------------
	public RaceController getRaceController() {
		return races;
	}

	//-------------------------------------------------------------------
	public CultureController getCultureController() {
		return cultures;
	}

	//-------------------------------------------------------------------
	public BackgroundController getBackgroundController() {
		return backgrounds;
	}

	//-------------------------------------------------------------------
	public EducationController getEducationController() {
		return educations;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getMastershipController()
//	 */
//	@Override
//	public MastershipController getMastershipController() {
//		return masteries;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getToolsOfTheTradeController()
	 */
	@Override
	public ToolsOfTheTradeGenerator getToolsOfTheTradeController() {
		return toolsOfTrade;
	}

}

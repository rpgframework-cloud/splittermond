package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.common.APerSkillMastershipController;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;

public interface SkillController extends PartialController<SMSkill>, NumericalValueController<SMSkill, SMSkillValue> {

	//--------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeft();

	//--------------------------------------------------------------------
	public List<SMSkillValue> getSelected();

	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<String> getToDos(SkillType type);

	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<String> getToDos(SMSkill skill);

	//-------------------------------------------------------------------
	public Possible canSelectSpecialization(SMSkillValue data, SkillSpecialization<SMSkill> spec);
	public Possible canDeselectSpecialization(SMSkillValue data, SkillSpecializationValue<SMSkill> val);
	public OperationResult<SkillSpecializationValue<SMSkill>> selectSpecialization(SMSkillValue data, SkillSpecialization<SMSkill> spec);
	public OperationResult<SkillSpecializationValue<SMSkill>> deselectSpecialization(SMSkillValue data, SkillSpecializationValue<SMSkill> spec);

	//-------------------------------------------------------------------
	default ComplexDataItemController<SkillSpecialization<SMSkill>, SkillSpecializationValue<SMSkill>> getSkillSpecializationController(SMSkillValue data) {
		CharacterController<Attribute,SpliMoCharacter> ctrl = getCharacterController();
		return new SkillSpecializationController((SpliMoCharacterController)ctrl, data);
	}

	public Possible canBeIncreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value);
	public Possible canBeDecreased(SMSkillValue data, SkillSpecializationValue<SMSkill> value);
	public OperationResult<SkillSpecializationValue<SMSkill>> increase(SMSkillValue data, SkillSpecializationValue<SMSkill> value);
	public OperationResult<SkillSpecializationValue<SMSkill>> decrease(SMSkillValue data, SkillSpecializationValue<SMSkill> value);

	//-------------------------------------------------------------------
	public int getFreeMastershipPoints();
	public PerSkillMastershipController getMastershipController(SMSkillValue data);
//		CharacterController<Attribute,SpliMoCharacter> ctrl = getCharacterController();
//		return new APerSkillMastershipController((SpliMoCharacterController)ctrl, data);
//	}
	public IPerSkillSpellController getSpellController(SMSkill school);
	
}

package org.prelle.splittermond.chargen.gen;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.ai.SpliMoAITool;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.chargen.GeneratorId;

/**
 * @author prelle
 *
 */
public class CharacterGeneratorRegistry {

	private static Map<String,Class<? extends SpliMoCharacterGenerator>> generators;

	//-------------------------------------------------------------------
	static {
		generators = new LinkedHashMap<String,Class<? extends SpliMoCharacterGenerator>>();
		addGenerator(AssistingGenerator.class);
		addGenerator(ModuleBasedGenerator.class);
		addGenerator(UncontrolledGenerator.class);
		SpliMoAITool.initialize();
	}

	//-------------------------------------------------------------------
	private static void addGenerator(Class<? extends SpliMoCharacterGenerator> clazz) {
		GeneratorId anno = clazz.getAnnotation(GeneratorId.class);
		if (anno==null)
			throw new RuntimeException(clazz+" needs a @GeneratorId");
		generators.put(anno.value(), clazz);
	}

	//-------------------------------------------------------------------
	public static List<Class<? extends SpliMoCharacterGenerator>> getGenerators() {
		return new ArrayList<>(generators.values());
	}

	//-------------------------------------------------------------------
	public static SpliMoCharacterGenerator getGenerator(String id, SpliMoCharacter model, CharacterHandle handle) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException  {

		Class<? extends SpliMoCharacterGenerator> clazz = generators.get(id);
		if (clazz==null)
			throw new NoSuchElementException("Unknown generator: "+id);
		return clazz.getConstructor(RuleSpecificCharacterObject.class, CharacterHandle.class).newInstance(model, handle);
	}

}

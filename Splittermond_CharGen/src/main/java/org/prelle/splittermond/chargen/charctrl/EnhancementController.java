package org.prelle.splittermond.chargen.charctrl;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.Possible.State;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class EnhancementController extends ControllerImpl<Enhancement>
		implements ComplexDataItemController<Enhancement, ItemEnhancementValue<Enhancement>>,
		NumericalValueController<Enhancement, ItemEnhancementValue<Enhancement>>{

	protected CarriedItem<ItemTemplate> model;

	private List<Consumer<CarriedItem<ItemTemplate>>> itemListener;

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public EnhancementController(SpliMoCharacterController parent, CarriedItem<ItemTemplate> model) {
		super(parent);
		this.model = model;
		itemListener = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addListener(Consumer<CarriedItem<ItemTemplate>> listener) {
		if (!itemListener.contains(listener))
			itemListener.add(listener);
	}
	//-------------------------------------------------------------------
	public void removeListener(Consumer<CarriedItem<ItemTemplate>> listener) {
		itemListener.remove(listener);
	}
	//-------------------------------------------------------------------
	private void fireChange() {
		for (Consumer<CarriedItem<ItemTemplate>> callback : itemListener) {
			try {
				callback.accept(model);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error in "+callback.getClass(),e);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Enhancement> getAvailable() {
		return SplitterMondCore.getItemList(Enhancement.class).stream()
				.filter(e -> parent.showDataItem(e))
				.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<ItemEnhancementValue<Enhancement>> getSelected() {
		return model.getEnhancements();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(Enhancement item) {
		if (item==null) return RecommendationState.NEUTRAL;
		// No recommender set, means no recommendation
		if (parent.getRecommender().isEmpty()) {
			return RecommendationState.NEUTRAL;
		}

		return RecommendationState.NEUTRAL;
//		return parent.getRecommender().get().getRecommendationState(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(ItemEnhancementValue<Enhancement> item) {
		if (item==null) return RecommendationState.NEUTRAL;
		// No recommender set, means no recommendation
		if (parent.getRecommender().isEmpty()) {
			return RecommendationState.NEUTRAL;
		}

		return RecommendationState.NEUTRAL;
//		return parent.getRecommender().get().getRecommendationState(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Enhancement value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Enhancement value, Decision... decisions) {
		logger.log(Level.INFO, "canBeSelected( {0}, {1} )", value, Arrays.toString(decisions));
		Possible poss =  GenericRPGTools.areAllDecisionsPresent(value, decisions);
		if (!poss.get())
			return poss;

		if (model.hasEnhancement(value)) {
			poss = new Possible(Severity.STOPPER, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_ALREADY_EXISTS);
			return poss;
		}

		// TODO: Check existing quality (may not be 7+, with exception)
		return poss;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<ItemEnhancementValue<Enhancement>> select(Enhancement value, Decision... decisions) {
		logger.log(Level.INFO, "select( {0} )", value);
		Possible poss = canBeSelected(value, decisions);
		if (!poss.get() && poss.getState()!=State.DECISIONS_MISSING) {
			logger.log(Level.WARNING, "Trying to select {0} although not possible");
			return new OperationResult<>();
		}

		ItemEnhancementValue<Enhancement> val = new ItemEnhancementValue<Enhancement>(value);
		for (Decision dec : decisions) {
			val.addDecision(dec);
		}
		logger.log(Level.INFO, "Add enhancement {0} to {1}", value, model.getNameWithoutRating());
		model.addEnhancement(val);

		SMGearTool.recalculate("", getModel(), model);
		fireChange();
		return new OperationResult<ItemEnhancementValue<Enhancement>>(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(ItemEnhancementValue value) {
		logger.log(Level.WARNING, "TODO: canBeDeselected({0})", value);
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(ItemEnhancementValue value) {
		// TODO Auto-generated method stub
		SMGearTool.recalculate("", getModel(), model);
		fireChange();
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Enhancement data, Decision... decisions) {
		return data.getSize();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getValue(ItemEnhancementValue<Enhancement> value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Possible canBeIncreased(ItemEnhancementValue<Enhancement> value) {
		logger.log(Level.WARNING, "canBeIncreased( {0} )", value.getResolved());

		Enhancement data = value.getResolved();
		Choice ratingChoice = data.getChoice(ItemTemplate.UUID_RATING);
		if (ratingChoice==null) return Possible.FALSE;

		int current = value.getDistributed();
		Formula maxForm = ratingChoice.getMaxFormula();


		return Possible.TRUE;
	}

	@Override
	public Possible canBeDecreased(ItemEnhancementValue<Enhancement> value) {
		logger.log(Level.WARNING, "canBeDecreased( {0} )", value.getResolved());
		return Possible.TRUE;
	}

	@Override
	public OperationResult<ItemEnhancementValue<Enhancement>> increase(ItemEnhancementValue<Enhancement> value) {
		logger.log(Level.WARNING, "increase( {0} of {1})", value, model);
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<ItemEnhancementValue<Enhancement>> decrease(ItemEnhancementValue<Enhancement> value) {
		logger.log(Level.WARNING, "decrease( {0} of {1})", value, model);
		// TODO Auto-generated method stub
		return null;
	}

}

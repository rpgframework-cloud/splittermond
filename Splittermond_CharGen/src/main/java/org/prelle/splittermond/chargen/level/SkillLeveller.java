package org.prelle.splittermond.chargen.level;

import java.util.Date;

import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.common.ASkillController;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * 
 */
public class SkillLeveller extends ASkillController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public SkillLeveller(SpliMoCharacterController parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<SMSkillValue> increase(SMSkillValue value) {
		OperationResult<SMSkillValue> result = super.increase(value);
		if (result.hasError())
			return result;

		// Pay with XP
		int val = value.getDistributed();
		int xp = 3;
		switch (val) {
		case 7: case 8: case 9: xp=5; break;
		case 10: case 11: case 12: xp=5; break;
		case 13: case 14: case 15: xp=9; break;
		}
		
		SpliMoCharacter model = parent.getModel();
		model.setExpFree( model.getExpFree() -xp);
		model.setExpInvested( model.getExpInvested() +xp);
		
		ValueModification mod = new ValueModification(SplittermondReference.SKILL, value.getKey(), val);
		mod.setDate(new Date(System.currentTimeMillis()));
		mod.setExpCost(xp);
		model.addToHistory(mod);
		
		parent.runProcessors();
		return new OperationResult<>(value);
	}

}

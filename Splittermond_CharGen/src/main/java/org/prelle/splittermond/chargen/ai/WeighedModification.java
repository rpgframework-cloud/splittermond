/**
 * 
 */
package org.prelle.splittermond.chargen.ai;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class WeighedModification implements Comparable<WeighedModification> {
	
	Modification mod;
	Float value;
	
	public WeighedModification(Modification mod, float value) {
		this.mod = mod;
		this.value = value;
	}

	@Override
	public int compareTo(WeighedModification o) {
		return value.compareTo(o.value);
	}

}

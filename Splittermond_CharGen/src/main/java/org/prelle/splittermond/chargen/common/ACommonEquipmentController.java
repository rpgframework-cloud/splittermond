package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.charctrl.IRejectReasons;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public abstract class ACommonEquipmentController extends ControllerImpl<ItemTemplate> implements IEquipmentController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public ACommonEquipmentController(SpliMoCharacterController parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.IEquipmentController#getMoneyRemaining()
	 */
	@Override
	public int getMoneyRemaining() {
		return getModel().getMoney();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(ItemTemplate item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		// TODO Auto-generated method stub
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<ItemTemplate> getAvailable() {
		return SplitterMondCore.getItemList(ItemTemplate.class);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<CarriedItem<ItemTemplate>> getSelected() {
		return getModel().getCarriedItems();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(CarriedItem<ItemTemplate> value) {
		return getRecommendationState(value.getResolved());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(ItemTemplate value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<CarriedItem<ItemTemplate>> select(ItemTemplate value, Decision... decisions) {
		logger.log(Level.TRACE, "ENTER select({0})", value);
		try {
			Possible poss = canBeSelected(value, decisions);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Tried to add gear ''{0}'' but {1}", value.getId(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			OperationResult<CarriedItem<ItemTemplate>> result = SMGearTool.buildItem(value, CarryMode.CARRIED, getModel(), true, decisions);
			if (result.hasError()) {
				return result;
			}
			CarriedItem<ItemTemplate> carried = result.get();
			getModel().addCarriedItem(carried);
			logger.log(Level.INFO, "Added carried item {0}",carried);

			return new OperationResult<CarriedItem<ItemTemplate>>(carried);
		} finally {
			logger.log(Level.TRACE, "LEAVE select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(CarriedItem<ItemTemplate> value) {
		if (!getModel().getCarriedItems().contains(value)) {
			return new Possible(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(CarriedItem<ItemTemplate> value) {
		return deselect(value, RemoveMode.UNDO);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue, de.rpgframework.genericrpg.chargen.ComplexDataItemController.RemoveMode)
	 */
	@Override
	public boolean deselect(CarriedItem<ItemTemplate> value, RemoveMode mode) {
		logger.log(Level.TRACE, "ENTER deselect({0})", value);
		try {
			Possible poss = canBeDeselected(value);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Tried to remove gear ''{0}'' but {1}", value.getKey(), poss.getMostSevere());
				return false;
			}

			getModel().removeCarriedItem(value);
			logger.log(Level.INFO, "Removed carried item {0}",value);

			parent.runProcessors();
			return true;
		} finally {
			logger.log(Level.TRACE, "LEAVE deselect({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(ItemTemplate data, Decision... decisions) {
		if (data.getAttribute(SMItemAttribute.PRICE)==null) {
			System.err.println("ACommonEquipmentController: No PRICE attribute in "+data.getId());
			return -1;
		}
		return data.getAttribute(SMItemAttribute.PRICE).getDistributed();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public String getSelectionCostString(ItemTemplate data) {
		int cost = (int)getSelectionCost(data);
		int telare = cost%100;
		int lunare = (cost/100)%100;
		int solare = cost/10000;
		StringBuffer buf = new StringBuffer();
		if (solare>0)
			buf.append(solare+"S ");
		if (lunare>0)
			buf.append(lunare+"L ");
		if (telare>0)
			buf.append(lunare+"T");
		return buf.toString().trim();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * Get all items that are embeddable in the given object
//	 */
//	public List<ItemTemplate> getEmbeddableIn(CarriedItem<ItemTemplate> ref, EnhancementType slot) {
////		return ItemUtil.getEmbeddableIn(ref, slot);
//		return List.of();
//	}
//
//	//-------------------------------------------------------------------
//	public Possible canBeEmbedded(CarriedItem<ItemTemplate> container, EnhancementType slot, ItemTemplate value, String variantID, Decision...decisions) {
//		if (!getEmbeddableIn(container, slot).contains(value)) {
//			return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_NOT_EMBEDDABLE, value.getName(), slot, container.getNameWithRating());
//		}
//
//		// Check for variant
//		SMPieceOfGearVariant variant = null;
//		if (variantID!=null) {
//			variant = (SMPieceOfGearVariant) value.getVariant(variantID);
//			if (variant==null) {
//				return new Possible(IRejectReasons.IMPOSS_MUST_CHOOSE_VARIANT);
//			}
//		}
//
//		// Check requirements
//		Requirement unmet = ItemUtil.areRequirementsMet(container, value, variant);
//		if (unmet!=null) {
//			return new Possible(unmet);
//		}
//
//		OperationResult<CarriedItem<ItemTemplate>> res = GearTool.buildItem(value, CarryMode.EMBEDDED, variant, getModel(), true, decisions);
//		if (res.hasError()) {
//			State state = State.POSSIBLE;
//			for (ToDoElement error : res.getMessages()) {
//				if (error.getSeverity()==Severity.STOPPER) state=State.IMPOSSIBLE;
//				if (error.toString().contains("Missing decision") && state.ordinal()<State.DECISIONS_MISSING.ordinal()) state=State.DECISIONS_MISSING;
//			}
//			return new Possible(state, res.getMessages().toString());
//		}
//		CarriedItem<ItemTemplate> toEmbed = res.get();
//
//
//		ItemAttributeNumericalValue<SMItemAttribute> val = res.get().getAsValue(SMItemAttribute.PRICE);
//		if (val==null) {
//			logger.log(Level.ERROR, "No PRICE attribute after building "+res.get());
//		} else {
//			int nuyen = val.getModifiedValue();
//			if (nuyen > getModel().getMoney()) {
//				return new Possible(Severity.WARNING, IRejectReasons.RES, IRejectReasons.IMPOSS_NOT_ENOUGH_MONEY, nuyen, getModel().getMoney());
//			}
//		}
//
////		// Check if capacity is sufficient
////		AvailableSlot realSlot = (AvailableSlot) container.getSlot(slot);
////		logger.log(Level.INFO, "Slot to add element in: {0}  with capacity = {1}", realSlot, slot.hasCapacity());
////		if (realSlot==null) {
////			return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_NO_SUCH_SLOT, slot.name(), container.getNameWithoutRating());
////		}
////		logger.log(Level.INFO, "Items in slot={0},  free capacity={1}  Slot {2} hasCapacity={3}", realSlot.getAllEmbeddedItems().size(), realSlot.getFreeCapacity(), slot.name(), slot.hasCapacity());
////		if (container.getUuid().equals(ItemTemplate.UUID_UNUSED_SOFTWARE_DEVICE))
////			return Possible.TRUE;
////		// If slot has a limit on size of accessories, verify it
////		if (realSlot.getMaxSizePerItem()!=null) {
////			float required = 1;
////			if (toEmbed.hasAttribute(SMItemAttribute.SIZE)) {
////				if (toEmbed.isFloat(SMItemAttribute.SIZE))
////					required = toEmbed.getAsFloat(SMItemAttribute.SIZE).getModifiedValue();
////				else
////					required = toEmbed.getAsValue(SMItemAttribute.SIZE).getModifiedValue();
////			}
////			if (required>realSlot.getMaxSizePerItem()) {
////				return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_SIZE, slot.name(), container.getNameWithoutRating());
////			}
////		}
//
//
//
//		return Possible.TRUE;
//
//	}
//
//	//-------------------------------------------------------------------
//	public OperationResult<CarriedItem<ItemTemplate>> embed(CarriedItem<ItemTemplate> container, EnhancementType slot, ItemTemplate value, String variantID, Decision...decisions) {
//		logger.log(Level.TRACE, "ENTER embed {0} into {1}", value, container);
//		try {
//			Possible poss = canBeEmbedded(container, slot, value, variantID, decisions);
//			if (!poss.get()) {
//				logger.log(Level.WARNING, "Trying to embed, which isn't possible: "+poss.getMostSevere());
//				return new OperationResult<>();
//			}
//
//			SMPieceOfGearVariant variant = null;
//			if (variantID!=null)
//				variant = (SMPieceOfGearVariant) value.getVariant(variantID);
//			OperationResult<CarriedItem<ItemTemplate>> res = GearTool.buildItem(value, CarryMode.EMBEDDED, variant, getModel(), true, decisions);
//			if (!res.wasSuccessful()) {
//				logger.log(Level.ERROR, "Error building item: "+res.getMessages());
//				return res;
//			}
//			container.addAccessory(res.get(), slot);
//			logger.log(Level.DEBUG, "recalculate item after embedding");
//			GearTool.recalculate("", SplittermondReference.ITEM_ATTRIBUTE, getModel(), container);
//			logger.log(Level.WARNING, "Embedded {0} into {1}/{2}", value.getId()+"/"+variant, container.getKey(), container.getUuid());
//
//			parent.runProcessors();
//			return res;
//		} finally {
//			logger.log(Level.TRACE, "LEAVE embed{0}", value);
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public Possible canBeRemoved(CarriedItem<ItemTemplate> container, EnhancementType slot, CarriedItem<ItemTemplate> toRemove) {
//		return new Possible( container.getAccessories().contains(toRemove) );
//	}
//
//	//-------------------------------------------------------------------
//	public Possible removeEmbedded(CarriedItem<ItemTemplate> container, EnhancementType slot, CarriedItem<ItemTemplate> toRemove) {
//		Possible poss = canBeRemoved(container, slot, toRemove);
//		if (!poss.get()) {
//			logger.log(Level.WARNING, "Tried to remove accessory {0} from {1}, but: {2}", toRemove, container, poss.toString());
//			return poss;
//		}
//
//		logger.log(Level.INFO, "Remove {0} from {1}", toRemove.getKey(), container.getKey());
//		boolean success = container.removeAccessory(toRemove, slot);
//		if (!success)
//			return Possible.FALSE;
//		logger.log(Level.DEBUG, "ToDo: recalculate item after removing embedded");
//		GearTool.recalculate("", SplittermondReference.ITEM_ATTRIBUTE, getModel(), container);
//
//		parent.runProcessors();
//		return Possible.TRUE;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.IEquipmentController#getEnhancementController(de.rpgframework.genericrpg.items.CarriedItem)
	 */
	@Override
	public EnhancementController getEnhancementController(CarriedItem<ItemTemplate> value) {
		return new EnhancementController(parent, value);
	}

	@Override
	public ICarriedItemController getCarriedItemController(CarriedItem<ItemTemplate> value) {
		return new CommonCarriedItemController(value, getEnhancementController(value));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(CarriedItem<ItemTemplate> value) {
		return value.getCount();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(CarriedItem<ItemTemplate> value) {
		if (!value.getResolved().isCountable())
			return Possible.FALSE;
		if (value.isAutoAdded()) {
			return new Possible(Severity.STOPPER, IRejectReasons.RES ,IRejectReasons.IMPOSS_AUTO_ADDED);
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(CarriedItem<ItemTemplate> value) {
		if (!value.getResolved().isCountable())
			return Possible.FALSE;
		if (value.getCount()<=0)
			return Possible.FALSE;
		if (value.isAutoAdded()) {
			return new Possible(Severity.STOPPER, IRejectReasons.RES ,IRejectReasons.IMPOSS_AUTO_ADDED);
		}
		if (value.getCount()==0)
			return canBeDeselected(value);

		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<CarriedItem<ItemTemplate>> decrease(CarriedItem<ItemTemplate> value) {
		Possible poss = canBeDecreased(value);
		if (!poss.get()) {
			logger.log(Level.WARNING, "Trying to decrease but {0}",poss);
			return new OperationResult<>(poss);
		}
		if (value.getCount()==1) {
			boolean removed = deselect(value);
			if (removed)
				return new OperationResult<CarriedItem<ItemTemplate>>(value);
			else {
				logger.log(Level.WARNING, "Trying to deselect but failed");
				return new OperationResult<>(Possible.FALSE);
			}
		}

		value.setCount( value.getCount()-1);
		parent.runProcessors();

		return new OperationResult<CarriedItem<ItemTemplate>>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<CarriedItem<ItemTemplate>> increase(CarriedItem<ItemTemplate> value) {
		Possible poss = canBeIncreased(value);
		if (!poss.get()) {
			return new OperationResult<>(poss);
		}
		value.setCount( value.getCount()+1);
		parent.runProcessors();

		return new OperationResult<CarriedItem<ItemTemplate>>(value);
	}

}

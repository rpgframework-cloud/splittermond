package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public interface ResourceController extends PartialController<Resource>, RecommendingController<Resource>, ComplexDataItemController<Resource, ResourceValue>, NumericalValueController<Resource, ResourceValue> {

	//-------------------------------------------------------------------
	public int getPointsLeft();

	//-------------------------------------------------------------------
	public int getPointsMax();

//	//-------------------------------------------------------------------
//	public boolean canBeSplit(ResourceValue ref);
//
//	//-------------------------------------------------------------------
//	public ResourceValue split(ResourceValue ref);
//
//	//--------------------------------------------------------------------
//	public boolean canBeJoined(ResourceValue... resources);
//
//	//--------------------------------------------------------------------
//	public void join(ResourceValue... resources);

	//--------------------------------------------------------------------
	public List<ComplexDataItemValue<?>> getLinkableItems(ResourceValue ref);

	//--------------------------------------------------------------------
	public boolean link(ResourceValue rsrc, ComplexDataItemValue<?> target);

}

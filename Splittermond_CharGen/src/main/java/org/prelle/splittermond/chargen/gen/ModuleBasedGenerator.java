package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.PowerValue;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.proc.ApplyModifications;
import org.prelle.splimo.proc.CalculateDerivedAttributes;
import org.prelle.splittermond.chargen.ai.ProfileControllerImpl;
import org.prelle.splittermond.chargen.ai.RecommenderWithModel;
import org.prelle.splittermond.chargen.gen.free.CharGenSettings;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.chargen.GeneratorId;
import de.rpgframework.genericrpg.chargen.RuleInterpretation;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.RuleController;

/**
 * @author Stefan
 *
 */
@GeneratorId("modules")
public class ModuleBasedGenerator extends SpliMoCharacterGeneratorImpl {

	protected final static Logger logger = System.getLogger(ModuleBasedGenerator.class.getPackageName());

	private boolean setupDone;

//	//-------------------------------------------------------------------
//	public ModuleBasedGenerator() {
//	}

	//-------------------------------------------------------------------
	public ModuleBasedGenerator(RuleSpecificCharacterObject model, CharacterHandle handle) {
		super((SpliMoCharacter) model, handle);
	}

	//-------------------------------------------------------------------
	@Override
	public String getId() {
		return "modules";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		return "Modul-basiert";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Bla Bla bla";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl#getWizardPages()
	 */
	@Override
	public List<PageType> getWizardPages() {
		return Arrays.asList(
				PageType.RACE,
				PageType.CULTURE,
				PageType.BACKGROUND,
				PageType.EDUCATION,
				PageType.ATTRIBUTES,
				PageType.SKILLS,
				PageType.POWERS,
				PageType.SPELLS,
				PageType.TOOLS_TRADE,
				PageType.RESOURCES,
				PageType.MOONSIGN,
				PageType.START_GEAR
);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun6.chargen.gen.CommonSR6CharacterGenerator#initializeModel()
	 */
	@Override
	protected void initializeModel() {
		if (model.getCharGenSettings(Object.class) == null  || !(model.getCharGenSettings(Object.class) instanceof CharGenSettings) ) {
			if (model.getChargenSettingsJSON() != null  && (model.getCharGenSettings(Object.class) instanceof CharGenSettings)) {
				logger.log(Level.INFO, "Restore generator config from {0}", model.getChargenSettingsJSON());
				CharGenSettings settings = model.getCharGenSettings(CharGenSettings.class);
				model.setCharGenSettings(settings);
			} else {
				logger.log(Level.INFO, "Create new generator config");
				CharGenSettings settings = new CharGenSettings();
				model.setCharGenUsed(getId());
				model.setCharGenSettings(settings);
				model.setExpFree(50);
			}
		}
		ruleCtrl = new RuleController(model, SplitterMondCore.getItemList(RuleInterpretation.class), SplittermondRules.values());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterControllerImpl#createPartialController()
	 */
	@Override
	protected void createPartialController() {
		races     = new RaceGenerator(this);
		cultures  = new CultureGenerator(this);
		backgrounds = new BackgroundGenerator(this);
		educations= new EducationGenerator(this);
		powers    = new PowerGenerator(this);
		attrib    = new AttributeGenerator(this);
		skills    = new SkillGenerator(this);
		resources = new ResourceGenerator(this);
		equip     = new EquipmentGenerator(this);
	}

	// --------------------------------------------------------------------
	@Override
	protected void setupProcessChain() {
		if (logger.isLoggable(Level.DEBUG))
			logger.log(Level.DEBUG, "ENTER: setupProcessChain()");
		try {
			if (setupDone) {
				return;
			}

			// First the regular processing steps
			processChain.addAll(SplitterTools.getCharacterProcessingSteps(model, locale));
			processChain.add(new ResetGenerator(this));
			processChain.add(races);
			processChain.add(cultures);
			processChain.add(backgrounds);
			processChain.add(educations);
			processChain.add(new ApplyModifications(model));
			processChain.add(powers);
			processChain.add(attrib);
			processChain.add(skills);
			processChain.add(resources);
			processChain.add(toolsOfTrade);
			processChain.add(equip);

			setupDone = true;
		} finally {
			if (logger.isLoggable(Level.DEBUG))
				logger.log(Level.DEBUG, "LEAVE: setupProcessChain()");
		}
	}

	//-------------------------------------------------------------------
	@Override
	public void runProcessors() {
		super.runProcessors();
		(new CalculateDerivedAttributes(this.getModel())).process(List.of());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#setModel(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	public void setModel(SpliMoCharacter model, CharacterHandle handle) {
		System.err.println("ModuleBasedGenerator.setModel("+model+")");
		this.model = model;
		this.handle= handle;
		recommender = Optional.of(new RecommenderWithModel(model));
		ruleCtrl = new RuleController(model, SplitterMondCore.getItemList(RuleInterpretation.class), SplittermondRules.values());
		profileCtrl  = new ProfileControllerImpl(this);
//		createPartialController();
		setupProcessChain();
		runProcessors();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#start(de.rpgframework.character.RuleSpecificCharacterObject)
//	 */
//	@Override
//	public void start(SpliMoCharacter model) {
//		this.model = model;
////		createPartialController();
//		runProcessors();
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#continueCreation(de.rpgframework.character.RuleSpecificCharacterObject)
//	 */
//	@Override
//	public void continueCreation(SpliMoCharacter model) {
//		this.model = model;
////		createPartialController();
//		runProcessors();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		logger.log(Level.DEBUG, "canBeFinished: "+attrib.getPointsLeft());
		if (attrib.getPointsLeft()>0) return false;
		if (model.getRace()==null) return false;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#finish()
	 */
	@Override
	public void finish() {
		logger.log(Level.INFO, "finish character");

		logger.log(Level.DEBUG, "Finalize attributes");
		for (AttributeValue<Attribute> val : model.getAttributes()) {
			val.setStart(val.getDistributed());
			val.setDistributed(val.getModifiedValue());
			val.clearIncomingModifications();
		}

		logger.log(Level.DEBUG, "Finalize skills");
		for (SMSkillValue val : model.getSkillValues()) {
			val.setDistributed(val.getModifiedValue());
			val.setStart(val.getDistributed());
			val.clearIncomingModifications();
		}

		logger.log(Level.DEBUG, "Finalize powers");
		for (PowerValue val : model.getPowers()) {
			if (val.getModifyable().hasLevel()) {
				val.setDistributed(val.getModifiedValue());
			} else {
				val.setDistributed(0);
			}
			val.clearIncomingModifications();
		}

		logger.log(Level.DEBUG, "Finalize resources");
		for (ResourceValue val : model.getResources()) {
			val.setDistributed(val.getModifiedValue());
			val.clearIncomingModifications();
		}

		super.finish();
	}

}

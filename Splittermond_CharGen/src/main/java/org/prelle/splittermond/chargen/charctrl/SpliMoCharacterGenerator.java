package org.prelle.splittermond.chargen.charctrl;

import java.util.List;
import java.util.Locale;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl.PageType;
import org.prelle.splittermond.chargen.gen.ToolsOfTheTradeGenerator;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;

/**
 * @author prelle
 *
 */
public interface SpliMoCharacterGenerator extends SpliMoCharacterController, CharacterGenerator<Attribute,SpliMoCharacter> {

	public final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SpliMoCharacterGenerator.class, Locale.GERMAN);;

	public RaceController getRaceController();

	public CultureController getCultureController();

	public BackgroundController getBackgroundController();

	public EducationController getEducationController();

	public List<PageType> getWizardPages();
	
	public ToolsOfTheTradeGenerator getToolsOfTheTradeController();

}

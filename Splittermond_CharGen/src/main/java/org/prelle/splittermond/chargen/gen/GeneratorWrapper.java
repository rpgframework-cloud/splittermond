package org.prelle.splittermond.chargen.gen;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.ICombinedSpellController;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SkillController;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl.PageType;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.IGeneratorWrapper;
import de.rpgframework.genericrpg.chargen.IRecommender;
import de.rpgframework.genericrpg.chargen.LevellingProfileController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.RuleController;

/**
 * @author stefan
 *
 */
public class GeneratorWrapper implements SpliMoCharacterGenerator, IGeneratorWrapper<Attribute, SpliMoCharacter, SpliMoCharacterGenerator> {

	private static Logger logger = System.getLogger(GeneratorWrapper.class.getPackageName());

	private SpliMoCharacter cached;
	private CharacterHandle cachedHandle;
	private SpliMoCharacterGenerator wrapped;

	//-------------------------------------------------------------------
	public GeneratorWrapper(SpliMoCharacter model, CharacterHandle handle) {
		this.cached = model;
		this.cachedHandle = handle;
	}

	@Override
	public String getId() { return wrapped.getId(); }
	@Override
	public String getName() {return wrapped.getName(); }
	@Override
	public String getDescription() { return wrapped.getDescription(); }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getLocale()
	 */
	@Override
	public Locale getLocale() {
		return wrapped.getLocale();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getWizardPages()
	 */
	@Override
	public List<PageType> getWizardPages() {
		return wrapped.getWizardPages();
	}

	//-------------------------------------------------------------------
	public boolean canBeFinished() {
		if (wrapped instanceof CharacterGenerator)
			return ((CharacterGenerator<Attribute,SpliMoCharacter>)wrapped).canBeFinished();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getModel()
	 */
	@Override
	public SpliMoCharacter getModel() {
		return wrapped.getModel();
	}
	@Override
	public void setModel(SpliMoCharacter data) {
		wrapped.setModel(data);
	}

	//-------------------------------------------------------------------
	public <T> RecommendingController<T> getRecommendingControllerFor(T item) {
		return wrapped.getRecommendingControllerFor(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getProfileController()
	 */
	@Override
	public LevellingProfileController getProfileController() {
		return wrapped.getProfileController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getRaceController()
	 */
	@Override
	public RaceController getRaceController() {
		return wrapped.getRaceController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getCultureController()
	 */
	@Override
	public CultureController getCultureController() {
		return wrapped.getCultureController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getBackgroundController()
	 */
	@Override
	public BackgroundController getBackgroundController() {
		return wrapped.getBackgroundController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return wrapped.getAttributeController();
	}

	@Override
	public void addListener(ControllerListener listener) {
		wrapped.addListener(listener);
	}

	@Override
	public void removeListener(ControllerListener listener) {
		wrapped.removeListener(listener);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#hasListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public boolean hasListener(ControllerListener callback) {
		return wrapped.hasListener(callback);
	}

	public SpliMoCharacterGenerator getWrapped() {
		return wrapped;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.IGeneratorWrapper#setWrapped(de.rpgframework.genericrpg.chargen.CharacterGenerator)
	 */
	@Override
	public void setWrapped(SpliMoCharacterGenerator newCtrl) {
		logger.log(Level.ERROR, "#################Generator changed to "+newCtrl+"\n\n\n");
		// Move all existing listener to new controller
		if (wrapped!=null) {
			logger.log(Level.WARNING, "#################Generator had {0} listener", wrapped.getListener().size());
			System.err.println("#################Generator had "+wrapped.getListener().size()+" listener");
			for (ControllerListener callback : new ArrayList<>(wrapped.getListener())) {
				newCtrl.addListener(callback);
				wrapped.removeListener(callback);
			}
		}
		wrapped = newCtrl;
		logger.log(Level.INFO, "#################Call setModel()");
		if (newCtrl.getModel()!=cached) {
			newCtrl.setModel(cached, cachedHandle);
		}
		wrapped.fireEvent(BasicControllerEvents.GENERATOR_CHANGED, newCtrl);
	}

	@Override
	public Collection<ControllerListener> getListener() {
		return wrapped.getListener();
	}

	@Override
	public void fireEvent(ControllerEvent type, Object...param) {
		wrapped.fireEvent(type, param);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#setModel(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void setModel(SpliMoCharacter model, CharacterHandle handle) {
		wrapped.setModel(model, handle);
	}

//	@Override
//	public void start(SpliMoCharacter model) {
//		wrapped.start(model);
//	}
//
//	@Override
//	public void continueCreation(SpliMoCharacter model) {
//		wrapped.continueCreation(model);
//	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		wrapped.finish();
	}

	//-------------------------------------------------------------------
	@Override
	public void setAllowRunProcessor(boolean value) {
		wrapped.setAllowRunProcessor(value);
	}

	@Override
	public void runProcessors() {
		wrapped.runProcessors();
	}

	@Override
	public List<ToDoElement> getToDos() {
		// TODO Auto-generated method stub
		return wrapped.getToDos();
	}

	@Override
	public boolean save(byte[] data) throws IOException {
		// TODO Auto-generated method stub
		return wrapped.save(data);
	}

	@Override
	public SkillController getSkillController() {
		// TODO Auto-generated method stub
		return wrapped.getSkillController();
	}

	@Override
	public EducationController getEducationController() {
		return wrapped.getEducationController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getPowerController()
	 */
	@Override
	public PowerController getPowerController() {
		return wrapped.getPowerController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getResourceController()
	 */
	@Override
	public ResourceController getResourceController() {
		return wrapped.getResourceController();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getMastershipController()
//	 */
//	@Override
//	public MastershipController getMastershipController() {
//		return wrapped.getMastershipController();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController()
	 */
	@Override
	public ICombinedSpellController getSpellController() {
		return wrapped.getSpellController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController(org.prelle.splimo.SMSkill)
	 */
	@Override
	public IPerSkillSpellController getSpellController(SMSkill school) {
		return wrapped.getSpellController(school);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getRuleController()
	 */
	@Override
	public RuleController getRuleController() {
		return wrapped.getRuleController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getRecommender()
	 */
	@Override
	public Optional<IRecommender<Attribute>> getRecommender() {
		return wrapped.getRecommender();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getToolsOfTheTradeController()
	 */
	@Override
	public ToolsOfTheTradeGenerator getToolsOfTheTradeController() {
		return wrapped.getToolsOfTheTradeController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getEquipmentController()
	 */
	@Override
	public IEquipmentController getEquipmentController() {
		return wrapped.getEquipmentController();
	}

}

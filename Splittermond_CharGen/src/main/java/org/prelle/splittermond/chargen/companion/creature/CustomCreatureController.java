package org.prelle.splittermond.chargen.companion.creature;

import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.creature.Companion;

/**
 * 
 */
public class CustomCreatureController extends ACreatureController {

	//-------------------------------------------------------------------
	public CustomCreatureController(SpliMoCharacter charModel, Companion model, ResourceValue rsrc) {
		super(charModel, model, rsrc);
	}
	
}

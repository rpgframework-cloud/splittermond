package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.splimo.Background;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.AllowModification;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public class BackgroundGenerator extends ControllerImpl<Background> implements BackgroundController {

	protected static Logger logger = System.getLogger(BackgroundGenerator.class.getPackageName()+".backg");

	private static Random RANDOM = new Random();

	private boolean allowUnusualBackgrounds = false;

	private List<Background> available;

	//-------------------------------------------------------------------
	protected BackgroundGenerator(SpliMoCharacterController parent) {
		super(parent);
		available = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.BackgroundController#setAllowUnusualBackgrounds(boolean)
	 */
	@Override
	public void setAllowUnusualBackgrounds(boolean allow) {
		allowUnusualBackgrounds = allow;
		updateAvailable();
	}
	@Override
	public boolean isAllowUnusualBackgrounds() { return allowUnusualBackgrounds;}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Background item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(Background decideFor, UUID choice, Decision decision) {
		logger.log(Level.INFO, "decide {0}={1} for {2}", choice, decision.getValue(), decideFor.getClass().getSimpleName());
		parent.getModel().getBackground().addDecision(decision);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		available.clear();

		nextBackground:
		for (Background cult : SplitterMondCore.getItemList(Background.class)) {
			if (!allowUnusualBackgrounds) {
				for (Requirement req : cult.getRequirements()) {
					boolean met = SplitterTools.isRequirementMet(parent.getModel(), req);
					if (!met) {
						continue nextBackground;
					}
				}
			}
			available.add(cult);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.BackgroundController#getAvailable()
	 */
	@Override
	public List<Background> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.BackgroundController#select(org.prelle.splimo.Background)
	 */
	@Override
	public void select(Background value) {
		logger.log(Level.INFO, "Select background ''{0}''",value);
		SpliMoCharacter model = getModel();
		if (value!=null && value.getId().equals(model.getBackground().getKey()))
			return;
		parent.getModel().setBackground(value);
		if (value!=null)
			super.updateChoices(value);

		// Recalculate
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ControllerImpl#roll()
	 */
	@Override
	public void roll() {
		List<Background> options = getAvailable().stream()
				.filter(c -> SplitterTools.areRequirementsMet(getModel(), c, new Decision[0]).get())
				.collect(Collectors.toList());
		if (parent.getRecommender().isPresent()) {
			options = (List<Background>) parent.getRecommender().get().getMostRecommended(options.get(0).getClass(), options);
		}
		int idx = random.nextInt(options.size());
		Background item = options.get(idx);
		logger.log(Level.ERROR, "Rolled {0} from {1} possible options", item.getId(), options.size());
		select(item);

		List<Decision> decisions = new ArrayList<>();
		for (Choice choice : item.getChoices()) {
			logger.log(Level.ERROR, "Choice "+choice);
			parent.getRecommender().ifPresent(r -> decide(item, choice.getUUID(),r.decide(choice)));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		SpliMoCharacter model = getModel();

		logger.log(Level.DEBUG, "START: process");
		todos.clear();
		int undecided = 0;

		Background selected = null;
		try {
			updateAvailable();

			for (Modification mod : previous) {
				if (mod.getReferenceType()==SplittermondReference.BACKGROUND) {
					if (mod instanceof AllowModification) {
						AllowModification allow = (AllowModification)mod;
						if (allow.isNegate()) {
							Background forbid = allow.getResolvedKey();
							if (forbid!=null) {
								logger.log(Level.INFO, "Forbid background {0} from {1}", forbid.getId(),mod.getSource());
								available.remove(forbid);
							} else if (allow.getAsKeys().length>1) {
								for (String forbKey : allow.getAsKeys()) {
									forbid = mod.getReferenceType().resolveAsDataItem(forbKey);
									logger.log(Level.INFO, "Forbid background {0} from {1}", forbid.getId(),mod.getSource());
									available.remove(forbid);
								}
							}
						}
					}
				} else {
					unprocessed.add(mod);
				}
			}

			// Apply modifications by culture
			// Create DecisionToMake for each ModificationChoice
			String key = model.getBackground().getKey();
			if (key==null) {
				todos.add(new ToDoElement(Severity.INFO, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_BACKGROUND_NOT_SELECTED));
			} else {
				selected = SplitterMondCore.getItem(Background.class, model.getBackground().getKey());
				if (selected==null) {
					logger.log(Level.ERROR, "Unknown background '"+model.getBackground()+"' selected");
					todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_BACKGROUND_UNKNOWN, key));
					return previous;
				}
				// selected != null  from here
				super.updateChoices(selected);
				logger.log(Level.DEBUG, "1. Apply modifications from background "+selected+" = "+selected.getOutgoingModifications());
				for (Modification tmp : selected.getOutgoingModifications()) {
					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						if ("CHOICE".equals(dMod.getKey())) {
							Choice choice = selected.getChoice(dMod.getConnectedChoice());
							Decision dec = parent.getModel().getBackground().getDecision(dMod.getConnectedChoice());
//							if (dec==null) {
//								dec = parent.getModel().getDecision(dMod.getConnectedChoice());
//							}
							if (dec!=null) {
								List<Modification> toAdd = GenericRPGTools.decisionToModifications(dMod, choice, dec);
								logger.log(Level.DEBUG, "  add modifications from choice {0}: {1}",dMod.getConnectedChoice(), toAdd);
								unprocessed.addAll(toAdd);
							} else {
								logger.log(Level.DEBUG, "  ignore modifications from open decision "+dMod.getConnectedChoice());
								undecided++;
							}
						} else {
							if (dMod.getReferenceType()==SplittermondReference.SKILL) {
								logger.log(Level.INFO, "Inject modification {0}",tmp);
							}
							unprocessed.add(tmp);
						}
					}
				}
			}

			/*
			 * Apply modifications from already made decisions
			 */
//			for (Choice choice : choices) {
//				if (model.hasDecisionBeenMade(choice.getUUID())) {
//					Decision dec = (Decision) model.getDecision(choice.getUUID());
//					logger.log(Level.WARNING, "TODO: apply modifications from "+dec);
//					System.err.println("TODO: apply modifications from "+dec);
//					unprocessed.addAll(GenericRPGTools.decisionToModifications(origMod, choice, dec));
////				if (decision.getDecision()!=null) {
////					logger.log(Level.DEBUG, "2. From "+decision.getChoice()+" add "+decision.getDecision());
////					unprocessed.addAll(decision.getDecision());
//				} else {
//					hasUndecided = true;
//					logger.log(Level.WARNING, "Undecided "+choice);
//				}
//			}
			if (undecided>0) {
				todos.add(new ToDoElement(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.TODO_BACKGROUND_DECISIONS, undecided));
			}
		} finally {
			logger.log(Level.DEBUG, "STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;

import org.prelle.splimo.Race;
import org.prelle.splimo.Size;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.RaceController;

import de.rpgframework.ResourceI18N;
import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan Prelle
 *
 */
public class RaceGenerator extends ControllerImpl<Race> implements RaceController {

	protected static Logger logger = System.getLogger(RaceGenerator.class.getPackageName()+".race");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(RaceGenerator.class.getName());
	private static Random RANDOM = new Random();

	//-------------------------------------------------------------------
	public RaceGenerator(SpliMoCharacterGeneratorImpl parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.RaceController#getAvailable()
	 */
	@Override
	public List<Race> getAvailable() {
		return SplitterMondCore.getItemList(Race.class);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.RaceController#select(org.prelle.splimo.Race)
	 */
	@Override
	public void select(Race value) {
		logger.log(Level.INFO, "Select race "+value);
		SpliMoCharacter model = getModel();
		if (value!=null && value.getId().equals(model.getRace()))
			return;

		model.setRace((value!=null)?value.getId():null);
		rollHair();
		rollEyes();
		rollSizeAndWeight();

		parent.runProcessors();
		parent.fireEvent(BasicControllerEvents.CHARACTER_CHANGED);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.processor.SpliMoCharacterProcessor#process(org.prelle.splimo.SpliMoCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		todos.clear();
		boolean hasUndecided = false;

		Race race = null;
		try {
			// Apply modifications by race
			// Create DecisionToMake for each ModificationChoice
			if (model.getRace()!=null) {
				race = SplitterMondCore.getItem(Race.class, model.getRace());
				updateChoices(race);
				logger.log(Level.DEBUG, "1. Search for undecided modifications from race "+model.getRace()+" = "+race.getOutgoingModifications());
				for (Modification tmp : race.getOutgoingModifications()) {
					if (tmp.getSource()==null)
						tmp.setSource(race);

					if (tmp instanceof DataItemModification) {
						DataItemModification dMod = (DataItemModification)tmp;
						// Does this modification require a choice?
						if (dMod.getConnectedChoice()!=null) {
							Choice choice = race.getChoice(dMod.getConnectedChoice());
							if (choice==null) {
								logger.log(Level.ERROR, "Modification "+dMod+" of "+race.getTypeString()+" references unknown modification "+dMod.getConnectedChoice());
								continue;
							}
							Decision dec = model.getDecision(dMod.getConnectedChoice());
							if (dec==null) {
								// User has not yet decided
								logger.log(Level.ERROR, "No decision yet for choice "+dMod.getConnectedChoice());
								hasUndecided = true;
							}
						}
					}
				}
			} else {
				logger.log(Level.WARNING, "Race not selected yet");
				todos.add(new ToDoElement(Severity.STOPPER, ResourceI18N.get(RES, "todos.race_not_selected")));
			}

			if (hasUndecided && race!=null) {
				todos.add(new ToDoElement(Severity.STOPPER, ResourceI18N.format(RES, "todos.decision_open_race", race.getName())));
			}
		} finally {
			logger.log(Level.TRACE, "STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return 0=size, 1=weight
	 */
	@Override
	public int[] rollSizeAndWeight() {
		logger.log(Level.ERROR, "rollSizeAndWeight");
		SpliMoCharacter model = getModel();
		if (model.getRace()==null)
			return new int[] {0,0};
		Race race = SplitterMondCore.getItem(Race.class, model.getRace());
		if (race==null)
			return new int[] {0,0};
		Size size = race.getSize();
		int result = size.getSizeBase();
		// D10
		for (int i=0; i<size.getSizeD10(); i++)
			result += (1+RANDOM.nextInt(10));
		// D6
		for (int i=0; i<size.getSizeD6(); i++)
			result += (1+RANDOM.nextInt(6));

		// Choose weight relative to size
		double relSize = ((double)result-(double)size.getSizeBase())/(10*size.getSizeD10());
		logger.log(Level.DEBUG, "Size "+result+" is "+relSize+" %");
		int weightSpan = size.getWeightMax() - size.getWeightMin();
		logger.log(Level.DEBUG, "Weight span = "+weightSpan);

		model.setSize(result);
		model.setWeight(size.getWeightMin() + (int)(relSize*weightSpan));

		return new int[]{result,  size.getWeightMin() + (int)(relSize*weightSpan)};
	}

	//-------------------------------------------------------------------
	/**
	 * Roll a random hair or fur color
	 * @return
	 */
	@Override
	public String rollHair() {
		SpliMoCharacter model = getModel();
		if (model.getRace()==null)
			return null;
		Race race = SplitterMondCore.getItem(Race.class, model.getRace());
		if (race==null)
			return null;
		int roll = 2+RANDOM.nextInt(10)+RANDOM.nextInt(10);
		String color = race.getHair().getColor(roll);
		// TODO: treat color string as ID not as an already translated word
		logger.log(Level.ERROR, "rollHair returns '"+color+"'");
		model.setHairColor(color);
		return color;
	}

	//-------------------------------------------------------------------
	/**
	 * Roll a random eye color
	 * @return
	 */
	@Override
	public String rollEyes() {
		SpliMoCharacter model = getModel();
		if (model.getRace()==null)
			return null;

		Race race = SplitterMondCore.getItem(Race.class, model.getRace());
		if (race==null)
			return null;

		int roll = 2+RANDOM.nextInt(10)+RANDOM.nextInt(10);
		String color = race.getEyes().getColor(roll);
		// TODO: treat color string as ID not as an already translated word
		logger.log(Level.INFO, "rollEyes returns '"+color+"'");
		model.setEyeColor(color);
		return color;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.RaceController#rollGender()
	 */
	@Override
	public Gender rollGender() {
		getModel().setGender( Gender.values()[RANDOM.nextInt(2)] );
		return getModel().getGender();
	}

	//-------------------------------------------------------------------
	@Override
	public void roll() {
		int idx = RANDOM.nextInt(getAvailable().size());
		Race race = getAvailable().get(idx);
		select(race);
		rollGender();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Race item) {
		return RecommendationState.NEUTRAL;
	}

}

package org.prelle.splittermond.chargen.charctrl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;

/**
 * @author prelle
 *
 */
public class OptionsTool {

	//-------------------------------------------------------------------
	/**
	 */
	public static List<?> calculateOptions(ComplexDataItem decideFor, Choice choice, PartialController<? extends DataItem> handler) {
		switch ((SplittermondReference)choice.getChooseFrom()) {
		case ATTRIBUTE:
			return calculateAttributes(decideFor, choice);
		}
		
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	private static List<Attribute> calculateAttributes(ComplexDataItem decideFor, Choice choice) {
		if (choice.getChoiceOptions()==null || choice.getChoiceOptions().length==0) {
			return Arrays.asList(Attribute.primaryValues());
		}
		
		List<Attribute> listed = new ArrayList<>();
		List<Attribute> notListed = new ArrayList<Attribute>(Arrays.asList(Attribute.primaryValues()));
		for (String tmp : choice.getChoiceOptions()) {
			Attribute attr = Attribute.valueOf(tmp);
			listed.add(attr);
			notListed.remove(attr);
		}
		
		return choice.isNegated()?notListed:listed;
	}

}

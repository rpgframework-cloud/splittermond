package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpliMoCharacter;

import de.rpgframework.genericrpg.chargen.CharacterController;

/**
 * @author Stefan Prelle
 *
 */
public interface SpliMoCharacterController extends CharacterController<Attribute,SpliMoCharacter> {

	public AttributeController getAttributeController();

	public SkillController getSkillController();

	public PowerController getPowerController();

	public ResourceController getResourceController();

//	public MastershipController getMastershipController();

	public ICombinedSpellController getSpellController();

	public IPerSkillSpellController getSpellController(SMSkill skill);

	public IEquipmentController getEquipmentController();

}

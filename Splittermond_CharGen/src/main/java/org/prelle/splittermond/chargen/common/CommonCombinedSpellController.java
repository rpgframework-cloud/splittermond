package org.prelle.splittermond.chargen.common;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellSchoolEntry;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.ICombinedSpellController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class CommonCombinedSpellController extends ControllerImpl<Spell> implements ICombinedSpellController {

	protected final static Logger logger = System.getLogger(CommonCombinedSpellController.class.getPackageName()+".spell");

	protected static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SpliMoCharacterGenerator.class.getName());

	protected static Random RANDOM = new Random();

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public CommonCombinedSpellController(SpliMoCharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		todos.clear();

		List<ToDoElement> ret = new ArrayList<>();
		for (SMSkill school : SplitterMondCore.getSkills(SkillType.MAGIC)) {
			SMSkillValue val = getModel().getSkillValue(school);
			if (val!=null && val.getModifiedValue()>0) {
				ret.addAll( parent.getSpellController(school).getToDos() );
			}
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Spell> getAvailable() {
		List<Spell> ret = new ArrayList<>();
		for (SMSkill school : SplitterMondCore.getSkills(SkillType.MAGIC)) {
			SMSkillValue val = getModel().getSkillValue(school);
			if (val!=null && val.getModifiedValue()>0) {
				ret.addAll( parent.getSpellController(school).getAvailable() );
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<SpellValue> getSelected() {
		return getModel().getSpells().stream()
				.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(Spell item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(SpellValue item) {
		if (item==null) return RecommendationState.NEUTRAL;
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item.getResolved());
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Spell value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Spell value, Decision... decisions) {
		Possible lastPoss = Possible.FALSE;
		for (SpellSchoolEntry entry : value.getSchools()) {
			Possible poss = parent.getSpellController(entry.getSchool()).canBeSelected(value, decisions);
			lastPoss = poss;
			if (poss.get())
				return poss;
		}

//		if (List.of(decisions).stream().anyMatch(dec -> dec.getChoiceUUID().equals(ICombinedSpellController.DEC_SCHOOL)))
//			return Possible.TRUE;
		return lastPoss;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<SpellValue> select(Spell value, Decision... decisions) {
		logger.log(Level.TRACE, "ENTER select({0})", value);
		try {
			Possible poss = canBeSelected(value, decisions);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to select spell ''{0}'', but {1}",value.getId(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			Optional<Decision> schoolDec = List.of(decisions).stream().filter(dec -> dec.getChoiceUUID().equals(ICombinedSpellController.DEC_SCHOOL)).findFirst();
			if (schoolDec.isEmpty()) {
				logger.log(Level.WARNING, "Trying to select spell ''{0}'', but no school selected",value.getId());
				return new OperationResult<>(Possible.FALSE);
			}

			SMSkill school = SplitterMondCore.getSkill(schoolDec.get().getValue());
			return parent.getSpellController(school).select(value, decisions);
		} finally {
			logger.log(Level.TRACE, "LEAVE select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(SpellValue value) {
		if (getModel().getSpells().contains(value))
			return Possible.TRUE;
		return new Possible(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.IMPOSS_DOES_NOT_EXIST);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(SpellValue value) {
		logger.log(Level.TRACE, "ENTER deselect({0})", value);
		try {
			SMSkill school = value.getSkill();
			return parent.getSpellController(school).deselect(value);
		} finally {
			logger.log(Level.TRACE, "LEAVE deselect({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Spell data, Decision... decisions) {
		return 0;
	}

}

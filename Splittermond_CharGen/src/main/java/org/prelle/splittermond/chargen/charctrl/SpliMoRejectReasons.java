package org.prelle.splittermond.chargen.charctrl;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;

public interface SpliMoRejectReasons {

	public final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SpliMoRejectReasons.class, Locale.GERMAN);

	public static String toLocale(String key) {
		return RES.getString(key);
	}

	public static final String TODO_CULTURE_NOT_SELECTED  = "todo.culture.not_selected";
	public static final String TODO_CULTURE_UNKNOWN       = "todo.culture.unknown";
	public static final String TODO_CULTURE_DECISIONS     = "todo.culture.decisions_open";
	public static final String TODO_BACKGROUND_NOT_SELECTED = "todo.background.not_selected";
	public static final String TODO_BACKGROUND_UNKNOWN      = "todo.background.unknown";
	public static final String TODO_BACKGROUND_DECISIONS    = "todo.background.decisions_open";
	public static final String TODO_EDUCATION_NOT_SELECTED  = "todo.education.not_selected";
	public static final String TODO_EDUCATION_UNKNOWN       = "todo.education.unknown";
	public static final String TODO_EDUCATION_DECISIONS     = "todo.education.decisions_open";
	public static final String TODO_SKILLPOINTS_LEFT        = "todo.skill.pointsLeft";
	public static final String TODO_FREE_SPELL_LEFT         = "todo.free_spell_left";
	public static final String TODOS_OVERSPENT_TOOLS_OF_TRADE = "todo.overspent_tools_of_trade";
	public static final String TODOS_UNDERSPENT_TOOLS_OF_TRADE= "todo.underspent_tools_of_trade";
	public static final String TODOS_OVERSPENT_START        = "todo.overspent_start";
	public static final String TODOS_OVERSPENT_POWER        = "todo.overspent_power";

	public static final String IMPOSS_NOT_ENOUGH_POINTS     = "imposs.not_enough_points";
	public static final String IMPOSS_NOT_ENOUGH_EXP        = "imposs.not_enough_exp";
	public static final String IMPOSS_NOT_ENOUGH_MONEY      = "imposs.not_enough_money";
	public static final String IMPOSS_ALREADY_EXISTS        = "imposs.already_exists";
	public static final String IMPOSS_DOES_NOT_EXIST        = "imposs.does_not_exist";
	public static final String IMPOSS_VALUE_TOO_LOW         = "imposs.value_too_low";
	public static final String IMPOSS_SKILLVALUE_NOT_MET    = "imposs.skillvalue_not_met";
	public static final String IMPOSS_LEVEL_TOO_LOW         = "imposs.level_too_low";
	public static final String IMPOSS_FREE_MASTERSHIPS_LEFT = "imposs.free_masterships_left";
	public static final String IMPOSS_FREE_MASTERSHIPS_IN_LEFT = "imposs.free_masterships_in_left";
	public static final String IMPOSS_NEED_SKILL_FOR_MASTER = "imposs.need_skill_for_master";

}

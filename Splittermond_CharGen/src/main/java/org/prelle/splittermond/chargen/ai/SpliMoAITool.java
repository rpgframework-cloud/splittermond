package org.prelle.splittermond.chargen.ai;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.prelle.simplepersist.Persister;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.chargen.IRecommender;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfileList;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.RecommendationModification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class SpliMoAITool {

	private final static Logger logger = System.getLogger("splittermond.ai");

	private final static Random random = new Random();
	private final static Persister serializer = new Persister();

	private static List<LevellingProfile> profiles = new ArrayList<LevellingProfile>();

	//-------------------------------------------------------------------
	public static void initialize() {
		DataSet core = new DataSet(new SpliMoAITool(), RoleplayingSystem.SPLITTERMOND, "profiles", null, Locale.GERMAN);
		Class<SpliMoAITool> clazz = SpliMoAITool.class;
		try {
			List list = SplitterMondCore.loadDataItems(LevellingProfileList.class, LevellingProfile.class, core, clazz.getResourceAsStream("profiles.xml"));
			logger.log(Level.DEBUG, "Loaded "+list.size()+" profiles");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	public static List<LevellingProfile> getProfiles() {
		return new ArrayList<LevellingProfile>(profiles);
	}

	//-------------------------------------------------------------------
	public static LevellingProfile getProfile(String id) {
		for (LevellingProfile prof : profiles) {
			if (prof.getId().equals(id))
				return prof;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<WeighedModification> convertToWeighedModification(IRecommender recommender, Modification choice) {
		// Weigh all modifications
		List<WeighedModification> weighed = new ArrayList<>();
//		for (Modification tmp2 : choice.getOptionList()) {
//			if (tmp2 instanceof SkillModification) {
//				SkillModification mod = (SkillModification)tmp2;
//				if (recommender.isRecommended(mod.getSkill())) {
//					weighed.add(new WeighedModification(mod, mod.getValue() * recommender.getRecommendation(mod.getSkill()).level.ordinal()));
//				} else
//					weighed.add(new WeighedModification(mod, 0));
//			} else
//				if (tmp2 instanceof ValueModification) {
//					ValueModification mod = (ValueModification)tmp2;
//					switch ( (SplittermondReference)mod.getReferenceType()) {
//					case ATTRIBUTE:
//						Attribute attribute = Attribute.valueOf(mod.getKey());
//						if (recommender.isRecommended(attribute)) {
//							weighed.add(new WeighedModification(mod, mod.getValue() * recommender.getRecommendation(attribute).level.ordinal()));
//						} else
//							weighed.add(new WeighedModification(mod, 0));
//						break;
//					default:
//						logger.warn("Unsupported for recommendation: "+mod);
//					}
//				}
//		}

		return weighed;
	}

	//-------------------------------------------------------------------
	public static List<Modification> makeDecision(IRecommender recommender, Modification choice) {
		List<WeighedModification> weighed = convertToWeighedModification(recommender, choice);
		Collections.sort(weighed);
		Collections.reverse(weighed);

		List<Modification> decided = new ArrayList<Modification>();
//		if (choice.getValues()!=null && choice.getValues().length>1) {
//			// Spent fix values on possible skills
//			if (choice.getOptions()[0] instanceof SkillModification) {
//				// Sort all options by recommendation value
//				List<Skill> chooseFrom = new ArrayList<Skill>();
//				for (Modification mod : choice.getOptionList()) {
//					chooseFrom.add(  ((SkillModification)mod).getSkill() );
//				}
//				Collections.sort(chooseFrom, recommender);
//
//				logger.warn("Sorted: "+chooseFrom);
//				for (Integer toSet : choice.getValues()) {
//					logger.warn("TO spend "+toSet+" on "+chooseFrom.get(0));
//					decided.add(new SkillModification(chooseFrom.remove(0), toSet));
//				}
//				logger.log(Level.INFO, "Decided for "+decided+" from choice "+choice);
//			} else {
//				logger.warn("Don't know how to spend points for "+choice.getOptions()[0].getClass());
//			}
//		} else {
//			if (choice.getOptionList().size()==0) {
//				logger.log(Level.ERROR, "Cannot select from empty option list: "+choice);
//			} else {
//				for (int i=0; i<choice.getNumberOfChoices(); i++) {
//					if (i<weighed.size()) {
//						// Still have a recommended option left
//						decided.add(weighed.get(i).mod);
//					} else {
//						// No recommendation - pick any
//						Modification randMod = null;
//						int maxLoop = 5;
//						do {
//							maxLoop--;
//							randMod = choice.getOptionList().get(random.nextInt(choice.getOptionList().size()));
//						} while (decided.contains(randMod) && maxLoop>0); // Not previously selected
//						if (!decided.contains(randMod)) {
//							logger.log(Level.INFO, "Random pick: "+randMod+" from "+choice);
//							decided.add(randMod);
//						} else {
//							logger.warn("Cannot make a random pick from "+choice+" because already picked "+decided);
//						}
//					}
//				}
//			}
//		}
		return decided;
	}

	public static Recommendation<? extends DataItem> convertModuleToRecommendation(ComplexDataItem parent, RecommenderWithoutModel recommender) {
		// Determine recommendation points ..
		Map<RecommendationModification,Integer> pointsByProfile = new HashMap<>();
		for (Modification tmp : parent.getOutgoingModifications()) {
			if (!(tmp instanceof DataItemModification))
				continue;
			// .. by inspecting modifications
			if (tmp instanceof ValueModification) {
				ValueModification mod = (ValueModification)tmp;
				switch ( (SplittermondReference)mod.getReferenceType()) {
				case ATTRIBUTE:
					Attribute attr = mod.getResolvedKey();
					Recommendation<?> rec = recommender.getRecommendation(attr);
//					if (rec!=null)
//						points += rec.getWeight().ordinal() * mod.getValue();
					break;
				default:
					rec=null;
					if ("CHOICE".equals(mod.getKey())) {
						Choice choice = parent.getChoice(mod.getConnectedChoice());
						if (choice==null) {
							logger.log(Level.ERROR,"Choice {0} of {1} cannot been found",mod.getConnectedChoice(), parent.toString());
							continue;
						}

						for (String opt : choice.getChoiceOptions()) {
							DataItem item = mod.getReferenceType().resolveAsDataItem(opt);
							if (item==null) {
								//logger.log(Level.WARNING, "Don't know how to deal with "+mod);
								continue;
							}
							Recommendation<? extends DataItem> testRec = recommender.getRecommendation(item);
							if (testRec!=null) {
								//logger.log(Level.DEBUG, "Recommendation of "+item.toString()+" is "+rec);
								if (rec==null || testRec.getWeight().ordinal()>rec.getWeight().ordinal()) {
									rec = testRec;
								}
							}
						}
					} else {
						DataItem item = mod.getResolvedKey();
						if (item==null) {
							logger.log(Level.WARNING, "Don't know how to deal with "+mod);
							continue;
						}
						rec = recommender.getRecommendation(item);
					}
					if (rec!=null) {
						for (RecommendationModification src : rec.sources) {
							//logger.log(Level.INFO, "Add "+mod.getValue()+"*"+src.getWeight().ordinal());
							int toAdd = src.getWeight().ordinal()*mod.getValue();
							pointsByProfile.put(src, pointsByProfile.containsKey(src)?(toAdd+pointsByProfile.get(src)):toAdd);
						}
					}
				}
			}
		}

		// Convert that into recommendations
		//logger.log(Level.DEBUG, "Education {0} \t= {1}", parent.getId(), pointsByProfile);
		Recommendation<? extends DataItem> rec = new Recommendation<>(parent);
		rec.sources.addAll(pointsByProfile.keySet());
		rec.pointsOverride = pointsByProfile.values().stream().collect(Collectors.summingInt( i -> i));
		logger.log(Level.TRACE, "Education {0} \t= {1}", parent.getId(), rec.getPoints());
		return rec;
	}

	//-------------------------------------------------------------------
	/**
	 * Sort the recommendations by points and assign recommendation states
	 * @param recMap
	 * @param percStrong The best percent that will get STRONGLY_RECOMMENDED
	 * @param percRecom The next best percent that will still be RECOMMENDED
	 * @param percUnrecom The lost percent that will be UNRECOMMENDED
	 */
	public static void determineRecommenationStates(Collection<Recommendation<? extends DataItem>> recoms, double percStrong, double percRecom, double percUnrecom) {
		List<Recommendation> list = new ArrayList<>(recoms);
		Collections.sort(list);
		int numStrong = (int)Math.round( ((double)list.size()*percStrong)/100.0);
		int numRecom  = (int)Math.round( ((double)list.size()*percRecom)/100.0);
		int numUnrec  = (int)Math.round( ((double)list.size()*percUnrecom)/100.0);
		// Dummy: Set all recommendations to NEUTRAL
		list.forEach(rec -> rec.setRecommendationState(RecommendationState.NEUTRAL));

		for (int i=0; i<numStrong; i++) list.get(i).setRecommendationState(RecommendationState.STRONGLY_RECOMMENDED);
		for (int i=0; i<numRecom; i++) list.get(i+numStrong).setRecommendationState(RecommendationState.RECOMMENDED);
		Collections.reverse(list);
		for (int i=0; i<numUnrec; i++) list.get(i).setRecommendationState(RecommendationState.UNRECOMMENDED);
	}

}

package org.prelle.splittermond.chargen.charctrl;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.ModificationChoice;

/**
 * @author Stefan Prelle
 *
 */
public abstract class ControllerImpl<A> implements PartialController<A> {

	protected static Logger logger = System.getLogger(ControllerImpl.class.getPackageName());

	protected static Random random = new Random();

	protected SpliMoCharacterController parent;
	protected List<ToDoElement> todos;
	protected List<UUID> choices;

	//-------------------------------------------------------------------
	protected ControllerImpl(SpliMoCharacterController parent) {
		this.parent = parent;
		this.todos  = new ArrayList<>();
		this.choices= new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getCharacterController()
	 */
	@Override
	public CharacterController<Attribute,SpliMoCharacter> getCharacterController() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
	 */
	@Override
	public SpliMoCharacter getModel() {
		return parent.getModel();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.GenerationProcessingStep#getDecisionsToMake()
	 */
	@Override
	public List<UUID> getChoiceUUIDs() {
		return choices;
	}

	//-------------------------------------------------------------------
	protected void updateChoices(ComplexDataItem value) {
		choices.clear();
		choices.addAll(value.getChoices().stream().map(c -> c.getUUID()).toList());
		choices.addAll(value.getOutgoingModifications().stream()
			.filter(m -> m instanceof ModificationChoice)
			.map(mc -> ((ModificationChoice)mc).getUUID()).toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(A decideFor, UUID choice, Decision decision) {
		logger.log(Level.WARNING, "decide {0}={1} for {2}", choice, decision.getValue(), decideFor.getClass().getSimpleName());
		parent.getModel().addDecision(decision);
//		model.setDecision(choice);
//		cached.put(decision.getChoice(), decision);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	@Override
	public void roll() {
		logger.log(Level.WARNING, "roll() not implemented in "+getClass());

		if (this instanceof ComplexDataItemController) {
			ComplexDataItemController<DataItem, DataItemValue<DataItem>> ctrl = (ComplexDataItemController<DataItem, DataItemValue<DataItem>>) this;
			List<DataItem> options = ctrl.getAvailable();
			if (parent.getRecommender().isPresent()) {
				Class<? extends DataItem> clz = options.get(0).getClass();
				options = (List<DataItem>) parent.getRecommender().get().getMostRecommended(clz, options);
			}
			int idx = random.nextInt(options.size());
			ComplexDataItem item = (ComplexDataItem) options.get(idx);
			logger.log(Level.INFO, "Rolled {0}", item.getId());
			Decision[] decisions = new Decision[0];
			if (!ctrl.getChoicesToDecide(item).isEmpty()) {
				logger.log(Level.ERROR, "Not implemented: randomizing decisions");
			}
			ctrl.select(item, decisions);
		}
	}

}

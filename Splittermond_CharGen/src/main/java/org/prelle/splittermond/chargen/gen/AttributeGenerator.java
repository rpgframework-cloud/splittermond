package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.Race;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.IRecommender;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan Prelle
 *
 */
public class AttributeGenerator extends ControllerImpl<Attribute> implements AttributeController {

	private final static Logger logger = System.getLogger(AttributeGenerator.class.getPackageName()+".attrib");
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AttributeGenerator.class.getName());

//	private SpliMoCharacterGeneratorImpl parent;

	private int pointsLeft = 18;
//	private List<ToDoElement> toDos;

	//-------------------------------------------------------------------
	public AttributeGenerator(SpliMoCharacterGeneratorImpl parent) {
		super(parent);
//		this.parent = parent;
//		toDos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	private Choice getRaceChoice(Race item) {
		if (item==null) return null;
		DataItemModification mod = null;
		Choice choice = null;
		for (Modification m : item.getOutgoingModifications()) {
			if (m instanceof DataItemModification) {
				mod = (DataItemModification) m;
				if (mod.getReferenceType()==SplittermondReference.ATTRIBUTE && mod.getConnectedChoice()!=null) {
					choice = item.getChoice(mod.getConnectedChoice());
				} else {
					mod=null;
				}
			}
		}
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		logger.log(Level.INFO, "ENTER: roll()");
		try {
			final IRecommender<Attribute> recom = parent.getRecommender().orElseGet( () -> null);
//			if (parent.getRecommender().isPresent())
//				recom = parent.getRecommender().get();

			// Attributes by recommendation state
			List<Attribute> attribs = new ArrayList<>();
			if (recom!=null) {
				attribs.addAll(recom.getMostRecommended(List.of(Attribute.primaryValues())));
			}
			// Now add remaining attributes in random order
			List<Attribute> temp = List.of(Attribute.primaryValues()).stream()
				.filter(a -> !attribs.contains(a))
				.collect(Collectors.toList());
			Collections.shuffle(temp);
			logger.log(Level.INFO, "Shuffeled remaining attributes: {0}", temp);
			attribs.addAll(temp);

			// Now distribute points - recommended attributes first
			outer:
			for (int i=0; i<pointsLeft; i++) {
				for (Attribute attr : attribs) {
					AttributeValue<Attribute> aVal = getModel().getAttribute(attr);
					Possible poss = canBeIncreased(aVal);
					if (poss.get()) {
						aVal.setDistributed(aVal.getDistributed()+1);
						// If a neutral attribute has been selected, move it to the end of the list
						if (recom!=null && recom.getRecommendationState(attr)==RecommendationState.NEUTRAL) {
							attribs.remove(attr);
							attribs.add(attr);
						}
						continue outer;
					}
				}
			}

			// TODO: Make decision
			if (getModel().getRace()!=null) {
				Race species = SplittermondReference.RACE.resolve(getModel().getRace());
				Choice choice = getRaceChoice(species);
				logger.log(Level.WARNING, "Race choice: "+choice);
				if (choice!=null) {
					Decision dec = getModel().getDecision(choice.getUUID());
					logger.log(Level.INFO, "Dec = "+dec);
					List<Attribute> options = List.of(Attribute.primaryValues());
					if (choice.getChoiceOptions()!=null) {
						options =  List.of( choice.getChoiceOptions() ).stream().map(id -> Attribute.valueOf(id)).toList();
					}
					options = new ArrayList<>(options);
					// Shuffle all
					Collections.shuffle(options);
					// If priorities exist, sort by recommendation
					if (recom!=null) {
					Collections.sort(options, new Comparator<Attribute>() {
						public int compare(Attribute o1, Attribute o2) {
							RecommendationState s1 = recom.getRecommendationState(o1);
							RecommendationState s2 = recom.getRecommendationState(o2);
							return -Integer.compare(s1.ordinal(), s2.ordinal());
						}});
					}
					logger.log(Level.WARNING, "Choice order {0}",options);

					int count = (choice.getCount()>1)?choice.getCount():1;
					List<Attribute> madeChoice = options.subList(0, count);
					logger.log(Level.WARNING, "Chosen {0}",madeChoice);
					dec = new Decision(choice, String.join(",", madeChoice.stream().map(attr -> attr.name()).toList()));
					((SpliMoCharacterGeneratorImpl)parent).getRaceController().decide(species, choice.getUUID(), dec);
				}
			}

			parent.runProcessors();
			for (Attribute attr : attribs) {
				logger.log(Level.WARNING, "{0} = {1}", attr, getRecommendationState(attr));
			}
			
		} finally {
			logger.log(Level.INFO, "LEAVE: roll()");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public Possible canBeIncreased(AttributeValue<Attribute> value) {
		if (value.getDistributed()>2) return Possible.FALSE;
		// TODo: experience
		if (pointsLeft<=0) return Possible.FALSE;
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public Possible canBeDecreased(AttributeValue<Attribute> value) {
		return new Possible(value.getDistributed()>1);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public OperationResult<AttributeValue<Attribute>> increase(AttributeValue<Attribute> value) {
		logger.log(Level.DEBUG, "increase");
		Possible allowed = canBeIncreased(value);
		if (!allowed.get()) {
			return new OperationResult<>(allowed);
		}

		value.setDistributed( value.getDistributed() +1);
		logger.log(Level.INFO, "Increased "+value.getModifyable()+" to "+value.getDistributed());

		parent.runProcessors();
		return new OperationResult<>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public OperationResult<AttributeValue<Attribute>> decrease(AttributeValue<Attribute> value) {
		logger.log(Level.DEBUG, "decrease");
		Possible allowed = canBeDecreased(value);
		if (!allowed.get()) {
			return new OperationResult<>(allowed);
		}

		value.setDistributed( value.getDistributed() -1);
		logger.log(Level.INFO, "Decreased "+value.getModifyable()+" to "+value.getDistributed());

		parent.runProcessors();
		return new OperationResult<>(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return pointsLeft;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.AttributeController#getIncreaseCost(org.prelle.splimo.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<Modification>(previous);
		todos.clear();

		// Debug
		if (logger.isLoggable(Level.INFO)) {
			for (Attribute attr : Attribute.primaryValues()) {
				logger.log(Level.INFO, "Recommendation for {0} is {1}", attr, getRecommendationState(attr));
			}
		}

		pointsLeft = 18;
//		for (Modification mod : previous) {
//			if (mod instanceof ValueModification) {
//				ValueModification vMod = (ValueModification)mod;
//				if (vMod.getReferenceType()==SplittermondReference.ATTRIBUTE) {
//					if ("CHOICE".equals(vMod.getKey())) {
//						UUID choice = vMod.getConnectedChoice();
//						if (!parent.getModel().hasDecisionBeenMade(choice)) {
//							todos.add(new ToDoElement(Severity.WARNING, "Choose an attribute"));
//						} else {
//							Decision dec = (Decision) parent.getModel().getDecision(choice);
//							for (String val : dec.getValues()) {
//								Attribute key = Attribute.valueOf(val);
//								parent.getModel().getAttribute(key).addModification(vMod);
//							}
//						}
//					} else {
//						Attribute key = Attribute.valueOf(vMod.getKey());
//						parent.getModel().getAttribute(key).addModification(vMod);
//					}
//				} else {
//					unprocessed.add(mod);
//				}
//			} else {
//				unprocessed.add(mod);
//			}
//		}

		/*
		 * Ensure the distributed points are between 1-3
		 */
		for (Attribute attr : Attribute.primaryValues()) {
			AttributeValue<Attribute> val = (AttributeValue<Attribute>) parent.getModel().getAttribute(attr);
			if (val==null)
				throw new NullPointerException("Missing value for attribute "+attr+" in character");
			if (val.getDistributed()<1)
				val.setDistributed(1);
			if (val.getDistributed()>3)
				val.setDistributed(3);
			pointsLeft -= val.getDistributed();
			logger.log(Level.DEBUG, "Pay {0} points for {1} \tattrVal= {2}", val.getDistributed(), attr, val);
		}

		// Make sure all points are spent
		if (pointsLeft>0) {
			todos.add(new ToDoElement(Severity.STOPPER, ResourceI18N.format(RES, "chargen.attrib.points_left", pointsLeft)));
		}
		if (pointsLeft<0) {
			todos.add(new ToDoElement(Severity.STOPPER, ResourceI18N.format(RES, "chargen.attrib.overspent", -pointsLeft)));
		}


		return unprocessed;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
//	 */
//	@Override
//	public RuleSpecificCharacterObject getModel() {
//		return parent.getModel();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Attribute key) {
		// No recommender set, means no recommendation
		if (parent.getRecommender().isEmpty()) {
			return RecommendationState.NEUTRAL;
		}

		return parent.getRecommender().get().getRecommendationState(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(AttributeValue<Attribute> value) {
		return value.getModifiedValue();
	}


}

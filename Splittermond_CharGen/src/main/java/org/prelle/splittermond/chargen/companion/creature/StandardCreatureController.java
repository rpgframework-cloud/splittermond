package org.prelle.splittermond.chargen.companion.creature;

import java.lang.System.Logger.Level;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.companion.proc.CompanionTool;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.SMLifeform.LifeformType;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;

/**
 * 
 */
public class StandardCreatureController extends ACreatureController {

	//-------------------------------------------------------------------
	public StandardCreatureController(SpliMoCharacter charModel, Companion model, ResourceValue rsrc) {
		super(charModel, model, rsrc);
		if (resource!=null)
			resource.setUuid(model.getLinkUUID());
		charModel.addCompanion(model);

	}
	//-------------------------------------------------------------------
	public StandardCreatureController(SpliMoCharacter charModel, ResourceValue rsrc) {
		super(charModel, new Companion(LifeformType.CREATURE), rsrc);
		if (resource!=null)
			resource.setUuid(model.getLinkUUID());
		charModel.addCompanion(model);
	}

	//-------------------------------------------------------------------
	public List<Creature> getAvailable() {
		return SplitterMondCore.getItemList(Creature.class).stream()
				.filter(c -> showDataItem(c))
				.filter(c -> resource==null || c.getLevel()==resource.getModifiedValue())
				.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	public Possible canBeSelected(Creature value) {
		return new Possible(getAvailable().contains(value));
	}
		
	//-------------------------------------------------------------------
	public Companion select(Creature value) {
		Possible poss = canBeSelected(value);
		if (!poss.get()) {
			logger.log(Level.WARNING, "Trying to select creature {0} but {1}", value.getId(), poss.getMostSevere());
			return null;
		}
		
		model.setStandardCreature(value.getId());
		
		CompanionTool.recalculate(model);
		return model;
	}
}

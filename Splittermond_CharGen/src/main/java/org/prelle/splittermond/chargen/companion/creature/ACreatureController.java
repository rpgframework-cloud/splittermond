package org.prelle.splittermond.chargen.companion.creature;

import java.lang.System.Logger;
import java.util.Collection;
import java.util.List;

import org.prelle.splimo.Race;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splittermond.chargen.companion.ALifeformController;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class ACreatureController extends ALifeformController {

	protected final static Logger logger = System.getLogger(ACreatureController.class.getPackageName()+".entourage");

	protected ResourceValue resource;
	
	//-------------------------------------------------------------------
	public ACreatureController(SpliMoCharacter charModel, Companion model, ResourceValue rsrc) {
		super(charModel, model);
		this.resource = rsrc;
	}

	//-------------------------------------------------------------------
	public ComplexDataItemController<CreatureModule, CreatureModuleValue> getTrainingController() {
		return null;
	}
	
}

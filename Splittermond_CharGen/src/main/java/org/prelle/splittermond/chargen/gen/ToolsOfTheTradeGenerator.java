package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.ItemType;
import org.prelle.splimo.items.ItemUtil;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.items.SMItemFlag;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoRejectReasons;
import org.prelle.splittermond.chargen.common.ACommonEquipmentController;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * Allow spending 35L
 */
public class ToolsOfTheTradeGenerator extends ACommonEquipmentController {

	protected static Logger logger = System.getLogger(ToolsOfTheTradeGenerator.class.getPackageName()+".tools");

	private int money = 3500;

	private CarriedItem<ItemTemplate> freeWeapon;

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public ToolsOfTheTradeGenerator(SpliMoCharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<CarriedItem<ItemTemplate>> getSelected() {
		return getModel().getCarriedItems().stream().filter(item -> item.hasFlag(SMItemFlag.TOOL_OF_THE_TRADE)).toList();
	}

	//-------------------------------------------------------------------
	public int getMoneyRemaining() {
		return money;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(ItemTemplate item, Decision... decisions) {
		ItemAttributeDefinition def = item.getAttribute(SMItemAttribute.PRICE);
		if (def==null) {
			logger.log(Level.WARNING, "No PRICE definition for {0}", item.getId());
			return Possible.TRUE;
		}
		int value = def.getDistributed();
		if (value>money) {
			return new Possible(Severity.WARNING, SpliMoRejectReasons.RES, SpliMoRejectReasons.IMPOSS_NOT_ENOUGH_MONEY, value, money);
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<CarriedItem<ItemTemplate>> select(ItemTemplate value, Decision... decisions) {
		logger.log(Level.TRACE, "ENTER select({0})", value);
		try {
			OperationResult<CarriedItem<ItemTemplate>> result = super.select(value, decisions);
			result.get().addFlag(SMItemFlag.TOOL_OF_THE_TRADE);
			parent.runProcessors();
			return result;
		} finally {
			logger.log(Level.TRACE, "LEAVE select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		logger.log(Level.TRACE, "ENTER process");
		todos.clear();
		try {
			// Find the most expensive weapon that is max. 10L and mark it as
			// a free weapon
			int lastCost =0;
			freeWeapon = null;
			for (CarriedItem<ItemTemplate> item : getModel().getCarriedItems()) {
				item.removeFlag(SMItemFlag.FREE_WEAPON);
				ItemAttributeNumericalValue<SMItemAttribute> aVal = item.getAsValue(SMItemAttribute.PRICE);
				if (aVal==null) {
					logger.log(Level.ERROR, "No PRICE attribute in "+item);
					continue;
				}
				// Only weapons
				ItemType type = ItemUtil.getItemType(item);
				if (type!=ItemType.WEAPON_CLOSE && type!=ItemType.WEAPON_RANGED)
					continue;
				// That are below 10L
				int telare = aVal.getModifiedValue();
				if (telare>lastCost && telare<=1000) {
					freeWeapon = item;
					lastCost = telare;
				}
			}
			// If there is such a weapon, give it the FREE_WEAPON flag
			if (freeWeapon!=null) {
				logger.log(Level.INFO, "Treat {0} as free <=10L weapon", freeWeapon.getNameWithoutRating());
				freeWeapon.setFlag(SMItemFlag.FREE_WEAPON, true);
			}


			money = 3500;
			for (CarriedItem<ItemTemplate> item : getModel().getCarriedItems()) {
				if (!item.hasFlag(SMItemFlag.TOOL_OF_THE_TRADE))
					continue;
				if (item.hasFlag(SMItemFlag.FREE_WEAPON))
					continue;
				ItemAttributeNumericalValue<SMItemAttribute> aVal = item.getAsValue(SMItemAttribute.PRICE);
				if (aVal==null) {
					logger.log(Level.ERROR, "No PRICE attribute in "+item);
				}
				int telare = aVal.getModifiedValue();
				logger.log(Level.INFO, "Pay {0} Lunare for {1}", ((double)telare)/100.0, item.getKey());
				money -= telare;
			}

			// Check money spent
			if (money<0) {
				double overspent = ((double)money)/100;
				logger.log(Level.WARNING, "Overspent tools of the trade money: {0}",money);
				todos.add(new ToDoElement(Severity.STOPPER, SpliMoRejectReasons.RES,SpliMoRejectReasons.TODOS_OVERSPENT_TOOLS_OF_TRADE, (int)overspent));
			} else if (money>1000) {
				double underspent = ((double)money)/100;
				logger.log(Level.WARNING, "Underspent tools of the trade money: {0}",money);
				todos.add(new ToDoElement(Severity.INFO, SpliMoRejectReasons.RES,SpliMoRejectReasons.TODOS_UNDERSPENT_TOOLS_OF_TRADE, (int)underspent));
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE process");
		}
		return unprocessed;
	}

}

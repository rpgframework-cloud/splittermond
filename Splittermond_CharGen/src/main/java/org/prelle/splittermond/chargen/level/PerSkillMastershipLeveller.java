package org.prelle.splittermond.chargen.level;

import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.common.APerSkillMastershipController;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;

/**
 * 
 */
public class PerSkillMastershipLeveller extends APerSkillMastershipController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 * @param value
	 */
	public PerSkillMastershipLeveller(SpliMoCharacterController parent, SMSkillValue value) {
		super(parent, value);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 * Child classes should override this
	 */
	@Override
	public OperationResult<MastershipReference> select(Mastership value, Decision... decisions) {
		logger.log(Level.TRACE, "select({0}, {1})", value, List.of(decisions));
		try {
			OperationResult<MastershipReference> result = super.select(value, decisions);
			if (result.hasError()) {
				return result;
			}
			// Pay Exp 
			
			
			parent.runProcessors();
			
			return result;
		} finally {
			logger.log(Level.TRACE, "select({0})", value);			
		}
	}

}

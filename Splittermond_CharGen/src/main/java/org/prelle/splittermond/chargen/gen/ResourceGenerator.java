package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.IRejectReasons;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.NumericalValueWith2PoolsController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ResourceGenerator extends ControllerImpl<Resource> implements ResourceController, NumericalValueWith2PoolsController<Resource, ResourceValue> {

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(ResourceGenerator.class, Locale.GERMAN);

	private int points, pointsMax;
	private List<Resource> available;

	//-------------------------------------------------------------------
	public ResourceGenerator(SpliMoCharacterController parent) {
		super(parent);
		available = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(Resource item) {
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(item);
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(ResourceValue value) {
		if (parent.getRecommender().isPresent()) {
			return parent.getRecommender().get().getRecommendationState(value.getResolved());
		}
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<Resource> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<ResourceValue> getSelected() {
		return getModel().getResources();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(Resource value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Resource value, Decision... decisions) {
		if (!getAvailable().contains(value)) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_PRESENT);
		}
		if (points>0 || getModel().getExpFree()>=7) {
			return new Possible(true);
		}

		return new Possible(false, IRejectReasons.IMPOSS_NOT_ENOUGH_EXP);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<ResourceValue> select(Resource value, Decision... decisions) {
		logger.log(Level.DEBUG, "ENTER select({0}, {1})", value.getId(), Arrays.toString(decisions));
		try {
			Possible poss = canBeSelected(value, decisions);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to select {0} but not possible due to {1}", value.getId(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			ResourceValue val = new ResourceValue(value, 1);
			getModel().addResource(val);
			if (!value.isBaseResource()) {
				val.setIdReference(UUID.randomUUID());
			}
			logger.log(Level.INFO, "Selected resource ''{0}''", value.getId());

			parent.runProcessors();

			return new OperationResult<>(val);
		} finally {
			logger.log(Level.DEBUG, "LEAVE select({0}, {1})", value.getId(), Arrays.toString(decisions));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(ResourceValue value) {
		if (!getModel().getResources().contains(value)) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_PRESENT);
		}
		if (value.getResolved().isBaseResource()) {
			return new Possible(false, IRejectReasons.IMPOSS_BASE_RESOURCE);
		}
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(ResourceValue value) {
		logger.log(Level.DEBUG, "ENTER deselect({0})", value.getKey());
		try {
			Possible poss = canBeDeselected(value);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to deselect {0} but not possible due to {1}", value.getKey(), poss.getMostSevere());
				return false;
			}

			if (!value.isAutoAdded()) {
				getModel().removeResource(value);
				logger.log(Level.INFO, "Deselected resource ''{0}''", value.getKey());
			} else {
				value.setDistributed( -value.getModifier() );
			}

			parent.runProcessors();

			return true;
		} finally {
			logger.log(Level.DEBUG, "LEAVE deselect({0})", value.getKey());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(Resource data, Decision... decisions) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreased(ResourceValue value) {
		if (!getModel().getResources().contains(value)) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		if (value.getModifiedValue()>=4 && !parent.getRuleController().getRuleValueAsBoolean(SplittermondRules.CHARGEN_EXTREME_RESOURCES)) {
			return new Possible(false, IRejectReasons.IMPOSS_MAX_LEVEL_REACHED);
		}

		if (value.getModifiedValue()>=6) {
			return new Possible(false, IRejectReasons.IMPOSS_MAX_LEVEL_REACHED);
		}

		if (points>0 || getModel().getExpFree()>=7) {
			return Possible.TRUE;
		}

		return new Possible(false, IRejectReasons.IMPOSS_NOT_ENOUGH_POINTS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(ResourceValue value) {
		if (!getModel().getResources().contains(value)) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		if (value.getModifiedValue()==0 && !parent.getRuleController().getRuleValueAsBoolean(SplittermondRules.CHARGEN_EXTREME_RESOURCES)) {
			return new Possible(false, IRejectReasons.IMPOSS_MIN_LEVEL_REACHED);
		}

		if (value.getModifiedValue()<0) {
			//if (value.getModifyable().isBaseResource())
			return new Possible(false, IRejectReasons.IMPOSS_MIN_LEVEL_REACHED);
		}

		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<ResourceValue> increase(ResourceValue value) {
		logger.log(Level.DEBUG, "ENTER increase({0})", value.getKey());
		try {
			Possible poss = canBeIncreased(value);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to increase {0} but not possible due to {1}", value.getKey(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			value.setDistributed( value.getDistributed() +1 );
			logger.log(Level.INFO, "Increased resource ''{0}'' to ", value.getKey(), value.getModifiedValue());

			parent.runProcessors();

			return new OperationResult<ResourceValue>(value);
		} finally {
			logger.log(Level.DEBUG, "LEAVE increase({0})", value.getKey());
		}
	}

	@Override
	public OperationResult<ResourceValue> decrease(ResourceValue value) {
		logger.log(Level.DEBUG, "ENTER decrease({0})", value.getKey());
		try {
			Possible poss = canBeDecreased(value);
			if (!poss.get()) {
				logger.log(Level.WARNING, "Trying to increase {0} but not possible due to {1}", value.getKey(), poss.getMostSevere());
				return new OperationResult<>(poss);
			}

			value.setDistributed( value.getDistributed() -1 );
			logger.log(Level.INFO, "Decreased resource ''{0}'' to ", value.getKey(), value.getModifiedValue());
			if (value.getModifiedValue()==0) {
				if (!value.isAutoAdded()) {
					getModel().removeResource(value);
				}
			}

			parent.runProcessors();

			return new OperationResult<ResourceValue>(value);
		} finally {
			logger.log(Level.DEBUG, "LEAVE decrease({0})", value.getKey());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith1PoolController#getColumn1()
	 */
	@Override
	public String getColumn1() {
		return "Punkte";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith1PoolController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return points;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith1PoolController#canBeIncreasedPoints(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeIncreasedPoints(ResourceValue value) {
		if (!getModel().getResources().contains(value)) {
			return new Possible(false, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		if (value.getModifiedValue()>=4 && !parent.getRuleController().getRuleValueAsBoolean(SplittermondRules.CHARGEN_EXTREME_RESOURCES)) {
			return new Possible(false, IRejectReasons.IMPOSS_MAX_LEVEL_REACHED);
		}

		if (value.getModifiedValue()>=6) {
			return new Possible(false, IRejectReasons.IMPOSS_MAX_LEVEL_REACHED);
		}

		if (points>0) {
			return Possible.TRUE;
		}

		return Possible.FALSE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith1PoolController#canBeDecreasedPoints(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreasedPoints(ResourceValue key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<ResourceValue> increasePoints(ResourceValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<ResourceValue> decreasePoints(ResourceValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColumn2() {
		return "Exp";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith2PoolsController#getPointsLeft2()
	 */
	@Override
	public int getPointsLeft2() {
		return getModel().getExpFree();
	}

	@Override
	public Possible canBeIncreasedPoints2(ResourceValue key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canBeDecreasedPoints2(ResourceValue key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<ResourceValue> increasePoints2(ResourceValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<ResourceValue> decreasePoints2(ResourceValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		available.clear();
		available.addAll( SplitterMondCore.getItemList(Resource.class).stream().filter(r -> !r.isBaseResource()).collect(Collectors.toList()) );
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		SpliMoCharacter model = getModel();

		logger.log(Level.TRACE, "START: process");
		choices.clear();
		todos.clear();
		points = 8;
		pointsMax = 8;

		try {
			for (Modification tmp : previous) {
				if (tmp.getReferenceType()!=SplittermondReference.RESOURCE) {
					// Does not relate to powers
					unprocessed.add(tmp);
					continue;
				}
				if (tmp instanceof DataItemModification) {
					DataItemModification mod = (DataItemModification)tmp;
					Resource rsrc = mod.getResolvedKey();
					if (rsrc==null) {
						logger.log(Level.ERROR, "Cannot find resource ''{0}'' from {1}", mod.getKey(), mod.getSource());
					}
					// See if there is a matching resource without modification
					ResourceValue val = null;
					if (rsrc.isBaseResource()) {
						val = getModel().getResource(mod.getKey());
					} else {
						for (ResourceValue r : getModel().getResources()) {
							if (r.getKey().equals(mod.getKey()) && !r.isAutoAdded()) {
								val = r;
							}
						}
					}


					if (val==null) {
						val = new ResourceValue(rsrc, 0);
						model.addResource(val);
						logger.log(Level.DEBUG, "add resource {0} {1} from {2}", rsrc, ((ValueModification)mod).getValue(), mod.getSource());
					} else {
						// Resource already exists
						logger.log(Level.DEBUG, "Add modification "+mod+" to existing resource "+val);
					}
					val.addIncomingModification(mod);
				} else {
					logger.log(Level.WARNING, "Don't know how to deal with modification "+tmp);
				}
			}

			// Iterate all resources
			for (ResourceValue ref : model.getResources()) {
				int level = ref.getModifiedValue();
				if (level<0) {
					// Grant more points
					points -= level;
				} else if (level>0) {
					int cost = Math.min(points, level);
					if (cost>0) {
						logger.log(Level.INFO, "pay {0} points for resource ''{1}''", cost, ref.getKey());
						points-= cost;
						level -= cost;
					}
					if (level>0) {
						int exp = level*7;
						logger.log(Level.INFO, "pay {0} exp for resource ''{1}'' {2}", exp, ref.getKey(), ref.getModifiedValue());
						points-= level;
						model.setExpFree( model.getExpFree() - exp);
					}
				} else {
					logger.log(Level.INFO, "pay nothing for resource ''{0}''", ref.getKey());
				}
			}

			// Update available
			updateAvailable();

			/*
			 * ToDos
			 */
			if (points>0) {
				todos.add(new ToDoElement(Severity.STOPPER, RES, "todos.pointsLeft", points));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			logger.log(Level.TRACE, "STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith1PoolController#getPoints(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getPoints(ResourceValue key) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueWith2PoolsController#getPoints2(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getPoints2(ResourceValue key) {
		return 7;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(ResourceValue value) {
		logger.log(Level.DEBUG, "getValue("+value.getKey()+") = "+value.getDistributed()+" vs "+value.getModifiedValue()+" from "+value.getIncomingModifications());
		return value.getModifiedValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ResourceController#getLinkableItems(org.prelle.splimo.ResourceValue)
	 */
	@Override
	public List<ComplexDataItemValue<?>> getLinkableItems(ResourceValue ref) {
		List<ComplexDataItemValue<?>> ret = new ArrayList<>();
		try {
			Resource rsrc = ref.getResolved();
			switch (rsrc.getId()) {
			case "creature":
				for (SMLifeform comp : getModel().getCompanions()) {
					if (comp.getLifeformType()==LifeformType.CREATURE && comp.getLevel()==ref.getModifiedValue()) {
//						ret.add(ret);
					}
				}
				break;
			}
		} finally {

		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ResourceController#link(org.prelle.splimo.ResourceValue, de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean link(ResourceValue rsrc, ComplexDataItemValue<?> target) {
		if (!getLinkableItems(rsrc).contains(target))
			return false;

		logger.log(Level.INFO, "Linked resource {0} with {1}", rsrc, target.getNameWithRating());
		rsrc.setIdReference(target.getUuid());

		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.ResourceController#getPointsMax()
	 */
	@Override
	public int getPointsMax() {
		return pointsMax;
	}

}

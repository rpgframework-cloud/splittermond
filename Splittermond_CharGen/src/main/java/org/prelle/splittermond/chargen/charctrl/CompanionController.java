package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Background;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.SMLifeform;
import org.prelle.splimo.items.ItemTemplate;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.items.CarriedItem;

/**
 * @author prelle
 *
 */
public interface CompanionController extends 
	PartialController<Companion>,
	RecommendingController<Companion> {

	public List<SMLifeform.LifeformType> getSupportedTypes();
	
	//public OperationResult<SMLifeform> select()
}

package org.prelle.splittermond.chargen.ai;

import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.charctrl.ControllerImpl;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.LevellingProfileController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.GenericRPGTools;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class ProfileControllerImpl extends ControllerImpl<LevellingProfile>
		implements LevellingProfileController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public ProfileControllerImpl(SpliMoCharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getAvailable()
	 */
	@Override
	public List<LevellingProfile> getAvailable() {
		return SplitterMondCore.getItemList(LevellingProfile.class).stream()
			.filter(p -> parent.showDataItem(p))
			//.filter(p -> !getModel().hasProfile(p) || p.canBeUsedMultiple)
			.collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<LevellingProfileValue> getSelected() {
		return getModel().getProfiles();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(LevellingProfile value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(LevellingProfileValue value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getChoicesToDecide(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public List<Choice> getChoicesToDecide(LevellingProfile value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(LevellingProfile value, Decision... decisions) {
		return GenericRPGTools.areAllDecisionsPresent(value, decisions);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<LevellingProfileValue> select(LevellingProfile value, Decision... decisions) {
		Possible poss = canBeSelected(value, decisions);
		if (!poss.get()) {
			return new OperationResult<>(poss);
		}
		
		LevellingProfileValue ret = new LevellingProfileValue(value);
		for (Decision dec : decisions) ret.addDecision(dec);
		
		getModel().addProfile(ret);
		logger.log(Level.INFO, "Add levelling profile {0} with decisions: {1}", value.getId(), Arrays.toString(decisions));
		
		System.err.println("ProfileControllerImpl: Changed "+parent.getRecommender().get());
		parent.fireEvent(BasicControllerEvents.CHARACTER_PROFILES_CHANGED, getModel());
		return new OperationResult<LevellingProfileValue>(ret);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(LevellingProfileValue value) {
		if (getModel().getProfiles().contains(value))
			return Possible.TRUE;
		return Possible.FALSE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(LevellingProfileValue value) {
		Possible poss = canBeDeselected(value);
		if (!poss.get())
			return false;
		
		getModel().removeProfile(value);
		logger.log(Level.INFO, "Removed profile ''{0}''");
		
		parent.fireEvent(BasicControllerEvents.CHARACTER_PROFILES_CHANGED, getModel());
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCost(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public float getSelectionCost(LevellingProfile data, Decision... decisions) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCostString(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public String getSelectionCostString(LevellingProfile data) {
		return String.valueOf((int)getSelectionCost(data));
	}

}

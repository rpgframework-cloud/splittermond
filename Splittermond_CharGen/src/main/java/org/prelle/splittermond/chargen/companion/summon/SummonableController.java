package org.prelle.splittermond.chargen.companion.summon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collection;
import java.util.List;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModule.Category;
import org.prelle.splimo.creature.CreatureModule.Type;
import org.prelle.splimo.creature.CreatureModuleValue;
import org.prelle.splimo.creature.SMLifeform.LifeformType;
import org.prelle.splimo.creature.SummonableCreature;
import org.prelle.splittermond.chargen.companion.ALifeformController;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * 
 */
public class SummonableController extends ALifeformController implements NumericalValueController<SMSkill, SMSkillValue> {

	private final static Logger logger = System.getLogger(SummonableController.class.getPackageName()+".entourage");

	private Collection<ControllerListener> listener;

	protected List<ProcessingStep> processChain;
	protected boolean dontProcess;
	private List<Modification> unitTestModifications;
	protected boolean allowRunProcessor = true;

	private ExtraSummonableController ctrlOptions;
	private ServicesController ctrlServices;

	//-------------------------------------------------------------------
	public SummonableController(SpliMoCharacter charModel, int max) {
		super(charModel, new Companion(LifeformType.SUMMONABLE));
		model = new Companion(LifeformType.SUMMONABLE);
		model.setSummonable(new SummonableCreature());
		ctrlOptions = new ExtraSummonableController(this);
		ctrlServices= new ServicesController(charModel,this);
		
		charModel.addCompanion(model);
	}

	//-------------------------------------------------------------------
	public OperationResult<CreatureModuleValue> selectBody(CreatureModule bodyModule) {
		// Check validity of module
		if (bodyModule==null || bodyModule.getCategory()!=Category.SUMMONABLE || bodyModule.getType()!=Type.BODY)
			return new OperationResult<>();

		CreatureModuleValue tmp = new CreatureModuleValue(bodyModule);
		model.getSummonable().setBody(tmp);

		runProcessors();
		return new OperationResult<CreatureModuleValue>(tmp);
	}

	//-------------------------------------------------------------------
	public OperationResult<CreatureModuleValue> selectType(CreatureModule bodyModule) {
		// Check validity of module
		if (bodyModule==null || bodyModule.getCategory()!=Category.SUMMONABLE || bodyModule.getType()!=Type.TYPE)
			return new OperationResult<>();

		CreatureModuleValue tmp = new CreatureModuleValue(bodyModule);
		model.getSummonable().setType(tmp);

		runProcessors();
		return new OperationResult<CreatureModuleValue>(tmp);
	}

	//-------------------------------------------------------------------
	public OperationResult<CreatureModuleValue> addModule(CreatureModule mod, Decision...decisions) {
		// Check validity of module
		if (mod==null || mod.getCategory()!=Category.SUMMONABLE || mod.getType()==Type.BODY || mod.getType()==Type.TYPE) {
			logger.log(Level.INFO, "trying to add invalid module {0}", mod.getId());
			return new OperationResult<>();
		}

		CreatureModuleValue tmp = new CreatureModuleValue(mod);
		for (Decision dec : decisions)
			tmp.addDecision(dec);
		model.getSummonable().addModule(tmp);
		logger.log(Level.INFO, "addModule {0}", mod.getId());

		runProcessors();
		return new OperationResult<CreatureModuleValue>(tmp);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.RecommendingController#getRecommendationState(java.lang.Object)
	 */
	@Override
	public RecommendationState getRecommendationState(SMSkill item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getValue(SMSkillValue value) {
		return value.getModifiedValue();
	}

	@Override
	public Possible canBeIncreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Possible canBeDecreased(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SMSkillValue> increase(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OperationResult<SMSkillValue> decrease(SMSkillValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	public IExtraCreatureModulesController getExtraModuleController() {
		return ctrlOptions;
	}

	//-------------------------------------------------------------------
	public ServicesController getServicesController() {
		return ctrlServices;
	}
	
	
}

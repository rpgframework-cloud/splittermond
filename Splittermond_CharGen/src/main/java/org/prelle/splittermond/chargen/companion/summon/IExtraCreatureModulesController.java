package org.prelle.splittermond.chargen.companion.summon;

import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splimo.creature.CreatureModuleValue;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;

/**
 * @author prelle
 *
 */
public interface IExtraCreatureModulesController
		extends ComplexDataItemController<CreatureModule, CreatureModuleValue> {

}

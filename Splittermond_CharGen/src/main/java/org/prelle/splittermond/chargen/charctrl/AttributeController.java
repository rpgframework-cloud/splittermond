package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Attribute;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.AttributeValue;

public interface AttributeController extends NumericalValueController<Attribute, AttributeValue<Attribute>>, PartialController<Attribute> {

	//--------------------------------------------------------------------
	/**
	 * During generation: Returns the available amount of points
	 * to distribute
	 * During career: Returns free experience points
	 */
	public int getPointsLeft();

	//-------------------------------------------------------------------
	public int getIncreaseCost(Attribute key);

}
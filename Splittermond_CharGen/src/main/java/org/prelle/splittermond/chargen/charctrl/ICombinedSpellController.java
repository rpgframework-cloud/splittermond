package org.prelle.splittermond.chargen.charctrl;

import java.util.UUID;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface ICombinedSpellController extends PartialController<Spell>, RecommendingController<Spell>, ComplexDataItemController<Spell, SpellValue> {

	public final static UUID DEC_SCHOOL = UUID.fromString("eca3fee0-388b-49c5-803f-fd3efa98b096");
}

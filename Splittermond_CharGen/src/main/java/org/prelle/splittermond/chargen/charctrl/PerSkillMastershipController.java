package org.prelle.splittermond.chargen.charctrl;

import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;

/**
 * 
 */
public interface PerSkillMastershipController extends ComplexDataItemController<Mastership, MastershipReference> {

}

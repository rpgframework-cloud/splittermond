package org.prelle.splittermond.chargen.companion;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.companion.proc.CompanionTool;
import org.prelle.splimo.creature.Companion;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class ALifeformController implements ILifeformController {

	protected final static Logger logger = System.getLogger(ALifeformController.class.getPackageName()+".main");

	protected Companion model;
	protected SpliMoCharacter charModel;

	private Collection<ControllerListener> listener;

	protected List<ProcessingStep> processChain;
	protected boolean dontProcess;
	private List<Modification> unitTestModifications;
	protected boolean allowRunProcessor = true;

	//-------------------------------------------------------------------
	/**
	 */
	public ALifeformController(SpliMoCharacter charModel, Companion model) {
		this.model = model;
		this.charModel = charModel;
		processChain = new ArrayList<>();
		unitTestModifications = new ArrayList<>();
		listener = new ArrayList<>();
		setupProcessChain();
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		unitTestModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		unitTestModifications.remove(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	@Override
	public Companion getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	@Override
	public void setModel(Companion data) {
		this.model = data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#addListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public void addListener(ControllerListener callback) {
		if (!listener.contains(callback)) {
			listener.add(callback);
		}
	}

	//-------------------------------------------------------------------
	@Override
	public void removeListener(ControllerListener callback) {
		listener.remove(callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#hasListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public boolean hasListener(ControllerListener callback) {
		return listener.contains(callback);
	}

	//-------------------------------------------------------------------
	@Override
	public Collection<ControllerListener> getListener() {
		return listener;
	}

	//-------------------------------------------------------------------
	@Override
	public void fireEvent(ControllerEvent type, Object...param) {
		logger.log(Level.WARNING, "########"+type+" to "+listener.size()+" listeners of "+this.getClass());
		if (listener.size()==0) {
			logger.log(Level.ERROR, "No listeners for lifeform controller - that can only be an error");
			System.err.println( "CharacterControllerImpl: No listeners for lifeform controller - that can only be an error");
		}
		for (ControllerListener callback : new ArrayList<>(listener)) {
			try {
				callback.handleControllerEvent(type, param);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error delivering generation event",e);
			}
		}
	}

	//-------------------------------------------------------------------
	@Override
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model==null)
			return ret;

		for (ProcessingStep step : processChain) {
			if (step instanceof PartialController) {
//				if (!((PartialController<?>)step).getToDos().isEmpty())
//					System.err.println("CharacterControllerImpl: Add "+((PartialController<?>)step).getToDos()+" from "+step);
				ret.addAll( ((PartialController<?>)step).getToDos());
			}
		}

		Collections.sort(ret, new Comparator<ToDoElement>() {
			public int compare(ToDoElement o1, ToDoElement o2) {
				return Integer.compare(o1.getSeverity().ordinal(), o2.getSeverity().ordinal());
			}
		});

		return ret;
	}

	//-------------------------------------------------------------------
	public void setupProcessChain() {};

	//-------------------------------------------------------------------
	public void setAllowRunProcessor(boolean value) {
		this.allowRunProcessor = value;
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		if (dontProcess || !allowRunProcessor)
			return;

		try {
			dontProcess = true;
			CompanionTool.recalculate(model);
			fireEvent(BasicControllerEvents.CREATURE_CHANGED, model);
		} finally {
			dontProcess = false;
		}
	}

}

package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.List;

import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.gen.free.CharGenSettings;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.chargen.GeneratorId;
import de.rpgframework.genericrpg.chargen.RuleInterpretation;
import de.rpgframework.genericrpg.data.RuleController;

/**
 * @author prelle
 *
 */
@GeneratorId("free")
public class UncontrolledGenerator extends SpliMoCharacterGeneratorImpl {

	//-------------------------------------------------------------------
	public UncontrolledGenerator(SpliMoCharacter model, CharacterHandle handle) {
		super(model, handle);
	}


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl#getWizardPages()
	 */
	@Override
	public List<PageType> getWizardPages() {
		return Arrays.asList(PageType.RACE,PageType.CULTURE,PageType.ATTRIBUTES);
	}

	//-------------------------------------------------------------------
	/**
	 */
	@Override
	protected void createPartialController() {
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#setModel(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void setModel(SpliMoCharacter model, CharacterHandle handle) {
		super.model = model;
		super.handle= handle;
		runProcessors();
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getId()
	 */
	@Override
	public String getId() {
		return "uncontrolled";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		return "Zügellos und unkontrolliert";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Bla Bla bla";
	}


	@Override
	protected void setupProcessChain() {
		// TODO Auto-generated method stub

	}


	@Override
	protected void initializeModel() {
		if (model.getCharGenSettings(Object.class) == null  || !(model.getCharGenSettings(Object.class) instanceof CharGenSettings) ) {
			if (model.getChargenSettingsJSON() != null  && (model.getCharGenSettings(Object.class) instanceof CharGenSettings)) {
				logger.log(Level.INFO, "Restore generator config from {0}", model.getChargenSettingsJSON());
				CharGenSettings settings = model.getCharGenSettings(CharGenSettings.class);
				model.setCharGenSettings(settings);
			} else {
				logger.log(Level.INFO, "Create new generator config");
				CharGenSettings settings = new CharGenSettings();
				model.setCharGenUsed(getId());
				model.setCharGenSettings(settings);
				model.setExpFree(50);
			}
		}
		ruleCtrl = new RuleController(model, SplitterMondCore.getItemList(RuleInterpretation.class), SplittermondRules.values());
	}

}

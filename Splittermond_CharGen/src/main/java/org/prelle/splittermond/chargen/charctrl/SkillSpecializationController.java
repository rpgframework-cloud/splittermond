package org.prelle.splittermond.chargen.charctrl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillSpecialization;
import org.prelle.splimo.SMSkillValue;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 *
 */
public class SkillSpecializationController extends ControllerImpl<SkillSpecialization<SMSkill>> implements
	ComplexDataItemController<SkillSpecialization<SMSkill>, SkillSpecializationValue<SMSkill>>,
	NumericalValueController<SkillSpecialization<SMSkill>, SkillSpecializationValue<SMSkill>>
	{

	private SMSkillValue sVal;

	//-------------------------------------------------------------------
	public SkillSpecializationController(SpliMoCharacterController parent, SMSkillValue data) {
		super(parent);
		this.sVal = data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		return unprocessed;
	}

	@Override
	public List<SkillSpecialization<SMSkill>> getAvailable() {
		List<SkillSpecialization<SMSkill>> ret = new ArrayList<>();
		ret.addAll(sVal.getSkill().getSpecializations().stream()
			// Not present yet
		    .filter(spec -> sVal.getSpecialization((SkillSpecialization<SMSkill>) spec)==null)
		    .map(spec -> (SkillSpecialization<SMSkill>) spec)
			.collect(Collectors.toList())
			);

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<SkillSpecializationValue<SMSkill>> getSelected() {
		return sVal.getSpecializations();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(SkillSpecialization<SMSkill> value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(SkillSpecializationValue<SMSkill> value) {
		return RecommendationState.NEUTRAL;
	}

	@Override
	public List<Choice> getChoicesToDecide(SkillSpecialization<SMSkill> value) {
		return List.of();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(SkillSpecialization<SMSkill> value, Decision... decisions) {
		return parent.getSkillController().canSelectSpecialization(sVal, value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.DataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> select(SkillSpecialization<SMSkill> value,
			Decision... decisions) {
		return parent.getSkillController().selectSpecialization(sVal, value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().canDeselectSpecialization(sVal, value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().deselectSpecialization(sVal, value).wasSuccessful();
	}

	@Override
	public float getSelectionCost(SkillSpecialization<SMSkill> data, Decision... decisions) {
		// TODO Auto-generated method stub
		return 7;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelectionCostString(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public String getSelectionCostString(SkillSpecialization<SMSkill> data) {
		return String.valueOf(getSelectionCost(data));
	}

	@Override
	public int getValue(SkillSpecializationValue<SMSkill> value) {
		return value.getDistributed();
	}

	@Override
	public Possible canBeIncreased(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().canBeIncreased(sVal, value);
	}

	@Override
	public Possible canBeDecreased(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().canBeDecreased(sVal, value);
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> increase(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().increase(sVal, value);
	}

	@Override
	public OperationResult<SkillSpecializationValue<SMSkill>> decrease(SkillSpecializationValue<SMSkill> value) {
		return parent.getSkillController().decrease(sVal, value);
	}

}

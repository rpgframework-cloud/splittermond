package org.prelle.splittermond.chargen.charctrl;

import java.util.List;

import org.prelle.splimo.Background;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * @author prelle
 *
 */
public interface BackgroundController extends PartialController<Background>, RecommendingController<Background> {
	
	//-------------------------------------------------------------------
	public void setAllowUnusualBackgrounds(boolean allow);
	public boolean isAllowUnusualBackgrounds();

	//-------------------------------------------------------------------
	public List<Background> getAvailable();
	
	//-------------------------------------------------------------------
	public void select(Background value);

}

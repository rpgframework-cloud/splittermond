package org.prelle.splittermond.chargen.gen;

import java.lang.System.Logger;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.chargen.GeneratorId;

/**
 * @author Stefan
 *
 */
@GeneratorId("assisting")
public class AssistingGenerator extends ModuleBasedGenerator {

	protected final static Logger logger = System.getLogger(AssistingGenerator.class.getPackageName());

//	//-------------------------------------------------------------------
//	public AssistingGenerator() {
//		super();
//	}

	//-------------------------------------------------------------------
	public AssistingGenerator(RuleSpecificCharacterObject model, CharacterHandle handle) {
		super(model, handle);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl#getWizardPages()
	 */
	@Override
	public List<PageType> getWizardPages() {
		List<PageType> ret = new ArrayList<PageType>(super.getWizardPages());
		ret.add(0, PageType.RECOMMENDER);
		return ret;
	}

}

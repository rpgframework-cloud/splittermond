package org.prelle.splittermond.chargen.gen;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.Rule.EffectOn;

/**
 * @author stefa
 *
 */
public interface SplittermondRules {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SplittermondRules.class, Locale.GERMAN);

	public static Rule CHARGEN_EXTREME_RESOURCES= new Rule(EffectOn.CHARGEN,"CHARGEN_EXTREME_RESOURCES", Rule.Type.BOOLEAN, RES, "false");
	public static Rule CAREER_PAY_GEAR          = new Rule(EffectOn.CAREER,"CAREER_PAY_GEAR", Rule.Type.BOOLEAN, RES, "true");
	public static Rule CAREER_UNDO_FROM_CAREER  = new Rule(EffectOn.CAREER,"CAREER_UNDO_FROM_CAREER", Rule.Type.BOOLEAN, RES, "true");
	public static Rule CAREER_UNDO_FROM_CHARGEN = new Rule(EffectOn.CAREER,"CAREER_UNDO_FROM_CHARGEN", Rule.Type.BOOLEAN, RES, "false");
	public static Rule CAREER_RESISTANCE_PER_LEVEL = new Rule(EffectOn.CAREER,"RESISTANCE_PER_LEVEL", Rule.Type.INTEGER, RES, "2");
	public static Rule CAREER_LEVEL2_EXP = new Rule(EffectOn.CAREER,"LEVEL2_EXP", Rule.Type.INTEGER, RES, "100");
	public static Rule CAREER_LEVEL3_EXP = new Rule(EffectOn.CAREER,"LEVEL3_EXP", Rule.Type.INTEGER, RES, "300");
	public static Rule CAREER_LEVEL4_EXP = new Rule(EffectOn.CAREER,"LEVEL4_EXP", Rule.Type.INTEGER, RES, "600");


	//-------------------------------------------------------------------
	public static Rule[] values() {
		return new Rule[] {
				CHARGEN_EXTREME_RESOURCES,
				CAREER_RESISTANCE_PER_LEVEL,
				CAREER_LEVEL2_EXP,
				CAREER_LEVEL3_EXP,
				CAREER_LEVEL4_EXP,
				CAREER_PAY_GEAR,
				CAREER_UNDO_FROM_CAREER,
				CAREER_UNDO_FROM_CHARGEN
		};
	}

	//-------------------------------------------------------------------
	static Rule getRule(String id) {
		for (Rule tmp : values()) {
			if (tmp.getID().equals(id))
				return tmp;
		}
		return null;
	}
}

/**
 * @author Stefan Prelle
 *
 */
open module splittermond.chargen {
	exports org.prelle.splittermond.chargen.ai;
	exports org.prelle.splittermond.chargen.charctrl;
	exports org.prelle.splittermond.chargen.common;
	exports org.prelle.splittermond.chargen.gen;
	exports org.prelle.splittermond.chargen.gen.free;
	exports org.prelle.splittermond.chargen.companion;
	exports org.prelle.splittermond.chargen.companion.summon;

	requires transitive de.rpgframework.core;
	requires transitive splittermond.core;
	requires simple.persist;
	requires java.prefs;
	requires de.rpgframework.rules;

}
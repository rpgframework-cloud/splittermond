package org.prelle.splittermond.chargen.creature;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.simplepersist.Persister;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Race;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.companion.proc.CompanionTool;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.CreatureModule;
import org.prelle.splittermond.chargen.companion.entourage.EntourageController;
import org.prelle.splittermond.chargen.companion.summon.SummonableController;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.data.Decision;

/**
 * @author prelle
 *
 */
public class CompanionTest {

	private static Persister serializer;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void loadDataTest() {
		System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.GERMANY);
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init( );

		serializer   = new Persister();
	}

	//-------------------------------------------------------------------
	private static byte[] encode(Companion data) {
		try {
			StringWriter out = new StringWriter();
			serializer.write(data, out);
			return out.toString().getBytes(Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			StringWriter mess = new StringWriter();
			mess.append("Failed saving character\n\n");
			e.printStackTrace(new PrintWriter(mess));
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, mess.toString());
		}
		return null;
	}

	//-------------------------------------------------------------------
	@Test
	public void testSchildMaid() {
		Companion result = createSchildmaid();
		byte[] raw = encode(result);
		String xml = new String(raw);
		System.out.println(xml);
		
		CompanionTool.recalculate(result);
		System.out.println(result.dump());
	}

	//-------------------------------------------------------------------
	private Companion createSchildmaid() {
		ResourceValue rsrc = new ResourceValue(SplitterMondCore.getItem(Resource.class, "entourage"), 2);
		EntourageController creatGen = new EntourageController(null,rsrc);
		creatGen.selectRace(SplitterMondCore.getItem(Race.class, "human"), new Decision(UUID.fromString("b52197f0-d051-4ae6-a3b0-d02649285bec"),"AGILITY,STRENGTH"));
		creatGen.selectCulture(SplitterMondCore.getItem(Culture.class, "zwingard"));
		creatGen.selectBackground(SplitterMondCore.getItem(Background.class, "warrior"));

		Companion result = creatGen.getModel();
		result.setName("Greta");
		return result;
	}

	//-------------------------------------------------------------------
	public Companion createCarimeasPferd(SpliMoCharacter charModel) {
		SummonableController creatGen = new SummonableController(charModel,10);
		creatGen.selectBody(SplitterMondCore.getItem(CreatureModule.class, "large_summon"));
		creatGen.selectType(SplitterMondCore.getItem(CreatureModule.class, "fairy_summon"));
		creatGen.addModule(SplitterMondCore.getItem(CreatureModule.class, "fire_summon"),
				new Decision(UUID.fromString("ecc9e37a-7bb7-45ab-b9b2-f4f842018fb7"), "0579e605-5eb8-4a66-8cc9-4d1b2f62a55f"),
				new Decision(UUID.fromString("b95e174c-3ce8-4988-b7f9-04b0f7a39e58"), "firemagic:heat"));
		creatGen.addModule(SplitterMondCore.getItem(CreatureModule.class, "fast_summon"));
		creatGen.addModule(SplitterMondCore.getItem(CreatureModule.class, "flying_summon"));
		creatGen.addModule(SplitterMondCore.getItem(CreatureModule.class, "magnificent_summon"));
		creatGen.addModule(SplitterMondCore.getItem(CreatureModule.class, "helper_summon"));

//		ServicesController services = creatGen.getServicesController();
//		for (ServiceValue val : services.getAvailable()) {
//			if (val.getKey().startsWith("transport"))
//				services.select(val);
//		}
//		services.markAsBaseService( services.getSelected().get(0));

		Companion result = creatGen.getModel();
		return result;
	}

	//-------------------------------------------------------------------
	@Test
	public void testCarimeasPferd() {
		Companion result = createCarimeasPferd(null);
		byte[] raw = encode(result);
		String xml = new String(raw);
		System.out.println(xml);
		
		CompanionTool.recalculate(result);
		System.out.println(result.dump());
	}

}

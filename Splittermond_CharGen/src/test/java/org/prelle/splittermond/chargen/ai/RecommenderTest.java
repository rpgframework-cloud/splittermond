/**
 * 
 */
package org.prelle.splittermond.chargen.ai;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.modifications.SplittermondReference;

import de.rpgframework.genericrpg.chargen.ai.Weight;
import de.rpgframework.genericrpg.modification.RecommendationModification;

/**
 * @author prelle
 *
 */
public class RecommenderTest {
	
	private SpliMoCharacter model;
	private RecommenderWithoutModel ai;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void setupClass() {
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init();
	}

	//-------------------------------------------------------------------
	@BeforeEach
	public void setup() {
		model = new SpliMoCharacter();
		ai = new RecommenderWithoutModel();
	}

	//-------------------------------------------------------------------
	@Test
	public void oneTest() {
		ai.addWeighedSkill(new RecommendationModification(SplittermondReference.SKILL, "history", Weight.GOOD));
		
		assertTrue(ai.isRecommended(Attribute.MYSTIC));
		assertTrue(ai.isRecommended(Attribute.MIND));
		assertFalse(ai.isRecommended(Attribute.AGILITY));
		
		assertTrue(ai.isRecommended(SplitterMondCore.getSkill("history")));
		assertFalse(ai.isRecommended(SplitterMondCore.getSkill("blades")));
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testChoice1() {
//		WeighedSkill profile1 = new WeighedSkill(SplitterMondCore.getSkill("fatemagic"), AILevel.MASTER);
//		WeighedSkill profile2 = new WeighedSkill(SplitterMondCore.getSkill("deathmagic"), AILevel.GOOD);
//		ai.addWeighedSkill(profile1);
//		ai.addWeighedSkill(profile2);
//		ai.update();
//		
//		assertEquals(15, ai.getRecommendationValueIntern(SplitterMondCore.getItem(Education.class,("soulguide")).getModifications()));
//	}
//
//	//-------------------------------------------------------------------
//	@Test
//	public void combatSkillTest() {
//		WeighedSkill profile = new WeighedSkill(SplitterMondCore.getSkill("blades"), AILevel.GOOD);
//		ai.addWeighedSkill(profile);
//		
//		assertTrue(ai.isRecommended(Attribute.AGILITY));
//		assertTrue(ai.isRecommended(Attribute.STRENGTH));
//		assertTrue(ai.isRecommended(Attribute.INTUITION));
//		assertFalse(ai.isRecommended(Attribute.MIND));
//		
//		assertTrue(ai.isRecommended(SplitterMondCore.getSkill("blades")));
//		assertFalse(ai.isRecommended(SplitterMondCore.getSkill("history")));
//		
//		System.out.println(ai.dump());
//	}

	//-------------------------------------------------------------------
	@Test
	public void testHealer() {
		ai.addWeighedSkills(
				new RecommendationModification(SplittermondReference.SKILL, "healmagic", Weight.GOOD),
				new RecommendationModification(SplittermondReference.SKILL, "heal", Weight.MASTER),
				new RecommendationModification(SplittermondReference.SKILL, "alchemy", Weight.GOOD),
				new RecommendationModification(SplittermondReference.SKILL, "nature", Weight.GOOD)
				);
		ai.update();
		
		System.out.println(ai.dump());
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testAgileFighter() {
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("blades"), AILevel.MASTER));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("acrobatics"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("protectionmagic"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("motionmagic"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("combatmagic"), AILevel.GOOD));
//		ai.update();
//		
//		System.out.println(ai.dump());
//	}
//
//	//-------------------------------------------------------------------
//	@Test
//	public void testDeathPriest() {
//		model.setRace("human");
//		model.setCulture(SplitterMondCore.getItem(Culture.class,"fedirin").getId());
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("deathmagic"), AILevel.MASTER));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("arcanelore"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("fatemagic"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("determination"), AILevel.GOOD));
//		ai.addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("history"), AILevel.GOOD));
//		ai.update();
//		
//		System.out.println(ai.dump());
//	}


}

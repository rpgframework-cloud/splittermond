/**
 *
 */
package org.prelle.splittermond.chargen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Education;
import org.prelle.splimo.Power;
import org.prelle.splimo.Race;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splittermond.chargen.ai.RecommenderWithoutModel;
import org.prelle.splittermond.chargen.ai.SpliMoAITool;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SkillController;
import org.prelle.splittermond.chargen.gen.ModuleBasedGenerator;
import org.prelle.splittermond.chargen.gen.PowerGenerator;
import org.prelle.splittermond.chargen.gen.ResourceGenerator;
import org.prelle.splittermond.chargen.gen.SkillGenerator;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.ModificationChoice;

/**
 * @author prelle
 *
 */
public class RollEverythingTest {

	private SpliMoCharacter model;
	private SpliMoCharacterGeneratorImpl charGen;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void setupClass() {
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init();
		SpliMoAITool.initialize();
	}

	//-------------------------------------------------------------------
	@BeforeEach
	public void setup() {
		model = new SpliMoCharacter();
		charGen = new ModuleBasedGenerator(model, null);
		System.err.println("------> "+charGen.getRecommender());
		
	}

	//-------------------------------------------------------------------
	private void tryIncreaseTo(Attribute attr, int value) {
		AttributeController ctrlAttr = charGen.getAttributeController();
		AttributeValue<Attribute> val = model.getAttribute(attr);
		while (val.getDistributed()<value) {
			assertTrue(ctrlAttr.canBeIncreased(val).get());
			assertTrue(ctrlAttr.increase(val).wasSuccessful());
		}
		assertEquals(value, val.getDistributed());
	}

	//-------------------------------------------------------------------
	@Test
	public void testAgileFighter() {
		assertTrue(charGen.getRecommender().isPresent());
		LevellingProfile prof = SplitterMondCore.getItem(LevellingProfile.class, "agileFighter");
		assertNotNull(prof);
		charGen.getProfileController().select(prof);
//		System.exit(1);
		
		charGen.getRaceController().roll();
		assertNotNull(model.getRace());
		charGen.getCultureController().roll();
		assertNotNull(model.getCulture());
		assertTrue(charGen.getCultureController().getToDos().isEmpty());

		charGen.getBackgroundController().roll();
		assertNotNull(model.getBackground());
		assertTrue(charGen.getBackgroundController().getToDos().isEmpty());

		charGen.getEducationController().roll();
		assertNotNull(model.getEducation());
		assertTrue(charGen.getEducationController().getToDos().isEmpty());

		charGen.getAttributeController().roll();
		assertTrue(charGen.getAttributeController().getToDos().isEmpty());
	
//		model.setName("Tiai Schimmersee");
//		model.setGender(Gender.FEMALE);

		charGen.finish();

		byte[] raw = SplitterMondCore.save(model);
		String xml = new String(raw);
		System.out.println(xml);
	}

}

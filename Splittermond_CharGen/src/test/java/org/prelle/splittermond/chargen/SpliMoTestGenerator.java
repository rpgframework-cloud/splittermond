package org.prelle.splittermond.chargen;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.ai.ProfileControllerImpl;
import org.prelle.splittermond.chargen.ai.RecommenderWithoutModel;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.ICombinedSpellController;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.charctrl.IPerSkillSpellController;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SkillController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator;
import org.prelle.splittermond.chargen.gen.SkillGenerator;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl.PageType;
import org.prelle.splittermond.chargen.gen.ToolsOfTheTradeGenerator;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.IRecommender;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.RuleController;

/**
 *
 */
public class SpliMoTestGenerator implements SpliMoCharacterGenerator {

	private SpliMoCharacter model;
	private RuleController ruleCtrl;

	public SkillGenerator skills;

	//-------------------------------------------------------------------
	/**
	 */
	public SpliMoTestGenerator(SpliMoCharacter model) {
		this.model = model;
		skills = new SkillGenerator(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getProfileController()
	 */
	@Override
	public ProfileControllerImpl getProfileController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getPowerController()
	 */
	@Override
	public PowerController getPowerController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getResourceController()
	 */
	@Override
	public ResourceController getResourceController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController()
	 */
	@Override
	public ICombinedSpellController getSpellController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController#getSpellController()
	 */
	@Override
	public IPerSkillSpellController getSpellController(SMSkill school) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getModel()
	 */
	@Override
	public SpliMoCharacter getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#setModel(de.rpgframework.character.RuleSpecificCharacterObject)
	 */
	@Override
	public void setModel(SpliMoCharacter data) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getLocale()
	 */
	@Override
	public Locale getLocale() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#addListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public void addListener(ControllerListener callback) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#removeListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public void removeListener(ControllerListener callback) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#hasListener(de.rpgframework.genericrpg.chargen.ControllerListener)
	 */
	@Override
	public boolean hasListener(ControllerListener callback) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getListener()
	 */
	@Override
	public Collection<ControllerListener> getListener() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#fireEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void fireEvent(ControllerEvent type, Object... param) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#setAllowRunProcessor(boolean)
	 */
	@Override
	public void setAllowRunProcessor(boolean value) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#runProcessors()
	 */
	@Override
	public void runProcessors() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#save(byte[])
	 */
	@Override
	public boolean save(byte[] data) throws IOException, CharacterIOException {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getRuleController()
	 */
	@Override
	public RuleController getRuleController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterController#getRecommendingControllerFor(java.lang.Object)
	 */
	@Override
	public <T> RecommendingController<T> getRecommendingControllerFor(T item) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getId()
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#getDescription()
	 */
	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#setModel(de.rpgframework.character.RuleSpecificCharacterObject, de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void setModel(SpliMoCharacter model, CharacterHandle handle) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterGenerator#finish()
	 */
	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getRaceController()
	 */
	@Override
	public RaceController getRaceController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getCultureController()
	 */
	@Override
	public CultureController getCultureController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getBackgroundController()
	 */
	@Override
	public BackgroundController getBackgroundController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getEducationController()
	 */
	@Override
	public EducationController getEducationController() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.charctrl.SpliMoCharacterGenerator#getWizardPages()
	 */
	@Override
	public List<PageType> getWizardPages() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<IRecommender<Attribute>> getRecommender() {
		// TODO Auto-generated method stub
		return Optional.of(new RecommenderWithoutModel());
	}

	@Override
	public ToolsOfTheTradeGenerator getToolsOfTheTradeController() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEquipmentController getEquipmentController() {
		// TODO Auto-generated method stub
		return null;
	}

}

package org.prelle.splittermond.chargen;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterControllerImpl;
import org.prelle.splittermond.chargen.gen.SkillGenerator;

/**
 * 
 */
class SkillGeneratorTest {
	
	private SpliMoCharacter model;
	private SpliMoCharacterController charGen;
	private SkillGenerator skillGen;

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		model   = new SpliMoCharacter();
		charGen = new SpliMoTestGenerator(model);
		skillGen = new SkillGenerator(charGen);
		((SpliMoTestGenerator)charGen).skills = skillGen;
	}

	/**
	 * Test method for {@link org.prelle.splittermond.chargen.gen.SkillGenerator#selectSpecialization(org.prelle.splimo.SMSkillValue, de.rpgframework.genericrpg.data.SkillSpecialization)}.
	 */
	@Test
	void testSelectSpecialization() {
		fail("Not yet implemented");
	}

}

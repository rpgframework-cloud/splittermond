package org.prelle.splittermond.chargen;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.charctrl.ICarriedItemController;
import org.prelle.splittermond.chargen.common.CommonCarriedItemController;

import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;

/**
 *
 */
class CarriedItemCtrlTest {

	private CarriedItem<ItemTemplate> model;
	private EnhancementController enhCtrl;
	private ICarriedItemController carriedCtrl;

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init();
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		model   = new CarriedItem<ItemTemplate>();
		enhCtrl = new EnhancementController(null, model);
		carriedCtrl = new CommonCarriedItemController(model, enhCtrl);
	}

	//-------------------------------------------------------------------
	@Test
	void test1() {
		model.setResolved(SplitterMondCore.getItem(ItemTemplate.class, "peacockfeather"));
		SMGearTool.recalculate("", null, model);

		assertEquals(4,model.getAsValue(SMItemAttribute.LOAD).getModifiedValue());

		ItemEnhancementValue eVal = new ItemEnhancementValue<AItemEnhancement>(SplitterMondCore.getItem(Enhancement.class, "load"));
		eVal.addDecision(new Decision(ItemTemplate.UUID_RATING, "2"));
		model.addEnhancement(eVal);
		SMGearTool.recalculate("", null, model);
		assertEquals(2,model.getAsValue(SMItemAttribute.LOAD).getModifiedValue());
	}

}

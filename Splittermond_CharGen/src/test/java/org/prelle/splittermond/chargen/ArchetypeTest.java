/**
 *
 */
package org.prelle.splittermond.chargen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Background;
import org.prelle.splimo.Culture;
import org.prelle.splimo.Education;
import org.prelle.splimo.Language;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Moonsign;
import org.prelle.splimo.Power;
import org.prelle.splimo.Race;
import org.prelle.splimo.Resource;
import org.prelle.splimo.ResourceValue;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SpliMoCharacter;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Companion;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.items.Enhancement;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.modifications.SplittermondReference;
import org.prelle.splittermond.chargen.ai.RecommenderWithoutModel;
import org.prelle.splittermond.chargen.ai.SpliMoAITool;
import org.prelle.splittermond.chargen.charctrl.AttributeController;
import org.prelle.splittermond.chargen.charctrl.BackgroundController;
import org.prelle.splittermond.chargen.charctrl.CultureController;
import org.prelle.splittermond.chargen.charctrl.EducationController;
import org.prelle.splittermond.chargen.charctrl.EnhancementController;
import org.prelle.splittermond.chargen.charctrl.IEquipmentController;
import org.prelle.splittermond.chargen.charctrl.PerSkillMastershipController;
import org.prelle.splittermond.chargen.charctrl.PowerController;
import org.prelle.splittermond.chargen.charctrl.RaceController;
import org.prelle.splittermond.chargen.charctrl.ResourceController;
import org.prelle.splittermond.chargen.charctrl.SkillController;
import org.prelle.splittermond.chargen.charctrl.SpliMoCharacterController;
import org.prelle.splittermond.chargen.common.APerSkillMastershipController;
import org.prelle.splittermond.chargen.companion.creature.StandardCreatureController;
import org.prelle.splittermond.chargen.companion.summon.SummonableController;
import org.prelle.splittermond.chargen.creature.CompanionTest;
import org.prelle.splittermond.chargen.gen.ModuleBasedGenerator;
import org.prelle.splittermond.chargen.gen.PowerGenerator;
import org.prelle.splittermond.chargen.gen.ResourceGenerator;
import org.prelle.splittermond.chargen.gen.SkillGenerator;
import org.prelle.splittermond.chargen.gen.SpliMoCharacterGeneratorImpl;
import org.prelle.splittermond.chargen.gen.ToolsOfTheTradeGenerator;

import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ArchetypeTest {

	private SpliMoCharacter model;
	private SpliMoCharacterGeneratorImpl charGen;

	private RecommenderWithoutModel recommender;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void setupClass() {
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init();
//		SpliMoConfigOptions.attachConfigurationTree(new ConfigContainerImpl(Preferences.userNodeForPackage(ArchetypeTest.class), "unittest"));
		SpliMoAITool.initialize();
	}

	//-------------------------------------------------------------------
	@BeforeEach
	public void setup() {
		model = new SpliMoCharacter();
		charGen = new ModuleBasedGenerator(model, null);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCultureOptions() {
		charGen.runProcessors();
		// Race
		RaceController ctrlRace = charGen.getRaceController();
		Race race = SplitterMondCore.getItem(Race.class,"alben");
		ctrlRace.select(race);

		// Culture
		CultureController ctrlCult = charGen.getCultureController();
		// Check option for unusual cultures
		Culture furgand = SplitterMondCore.getItem(Culture.class,"furgand");
		Culture ashur   = SplitterMondCore.getItem(Culture.class,"ashurmazaan");
		Culture seealb  = SplitterMondCore.getItem(Culture.class,"albseebund");
		ctrlCult.setAllowUnusualCultures(false);
		assertFalse(ctrlCult.getAvailable().contains(furgand), "Hiding unusual cultures does not work");
		assertTrue(ctrlCult.getAvailable().contains(ashur)   , "Hiding unusual cultures hides those without req");
		assertTrue(ctrlCult.getAvailable().contains(seealb)  , "Hiding unusual cultures hides allowed");
		ctrlCult.setAllowUnusualCultures(true);
		assertTrue(ctrlCult.getAvailable().contains(furgand), "Showing unusual cultures does not work");
		assertTrue(ctrlCult.getAvailable().contains(ashur)  , "Showing unusual cultures hides those without req");
		assertTrue(ctrlCult.getAvailable().contains(seealb) , "Showing unusual cultures hides allowed");
	}

	//-------------------------------------------------------------------
	private void tryIncreaseTo(Attribute attr, int value) {
		AttributeController ctrlAttr = charGen.getAttributeController();
		AttributeValue<Attribute> val = model.getAttribute(attr);
		while (val.getDistributed()<value) {
			assertTrue(ctrlAttr.canBeIncreased(val).get());
			assertTrue(ctrlAttr.increase(val).wasSuccessful());
		}
		assertEquals(value, val.getDistributed());
	}

	//-------------------------------------------------------------------
	@Test
	public void testTiai() {
		LevellingProfile prof = SplitterMondCore.getItem(LevellingProfile.class, "agileFighter");
		assertNotNull(prof);
		charGen.getProfileController().select(prof);
		//charGen.getRecommender().addWeighedSkill(new WeighedSkill(SplitterMondCore.getSkill("blades"), AILevel.VERY_GOOD));
		charGen.runProcessors();
		// From 18 possible, each of the 8 attributes must have at least 1 assigned
		assertEquals(10, charGen.getAttributeController().getPointsLeft());
//		assertEquals(55, charGen.getSkillController().getPointsLeft());
		PowerController power = charGen.getPowerController();
		assertEquals(5, ((PowerGenerator)power).getPointsLeft()); // 1 Kultur, 2 Ausbildung, 3 frei - Gesellig von Auto-Selenia
		// Race
		RaceController ctrlRace = charGen.getRaceController();
		Race race = SplitterMondCore.getItem(Race.class,"alben");
		ctrlRace.select(race);
		assertEquals(5, ((PowerGenerator)power).getPointsLeft()); // 1 Kultur, 2 Ausbildung, 3 frei - 1 von Seenbund
		assertEquals(1, ctrlRace.getChoiceUUIDs().size());
		Choice decide = race.getChoice( ctrlRace.getChoiceUUIDs().get(0) );
		String chosen = (decide.getChoiceOptions()==null)?Attribute.AGILITY.name():decide.getChoiceOptions()[0];
		ctrlRace.decide(race, decide.getUUID(), new Decision(decide, chosen));
		assertTrue(ctrlRace.getToDos().isEmpty());
		assertTrue(model.hasDecisionBeenMade(UUID.fromString("b52197f0-d051-4ae6-a3b0-d02649285bec")));
		assertEquals(5, ((PowerGenerator)power).getPointsLeft()); // 1 Kultur, 2 Ausbildung, 3 frei - Gesellig von Auto-Selenia
		charGen.runProcessors();

		// Culture
		CultureController ctrlCult = charGen.getCultureController();
		Culture cult = SplitterMondCore.getItem(Culture.class,"albseebund");
		ctrlCult.select(cult);
		assertTrue( model.getLanguages().stream().map(Language::getId).anyMatch(l -> "vintial".equals(l)));
		assertTrue(model.hasDecisionBeenMade(UUID.fromString("b52197f0-d051-4ae6-a3b0-d02649285bec")));
		assertEquals(43, charGen.getSkillController().getPointsLeft());
		assertEquals(4, ctrlCult.getChoiceUUIDs().size());
		assertEquals(5, ((PowerGenerator)power).getPointsLeft()); // Noch 2 Ausbildung, 3 frei

		// Kettenwaffen
		Choice choice = cult.getChoice( ctrlCult.getChoiceUUIDs().get(0) );
		ctrlCult.decide(cult, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[0]));
		assertEquals(42, charGen.getSkillController().getPointsLeft());
		// Darbietung
		choice = cult.getChoice( ctrlCult.getChoiceUUIDs().get(1) );
		ctrlCult.decide(cult, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[0]));
		assertEquals(41, charGen.getSkillController().getPointsLeft());
		// Wassermagie
		choice = cult.getChoice( ctrlCult.getChoiceUUIDs().get(2) );
		ctrlCult.decide(cult, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[1]));
		assertEquals(40, charGen.getSkillController().getPointsLeft());
		// Beine des Seemanns
		ModificationChoice mChoice = ctrlCult.getAsModificationChoice(cult, ctrlCult.getChoiceUUIDs().get(3));
		ctrlCult.decide(cult, mChoice.getUUID(), new Decision(mChoice.getUUID(), ((DataItemModification)mChoice.getModificiations().get(1)).getId()));
		assertEquals(40, charGen.getSkillController().getPointsLeft());
		assertTrue(ctrlCult.getToDos().isEmpty(), ctrlCult.getToDos()+"");

		// Abstammung
		BackgroundController ctrlBack = charGen.getBackgroundController();
		assertNotNull(ctrlBack, "Missing background controll");
		Background back = SplitterMondCore.getBackground("merchant");
		assertNotNull(back, "Text background not loaded");
		ctrlBack.select(back);
		assertEquals(36, charGen.getSkillController().getPointsLeft());
//		assertEquals(3, ctrlBack.getDecisionsToMake().size());
		// Seefahrt
		choice = back.getChoice( ctrlBack.getChoiceUUIDs().get(2) );
		ctrlBack.decide(back, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[1]));
		assertEquals(35, charGen.getSkillController().getPointsLeft());
		// Vermögen vs. Kontakte
		choice = back.getChoices().get(0);
		ctrlBack.decide(back, choice.getUUID(), new Decision(choice.getUUID(), "wealth:2,contacts:1"));
//		decide.setDecision(Arrays.asList(new ResourceModification(SplitterMondCore.getResource("wealth"), 2), new ResourceModification(SplitterMondCore.getResource("contacts"), 1)));
		// Stand
		choice = back.getChoices().get(1);
		ctrlBack.decide(back, choice.getUUID(), new Decision(choice.getUUID(), "status"));
		assertTrue(ctrlBack.getToDos().isEmpty());

		// Ausbildung
		assertEquals(35, charGen.getSkillController().getPointsLeft());
		EducationController ctrlEduc = charGen.getEducationController();
		Education edu = SplitterMondCore.getItem(Education.class, "bladedancer");
		assertNotNull(edu);
		ctrlEduc.select(edu);
		assertEquals(8, ctrlEduc.getChoiceUUIDs().size());
		assertEquals(20, charGen.getSkillController().getPointsLeft());
		// Stand
		choice = edu.getChoices().get(0);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "status"));
		// Mentor
		choice = edu.getChoices().get(1);
//		ctrlEduc.decide(edu, choice, new Decision(choice.getUUID(), "status"));
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[1]));
		// Kettenwaffen +3,  Klingenwaffen +3
		choice = edu.getChoices().get(2);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "blades:3,chains:3"));
		assertEquals(14, charGen.getSkillController().getPointsLeft());
		// Heilkunde
		choice = edu.getChoices().get(3);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "heal"));
		assertEquals(13, charGen.getSkillController().getPointsLeft());
		// Stärkungsmagie
		choice = edu.getChoices().get(4);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "enhancemagic:3,protectionmagic:2"));
		assertEquals(8, charGen.getSkillController().getPointsLeft());
		// Wassermagie
		choice = edu.getChoices().get(5);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "watermagic"));
		assertEquals(5, charGen.getSkillController().getPointsLeft());
		// Kind der Wellen
		choice = edu.getChoices().get(6);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), choice.getChoiceOptions()[0]));
		// Umreißen in Kettenwaffen
		choice = edu.getChoices().get(7);
		ctrlEduc.decide(edu, choice.getUUID(), new Decision(choice.getUUID(), "chains/knock"));

		assertEquals(5, charGen.getSkillController().getPointsLeft());
		assertTrue(ctrlEduc.getToDos().isEmpty());
		assertEquals(3, ((PowerGenerator)power).getPointsLeft()); // Noch 3 frei

		// Schritt 6: Die Attribute
		AttributeController ctrlAttr = charGen.getAttributeController();
		assertEquals(10, ctrlAttr.getPointsLeft());
		assertTrue(ctrlAttr.canBeIncreased(model.getAttribute(Attribute.AGILITY)).get());
		tryIncreaseTo(Attribute.AGILITY, 3);
		tryIncreaseTo(Attribute.MYSTIC, 3);
		tryIncreaseTo(Attribute.CONSTITUTION, 2);
		tryIncreaseTo(Attribute.CHARISMA, 2);
		tryIncreaseTo(Attribute.INTUITION, 2);
		tryIncreaseTo(Attribute.STRENGTH, 2);
		tryIncreaseTo(Attribute.MIND, 2);
		tryIncreaseTo(Attribute.WILLPOWER, 2);
		assertEquals(0, ctrlAttr.getPointsLeft());
		assertEquals(2, model.getAttribute(Attribute.CHARISMA).getDistributed());
		assertEquals(3, model.getAttribute(Attribute.CHARISMA).getModifiedValue()); // Bonus from Alb
		assertEquals(5, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.CONSTITUTION).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.MYSTIC).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.MIND).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertTrue(ctrlAttr.getToDos().isEmpty(), ctrlAttr.getToDos()+"");

		// Schritt 7:Stärken, Ressourcen und freie Fertigkeitspunkte
		ResourceController resources = charGen.getResourceController();
		assertEquals(2, ((ResourceGenerator)resources).getPointsLeft());
		OperationResult<ResourceValue> res = resources.select(SplitterMondCore.getItem(Resource.class, "creature"));
		assertTrue( res.wasSuccessful() );
		assertTrue( resources.increase(res.get()).wasSuccessful() );
		assertEquals(0, ((ResourceGenerator)resources).getPointsLeft());
		assertTrue(resources.getToDos().isEmpty(), resources.getToDos()+"");

		assertEquals(3, ((PowerGenerator)power).getPointsLeft());
		assertTrue( power.select(SplitterMondCore.getItem(Power.class, "animalfamiliar")).wasSuccessful() );
		assertEquals(2, ((PowerGenerator)power).getPointsLeft());
		assertTrue( power.select(SplitterMondCore.getItem(Power.class, "focuspool")).wasSuccessful() );
		assertEquals(0, ((PowerGenerator)power).getPointsLeft());

		/* Die 5 Fertigkeitspunkte wendet sie auf, um Kettenwaffen, Akrobatik und Arkane Kunde jeweils auf den Höchstwert von 6 zu steigern. */
		SkillController skills = charGen.getSkillController();
		assertEquals(5, ((SkillGenerator)skills).getPointsLeft());
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("chains")) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("chains")) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("acrobatics")) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("acrobatics")) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("arcanelore")) ).wasSuccessful() );
		assertEquals(0, ((SkillGenerator)skills).getPointsLeft());

//		// Schritt 8: Mondsplitter und Schwächen
		model.setMoonsign(SplitterMondCore.getItem(Moonsign.class, "flash"));
		model.addWeakness("Stolz");
		model.addWeakness("Ehrgeizig");

		// Schritt 9: Start-Erfahrung
		assertEquals(0, ((SkillGenerator)skills).getPointsLeft());
		assertEquals(15, model.getExpFree());
		/* Sie verwendet 6 Punkte, um Wassermagie von 4 auf 6 zu steigern,  */
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("watermagic")) ).wasSuccessful() );
		assertEquals(0, ((SkillGenerator)skills).getPointsLeft());
		assertEquals(12, model.getExpFree());
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("watermagic")) ).wasSuccessful() );
		assertEquals(9, model.getExpFree());
		/* und 3 Punkte, um Schutzmagie von 2 auf 3 zu steigern. */
		assertTrue( skills.increase( model.getSkillValue(SplitterMondCore.getSkill("protectionmagic")) ).wasSuccessful() );
		assertEquals(6, model.getExpFree());
		/* Außerdem gibt sie 5 Punkte aus, um sich eine zusätzliche Meisterschaft zu holen (Akrobatik: Blitzreflexe) */
		SMSkill acrobatics = SplitterMondCore.getSkill("acrobatics");
		PerSkillMastershipController masterCtrl = charGen.getSkillController().getMastershipController(model.getSkillValue(acrobatics));
		masterCtrl.select(SplitterMondCore.getItem(Mastership.class, "acrobatics/flashreflexes"));
		/*Den letzten Punkt wendet sie auf, um sich einen weiteren Zauber des Grades 0 zu sichern, auch wenn sie noch nicht genau weiß, welchen. */

		// Schritt 10: Feinschliff
		/* Um die Möglichkeit eines kleinen Wasserwesens als Begleiter wahrzunehmen, streicht sie die Kontakte und 1 Punkt von ihrem Stand und erhöht stattdessen ihre Kreatur um 2 Punkte. */
		assertEquals(0, ((ResourceGenerator)resources).getPointsLeft());
		assertTrue( resources.canBeDecreased(model.getResource("contacts")).get() );
		assertTrue( resources.decrease(model.getResource("contacts")).wasSuccessful() );
		assertEquals(1, ((ResourceGenerator)resources).getPointsLeft());
		assertTrue( resources.decrease(model.getResource("status")).wasSuccessful() );
		assertEquals(2, ((ResourceGenerator)resources).getPointsLeft());
		assertTrue( resources.increase(model.getResource("creature")).wasSuccessful() );
		assertTrue( resources.increase(model.getResource("creature")).wasSuccessful() );
		assertEquals(0, ((ResourceGenerator)resources).getPointsLeft());

		/* Nachdem sie sich die Zauber angesehen hat, fällt ihr auf, dass
			Stärkungsmagie nicht so recht zu ihrer Vorstellung passen will.
			Sie ersetzt die 3 Punkte Stärkungsmagie daher durch 3 Punkte Kampfmagie. */
		SMSkill enhance = SplitterMondCore.getSkill("enhancemagic");
		SMSkill combat  = SplitterMondCore.getSkill("combatmagic");
		assertTrue( skills.decrease( model.getSkillValue(enhance) ).wasSuccessful() );
		assertTrue( skills.decrease( model.getSkillValue(enhance) ).wasSuccessful() );
		assertTrue( skills.decrease( model.getSkillValue(enhance) ).wasSuccessful() );
		assertEquals(0, ((SkillGenerator)skills).getPointsLeft());
		assertEquals(15, model.getExpFree());
		assertTrue( skills.increase( model.getSkillValue(combat) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(combat) ).wasSuccessful() );
		assertTrue( skills.increase( model.getSkillValue(combat) ).wasSuccessful() );
		assertEquals(0, ((SkillGenerator)skills).getPointsLeft());
		assertEquals(6, model.getExpFree());

		charGen.getSkillController().getMastershipController(model.getSkillValue("chains")).select(SplitterMondCore.getItem(Mastership.class, "chains/ignoreshield"));
		charGen.getSkillController().getMastershipController(model.getSkillValue("acrobatics")).select(SplitterMondCore.getItem(Mastership.class, "acrobatics/evade1"));
		charGen.getSkillController().getMastershipController(model.getSkillValue("arcanelore")).select(SplitterMondCore.getItem(Mastership.class, "arcanelore/arcanedefense1"));
		charGen.getSkillController().getMastershipController(model.getSkillValue("watermagic")).select(SplitterMondCore.getItem(Mastership.class, "savingcaster"));

		// Spells
		/* Die Klingentänzerin (Kampfmagie 3, Schutzmagie 3, Wassermagie 6) erhält also
		 * in jeder Magieschule je einen Zauber der Grade 0 und 1 und in Wassermagie einen Zauber des Grades 2. A
		 * Auf Grad 1 wählt sie Einfrieren, Flammende Waffe und Magische Rüstung. Der Grad-2-Zauber wird die Eislanze. Außerdem hat sie
		 * sich durch die Start-Erfahrung noch einen weiteren Grad-0-Zauber gekauft und wählt hier Gegenstand beschädigen.
		 * */
		assertNotNull(skills.getSpellController(combat));
		// Level 0: Auf Grad 0 entscheidet 		 * sie sich für Geisterdolch, Katzenwäsche und Kleiner Magieschutz.
		skills.getSpellController(combat).select(SplitterMondCore.getSpell("ghostdagger"));
		skills.getSpellController(SplitterMondCore.getSkill("watermagic")).select(SplitterMondCore.getSpell("quickwash"));
		skills.getSpellController(SplitterMondCore.getSkill("protectionmagic")).select(SplitterMondCore.getSpell("minorprotectmagic"));
		// Level 1: Auf Grad 1 wählt sie Einfrieren, Flammende Waffe und Magische Rüstung.
		skills.getSpellController(SplitterMondCore.getSkill("watermagic")).select(SplitterMondCore.getSpell("freeze"));
		skills.getSpellController(combat).select(SplitterMondCore.getSpell("flamingweapon"));
		skills.getSpellController(SplitterMondCore.getSkill("protectionmagic")).select(SplitterMondCore.getSpell("minormagicarmor"));
		// Level 2: Der Grad-2-Zauber wird die Eislanze.
		skills.getSpellController(SplitterMondCore.getSkill("watermagic")).select(SplitterMondCore.getSpell("icelance"));
		// Außerdem hat sie sich durch die Start-Erfahrung noch einen weiteren Grad-0-Zauber gekauft und wählt hier Gegenstand beschädigen.
		skills.getSpellController(combat).select(SplitterMondCore.getSpell("harmitem"));


		// Handwerkszeug
		ToolsOfTheTradeGenerator tradeGear = charGen.getToolsOfTheTradeController();
		assertTrue(tradeGear.select(SplitterMondCore.getItem(ItemTemplate.class, "chainsickle")).wasSuccessful());
		assertTrue(tradeGear.select(SplitterMondCore.getItem(ItemTemplate.class, "maira")).wasSuccessful());

		// Ausrüstung
		IEquipmentController equipCtrl = charGen.getEquipmentController();
		OperationResult<CarriedItem<ItemTemplate>> gearRet = equipCtrl.select(SplitterMondCore.getItem(ItemTemplate.class, "tuchrüstung"));
		assertNotNull(gearRet);
		assertTrue(gearRet.wasSuccessful());
		assertNotNull(gearRet.get());
		CarriedItem<ItemTemplate> gear = gearRet.get();
		EnhancementController enhanceCtrl = equipCtrl.getEnhancementController(gear);
		OperationResult<ItemEnhancementValue<Enhancement>> enhanceRet = enhanceCtrl.select(SplitterMondCore.getItem(Enhancement.class, "damage"));
		assertNotNull(enhanceRet);
		enhanceCtrl.select(SplitterMondCore.getItem(Enhancement.class, "speed"));

//		// Kreaturen
//		StandardCreatureController creatCtrl = new StandardCreatureController(model, null);
//		Companion comp1 = creatCtrl.select(SplitterMondCore.getItem(Creature.class, "wolf"));
//		assertNotNull(comp1);
//		assertTrue(model.getCompanions().contains(comp1));
//
//		Companion comp2 = (new CompanionTest()).createCarimeasPferd(model);
//		model.addCompanion(comp2);
//		comp2.setName("Pegasus");


		model.setName("Tiai Schimmersee");
		model.setGender(Gender.FEMALE);
//		try {
//			model.setImage(ClassLoader.getSystemResourceAsStream("Tiai.png").readAllBytes());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

//		charGen.finish();

		byte[] raw = SplitterMondCore.save(model);
		String xml = new String(raw);
		System.out.println(xml);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCarimea() {
		model.getCulture().setCustomName("Feenwelt des Feuers");
		model.getBackground().setCustomName("von Feen großgezogen");
		model.getEducation().setCustomName("Feenritter vom Flammenden Hof");
		model.addWeakness("Kodex (muss gegebenes Wort halten)");
		model.addWeakness("Impulsiv");
		model.setHairColor("hellrot");
		model.setEyeColor("grün");
		model.setBirthPlace("Feenwelt");
		model.setSize(181);
		model.setWeight(85);
		model.setName("Carimea");
		model.setGender(Gender.FEMALE);

		model.getAttribute(Attribute.CHARISMA).setDistributed(3);
		model.getAttribute(Attribute.AGILITY).setDistributed(5);
		model.getAttribute(Attribute.INTUITION).setDistributed(2);
		model.getAttribute(Attribute.CONSTITUTION).setDistributed(3);
		model.getAttribute(Attribute.MYSTIC).setDistributed(6);
		model.getAttribute(Attribute.STRENGTH).setDistributed(4);
		model.getAttribute(Attribute.MIND).setDistributed(2);
		model.getAttribute(Attribute.WILLPOWER).setDistributed(3);

		model.getSkillValue(SplitterMondCore.getSkill("acrobatics")).setDistributed(2);
		model.getSkillValue(SplitterMondCore.getSkill("leadership")).setDistributed(3);
		model.getSkillValue(SplitterMondCore.getSkill("blades")).setDistributed(9);
		model.getSkillValue(SplitterMondCore.getSkill("blades")).addMastership(new MastershipReference(SplitterMondCore.getSkill("blades").getMastership("knock")));
		model.getSkillValue(SplitterMondCore.getSkill("blades")).addMastership(new MastershipReference(SplitterMondCore.getSkill("blades").getMastership("defender")));
		model.getSkillValue(SplitterMondCore.getSkill("blades")).addMastership(new MastershipReference(SplitterMondCore.getSkill("blades").getMastership("whirlingblades")));

		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).setDistributed(9);
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("fairylore1")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("orientOtherworld1")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("powerful_conjuror")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("fairylore2")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("transporting_beings_1")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("lore_of_beings")));
		model.getSkillValue(SplitterMondCore.getSkill("arcanelore")).addMastership(new MastershipReference(SplitterMondCore.getSkill("arcanelore").getMastership("senseattribute")));

		model.getSkillValue(SplitterMondCore.getSkill("athletics")).setDistributed(2);
		model.getSkillValue(SplitterMondCore.getSkill("athletics")).addMastership(new MastershipReference(SplitterMondCore.getSkill("athletics").getMastership("sprinter")));
		model.getSkillValue(SplitterMondCore.getSkill("diplomacy")).setDistributed(6);
		model.getSkillValue(SplitterMondCore.getSkill("diplomacy")).addMastership(new MastershipReference(SplitterMondCore.getSkill("diplomacy").getMastership("fairytongue")));
		model.getSkillValue(SplitterMondCore.getSkill("diplomacy")).addMastership(new MastershipReference(SplitterMondCore.getSkill("diplomacy").getMastership("diplomat")));
		model.getSkillValue(SplitterMondCore.getSkill("empathy")).setDistributed(3);
		model.getSkillValue(SplitterMondCore.getSkill("determination")).setDistributed(6);
		model.getSkillValue(SplitterMondCore.getSkill("determination")).addMastership(new MastershipReference(SplitterMondCore.getSkill("determination").getMastership("ironwill")));
		model.getSkillValue(SplitterMondCore.getSkill("nature")).setDistributed(1);
		model.getSkillValue(SplitterMondCore.getSkill("eloquence")).setDistributed(4);
		model.getSkillValue(SplitterMondCore.getSkill("swim")).setDistributed(2);
		model.getSkillValue(SplitterMondCore.getSkill("survival")).setDistributed(2);
		model.getSkillValue(SplitterMondCore.getSkill("perception")).setDistributed(5);
		model.getSkillValue(SplitterMondCore.getSkill("endurance")).setDistributed(9);
		model.getSkillValue(SplitterMondCore.getSkill("endurance")).addMastership(new MastershipReference(SplitterMondCore.getSkill("endurance").getMastership("armour1")));
		model.getSkillValue(SplitterMondCore.getSkill("endurance")).addMastership(new MastershipReference(SplitterMondCore.getSkill("endurance").getMastership("armour2")));

		model.getSkillValue(SplitterMondCore.getSkill("antimagic")).setDistributed(1);
		model.getSkillValue(SplitterMondCore.getSkill("firemagic")).setDistributed(9);
		model.getSkillValue(SplitterMondCore.getSkill("firemagic")).addMastership(new MastershipReference(SplitterMondCore.getSkill("firemagic").getMastership("flameheart")));

		charGen.finish();

		Reward reward = new Reward();
		reward.setDate(Date.from(Instant.now()));
		reward.setExperiencePoints(10);
		reward.setTitle("Erstes Abenteuer");
		reward.setGamemaster("Anja");
		reward.setMoney(1500);
		model.addReward(reward);
		model.setExpFree(model.getExpFree()+10);

		ValueModification mod = new ValueModification(SplittermondReference.SKILL, "empathy", 5);
		mod.setExpCost(3);
		mod.setDate(Date.from(Instant.now().plusSeconds(60)));
		model.addToHistory(mod);

		byte[] raw = SplitterMondCore.save(model);
		String xml = new String(raw);
		System.out.println(xml);
	}
}

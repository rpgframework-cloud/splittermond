package org.prelle.rpgframework.splittermond.data;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;

/**
 * @author prelle
 *
 */
public class LoadSpliMoDataTest {

	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.GERMANY);
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init( );
		System.out.println("Loaded "+SplitterMondCore.getItemList(SMSkill.class).size()+" skills");
		System.out.println("Loaded "+SplitterMondCore.getSkills(SkillType.NORMAL).size()+" normal skills");

		assertFalse(SplitterMondCore.getCreatures().get(0).getCreatureTypes().isEmpty());
		assertFalse(SplitterMondCore.getCreatures().get(0).getSkillValues().isEmpty());

		ItemTemplate sabre = SplitterMondCore.getItem(ItemTemplate.class, "sabre");
		assertNotNull(sabre);
		System.out.println("Requires: "+sabre.getRequirements());
		System.out.println("Features: "+sabre.getAttribute(SMItemAttribute.FEATURES));
		System.out.println("Features2: "+sabre.getAttribute(SMItemAttribute.FEATURES).getValue());
		System.out.println("Features3: "+((List<?>)sabre.getAttribute(SMItemAttribute.FEATURES).getValue()).get(0).getClass());
	}

}

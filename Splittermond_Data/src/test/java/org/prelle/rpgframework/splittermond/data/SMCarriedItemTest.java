package org.prelle.rpgframework.splittermond.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.items.Availability;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMGearTool;
import org.prelle.splimo.items.SMItemAttribute;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SMCarriedItemTest {

	//-------------------------------------------------------------------
	@BeforeClass
	public static void beforeClass() {
		//System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.GERMAN);
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init( );
	}

	//-------------------------------------------------------------------
	/**
	 * 	<item id="combat_axe" avail="4" price="500" type="WEAPON_CLOSE_COMBAT" subtype="BLADES">
	 *	  <equip mode="NORMAL"/>
	 *	  <weapon dmg="5P" attack="9,,,," skill="close_combat" spec="close_combat/blades" />
	 *  </item>
	 */
	@Test
	public void testSimpleItem() {
		ItemTemplate axe = SplitterMondCore.getItem(ItemTemplate.class, "longsword");
		assertNotNull(axe);

		CarriedItem<ItemTemplate> item = new CarriedItem<ItemTemplate>(axe, null, CarryMode.CARRIED);
		assertNotNull(item);
		OperationResult<List<Modification>> modR = SMGearTool.recalculate("", null, item);
		assertTrue(modR.wasSuccessful());
		assertNotNull(item.getAsObject(SMItemAttribute.AVAILABILITY));
		assertEquals(Availability.SMALL_TOWN, ((Availability)item.getAsObject(SMItemAttribute.AVAILABILITY).getModifiedValue()));
		assertNotNull(item.getAsValue(SMItemAttribute.PRICE));
		assertEquals(1000, item.getAsValue(SMItemAttribute.PRICE).getModifiedValue());
		assertNotNull(item.getAsValue(SMItemAttribute.DAMAGE));
//		assertEquals(5, ((Damage)item.getAsObject(SMItemAttribute.DAMAGE).getModifiedValue()).getValue());
		assertNotNull(item.getAsObject(SMItemAttribute.FEATURES));
		assertNotNull(item.getAsObject(SMItemAttribute.FEATURES).getModifiedValue());
	}

}

open module de.rpgframework.splittermond.data {
	exports org.prelle.rpgframework.splittermond.data;

	requires de.rpgframework.core;
	requires de.rpgframework.rules;
	requires simple.persist;
	requires splittermond.core;
}
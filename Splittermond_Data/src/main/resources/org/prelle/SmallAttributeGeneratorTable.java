package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.jfx.NumericalValueField;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.APrioritySettings;
import de.rpgframework.shadowrun.chargen.gen.IPriorityGenerator;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.gen.PerAttributePoints;
import de.rpgframework.shadowrun.chargen.gen.PriorityAttributeController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * @author stefa
 *
 */
public class SmallAttributeGeneratorTable extends GridPane {

	private final static Logger logger = LogManager.getLogger(SmallAttributeGeneratorTable.class);
	
	private Map<ShadowrunAttribute, PerAttributePointsField> fields;

	// -------------------------------------------------------------------
	public SmallAttributeGeneratorTable(int x) {
		fields = new HashMap<>();
		
		int y=0;
		for (ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			y++;
			PerAttributePointsField field = new PerAttributePointsField(attr);
			fields.put(attr, field);
			
			Label name = new Label(attr.getName());
			add(name, 0, y);
			field.layout(this, 1, y);
		}
	}

	// -------------------------------------------------------------------
	public void setGenerator(IPriorityGenerator charGen) {
		APrioritySettings settings = charGen.getSettings();
		for (ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			PerAttributePointsField field = fields.get(attr);
			field.setData( charGen.getSettings().perAttrib.get(attr) );
			field.setController(charGen.getPriorityAttributeController());
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		for (final ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			if (attr==ShadowrunAttribute.ESSENCE)
				continue;
			PerAttributePointsField field = fields.get(attr);
			field.refresh();
		}
	}

}

enum Mode {
	ADJUST,
	REGULAR,
	KARMA
}

class PerAttributePointsField  {

	private ShadowrunAttribute attrib;
	private PerAttributePoints per;
	private PriorityAttributeController ctrl;
	private Mode mode;

	private Button dec;
	private Button inc;
	private Label lbValue1;
	private Label lbValue2;
	private Label lbValue3;
	

	//--------------------------------------------------------------------
	public PerAttributePointsField(ShadowrunAttribute attrib) {
		this.attrib = attrib;
		initComponents();
		mode = Mode.REGULAR;
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		dec = new Button(null, new SymbolIcon("remove"));
		inc = new Button(null, new SymbolIcon("add"));
		lbValue1 = new Label();
		lbValue2 = new Label();
		lbValue3 = new Label();
	}

	//--------------------------------------------------------------------
	public void layout(GridPane grid, int x, int y) {
		grid.add(dec, x++, y);
		grid.add(lbValue1, x++, y);
		grid.add(lbValue2, x++, y);
		grid.add(lbValue3, x++, y);
		grid.add(inc, x++, y);			
	}

	//--------------------------------------------------------------------
	public void refresh() {
		if (per==null) return;
		lbValue1.setText( (per.adjust==0)?"":String.valueOf(per.adjust));
		lbValue2.setText( (per.regular==0)?"":String.valueOf(per.regular));
		lbValue2.setText( (per.karma==0)?"":String.valueOf(per.karma));
		if (ctrl!=null) {
			switch (mode) {
			case ADJUST:
				dec.setDisable(!ctrl.canDecreaseAdjust(attrib));
				inc.setDisable(!ctrl.canIncreaseAdjust(attrib));
				break;
			case REGULAR:
				dec.setDisable(!ctrl.canDecreaseAttrib(attrib));
				inc.setDisable(!ctrl.canIncreaseAttrib(attrib));
				break;
			case KARMA:
				dec.setDisable(!ctrl.canDecreaseKarma(attrib));
				inc.setDisable(!ctrl.canIncreaseKarma(attrib));
				break;
			}
		}
	}

	//-------------------------------------------------------------------
	public void setData(PerAttributePoints data) {
		this.per = data;
		refresh();
	}

	//-------------------------------------------------------------------
	public void setController(PriorityAttributeController ctrl) {
		this.ctrl = ctrl;
		refresh();
	}

	//-------------------------------------------------------------------
	public void setMode(Mode value) {
		this.mode = value;
		refresh();
	}

}
